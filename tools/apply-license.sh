#!/bin/bash
for i in $(find ../src/. | grep .py); do 
	if ! grep -q 'CERN/Switzerland'  $i; then
 		cat ../LICENSE $i > $i.new && mv $i.new $i; 
	fi
done

