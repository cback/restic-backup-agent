# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import List, Tuple

import shlex

SPEC_FILE = "cback.spec"


def release_new_version(author: str, author_email: str, version: Tuple[int, ...]) -> None:
    # release from master branch
    run("git checkout master")

    spec_content = []
    with open(SPEC_FILE, 'r') as spec:
        spec_content = spec.readlines()

    # update version and release
    release = version[-1]
    ver = ".".join(map(str, version[:-1]))

    for i, line in enumerate(spec_content):
        if line.startswith("Version:"):
            spec_content[i] = f"Version:   {ver}\n"
        elif line.startswith("Release:"):
            spec_content[i] = "Release:   "+str(release)+"%{?dist}\n"

    # add the changelog
    changelog_index = -1
    for i, line in enumerate(spec_content):
        if line.startswith(r"%changelog"):
            changelog_index = i
            break

    # the changelog is of type
    # * Thu May 19 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-3
    # - changelog 1
    # - changelog 2
    changelog = get_changelog_commits()
    if not changelog:
        raise Exception("no commits since last tag")
    header_chang_title = spec_content[:changelog_index+1]
    # FIXME: the changelog could be empty
    old_changelog = spec_content[changelog_index+1:]

    today = datetime.now().strftime(r"%a %b %d %Y")
    new_changelog = []
    new_changelog.append(
        f"* {today} {author} <{author_email}> {ver}-{release}\n")

    for c in changelog:
        new_changelog.append("- "+c+"\n")

    new_spec_file = header_chang_title + new_changelog + old_changelog
    # write back to the spec file
    with open(SPEC_FILE, 'w') as spec:
        spec.writelines(new_spec_file)

    # commit, tag and push the new spec file for the new version
    run("git add "+SPEC_FILE)
    run(f"git commit -m 'version {ver}-{release}' --author='{author} <{author_email}>'")
    run(f"git tag -a v{ver}-{release} -m 'version {ver}-{release}'")
    run(f"git push origin master --tags")


def get_changelog_commits() -> List[str]:
    last = last_tag()
    raw = run(f"git log {last}..HEAD --pretty=format:%s")
    res = []
    for line in raw.splitlines(keepends=False):
        if line.startswith("Merge branch"):
            continue
        res.append(line)
    return res


def last_tag() -> str:
    return run("git describe --tags --abbrev=0")


def current_version() -> Tuple[int, ...]:
    tag = last_tag()[1:]
    ver, rel = tag.split("-", maxsplit=2)
    return tuple(map(int, ver.split("."))) + (int(rel), )


def commit_spec(version: Tuple[int, ...]) -> None:
    release = version[-1]
    ver = ".".join(map(str, version[:-1]))
    v = f"{ver}-{release}"
    run(f"git add {SPEC_FILE}")
    run(f"git commit -m 'version {v}'")
    run(f"git tag -a v{v} -m 'version {v}'")
    run(f"git push origin master --tags")


def run(cmd: str) -> str:
    import subprocess
    process = subprocess.Popen(shlex.split(
        cmd), stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    stdout, _ = process.communicate()
    return stdout.decode('utf-8').strip()


def next_version(major: bool, minor: bool, build: bool, revision: bool) -> Tuple[int, ...]:
    # (major, minor, build, revision)
    current = current_version()
    if revision:
        return current[:3] + (current[3]+1,)
    if build:
        return current[:2] + (current[2]+1, 1)
    if minor:
        return current[:1] + (current[1]+1, 0, 1)
    if major:
        return (current[0]+1, 0, 0, 1)

    raise ValueError("specify one of major, minor, build or revision")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description='Release a new cback version.')
    parser.add_argument(
        '--author', help="who creates the release in the form 'Name Surname'", required=True)
    parser.add_argument(
        '--email', help="email of whom creates the release in the form 'email@example.org'", required=True)

    ver = parser.add_mutually_exclusive_group(required=True)
    ver.add_argument('--major', help='release a major version',
                     action='store_true')
    ver.add_argument('--minor', help="release a minor version",
                     action='store_true')
    ver.add_argument('--build', help="release a build version",
                     action='store_true')
    ver.add_argument(
        '--revision', help="release a revision version", action='store_true')

    args = parser.parse_args()

    next = next_version(args.major, args.minor, args.build, args.revision)
    release_new_version(args.author, args.email, next)
