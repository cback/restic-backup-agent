# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
#!/usr/bin/env python3
# CERNBack Gently Enabler
# Gently enable new backup jobs to not disturb the incremental backup.
# rvalverd@cern.ch
# -*- coding: utf8 -*-

"""CBack Enabler.

Usage:
  cback-enabler --max-running=<max>

Options:
  -h --help     Show this screen.
  --max-running Enable jobs up to this maximum at any moment
  --version     Show version.
"""

from __future__ import print_function
import mysql.connector
import os
import json
from docopt import docopt
import random
import subprocess

# Main Config [Defaults]
LOGFILE = '/var/log/cback/cback-backup-switch.log'
CONFIG_FILE = "/etc/cback/cback-config.json"
VERSION = "v0.0.1"
MAX = 8


# Load the config file from disk
def load_config():
    if os.path.isfile(CONFIG_FILE):
        with open(CONFIG_FILE, "r") as json_file:
            return json.load(json_file)
    else:
        print("No config file found, Exiting.")
        exit(1)


# Executes a command. Returns error and output
def execute(command):
    try:
        output = subprocess.check_output(command, shell=True, universal_newlines=True)
        return True, output
    except subprocess.CalledProcessError as e:
        return False, e


# return the count of jobs in the given status
def get_backup_count_by_status(status_id):
    ret = 0
    err = False
    query = (
            "SELECT count(*) FROM backup_backup WHERE status_id=" + str(status_id)
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


# Get n number of jobs which were never executed yet.
def get_random_new_jobs(number):
    ret = None
    query = (
        "SELECT backup_backup.id FROM backup_backup WHERE enabled=0 AND worker IS NULL "
        "AND repository_url LIKE \'%s3-fr-prevessin-1.cern.ch%\'"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor(dictionary=True)
    try:
        cursor.execute(query)
        rows = cursor.fetchall()
        if len(rows) > 0:
            random_job = random.sample(rows, int(number))
            ret = random_job
    except mysql.connector.Error as err:
        print(err.msg)
    cursor.close()
    cnx.close()
    return ret


# Main program
if __name__ == '__main__':
    args = docopt(__doc__, version='CERNBack Gently Enabler ' + VERSION)
    config = load_config()
    db_config = {
        'user': config["database"]["user"],
        'password': config["database"]["password"],
        'host': config["database"]["host"],
        'port': config["database"]["port"],
        'database': config["database"]["database"],
        'raise_on_warnings': True
    }

    MAX = args["--max-running"]
    # We get the number of running jobs and compute how many we can add until we reach MAX.
    num_running = get_backup_count_by_status(2)
    to_add = int(MAX) - int(num_running)
    if to_add > 0:
        print("We can enable ", str(to_add), "jobs!")
        # Enable 5 random jobs.
        to_enable = get_random_new_jobs(to_add)
        for j in to_enable:
            command = "/usr/bin/cback backup enable " + str(j["id"])
            ok, out = execute(command)
            if not ok:
                print("something bad executing cback backup enable...")
                exit(1)
            else:
                print(out)
    else:
        print("Running: " + str(num_running) + ", MAX: " + str(MAX))
