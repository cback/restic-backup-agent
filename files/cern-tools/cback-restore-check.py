# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
#!/usr/bin/env python3
# CBACK RESTORE CHECK
# This script uses dump generated via eos-ns-inspect tool to cross-check a restored folder via file, size and checksum
# -*- coding: utf8 -*-

"""cback-restore-check

Usage:
  cback-restore-check (run --job_id=<job_id> [--snapshot=<snapshot_short_id>]|(--metadata_file=<metadata_file> [--snap_time=<snapshot_time>])) --rest_dir=<rest_dir>
  cback-restore-check (-h | --help)

Options:
  -h --help     Show this screen.
"""

from pathlib import Path
from zlib import adler32
from docopt import docopt
from cback.controllers.restoreController import RestoreController
from cback.models.backupModel import BackupModel
import time
import dateutil.parser
from cback.utils.utils import Utils as Utility
from cback.controllers.resticController import ResticController
from cback.utils.monitoring import Monitoring
import random
from multiprocessing.dummy import Pool
import multiprocessing

try:
    import ijson
    import json
    import os
    import mysql.connector
except ImportError:
    print("Missing modules, please install ijson using pip")
    exit(1)

CONFIG_FILE = "/etc/cback/cback-config.json"
CORES = multiprocessing.cpu_count()  # Get the number of physical cores of the host.


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# Load the config file from disk
def load_config():
    if not os.path.isfile(CONFIG_FILE):
        print("Config file " + CONFIG_FILE + " not found, bye! ")
        exit(1)
    with open(CONFIG_FILE, "r") as json_file:
        return json.load(json_file)


# We check database connection
def check_db(config_db):
    try:
        cnx = mysql.connector.connect(**config_db)
        cnx.close()
    except mysql.connector.Error as err:
        print("FATAL: Cannot connect to mysql database. Check config file.")
        print(err.msg)
        exit(1)


# Returns the adler32 checksum of a given filename
def calculate_adler32_checksum(filename):
    """
    Calculate the adler32 checksum for the given file.
    The file is assumed to exist.

    :param filename: file name (string).
    :return: checksum value (string).
    """

    asum = 1  # default adler32 starting value
    blocksize = 64 * 1024 * 1024  # read buffer size, 64 Mb

    with open(filename, 'rb') as f:
        while True:
            data = f.read(blocksize)
            if not data:
                break
            asum = adler32(data, asum)
            if asum < 0:
                asum += 2**32
    return "{0:0{1}x}".format(asum, 8)


# Do check the file
def check_file(item):
    global TOTAL, OK, NOT_FOUND, XS_MISMATCH, SIZE_MISMATCH, EXC_ERROR, MTIME_MISMATCH, SKIPPED, SKIPPED_BY_CTIME, \
        SKIPPED_BY_MTIME, SKIPPED_FALSE_POSITIVE
    file_ok = True
    TOTAL += 1
    path_ini = restore_dst + "/" + (item["path"].replace('user/', 'home-'))
    fname = Path(path_ini)
    if any(x in str(fname) for x in ignore_list):
        SKIPPED.append(fname)
        return
    if snap_timestamp:
        if float(item["ctime"]) > float(snap_timestamp):
            print(f"{fname}{bcolors.OKBLUE} SKIPPED_CTIME_" + item["ctime"] + f"_GT_SNAPSHOT_{snap_timestamp}{bcolors.ENDC}")
            SKIPPED_BY_CTIME.append(fname)
            return
        if float(item["mtime"]) > float(snap_timestamp):
            print(f"{fname}{bcolors.OKBLUE} SKIPPED_MTIME_" + item["mtime"] + f"_GT_SNAPSHOT_{snap_timestamp}{bcolors.ENDC}")
            SKIPPED_BY_MTIME.append(fname)
            return
    try:
        stat_info = Path(fname).lstat()
        rest_size = stat_info.st_size

        # File size check
        if int(rest_size) != int(item["size"]):
            if int(item["size"]) == 0:
                SKIPPED_FALSE_POSITIVE.append({"path": fname, "reason": "size is 0 on EOS"})
                print(f"{fname}{bcolors.OKBLUE} SKIPPED_FALSE_POSITIVE_EOS_SIZE_" + item["size"] + f"_RES_SIZE_{rest_size}{bcolors.ENDC}")
                return
            else:
                file_ok = False
                SIZE_MISMATCH.append({"path": fname, "eos_size": item["size"], "restore_size": rest_size})
                print(f"{fname}{bcolors.FAIL} SIZE_MISMATCH_EOS_" + item["size"] + f"_RES_{rest_size}{bcolors.ENDC}")

        # Adler32 checksum check
        rest_checksum = calculate_adler32_checksum(fname)
        if not item["xs"]:
            SKIPPED_FALSE_POSITIVE.append({"path": fname, "reason": "checksum missing in EOS"})
            print(f"{fname}{bcolors.OKBLUE} SKIPPED_FALSE_POSITIVE_EOS_XS_MISSING{bcolors.ENDC}")
            return
        elif int(item["size"]) == 0 and item["xs"] != "00000001":
            SKIPPED_FALSE_POSITIVE.append({"path": fname, "reason": "EOS checksum wrong for a 0-size file"})
            print(f"{fname}{bcolors.OKBLUE} SKIPPED_FALSE_POSITIVE_EOS_XS_" + item[
                "xs"] + f"_EOS_SIZE_{rest_size}{bcolors.ENDC}")
            return
        elif item["xs"] == "00000001" and int(item["size"]) != 0:
            SKIPPED_FALSE_POSITIVE.append({"path": fname, "reason": "0-size EOS checksum for a non 0-size file"})
            print(f"{fname}{bcolors.OKBLUE} SKIPPED_FALSE_POSITIVE_EOS_XS_" + item[
                "xs"] + f"_RES_SIZE_{rest_size}{bcolors.ENDC}")
            return
        elif item["xs"] != rest_checksum:
            file_ok = False
            XS_MISMATCH.append({"path": fname, "eos_xs": item["xs"], "restore_xs": rest_checksum})
            print(f"{fname}{bcolors.FAIL} CHECKSUM_MISMATCH_EOS_" + item[
                "xs"] + f"_RES_{rest_checksum}{bcolors.ENDC}")

        # Mtime check
        #rest_mtime = stat_info.st_mtime
        #if float(o["mtime"]) != float(rest_mtime):
        #    file_ok = False
        #    MTIME_MISMATCH.append({"path": fname, "eos_mtime": o["mtime"], "restore_mtime": rest_mtime})
        #    print(f"{fname}{bcolors.FAIL} MTIME_MISMATCH_EOS_" + o[
        #        "mtime"] + f"_RES_{rest_mtime}{bcolors.ENDC}")

        # Ctime check
        #rest_ctime = stat_info.st_ctime
        #if float(o["ctime"]) != float(rest_ctime):
        #    file_ok = False
        #    MTIME_MISMATCH.append({"path": fname, "eos_ctime": o["ctime"], "restore_ctime": rest_ctime})
        #    print(f"{fname}{bcolors.FAIL} CTIME_MISMATCH_EOS_" + o[
        #        "ctime"] + f"_RES_{rest_ctime}{bcolors.ENDC}")

        # Final check
        if file_ok:
            OK += 1
            # print(f"{fname}{bcolors.OKGREEN} OK {bcolors.ENDC}")

    except IOError as e:
        if e.errno == 2:
            NOT_FOUND.append(fname)
            print(f"{fname}{bcolors.FAIL} FILE_NOT_FOUND {bcolors.ENDC}")
        else:
            EXC_ERROR.append({"path": fname, "error": e.strerror})
            print(f"{fname}{bcolors.FAIL} " + str(e.strerror) + f"{bcolors.ENDC}")


def print_summary():
    # print summary
    print_box("SUMMARY")
    print("TOTAL: " + str(TOTAL))
    print("SKIPPED [EXPLICITLY]: " + str(len(SKIPPED)))
    print("SKIPPED [CTIME NEWER THAN SNAPSHOT]: " + str(len(SKIPPED_BY_CTIME)))
    print("SKIPPED [MTIME NEWER THAN SNAPSHOT]: " + str(len(SKIPPED_BY_MTIME)))
    print("SKIPPED [FALSE POSITIVES]: " + str(len(SKIPPED_FALSE_POSITIVE)))
    print("CHECKED: " + str(TOTAL - len(SKIPPED) - len(SKIPPED_BY_CTIME) - len(SKIPPED_FALSE_POSITIVE) - len(SKIPPED_BY_MTIME)))
    print("OK: " + str(OK))
    print("NOT FOUND: " + str(len(NOT_FOUND)))
    print("SIZE MISMATCH: " + str(len(SIZE_MISMATCH)))
    print("CHECKSUM MISMATCH: " + str(len(XS_MISMATCH)))
    # print("MTIME MISMATCH: " + str(len(MTIME_MISMATCH)))
    # print("CTIME MISMATCH: " + str(len(CTIME_MISMATCH)))
    print("OTHER ERRORS: " + str(len(EXC_ERROR)))
    print_box("FILES NOT FOUND / MAYBE RENAMED: " + str(len(NOT_FOUND)))
    if len(NOT_FOUND) > 0:
        for f in NOT_FOUND:
            print(f)
    print_box("FILES WITH SIZE MISMATCH: " + str(len(SIZE_MISMATCH)))
    if len(SIZE_MISMATCH) > 0:
        for f in SIZE_MISMATCH:
            print("path=" + str(f["path"]) + " eos_size=" + str(f["eos_size"]) + " restore_size=" + str(f["restore_size"]))
    print_box("FILES WITH CHECKSUM MISMATCH: " + str(len(XS_MISMATCH)))
    if len(XS_MISMATCH) > 0:
        for f in XS_MISMATCH:
            print("path=" + str(f["path"]) + " eos_xs=" + f["eos_xs"] + " restore_xs=" + f["restore_xs"])
    #print(" ")
    #print("FILES WITH MTIME MISMATCH: " + str(len(MTIME_MISMATCH)))
    #print("-------------------------------------------------------")
    #if len(MTIME_MISMATCH) > 0:
    #    for f in MTIME_MISMATCH:
    #        print(
    #            "path=" + str(f["path"]) + " eos_mtime=" + str(f["eos_mtime"]) + " restore_mtime=" + str(f["restore_mtime"]))
    #print(" ")
    #print("FILES WITH CTIME MISMATCH: " + str(len(CTIME_MISMATCH)))
    #print("-------------------------------------------------------")
    #if len(CTIME_MISMATCH) > 0:
    #    for f in CTIME_MISMATCH:
    #        print(
    #            "path=" + str(f["path"]) + " eos_ctime=" + str(f["eos_ctime"]) + " restore_ctime=" + str(f["restore_ctime"]))
    print_box("FILES SKIPPED [EXPLICITLY]: " + str(len(SKIPPED)))
    if len(SKIPPED) > 0:
        for f in SKIPPED:
            print(f)
    print_box("FILES SKIPPED [FALSE POSITIVES]: " + str(len(SKIPPED_FALSE_POSITIVE)))
    if len(SKIPPED_FALSE_POSITIVE) > 0:
        for f in SKIPPED_FALSE_POSITIVE:
            print("path=" + str(f["path"]) + " reason=" + str(f["reason"]))
    print_box("FILES SKIPPED [CTIME NEWER THAN SNAPSHOT]: " + str(len(SKIPPED_BY_CTIME)))
    if len(SKIPPED_BY_CTIME) > 0:
        for f in SKIPPED_BY_CTIME:
            print(f)
    print_box("FILES SKIPPED [MTIME NEWER THAN SNAPSHOT]: " + str(len(SKIPPED_BY_MTIME)))
    if len(SKIPPED_BY_MTIME) > 0:
        for f in SKIPPED_BY_MTIME:
            print(f)
    print_box("OTHER FILE ERRORS: " + str(len(EXC_ERROR)))
    if len(EXC_ERROR) > 0:
        for f in EXC_ERROR:
            print("path=" + str(f["path"]) + " error=" + str(f["error"]))


def guess_home_instance(username):
    # We hardcode the logical to physical instance mapping
    instance_map = {"home-i00": ('d', 'l', 'n', 't', 'z'),
                    "home-i01": ('a', 'g', 'j', 'k', 'w'),
                    "home-i02": ('h', 'o', 'r', 's', 'y'),
                    "home-i03": ('b', 'e', 'm', 'v', 'x'),
                    "home-i04": ('c', 'f', 'i', 'p', 'q', 'u')}
    initial = username[:1]
    for i in instance_map:
        if initial in (instance_map[i]):
            return i


def get_snapshot_info(job_data, snapshot_id=None):
    repository = job_data["repository_url"]
    job_id = job_data["id"]
    path = job_data["path"]
    success = False
    ret = "Something obscure has happened!"
    print("[job:" + str(job_id) + "] " + "Environment loaded.")
    print("Checking Restic Repository " + repository)
    repository_ok, out = restic.check_repo(repository)
    if repository_ok:
        snapshot_info_ok, snapshots_info = restic.get_snapshots_json(repository, path)
        if snapshot_info_ok and snapshots_info:
            if not snapshot_id: # We get a random snapshot id from the repo
                ret = random.choice(snapshots_info)
            else:
                ret = [s for s in snapshots_info if s["short_id"] == snapshot_id][0]
            success = True
        else:  # not snapshot_info_ok
            print("[job:" + str(job_id) + "] " + "Error getting snapshot information: " + str(out))

    else:  # not repository_ok
        print("[job:" + str(job_id) + "] " + "Error checking repository: " + str(out))
        ret = str(out)
    return success, ret


def print_box(text):
    print(" ")
    print("-" * (len(text)+4))
    print("| " + str(text) + " |")
    print("-" * (len(text)+4))
    print(" ")


# Main program
if __name__ == '__main__':
    # Instantiate the parser
    args = docopt(__doc__, version='cback restore check')
    ignore_list = (".sys.a#", ".sys.v#", "...eos.ino...")
    config = load_config()
    db_config = {
        'user': config["database"]["user"],
        'password': config["database"]["password"],
        'host': config["database"]["host"],
        'database': config["database"]["database"],
        'port': config["database"]["port"],
        'raise_on_warnings': True
    }
    check_db(db_config)
    cache_base_dir = config["general"]["cache_base_dir"]
    logs_dir = config["general"]["logs_dir"]
    env_dir = config["general"]["env_dir"]
    snap_timestamp = None
    restore_dst = args["--rest_dir"]

    TOTAL = 0
    SKIPPED = list()
    SKIPPED_BY_CTIME = list()
    SKIPPED_BY_MTIME = list()
    OK = 0
    NOT_FOUND = list()
    SIZE_MISMATCH = list()
    XS_MISMATCH = list()
    EXC_ERROR = list()
    MTIME_MISMATCH = list()
    CTIME_MISMATCH = list()
    SKIPPED_FALSE_POSITIVE = list()

    # Setup monitoring
    mon_config = dict()
    mon_config["host"] = config["general"]["monitoring_host"]
    mon_config["port"] = config["general"]["monitoring_port"]
    mon_config["send"] = config["general"]["send_metrics"]

    if args["run"]:
        restic = ResticController()
        # We get backup job information
        backup_model = BackupModel(db_config)
        job_data = backup_model.get_by_id(args["--job_id"])[0]
        mon_prefix = config["general"]["monitoring_prefix"] + ".restic.check." + job_data["user_name"] + "."
        monitoring = Monitoring(mon_config, mon_prefix)
        print_box("RESTORING RANDOM SNAPSHOT FROM USER " + str(job_data["user_name"]))
        if job_data["prune_status_id"] == 2:
            print("There is a prune process ongoing for that job. Try again later")
            exit(1)
        if Utility.load_environment(env_dir, job_data["repository_url"], job_data["user_name"]):
            if args["--snapshot"]:
                ok, out = get_snapshot_info(job_data, snapshot_id=args["--snapshot"])
            else:
                ok, out = get_snapshot_info(job_data)
            if ok:
                snap_info = out
                snap_id = snap_info["short_id"]
                restore_dst = restore_dst + "/" + str(snap_id)
                # We launch the restore job
                command = "/usr/local/bin/restic --cache-dir /var/cache/restic/" + job_data["user_name"] + " -o s3.connections=" \
                          + str(config["restore"]["s3_connections"]) + " -r " + job_data["repository_url"] + " restore " \
                          + snap_id + " --path " + job_data["path"] + " --target " + str(restore_dst)
                print(command)
                ok, out = Utility.execute(command)
                if ok:
                    # we get eos namespace information
                    print_box("GETTING EOS NAMESPACE INFORMATION")
                    path = Utility.fname_transform(job_data["path"], reverse=True)
                    instance = guess_home_instance(job_data["user_name"])
                    keytab_file = "/tmp/" + instance + "_keytab"
                    metadata_out = "/tmp/" + job_data["user_name"] + "_out.json"
                    if os.path.exists(keytab_file):
                        command = "/usr/bin/eos-ns-inspect scan --path " + path + " --no-dirs --json --members "\
                                  + "eos" + instance + "-qdb:7777 --password-file " + keytab_file + " > " + metadata_out
                        print(command)
                        ok = True
                        ok, out = Utility.execute(command)
                        if ok:
                            # At this point we are ready to start checking!
                            metadata_file = metadata_out
                        else:
                            "Error getting metadata information from eos"
                            exit(1)
                    else:
                        print("Keytab file", keytab_file, "does not exists!")
                        exit(1)
                else:
                    print("Error restoring job")
                    print(out)
                    exit(1)
                snap_date = dateutil.parser.parse(snap_info['time'].split("+")[0])
                snap_timestamp = time.mktime(snap_date.timetuple())
                print("Snapshot date: " + str(snap_date) + " (" + str(snap_timestamp) + ")")
            else:
                print("Error getting snapshot information.")
                exit(1)
        else:  # not Utility.load_environment
            ret = "Environment File not found"
            exit(1)

    else:
        metadata_file = args["--metadata_file"]
        if args["--snap_time"]:
            snap_date = dateutil.parser.parse(str(args["--snap_time"]).split("+")[0])
            snap_timestamp = time.mktime(snap_date.timetuple())
    f = open(metadata_file, 'rb')
    objects = ijson.items(f, 'item')
    print_box("CHECKING FILES (OK ARE OMITTED)")
    with Pool(CORES) as p:
        p.map(check_file, objects)
    #for o in objects:
    #    check_file(o)
    print_summary()
    if args["run"]:
        total_checked = TOTAL - len(SKIPPED) - len(SKIPPED_BY_CTIME) - len(SKIPPED_FALSE_POSITIVE) - len(SKIPPED_BY_MTIME)
        check_result = 1
        if total_checked == OK:
            check_result = 0
        metrics = {"total_checked": str(total_checked),
                   "total_ok": str(OK),
                   "not_found": str(len(NOT_FOUND)),
                   "size_mismatch": str(len(SIZE_MISMATCH)),
                   "checksum_mismatch": str(len(XS_MISMATCH)),
                   "other_errors": str(len(EXC_ERROR))}
        if check_result == 1:
            metrics["check_failed"] = 1
        else:
            metrics["check_success"] = 1
        print_box("PROCESS ENDED")
        monitoring.push(metrics)
        print(" ")

