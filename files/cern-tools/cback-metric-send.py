# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
#!/usr/bin/env python3
# CBOX BACKUP METRICS SEND
# -*- coding: utf8 -*-

"""cback metric send tool.

Usage:
  cback-metric-send [(--send-email --email-to=<email-to>)]
  cback-metric-send (-h | --help)

Options:
  -h --help     Show this screen.
"""

from __future__ import print_function
import mysql.connector
import os
import socket
import time
import json
from datetime import datetime, timedelta
import smtplib
from docopt import docopt

HOSTNAME = socket.gethostname()
CONFIG_FILE = "/etc/cback/cback-config.json"

BACKUP_CAUT = 75
BACKUP_WARN = 50
PRUNE_CAUT = 75
PRUNE_WARN = 50


# Load the config file from disk
def load_config():
    if os.path.isfile(CONFIG_FILE):
        with open(CONFIG_FILE, "r") as json_file:
            return json.load(json_file)
    else:
        print("No config file found, Exiting.")
        exit(1)


def push_to_monitoring(metric, value, timestamp=None):
    print("Sending metrics to monitoring")
    metric = METRIC_PREFIX + ".restic." + str(HOSTNAME).split('.')[0] + "." + str(metric)
    if not timestamp:
        timestamp = time.time()
    print(metric, str(value), str(timestamp))
    os.system("echo '%s %s %s' | nc %s %s" % (metric, value, timestamp, MONITORING_HOST, MONITORING_PORT))


def get_backup_count_by_status(status_id):
    query = (
            "SELECT count(*) FROM backup_backup WHERE status=" + str(status_id)
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_backup_count():
    query = (
            "SELECT count(*) FROM backup_backup"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_backup_count_disabled():
    query = (
            "SELECT count(*) FROM backup_backup WHERE enabled=0"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_backup_coverage_count(timestamp):
    query = (
            "SELECT count(*) FROM backup_backup WHERE finished>\'" + str(timestamp) + "\' AND status = 3"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query, timestamp)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_prune_count_by_status(status_id):
    query = (
            "SELECT count(*) FROM backup_backup WHERE prune_status=" + str(status_id)
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_prune_count_disabled():
    query = (
            "SELECT count(*) FROM backup_backup WHERE prune_enabled=0"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_prune_coverage_count(timestamp):
    query = (
            "SELECT count(*) FROM backup_backup WHERE prune_finished>\'"
            + str(timestamp) + "\' AND prune_status=3"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query, timestamp)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_verify_count_by_status(status_id):
    query = (
            "SELECT count(*) FROM backup_backup WHERE verify_status=" + str(status_id)
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_verify_count_disabled():
    query = (
            "SELECT count(*) FROM backup_backup WHERE verify_enabled=0"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


def get_verify_coverage_count(timestamp):
    query = (
            "SELECT count(*) FROM backup_backup WHERE verify_finished>\'"
            + str(timestamp) + "\' AND verify_status=3"
    )
    cnx = mysql.connector.connect(**db_config)
    cursor = cnx.cursor()
    try:
        cursor.execute(query, timestamp)
        ret = cursor.fetchone()[0]
    except mysql.connector.Error as err:
        print(err.msg)
        ret = "ERROR"
    cursor.close()
    cnx.close()
    return ret


# Sends email
def send_email(mailto, agent, coverage, caution, warning):
    host = socket.gethostname()
    sender = "root@%s" % (host)
    sig = "sent by " + host
    if coverage >= caution:
        subject = "CAUTION: Restic " + agent + " coverage under 90%"
    if coverage < warning:
        subject = "WARNING: Restic " + agent + " coverage under 75%"
    body = "Restic " + agent + " coverage is currently at " + str(coverage) + "%. Please check!"
    message = "Subject: %s\nFrom: %s\nTo: %s\n\n%s\n-- \n%s\n" % (subject, sender, mailto, body, sig)
    try:
        print("Sending email to " + mailto + "...")
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, mailto, message)
    except smtplib.SMTPException as e:
        print("Error: unable to send email: %s\n%s" % (e, message))


# Main program
if __name__ == '__main__':
    # Instantiate the parser
    args = docopt(__doc__, version='cback metric send')

    config = load_config()
    db_config = {
        'user': config["database"]["user"],
        'password': config["database"]["password"],
        'host': config["database"]["host"],
        'port': config["database"]["port"],
        'database': config["database"]["database"],
        'raise_on_warnings': True
    }

    # Monitoring config
    MONITORING_SEND = config["general"]["send_metrics"]
    MONITORING_HOST = config["general"]["monitoring_host"]
    MONITORING_PORT = config["general"]["monitoring_port"]
    METRIC_PREFIX = config["general"]["monitoring_prefix"]

    BACKUP_THRESHOLD = int(config["switch"]["backup_expiration_time"])
    try:
        PRUNE_THRESHOLD = int(config["switch"]["prune_expiration_time"])
    except KeyError:
        pass
    VERIFY_THRESHOLD = int(config["switch"]["verify_expiration_time"])

    num_pending = get_backup_count_by_status(1)
    num_running = get_backup_count_by_status(2)
    num_completed = get_backup_count_by_status(3)
    num_failed = get_backup_count_by_status(5)
    num_archived = get_backup_count_by_status(6)
    # num_total = get_backup_count()
    disabled_count = get_backup_count_disabled()
    num_total = get_backup_count()
    backup_total = get_backup_count()-num_archived

    # we calculate backup coverage
    threshold_backup = datetime.now() - timedelta(hours=BACKUP_THRESHOLD)
    covered_count = get_backup_coverage_count(threshold_backup)
    coverage = round((covered_count / backup_total) * 100, 2)
    if covered_count != 0:
        coverage = round((covered_count / backup_total) * 100, 2)
    else:
        coverage = 0.00

    # Prune metrics

    num_prune_pending = get_prune_count_by_status(1)
    num_prune_running = get_prune_count_by_status(2)
    num_prune_completed = get_prune_count_by_status(3)
    num_prune_failed = get_prune_count_by_status(5)
    prune_disabled_count = get_prune_count_disabled()
    prune_total = get_backup_count()

    # we calculate prune coverage
    threshold_prune = datetime.now() - timedelta(hours=PRUNE_THRESHOLD)
    prune_covered_count = get_prune_coverage_count(threshold_prune)
    if prune_covered_count != 0:
        prune_coverage = round((prune_covered_count / prune_total) * 100, 2)
    else:
        prune_coverage = 0.00

    # Verify metrics

    num_verify_pending = get_verify_count_by_status(1)
    num_verify_running = get_verify_count_by_status(2)
    num_verify_completed = get_verify_count_by_status(3)
    num_verify_failed = get_verify_count_by_status(5)
    verify_disabled_count = get_verify_count_disabled()
    verify_total = get_backup_count()

    # we calculate verify coverage
    threshold_verify = datetime.now() - timedelta(hours=VERIFY_THRESHOLD)
    verify_covered_count = get_verify_coverage_count(threshold_verify)
    if verify_covered_count != 0:
        verify_coverage = round((verify_covered_count / verify_total) * 100, 2)
    else:
        verify_coverage = 0.00

    if config["general"]["send_metrics"]:
        push_to_monitoring("summary_total", num_total)
        push_to_monitoring("summary_pending", num_pending)
        push_to_monitoring("summary_running", num_running)
        push_to_monitoring("summary_completed", num_completed)
        push_to_monitoring("summary_failed", num_failed)
        push_to_monitoring("summary_disabled", disabled_count)
        push_to_monitoring("summary_coverage", coverage)
        push_to_monitoring("summary_prune_pending", num_prune_pending)
        push_to_monitoring("summary_prune_running", num_prune_running)
        push_to_monitoring("summary_prune_completed", num_prune_completed)
        push_to_monitoring("summary_prune_failed", num_prune_failed)
        push_to_monitoring("summary_prune_disabled", prune_disabled_count)
        push_to_monitoring("summary_prune_coverage", prune_coverage)
        push_to_monitoring("summary_verify_pending", num_verify_pending)
        push_to_monitoring("summary_verify_running", num_verify_running)
        push_to_monitoring("summary_verify_completed", num_verify_completed)
        push_to_monitoring("summary_verify_failed", num_verify_failed)
        push_to_monitoring("summary_verify_disabled", verify_disabled_count)
        push_to_monitoring("summary_verify_coverage", verify_coverage)
    if args["--send-email"]:
        mailto = args["--email-to"]
        if coverage < BACKUP_CAUT:
            print("Backup warning, sending email")
            send_email(mailto, "backup", coverage, BACKUP_CAUT, BACKUP_WARN)
        if prune_coverage < PRUNE_CAUT:
            print("Prune warning, sending email")
            send_email(mailto, "prune", prune_coverage, PRUNE_CAUT, PRUNE_WARN)
