# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
#!/usr/bin/env python3.4
# CERNBack Error Report
# rvalverd@cern.ch
# -*- coding: utf8 -*-

from __future__ import print_function
import os
from datetime import date, timedelta
import time
import random
import subprocess

# Main Config [Defaults]
LOGFILE = '/var/log/cback/cback-error-report.log'
VERSION = "v0.1.0"
EOS_DST_PATH = '/eos/project-e/eos/Durability/Backup/'
LOG_PATH = '/var/log/cback/'
WRITE_RETRY = 3
TMP_PATH = '/tmp/restic-error-report'


def execute(command, atimeout=None):
   try:
      output = subprocess.check_output(command, shell=True, universal_newlines=True, stderr=subprocess.STDOUT,
                                             timeout=atimeout)
      return True, output
   except subprocess.CalledProcessError as e:
      return False, e.output
   except subprocess.TimeoutExpired:
      return False, "timeout: " + str(command)


# Main program
if __name__ == '__main__':
    # We hardcode the logical to physical instance mapping
    instance_map = {"HOME-I00": ('d', 'l', 'n', 't', 'z'),
                    "HOME-I01": ('a', 'g', 'j', 'k', 'w'),
                    "HOME-I02": ('h', 'o', 'r', 's', 'y'),
                    "HOME-I03": ('b', 'e', 'm', 'v', 'x'),
                    "HOME-I04": ('c', 'f', 'i', 'p', 'q', 'u')}

    if not os.path.exists(TMP_PATH):
        os.mkdir(TMP_PATH)

    today = date.today()
    yesterday = today - timedelta(1)
    yesterday_formatted = yesterday.strftime('%Y%m%d')
    yesterday_formatted_2 = yesterday.strftime('%Y-%m-%d')
    log_fn = "cback-backup-errors-" + yesterday_formatted + ".log"
    source_file = os.path.join(LOG_PATH, log_fn)
    file_map = dict()
    print("Processing source file...")
    try:
        with open(source_file, 'r') as srcf:
            for line in srcf.readlines():
                if "argument list too long" in line:
                    continue
                sub = str(line).split(':')[1]
                fname_list = str(sub).split(' ')[2:]  # bit ugly but supports spaces...
                fname = ' '.join(fname_list)
                if str(fname).startswith('/eos/'):
                    instance = fname.split("/")[2]
                    letter = instance.split('-')[1]
                    fname_old_format = fname.replace('home-', 'user/')
                    for i in instance_map:
                        if letter in (instance_map[i]):
                            real_instance = i
                            dst_fn = "eos" + real_instance.lower() + "_backup_restic_error_files_"\
                                     + str(yesterday_formatted_2) + ".txt"
                            final_path = os.path.join(EOS_DST_PATH, real_instance, dst_fn)
                            temp_file = os.path.join(TMP_PATH, dst_fn)
                            with open(temp_file, 'a') as tempf:
                                tempf.write(fname_old_format + '\n')
                            file_map[temp_file] = final_path
    except OSError as e:
        print("OSError")
        print(e.strerror)

    # We transfer the files to eos if there is anything to send
    if len(list(file_map)) == 0:
        print("Nothing to send")
        exit(1)
    print("We transfer the files to eos...")
    for src, dst in file_map.items():
        retries = 0
        writen = False
        print("will transfer ", src, "to", dst)
        command = '/usr/sbin/runuser eos -c \"cat ' + str(src) + ' >> ' + str(dst) + '\"'
        print(command)
        while not writen and retries < WRITE_RETRY:
            ok, out = execute(command, 30)
            if ok:
                writen = True
            else:
                print("ERROR: ", str(out))
                print("Will retry...")
                retries = retries + 1
                with open(LOGFILE, 'a') as out:
                    out.write("Failed to copy to eos: " + str(out) + "\n")
                time.sleep(random.randint(30, 90))
        if retries >= WRITE_RETRY:
            with open(LOGFILE, 'a') as out:
                out.write("Failed to copy to eos: " + str(src) + "\n")
