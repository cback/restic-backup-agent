# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
#!/usr/bin/env python3
# CERNBack cda user add utility
# rvalverd@cern.ch
# -*- coding: utf8 -*-

from datetime import date
from cback.models.backupModel import BackupModel
import subprocess
import os
import json

SVC_ACCOUNT = "cboxmacwin"
PATH = "/eos/project-c/cboxmacwin/www/dfs2bemigrated.txt"
CONFIG_FILE = "/etc/cback/cback-config.json"


# Executes a command. Returns error and output
def execute(command):
    try:
        output = subprocess.check_output(command, shell=True, universal_newlines=True)
        return True, output
    except subprocess.CalledProcessError as e:
        return False, e


def get_users():
    command = "/usr/sbin/runuser " + SVC_ACCOUNT + " -c \"cat " + PATH + "\""
    return execute(command)


# check if a user name is resolvable
def check_user(username):
    command = "/usr/bin/id " + str(username)
    return execute(command)


# Check if a path exists on eos
def check_path(username, path):
    command = "/usr/sbin/runuser " + username + " -c \"stat " + path + "\""
    return execute(command)


# Main program
if __name__ == '__main__':
    ADDED = list()
    USER_NOT_FOUND = list()
    PATH_NOT_FOUND = list()
    # We check if the file exists:
    # We check if we can access the source path

    print(" ")
    command = "/usr/sbin/runuser " + SVC_ACCOUNT + " -c \"stat " + PATH + "\""
    ret, _ = execute(command)
    if ret:
        ret, users = get_users()
        if ret:
            # we get the list of users that we have in the backup
            with open(CONFIG_FILE, "r") as json_file:
                config = json.load(json_file)
            backup_model = BackupModel(config["database"])
            backup_dict = backup_model.get_list()
            user_list = list()
            for b in backup_dict:
                user_list.append(b["user_name"])
            for u in users.splitlines():
                if u not in user_list:
                    user_ok, _ = check_user(u)
                    if user_ok:
                        initial = u[:1]
                        instance = "home-" + initial
                        path = os.path.join("/eos/", instance, u)
                        path_ok, _ = check_path(u, path)
                        if path_ok:
                            command = "/usr/bin/cback backup add " + u + " " + path + " " + instance + " --enable"
                            _, out = execute(command)
                            ADDED.append(u)
                            print(out)
                        else:
                            # print("path does not exists, skipping")
                            PATH_NOT_FOUND.append(u)
                    else:
                        # print("user " + str(u) + " does not seem resolvable. Skipping")
                        USER_NOT_FOUND.append(u)
            # We print a summary
            today = date.today()
            print("----SUMMARY----")
            print("PROCESS DATE: ", str(today))
            print("USERS PROCESSED: ", str(len(users.splitlines())))
            print("USERS ADDED: ", str(len(ADDED)))
            print("USERS NOT FOUND: ", str(len(USER_NOT_FOUND)))
            print("PATHS NOT FOUND: ", str(len(PATH_NOT_FOUND)))
            print("============================================")
            #print("USERS ADDED")
            #for u in ADDED:
            #    print(u)
            #print("")
            #print("USERS NOT FOUND")
            #for u in USER_NOT_FOUND:
            #    print(u)
            #print("")
            #print("PATHS NOT FOUND")
            #for p in PATH_NOT_FOUND:
            #    print(p)
        else:
            print("problem getting user list")
            exit(1)
    else:
        print("cannot check path")
        exit(1)
