#
# cback spec file
#
Name:      cback
Summary:   A Restic backup orchestrator tool
Version:   0.3.9
Release:   1%{?dist}
License:   GPLv3
Buildroot: %{_tmppath}/%{name}-buildroot
Group:     CERN-IT/ST
BuildArch: noarch
Source: %{name}-%{version}.tar.gz

# The required Python version makes this package depend on Fedora 29 or similar recent distros to compile and run.
BuildRequires: python(abi) = 3.9
Requires: python(abi) = 3.9
BuildRequires: python39-pip
Requires: python39-pip

%description
This RPM provides a orchestration tool for restic

# Don't do any post-install weirdness, especially compiling .py files
%define __os_install_post %{nil}

# Build date
%define _build_date %(date +%FT%T%z)

# Git commit
%define _git_commit %(git rev-parse --short HEAD)

# Get the python lib directory
%define _python_lib %(/usr/bin/python3.9 -c "from distutils import sysconfig; print(sysconfig.get_python_lib())")

%prep
%setup -q -n %{name}-%{version}

%build

%install
# set version, build date and git commit
sed -i "s/VERSION = '.*'/VERSION = '%{version}-%{release}'/" src/cback.py
sed -i "s/BUILD_DATE = '.*'/BUILD_DATE = '%{_build_date}'/" src/cback.py
sed -i "s/GIT_COMMIT = '.*'/GIT_COMMIT = '%{_git_commit}'/" src/cback.py
# installation
rm -rf %buildroot/
mkdir -p %buildroot/usr/bin
mkdir -p %buildroot/etc/cback
mkdir -p %buildroot/var/log/cback
mkdir -p %buildroot/var/cache/cback
mkdir -p %buildroot/cback/venv
# create all the folders that are in src
find src -mindepth 1 -type d -not -path '*/__pycache__*' | sed -s 's/src\///' | xargs -i mkdir -p %buildroot/%_python_lib/cback/{}
install -m 644 pyproject.toml %buildroot/%_python_lib/cback
install -m 644 poetry.lock %buildroot/%_python_lib/cback
# copy all python files in the directories
find src -type f -name *.py | sed -s 's/src\///' | xargs -i install -m 644 "src/{}" "%buildroot/%_python_lib/cback/{}"
ln -sr %buildroot/%_python_lib/cback/cback.py %buildroot/usr/bin/cback
chmod 755 %buildroot/%_python_lib/cback/cback.py

%clean
rm -rf %buildroot/

%post
pip3.9 install poetry
cd %_python_lib/cback
/bin/sh <<< "/usr/local/bin/poetry install --no-dev && /usr/bin/cp -r \`/usr/local/bin/poetry env info --path\` %_python_lib/cback/venv && /usr/bin/sed -i \"s|/usr/bin/python3|/usr/bin/env %_python_lib/cback/venv/bin/python|g\" %_python_lib/cback/cback.py"

%files
%defattr(-,root,root,-)
/usr/bin/cback
/etc/cback
/var/log/cback
/var/cache/cback
%_python_lib/cback/*

%changelog
* Thu May 23 2024 Zachary Goggin <zachary.goggin@cern.ch> 0.3.9
- Fix: fixed group --force configuration to make it take precidence over job force field 
- Fix: agent disable cli feedback corrected, previously was enable for enable+disable 
- Enh: Add DB versioning table for schema -> rpm version corrospondance
- Enh: Add graceful deletion parameters to prune agents
- Enh: add cback backup add --no-workload for individual backup job creation
- Enh: add atomic, trimmed restores using a scratch path to portal restore 
- Enh: add snapshot tagging from the portal on snapshot creation
- Enh: add portal https support
- Enh: add progress fields for restore and backup (portal and cli queryable)

* Mon Sep 25 2023 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.8-1
- Enh: On error reschedule job after 30 min
- Fix: Fix timestamp on find command
- Fix: Create prune and verify jobs when creating new backup from portal
* Tue Aug 08 2023 Roberto Valverde <rvalverd@cern.ch> 0.3.7-5
- Release for alma8, rhel8
- Pin specific python version 3.9 in spec file.
- Require python39-pip in spec file. 
* Tue Mar 07 2023 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.7-4
- Disable file scanning on backups
* Tue Jan 10 2023 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.7-3
- Fix restore for patterns containing spaces
* Fri Dec 09 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.7-2
- Fix cback release
* Fri Dec 09 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.7-1
- version 0.3.7-1
* Mon Nov 07 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.6-7
- Update restore destination when not set during job creation
* Thu Nov 03 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.6-6
- Fix snapshots print
* Wed Nov 02 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.6-5
- Fix group modify
* Wed Oct 26 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.6-4
- Bail out if machine does not have permissions to read the backup source
* Tue Oct 25 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.6-3
- Relax path conversion to enable eosmedia
* Mon Oct 24 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.6-2
- Fix print of group in job status
* Mon Oct 24 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.6-1
- Show rescheduled jobs in status
- Do not lock restic repository when checking if it exists
- Define a cronjob like desired start time for a backup job
- Print jobs group
- Move backup into an other group
- Fix sql procedures for atomicity
* Mon Oct 03 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.5-2
- Add logs when picking a backup job
* Mon Oct 03 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.5-1
- Expose mounter via HTTP
- Remove virtualenv folder when unistalling rpm
- Fix time serialization when printing in json format
* Wed Sep 21 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.4-3
- Use time with local timestamp on eos
* Mon Sep 19 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.4-2
- Fix EOS timestamp when year is out of range
* Tue Sep 13 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.4-1
- fix manual scope for all groups
* Wed Aug 31 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.3-1
- Handle user defined scope from CLI
- Add dearchiver service to re-enable archived jobs of users that rejoined CERN
- Impl: Cache scopes, users and groups to improve the performances
- Fix: Do not show in the scope user folders having same name of projects
- Fix: Restore status long table
- Use restic mount repository exposed via ftp for the portal
* Fri Aug 12 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.2-4
- Fix switch agent when an exeption is raised
* Wed Aug 03 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.2-3
- Fix empty job list in portal requests
- Fix: Create only scope providers in the config mapping
* Wed Aug 03 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.2-2
- Fix systemd agent with python3.9
- Update dependencies
* Tue Aug 02 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.2-1
- Portal
* Thu Jul 28 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.1-5
- Imp: Do not raise exception if group is not in the config
* Tue Jul 26 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.1-4
- Imp: Allow pinned node in backup job to be a regex
- Fix: Use simple table for snapshots
- Imp: Send archived backup jobs in summary
* Thu Jul 21 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.1-3
- Imp: Specify restic repository path on backup creation
- Fix: Fix status fields
* Wed Jul 20 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.1-2
- Fix: Set correct info status if the user does not exist
- Imp: Set custom headers in table for every job
* Tue Jul 19 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.1-1
- Imp: Add output in json format
* Fri Jul 15 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-9
- Imp: Improve performance of all jobs commands
- Fix: restic process does not hang on long output
* Wed Jul 06 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-8
- Fix: Cancel progress job thread after it finihes running
- Imp: Better status print
- Fix: Use last snapshot id as parent when running a backup
* Tue Jul 05 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-7
- Imp: Pass destionation options to restic binary from config
* Wed Jun 01 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-6
- Fix: Restore agent running as a prune agent
- Fix: Check correct error when repo is locked for restic stats, get, find cmds
* Wed Jun 01 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-5
- Fix: Patch missing dependency when creating managers
* Tue May 31 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-4
- Fix: Use release day and push new release to origin
- Imp: Get status of all groups when group is not provided
- Imp: Add command to release a new version
- Fix: Typo in verify status cmd
- Fix: Prune snapshots even if created by different hosts
* Thu May 19 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-3
- Imp: Revamp monitoring
- Imp: User friendly errors during backup
* Mon May 16 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-2
- Fix: install python dependencies after installing the rpm
* Mon May 16 2022 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.3.0-1
- Imp: support multiple services in one single instance
* Fri Jun 25 2021 Roberto Valverde <rvalverd@cern.ch> 0.2.5-4
- Fix: node pinning patch
* Fri Jun 25 2021 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.2.5-3
- Fix: exception handling for max retries
* Fri Jun 25 2021 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.2.5-2
- Fix: backup still in RUNNING if there is a post backup error
- Fix: backup automatic archival
- Fix: retry mechanism for backups
* Thu Jun 24 2021 Gianmaria Del Monte <gianmaria.del.monte@cern.ch> 0.2.5-1
- Fix: rpm requirements
* Mon May 17 2021  <rvalverd@cern.ch> 0.2.4-1
- Add: flag for showing all snapshots, not only for job path
- Cha: add force backup configurable.
- Cha: archived jobs are no longer disabled
- Fix: locking issue
- New: migration and sync of jobs
* Wed Apr 14 2021 Roberto Valverde <rvalverd@cern.ch> 0.2.3-1
- Fix: Verify, check that adler32 is installed
- Imp: Configurable job timeouts
- Imp: Timeout for sending metrics
- Imp: Verify: distinguish metrics for phase1/2 jobs.
* Mon Mar 29 2021 Roberto Valverde <rvalverd@cern.ch> 0.2.2-1
- verify: fix data column too long CERNBOX-1762
- prune: keep last 1 for archived backups
- prune: fix metric send
* Mon Mar 29 2021 Gianmaria Del Monte <gdelmont.cern.ch> 0.2.1-1
- gdelmont: fix for 'NULL' in verify
* Fri Mar 19 2021 Roberto Valverde <rvalverd@cern.ch>, Gianmaria Del Monte <gdelmont.cern.ch> 0.2.0-1
- rvalverd: several fixes for ceph deployment
- gdelmont: support for restic v0.12.0
* Tue Mar 16 2021 Roberto Valverde <rvalverd@cern.ch> 0.1.19-1
- rvalverd - Enhance: allow having S3 env file per user.
- gdelmont - Enhance: cback-verify kill
- gdelmont - Fix: change running from pending.
* Wed Feb 03 2021 Roberto Valverde <rvalverd@cern.ch> 0.1.18-1
- Fix: cback recover parsing problem.
- Added: cback prune reset-failed and cback prune kill
* Mon Dec 07 2020 João Vicente <joao.calado.vicente@cern.ch> 0.1.17-1
- Added: Logging and cleanup to verify tool
- Changed: Streamed metadata json for verification of big backups
* Thu Nov 12 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.16-1
- Added: backup kill and reset-old options.
- Change: modified metadata save to use specific DB field and store snapshot id
* Tue Nov 03 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.15-1
- Added: archive backups
* Tue Oct 20 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.14-1
- Handle return code 3 on restic 0.10.0
* Tue Oct 06 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.13-1
- Fix autounlock in restic 0.10.0
* Tue Oct 06 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.12-1
- Fix crash backup
- Fix prune_info length
* Wed Sep 23 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.11-1
- cback-recover: make data range not mandatory
* Tue Sep 8 2020 João Vicente <joao.calado.vicente@cern.ch> 0.1.10-1
- Add: Save post backup metadata on successful backup.
* Mon Aug 31 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.9-1
- Add: Post check of mount health.
* Wed Aug 19 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.8-1
- Change: increased timeout for restic find commands.
- Sends duration of backup, prune, verify and restore jobs.
* Mon Aug 17 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.7-1
- Fixed: cback prune run command.
* Wed Aug 5 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.6-1
- Added: cback backup find command
* Mon Jul 6 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.5-1
- Fix: prune not marking success jobs as completed
* Tue Jun 30 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.4-1
- Fix: prune function naming
* Tue Jun 30 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.3-1
- Feature: switchable backup selection method
* Fri Jun 26 2020 Roberto Valverde <rvalverd@cern.ch> 0.1.2-1
- Feature: enable/disable jobs per instance
* Thu Jun 25 2020 João Vicente <joao.calado.vicente@cern.ch> 0.1.1-2
- Bugfix: Agent db query missing force_backup field.
* Tue Jun 23 2020 João Vicente <joao.calado.vicente@cern.ch> 0.1.1
- Add forced retry for user backups with errors.
* Mon Jul  1 2019 Roberto Valverde <rvalverd@cern.ch> 0.1.0
- Initial version.
