FILES_TO_RPM = src pyproject.toml poetry.lock
SPECFILE = $(shell find . -type f -name *.spec)
PACKAGE  = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
VERSION  = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
RELEASE  = $(shell awk '$$1 == "Release:"  { print $$2 }' $(SPECFILE) )
DIST    ?= $(shell rpm --eval %{dist})
rpmbuild = ${shell pwd}/build

# ------------------------------------------

clean:
	@rm -rf $(PACKAGE)-$(VERSION)
	@rm -rf $(rpmbuild)
	@rm -rf *.rpm
	@find . -name '*.pyc' -exec rm -f \{\} \;
	@find . -name '*.pyo' -exec rm -f \{\} \;
	@find . -name '__pycache__' -exec rm -rf \{\} \;

lint:
	@mypy src/ --ignore-missing-imports --install-types

test:
	@pytest --cov=src --cov-config=.pycoverage --ignore src/repository/mysql --junitxml=test.xml

testmysql:
	@pytest -k mysql --cov=src/repository/mysql --junitxml=mysql.xml

rpmdefines=--define='_topdir ${rpmbuild}' \
	--define='_sourcedir %{_topdir}/SOURCES' \
	--define='_builddir %{_topdir}/BUILD' \
	--define='_srcrpmdir %{_topdir}/SRPMS' \
	--define='_rpmdir %{_topdir}/RPMS' \
	--define 'dist $(DIST)'

dist: clean
	@mkdir -p $(PACKAGE)-$(VERSION)
	@cp -r $(FILES_TO_RPM) $(PACKAGE)-$(VERSION)
	tar cpfz ./$(PACKAGE)-$(VERSION).tar.gz $(PACKAGE)-$(VERSION)

prepare: dist
	@mkdir -p $(rpmbuild)/RPMS/noarch
	@mkdir -p $(rpmbuild)/SRPMS/
	@mkdir -p $(rpmbuild)/SPECS/
	@mkdir -p $(rpmbuild)/SOURCES/
	@mkdir -p $(rpmbuild)/BUILD/
	@mv $(PACKAGE)-$(VERSION).tar.gz $(rpmbuild)/SOURCES
	@cp $(SPECFILE) $(rpmbuild)/SOURCES

srpm: prepare $(SPECFILE)
	rpmbuild --nodeps -bs $(rpmdefines) $(SPECFILE)
	cp $(rpmbuild)/SRPMS/* .

rpm: srpm
	rpmbuild --nodeps -bb $(rpmdefines) $(SPECFILE)
	cp $(rpmbuild)/RPMS/noarch/* .