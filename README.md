# cback  - restic orchestrator
CernBACKup is a orchestration tool built on [Restic](https://restic.net/) for the management and automation of high volume backups to an S3 endpoint.

cback uses a centralized database to persist stateful information on backup, restore & prune jobs (their state, progress, target etc) which can be
queried, run and subsequently updated by a stateless agent of a corrosponding type. cback allows for a scalable number of these agents. The agent
types are as follows: 

- backup agent: queries the database for backup jobs and process them.
- restore agent: queries the database for restore jobs and process them. 
- prune agent: works directly with S3 storage backend and removes old snapshots.
- switch agent: decides when a backup job is outdated and need to be run again.
- verify agent: selects random backup jobs and verifies them.
- enabler agent [optional]: enable gently the new jobs added to the system.
- portal agent [optional]: REST API webserver for manipulating jobs in the system.

Agents can be run manually, or as systemd units, likewise agent location is not static, one cback "worker" node can have multiple agents of different
types or multiple of the same type or even just one per node. A cback worker node can also be used to interact manually with the jobs DB using the 
`cback` cli bundled with the program.

further steps on operating and installing cback be found in our [docs](https://cback.docs.cern.ch)

## Building a new release
To release a new version of cback, do the following:
- ensure your desired features / feature branch has been merged into master with a clean CI pipeline.
- ensure you are on the master branch with `git checkout master; git pull`
- update [cback.spec](cback.spec) 
  - with the new version number, `VERSION=<NEW_VER_NO_HERE>; sed -i -e "s/Version:   \([0-9]\.\)\{2\}[0-9]/Version:   $VERSION/ " cback.spec`
  - add a changelog entry to cback.spec in the `%changelog` section describing changes made for this new version no.
- update the sql schema version in `src/repository/mysql/init.sql`, line concerned is: `INSERT INTO release_version (version) VALUES ('NEW_VER_NO_HERE'); 
- Commit the updated cback.spec `git commit -am 'version <NEW_VER_NO_HERE>'`
- Add a tag on the latest commit `git tag -a v0.2.3 -m 'version 0.2.3'`
- Push to master `git push origin master`

## known issues
- **failed install**, Sometimes, cback will complain with `/usr/bin/env: ‘/cback/venv/bin/python’: No such file or directory` on a fresh install.
In this case navigate to repository and issue `dnf remove cback; dnf install cback-*.cern.noarch.rpm; poetry install` to fix.

- **Old restic versions**, certain older versions of restic are known not to work with cback due to a reliance on features that are simply not present
releases, you can check what version of restic cback is using with `cback --version` ensure that you see a release > or == to  `0.14.1` if you do not,
you can use `restic self-update` to [update](https://restic.readthedocs.io/en/latest/020_installation.html).

- **Using eos as a fuse source**, the backup and restore agents will impersonate users using `runuser` commands, so 
the agents should be trusted as vid gateways by the eos instance `eos vid add gateway <agent_hostname> unix`
