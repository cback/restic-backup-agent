# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime, time
from typing import Any, Dict, Optional, Type, TypeVar
from views.view import Formatter

from model.jobs.backup import Backup
from model.jobs.verify import Verify
from model.jobs.restore import Restore
from model.jobs.prune import Prune
from model.jobs.status import Status
from model.jobs.group import Group

from model.jobs.job import Job


class BackupNestedFormatter(Formatter[Backup]):

    def format(self, b: Backup) -> Dict[str, Any]:
        return {
            'id': b.id,
            'group': _format_group(b.group),
            'enabled': b.enabled,
            'name': b.name,
            'username': b.username,
            'source': b.source,
            'instance': {
                'name': b.instance.name,
            },
            'desired_start_time': b.desired_start_time,
            'repository': b.repository.path,
            'status': Status.to_str(b.status),
            'worker': b.worker,
            'stats': {
                'files_new': b.files_new,
                'dirs_new': b.dirs_new,
                'files_changed': b.files_changed,
                'dirs_changed': b.dirs_changed,
                'files_unmodified': b.files_unmodified,
                'dirs_unmodified': b.dirs_unmodified,
                'data_added': b.data_added
            },
            'pinned_node': b.pinned_node,
            'started': _datetime_to_str(b.started),
            'finished': _datetime_to_str(b.finished),
            'info': b.info
        }


def _format_group(g: Group) -> Dict[str, Any]:
    return {
        'id': g.id,
        'name': g.name,
    }


class PruneNestedFormatter(Formatter[Prune]):

    def __init__(self, backup_formatter: BackupNestedFormatter) -> None:
        self.backup_formatter = backup_formatter

    def format(self, p: Prune) -> Dict[str, Any]:
        return {
            'id': p.id,
            'backup': self.backup_formatter.format(p.backup),
            'group': _format_group(p.group),
            'status': Status.to_str(p.status),
            'started': _datetime_to_str(p.started),
            'finished': _datetime_to_str(p.finished),
            'enabled': p.enabled,
            'worker': p.worker,
            'info': p.info
        }


class RestoreNestedFormatter(Formatter[Restore]):

    def __init__(self, backup_formatter: BackupNestedFormatter) -> None:
        self.backup_formatter = backup_formatter

    def format(self, r: Restore) -> Dict[str, Any]:
        return {
            'id': r.id,
            'backup': self.backup_formatter.format(r.backup),
            'group': _format_group(r.group),
            'enabled': r.enabled,
            'username': r.username,
            'pattern': r.pattern,
            'destination': r.destination,
            'snapshot': r.snapshot,
            'status': Status.to_str(r.status),
            'created': _datetime_to_str(r.created),
            'started': _datetime_to_str(r.started),
            'finished': _datetime_to_str(r.finished),
            'worker': r.worker,
            'info': r.info
        }


class VerifyNestedFormatter(Formatter[Verify]):

    def __init__(self, backup_formatter: BackupNestedFormatter) -> None:
        self.backup_formatter = backup_formatter

    def format(self, v: Verify) -> Dict[str, Any]:
        return {
            'id': v.id,
            'backup': self.backup_formatter.format(v.backup),
            'group': _format_group(v.group),
            'status': Status.to_str(v.status),
            'started': _datetime_to_str(v.started),
            'finished': _datetime_to_str(v.finished),
            'enabled': v.enabled,
            'worker': v.worker,
            'info': v.info
        }


J = TypeVar('J', bound=Job)


def get_nested_formatter_from_job_type(job_type: Type[J]) -> Formatter:
    if job_type == Backup:
        return BackupNestedFormatter()
    elif job_type == Prune:
        return PruneNestedFormatter(BackupNestedFormatter())
    elif job_type == Restore:
        return RestoreNestedFormatter(BackupNestedFormatter())
    elif job_type == Verify:
        return VerifyNestedFormatter(BackupNestedFormatter())
    raise ValueError(f"job type {job_type} not recognised")


def _datetime_to_str(d: Optional[datetime]) -> Optional[str]:
    return d.strftime("%Y/%m/%d %H:%M:%S") if d else None


def _time_to_str(t: Optional[time]) -> Optional[str]:
    return t.strftime("%H:%M") if t else None
