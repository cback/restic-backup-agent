# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import Any, Dict, Type, TypeVar, Optional

from views.view import Formatter

from model.jobs.backup import Backup
from model.jobs.verify import Verify
from model.jobs.restore import Restore
from model.jobs.prune import Prune
from model.jobs.status import Status

from humanfriendly import format_size
from model.jobs.job import Job


class BackupFlattenFormatter(Formatter[Backup]):

    def _format_stats(self, b: 'Backup') -> str:
        return f'N[{b.files_new}/{b.dirs_new}] C[{b.files_changed}/{b.dirs_changed}] U[{b.files_unmodified}/{b.dirs_unmodified}] - Added: {format_size(b.data_added)}'

    def format(self, b: Backup) -> Dict[str, Any]:
        return {
            'id': b.id,
            'group': b.group.name,
            'enabled': b.enabled,
            'name': b.name,
            'username': b.username,
            'source': b.source,
            'instance_name': b.instance.name,
            'desired_start_time': b.desired_start_time,
            'repository': b.repository.path,
            'status': Status.to_str(b.status),
            'worker': b.worker,
            'stats': self._format_stats(b),
            'pinned_node': b.pinned_node,
            'started': b.started,
            'finished': b.finished,
            'info': b.info
        }


class PruneFlattenFormatter(Formatter[Prune]):

    def format(self, p: Prune) -> Dict[str, Any]:
        return {
            'id': p.id,
            'backup_id': p.backup.id,
            'group': p.group.name,
            'name': p.backup.name,
            'source': p.backup.source,
            'instance': p.backup.instance.name,
            'repository': p.backup.repository.path,
            'status': Status.to_str(p.status),
            'started': p.started,
            'finished': p.finished,
            'enabled': p.enabled,
            'worker': p.worker,
            'info': p.info
        }


class VerifyFlattenFormatter(Formatter[Verify]):

    def format(self, v: Verify) -> Dict[str, Any]:
        return {
            'id': v.id,
            'backup_id': v.backup.id,
            'group': v.group.name,
            'name': v.backup.name,
            'source': v.backup.source,
            'instance': v.backup.instance.name,
            'repository': v.backup.repository.path,
            'status': Status.to_str(v.status),
            'started': v.started,
            'finished': v.finished,
            'enabled': v.enabled,
            'worker': v.worker,
            'info': v.info
        }


class RestoreFlattenFormatter(Formatter[Restore]):

    def format(self, r: Restore) -> Dict[str, Any]:
        return {
            'id': r.id,
            'backup_id': r.backup.id,
            'group': r.group.name,
            'name': r.backup.name,
            'enabled': r.enabled,
            'username': r.username,
            'repository': r.backup.repository.path,
            'source': r.backup.source,
            'pattern': r.pattern,
            'destination': r.destination,
            'snapshot': r.snapshot,
            'status': Status.to_str(r.status),
            'created': r.created,
            'started': r.started,
            'finished': r.finished,
            'worker': r.worker,
            'info': r.info
        }


J = TypeVar('J', bound=Job)


def get_flatten_formatter_from_job_type(job_type: Type[J]) -> Formatter:
    if job_type == Backup:
        return BackupFlattenFormatter()
    elif job_type == Prune:
        return PruneFlattenFormatter()
    elif job_type == Restore:
        return RestoreFlattenFormatter()
    elif job_type == Verify:
        return VerifyFlattenFormatter()
    raise ValueError(f"job type {job_type} not recognised")
