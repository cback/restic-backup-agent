# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, List, Mapping, Type, TypeVar, Union, Tuple
from portal.scope.scope import Scope, parse
from views.view import Formatter, GenericView, GroupView, JobView, Printer, SnapshotView
from views.formatters.flatten_formatter import get_flatten_formatter_from_job_type
from views.formatters.nested_formatter import get_nested_formatter_from_job_type
from views.printers.json_printer import JSONPrinter
from views.printers.table_printer import SimpleTablePrinter
from views.printers.table_printer import FancyGridTablePrinter
from model.jobs.job import Job
from model.jobs.backup import Backup
from model.jobs.prune import Prune
from model.jobs.restore import Restore
from model.jobs.verify import Verify
from portal.auth.providers.impersonator import Token


def _get_group_printer_from_mode(mode: str) -> Printer:
    if mode == "json":
        return JSONPrinter()
    elif mode == "table":
        short_list = ["id", "name"]
        headers = {"id": "Id", "name": "Name"}
        return FancyGridTablePrinter(
            short_list=short_list,
            header_list=headers,
            long=False,
        )
    raise ValueError(f"mode {mode} not recognised")


def get_group_view(mode: str = "table") -> GroupView:
    printer = _get_group_printer_from_mode(mode)
    return GroupView(printer)


J = TypeVar('J', bound=Job)


def _get_job_formatter_from_mode(job_type: Type[J], mode: str) -> Formatter:
    if "table" in mode:
        return get_flatten_formatter_from_job_type(job_type)
    elif mode == "json":
        return get_nested_formatter_from_job_type(job_type)
    raise ValueError(f"mode {mode} not recognised")


def _get_lists_from_job_type(job_type: Type[J]) -> Tuple[List[str], List[str], Dict[str, str]]:
    if job_type == Backup:
        short_list = ["id", "group", "name", "source", "instance_name", "repository", "status", "started",
                      "finished", "info"]
        long_list = ["id", "group", "enabled", "name", "username", "source", "instance_name", "repository", "status", "worker", "stats",
                     "pinned_node", "desired_start_time", "started", "finished", "info"]
        headers = {"id": "Id", "group": "Group", "enabled": "Enabled", "name": "Name", "username": "Username", "source": "Source",
                   "repository": "Repository", "status": "Status", "worker": "Worker",
                   "pinned_node": "Pinned Node", "started": "Started", "finished": "Finished",
                   "info": "Info", "instance_name": "Instance", "stats": "Stats", "desired_start_time": "Desidered start time"}
        return short_list, long_list, headers
    elif job_type == Prune:
        short_list = ["id", "group", "name", "source", "instance",
                      "status", "started", "finished", "info"]
        long_list = ["id", "group", "backup_id", "enabled", "name", "source", "instance",
                     "repository", "status", "started", "finished", "worker", "info"]
        headers = {"id": "Id", "group": "Group", "backup_id": "Backup Id", "name": "Name", "enabled": "Enabled", "source": "Source Backup", "status": "Status", "started": "Started",
                   "finished": "Finished", "worker": "Worker", "info": "Info", "repository": "Repository", "instance": "Instance"}
        return short_list, long_list, headers
    elif job_type == Restore:
        short_list = ["id", "group", "name", "pattern", "destination",
                      "status", "created", "started", "finished", "info"]
        long_list = ["id", "group", "backup_id", "enabled", "name", "username", "repository", "source", "pattern",
                     "destination", "snapshot", "status", "created", "started", "finished", "worker",
                     "info"]
        headers = {"id": "Restore Id", "group": "Group", "backup_id": "Backup Id", "enabled": "Enabled", "name": "Name", "username": "Username",
                   "source": "Source Backup", "destination": "Destination", "status": "Status", "snapshot": "Snapshot",
                   "created": "Created", "started": "Started", "finished": "Finished",
                   "worker": "Worker", "info": "Info", "repository": "Repository", "pattern": "Pattern"}
        return short_list, long_list, headers
    elif job_type == Verify:
        short_list = ["id", "group", "name", "source", "instance",
                      "status", "started", "finished", "info"]
        long_list = ["id", "group", "backup_id", "enabled", "name", "source", "instance",
                     "repository", "status", "started", "finished", "worker", "info"]
        headers = {"id": "Id", "group": "Group", "backup_id": "Backup Id", "name": "Name", "enabled": "Enabled", "source": "Source Backup", "status": "Status", "started": "Started",
                   "finished": "Finished", "worker": "Worker", "info": "Info", "repository": "Repository", "instance": "Instance"}
        return short_list, long_list, headers
    raise ValueError(f"job type {job_type} not recognised")


def _get_job_printer_from_mode(job_type: Type[J], mode: str, long: bool, *, custom_field: List[str] = []) -> Printer:
    if mode == "json":
        return JSONPrinter()

    short_list, long_list, headers = _get_lists_from_job_type(job_type)
    if custom_field:
        short_list = custom_field
    if mode == "simple_table":
        return SimpleTablePrinter(
            short_list=short_list,
            long_list=long_list,
            header_list=headers,
            long=long
        )
    elif mode == "grid_table":
        return FancyGridTablePrinter(
            short_list=short_list,
            long_list=long_list,
            header_list=headers,
            long=long
        )
    raise ValueError(f"mode {mode} not recognised")


def get_job_view(job_type: Type[J], mode: str = "grid_table", long: bool = False, *, custom_fields: List[str] = []) -> JobView[J]:
    formatter = _get_job_formatter_from_mode(job_type, mode)
    printer = _get_job_printer_from_mode(
        job_type, mode, long, custom_field=custom_fields)
    return JobView(job_type, formatter, printer)


def _get_snapshots_printer_from_mode(mode: str, long: bool) -> Printer:
    if mode == "table":
        short_list = ["short_id", "time", "hostname", "tags", "paths"]
        long_list = ["short_id", "time", "hostname", "tags", "paths",
                     "parent", "tree", "username", "excludes"]

        headers = {"short_id": "Id", "time": "Time", "hostname": "Host", "tags": "Tags", "paths": "Paths",
                   "parent": "Parent", "tree": "Tree", "username": "Username", "excludes": "Excludes"}
        return SimpleTablePrinter(
            short_list=short_list,
            long_list=long_list,
            header_list=headers,
            long=long
        )
    elif mode == "json":
        return JSONPrinter()
    raise ValueError(f"mode {mode} not recognised")


def get_snapshots_view(mode: str = "table", long: bool = False) -> SnapshotView:
    printer = _get_snapshots_printer_from_mode(mode, long)
    return SnapshotView(printer)


def print_status_box(data: Dict[str, Dict[str, Any]]) -> None:
    # data = map[group_name]{key: value}
    # the status header is the same for each subdict, as they
    # are computed on the same job type
    status_headers: List[str] = []
    rows: List[Dict[str, Any]] = []

    for group, status in data.items():
        if not status_headers:
            status_headers = ["Group"]+list(status.keys())
        d = {"Group": group, **status}
        rows.append(d)

    printer = FancyGridTablePrinter(status_headers, status_headers, {
                                    s: s for s in status_headers})
    printer.print(rows)


def print_tokens(tokens: Union[Token, List[Token]]) -> None:
    short_list = ["id", "hashed_token", "created",
                  "expiration", "comment", "groups"]

    headers = {"id": "Id", "hashed_token": "Token", "created": "Created",
               "expiration": "Expiration", "comment": "Comment", "groups": "Groups"}
    printer = FancyGridTablePrinter(
        short_list=short_list,
        header_list=headers,
    )
    view: GenericView[Token] = GenericView(printer)
    view.print(tokens, lambda t: {
        'id': t.id,
        'hashed_token': t.hashed_token,
        'created': t.created,
        'expiration': t.expiration,
        'comment': t.comment,
        'groups': ', '.join(t.groups)
    })


def print_scopes(scopes: Mapping[str, Scope]) -> None:
    def _print(t: Mapping[str, Scope]) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        for u, scope in t.items():
            res[u] = [s.dict() for s in parse(scope)]
        return res

    view: GenericView[Mapping[str, Scope]] = GenericView(JSONPrinter())
    view.print([scopes], _print)
