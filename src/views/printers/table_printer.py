# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, Iterable, List
from views.view import Printer
from tabulate import tabulate


class _TablePrinter(Printer):

    def __init__(self, mode: str, short_list: List[str] = [], long_list: List[str] = [], header_list: Dict[str, str] = {}, long: bool = False, custom_fields: List[str] = []) -> None:
        self.mode = mode
        self.short_list = short_list
        self.long_list = long_list
        self.headers = header_list
        self.custom_fields = custom_fields
        self.long = long

    # This function filters a dictionary with only specified keys
    def _filter_data(self, old_data: Iterable[Dict[str, Any]], key_filters: List[str]) -> Iterable[Dict[str, Any]]:
        return ({k: d[k] for k in key_filters} for d in old_data)

    def print(self, lst: Iterable[Dict[str, Any]]) -> None:
        fields = self.short_list
        if self.long:
            fields = self.long_list
        if self.custom_fields:
            fields = self.custom_fields
        filtered_data = self._filter_data(lst, fields)

        try:
            print(tabulate(filtered_data, headers=self.headers, tablefmt=self.mode))
        except UnicodeEncodeError:
            print(tabulate(filtered_data, headers=self.headers, tablefmt='simple'))


class SimpleTablePrinter(Printer):

    def __init__(self, short_list: List[str] = [], long_list: List[str] = [], header_list: Dict[str, str] = {}, *, long: bool = False, custom_fields: List[str] = []) -> None:
        self.table_printer = _TablePrinter(
            'simple', short_list, long_list, header_list, long, custom_fields)

    def print(self, lst: Iterable[Dict[str, Any]]) -> None:
        self.table_printer.print(lst)


class FancyGridTablePrinter(Printer):

    def __init__(self, short_list: List[str] = [], long_list: List[str] = [], header_list: Dict[str, str] = {}, *, long: bool = False, custom_fields: List[str] = []) -> None:
        self.table_printer = _TablePrinter(
            'fancy_grid', short_list, long_list, header_list, long, custom_fields)

    def print(self, lst: Iterable[Dict[str, Any]]) -> None:
        self.table_printer.print(lst)
