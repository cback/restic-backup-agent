# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from typing import Any, Callable, Dict, Generic, Iterable, List, Type, TypeVar, Union

from model.jobs.job import Job
from model.jobs.group import Group
from model.restic.restic import Snapshot


class Printer(ABC):

    @abstractmethod
    def print(self, lst: Iterable[Dict[str, Any]]) -> None: ...


J = TypeVar('J', bound=Job)


class Formatter(ABC, Generic[J]):

    @abstractmethod
    def format(self, job: J) -> Dict[str, Any]: ...


class JobView(Generic[J]):

    def __init__(self, job_type: Type[J], formatter: Formatter[J], printer: Printer) -> None:
        self.printer = printer
        self.formatter = formatter
        self.job_type = job_type

    def print(self, jobs: Union[J, Iterable[J]]) -> None:
        if not isinstance(jobs, Iterable):
            jobs = [jobs]

        d = map(self.formatter.format, jobs)
        self.printer.print(d)


class GroupView:

    def __init__(self, printer: Printer) -> None:
        self.printer = printer

    def _group_to_dict(self, g: 'Group') -> Dict[str, Any]:
        return {
            'id': g.id,
            'name': g.name
        }

    def print(self, groups: Union[Group, Iterable[Group]]) -> None:
        if isinstance(groups, Group):
            groups = [groups]

        d = map(self._group_to_dict, groups)
        self.printer.print(d)


class SnapshotView:

    def __init__(self, printer: Printer) -> None:
        self.printer = printer

    def _snapshot_to_dict(self, s: Snapshot) -> Dict[str, Any]:
        return {
            'short_id': s.short_id,
            'time': s.time.strftime("%Y-%m-%d %H:%M:%S"),
            'hostname': s.hostname,
            'tags': ', '.join(s.tags),
            'paths': '\n'.join(s.paths),
            'parent': s.parent[:8],
            'tree': s.tree[:8],
            'username': s.username,
            'excludes': ', '.join(map(lambda s: f'\'{s}\'', s.excludes))
        }

    def print(self, snapshots: Union[Snapshot, Iterable[Snapshot]]) -> None:
        if isinstance(snapshots, Snapshot):
            snapshots = [snapshots]

        d = map(self._snapshot_to_dict, snapshots)
        self.printer.print(d)


T = TypeVar('T')


class GenericView(Generic[T]):

    def __init__(self, printer: Printer) -> None:
        self.printer = printer

    def print(self, data: Union[T, Iterable[T]], map_func: Callable[[T], Dict[str, Any]]) -> None:
        if not isinstance(data, Iterable):
            data = [data]
        self.printer.print(map(map_func, data))
