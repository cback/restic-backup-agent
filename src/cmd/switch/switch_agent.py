# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from typing import Optional

from cmd.utils import GlobalOptions, pass_global_options, MutexOption
from model.cback import CBack


@click.command('agent', short_help='spawn a switch agent')
@click.option('--enable', is_flag=True, cls=MutexOption, not_required_if=['disable'], help='enable the agent')
@click.option('--disable', cls=MutexOption, not_required_if=['enable'], help='disable the agent (takes a reason, as an argument)')
@pass_global_options
def switch_agent(global_options: GlobalOptions, enable: bool, disable: Optional[str]) -> None:

    config: str = global_options.get('config')

    cback = CBack(config)
    switch_agent = cback.get_switch_agent()

    if not enable and disable is None:
        # start the agent
        switch_agent.loop()
        return

    if enable:
        switch_agent.enable()
    if disable:
        switch_agent.disable(disable)
