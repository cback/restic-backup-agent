# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from multiprocessing import Process
import os
import signal
from time import sleep
from typing import Any
from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.restic.restic import Restic, ResticMount
from model.agents.utils import get_credentials
import click
import daemon
import sys
from daemon import pidfile


@click.command('mount', short_help='Mount a restic repository')
@click.argument('id', type=int)
@click.argument('mount-dir')
@click.option('--pid-file', '-p', help='file where to store the pid of the daemon running the restic mount', default='/var/run/cback.pid')
@pass_global_options
def backup_mount(global_options: GlobalOptions, id: int, mount_dir: str, pid_file: str) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    shared_config = cback.get_shared_config()
    backup_mgr = cback.get_backup_manager()

    backup = backup_mgr.get_by_id(id)
    try:
        shared_group_config = shared_config[backup.group.name]
    except KeyError:
        print(f"Config for group {backup.group.name} not found")
        return
    password = get_credentials(shared_group_config, backup.repository)

    cache_dir = os.path.join(shared_group_config.cache_dir, backup.name)

    restic = Restic(backup.repository.path, password,
                    env=shared_group_config.destination_env, cache_dir=cache_dir)

    mount = restic.mount(mount_dir, debug=True)

    # register signal to be informed when the repo is mounted
    def exit_process(signal: Any, frame: Any) -> None:
        sys.exit(0)
    signal.signal(signal.SIGUSR1, exit_process)

    p = Process(target=spawn_restic_mount, args=(pid_file, mount, id))
    p.start()

    p.join()

    # wait until signal
    while True:
        sleep(0.1)


def spawn_restic_mount(pid_file: str, mount: ResticMount, id: int) -> None:

    ppid = os.getppid()

    # workaround: https://pagure.io/python-daemon/issue/45
    if sys.stdin and sys.__stdin__ and sys.__stdin__.closed:
        sys.__stdin__ = sys.stdin  # type: ignore

    pid_file = pidfile.PIDLockFile(pid_file)

    keep_mounted = True

    def cleanup(signal: Any, frame: Any) -> None:
        nonlocal keep_mounted
        keep_mounted = False

    file_log = f'/var/log/cback/cback-restic-mount-{id}.log'
    with open(file_log, 'a') as f:

        with daemon.DaemonContext(
                signal_map={
                    signal.SIGTERM: cleanup,
                    signal.SIGHUP: cleanup,
                    signal.SIGINT: cleanup,
                },
                pidfile=pid_file,
                stderr=f,
                stdout=f
        ):

            with mount:
                # inform ppid that the repo as been mounted
                os.kill(ppid, signal.SIGUSR1)

                # wait
                while keep_mounted:
                    sleep(0.1)

            sys.exit(0)
