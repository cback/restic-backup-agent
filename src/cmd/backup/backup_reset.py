# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.jobs.backup import Backup
from views import get_job_view


@click.command('reset', short_help='reset a backup job')
@click.argument('id', type=int)
@pass_global_options
def backup_reset(global_options: GlobalOptions, id: int) -> None:

    config = global_options.get('config')

    cback = CBack(config)

    backup_mgr = cback.get_backup_manager()
    view = get_job_view(Backup)

    backup = backup_mgr.reset(id)
    view.print(backup)
