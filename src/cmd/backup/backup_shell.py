# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional
from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.restic.restic import Restic
from model.agents.utils import get_credentials, get_username
import click


@click.command('shell', short_help='Spawn a shell with an env ready for using restic')
@click.argument('id', type=int)
@click.option('--mount', '-m', is_flag=True, help='whether mount the restic repository')
@click.option('--mount-dir', '-d', help='where mount the folder', show_default='tmp dir')
@click.option('--impersonate', '-i', is_flag=True, help='whether impersonate the user in the backup (useful to access the backup source)')
@pass_global_options
def backup_shell(global_options: GlobalOptions, id: int, mount: bool, mount_dir: Optional[str], impersonate: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    shared_config = cback.get_shared_config()
    backup_mgr = cback.get_backup_manager()

    backup = backup_mgr.get_by_id(id)
    try:
        shared_group_config = shared_config[backup.group.name]
    except KeyError:
        print(f"Config for group {backup.group.name} not found")
        return
    password = get_credentials(shared_group_config, backup.repository)
    username = get_username(shared_group_config,
                            backup) if impersonate else None
    restic = Restic(backup.repository.path, password,
                    env=shared_group_config.destination_env)

    restic.shell(mount=mount, mount_dir=mount_dir, username=username)
