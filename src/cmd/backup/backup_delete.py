# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click

from cmd.utils import GlobalOptions, pass_global_options, prompt
from model.cback import CBack
from model.jobs.backup import Backup
from views import get_job_view


@click.command('delete', short_help='delete a backup job')
@click.argument('id', type=int)
@click.option('--force', '-f', is_flag=True, help='force the deletion')
@pass_global_options
def backup_delete(global_options: GlobalOptions, id: int, force: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)

    backup_mgr = cback.get_backup_manager()

    backup = backup_mgr.get_by_id(id)

    view = get_job_view(Backup)
    view.print(backup)

    if force:
        backup_mgr.delete(id, force, delete_job=True, delete_repository=True)
        return

    # ask some info to the user
    print("No snapshots will be erased at this step")
    delete_repository = prompt(
        f"Do you want to delete the repository \"{backup.repository}\"?")
    delete_job = prompt(f'The repository will{"" if delete_repository else " not"} be deleted.\n' +
                        f'Delete {"also" if delete_repository else "only"} the backup job in the database?')

    if not delete_repository and not delete_job:
        print("Nothing to do")
        return

    print("\nSUMMARY:")
    print(
        f"delete \"{backup.repository}\": {'yes' if delete_repository else 'no'}")
    print(f"delete backup job: {'yes' if delete_job else 'no'}")

    if prompt("Do you want to continue?"):
        backup_mgr.delete(id, force, delete_job=delete_job,
                          delete_repository=delete_repository)
