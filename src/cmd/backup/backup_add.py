# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional
import click

from model.cback import CBack
from cmd.utils import GlobalOptions, pass_global_options
from model.jobs.backup import Backup
from views import get_job_view

@click.command('add', short_help='add a backup job')
@click.argument('name')
@click.argument('source')
@click.option('--instance', '-i', default='default', help='instance the source belongs')
@click.option('--username', '-u', help='owner of the source')
@click.option('--repository', '-r', help='location of the restic repository (override the default one)')
@click.option('--group', '-g', required=True, help='group in which backup will be')
@click.option('--enabled', '-e', is_flag=True, help='whether the backup is enabled')
@click.option('--force', '-f', is_flag=True, help='whether the backup is force to run (no checks on last snapshot will be done)')
@click.option('--pinned-node', '-n', help='force a backup to run on a specific node')
@click.option('--no-workload', '-w', is_flag=True, help='create a backup job without the rest of its workload (no prune or verify job will be created)')
@pass_global_options
def backup_add(global_options: GlobalOptions, name: str, username: Optional[str], source: str, instance: str, group: str, enabled: bool, force: bool, pinned_node: Optional[str], repository: Optional[str], no_workload: bool) -> None:

    config: str = global_options.get('config')

    cback = CBack(config)
    backup_mgr = cback.get_backup_manager()
    prune_mgr = cback.get_prune_manager()
    verify_mgr = cback.get_verify_manager()

    backup = backup_mgr.add(group=group, name=name, username=username, source=source, enabled=enabled,
                            instance=instance, force=force, pinned_node=pinned_node, repository=repository)

    if not no_workload:
        # add the corresponding prune job
        prune_mgr.add(backup_id=backup.id, enabled=enabled)

        # add the corresponding verify job
        verify_mgr.add(backup_id=backup.id, enabled=enabled)

    view = get_job_view(Backup)
    view.print(backup)

