# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional
import click

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.jobs.backup import Backup
from model.jobs.status import Status
from model.managers.jobs.job_manager import NoJobsError
from views import get_job_view


@click.command('ls', short_help='list the backup jobs')
@click.argument('status', required=False, type=click.Choice(['failed', 'running', 'pending', 'completed', 'archived', 'enabled', 'disabled']))
@click.argument('enabled', required=False, type=click.Choice(['enabled', 'disabled']))
@click.option('--group', '-g', 'group_name', help='filter by group')
@click.option('--long', '-l', is_flag=True, help='print in long mode')
@click.option('--json', is_flag=True, help='format the output in json')
@pass_global_options
def backup_ls(global_options: GlobalOptions, status: Optional[str], enabled: Optional[str], group_name: Optional[str], long: bool, json: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    group_mgr = cback.get_group_manager()

    # get group
    group = group_mgr.get_by_name(group_name) if group_name else None

    enab = None
    if status == 'enabled' or enabled == 'enabled':
        enab = True
    elif status == 'disabled' or enabled == 'disabled':
        enab = False

    try:
        st = Status.from_str(status) if status else None
    except ValueError:  # we can pass as status enabled or disabled
        st = None

    backup_mgr = cback.get_backup_manager()
    try:
        backup_lst = backup_mgr.get_jobs(group, st, enab)
    except NoJobsError:
        print(
            f'No {enabled + " " if enabled else ""}{status + " " if status else ""}backup jobs')
        return

    view = get_job_view(Backup, "json" if json else "grid_table", long=long)
    view.print(backup_lst)
