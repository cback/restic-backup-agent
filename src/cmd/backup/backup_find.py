# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from typing import List
from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.restic.restic import Matches, Restic, Snapshot
from model.agents.utils import get_credentials


@click.command('find', short_help='searches for files or directories in snapshots stored in the repo')
@click.argument('id', type=int)
@click.argument('pattern')
@click.option('--snapshot', '-s', multiple=True, help='snapshot in which find the pattern')
@pass_global_options
def backup_find(global_options: GlobalOptions, id: int, pattern: str, snapshot: List[str]) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    shared_config = cback.get_shared_config()
    backup_mgr = cback.get_backup_manager()

    backup = backup_mgr.get_by_id(id)
    try:
        shared_group_config = shared_config[backup.group.name]
    except KeyError:
        print(f"Config for group {backup.group.name} not found")
        return
    password = get_credentials(shared_group_config, backup.repository)
    restic = Restic(backup.repository.path, password,
                    env=shared_group_config.destination_env)

    all_matches = restic.find(pattern, snapshot=snapshot)
    snapshots = restic.snapshots()

    _print_matches(all_matches, snapshots)


def _print_matches(lst: List[Matches], snapshots: List[Snapshot]) -> None:
    for matches in lst:
        # TODO: print time associated to snapshot
        snapshot = _get_snapshot(snapshots, matches.snapshot_id)
        print(
            f'Found matching entries in snapshot {matches.snapshot_id[:8]} from {snapshot.time.strftime("%Y-%m-%d %H:%M:%S")}')
        for entry in matches.matches:
            print(entry.path, entry.mtime.strftime(
                "%Y-%m-%d %H:%M:%S"), entry.size)
        print()


def _get_snapshot(snapshots: List[Snapshot], id: str) -> Snapshot:
    for snap in snapshots:
        if snap.id.startswith(id):
            return snap
    raise Exception(f"snapshot {id} not found")
