# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from typing import Optional

from cmd.utils import GlobalOptions, MutexArgument, MutexOption, pass_global_options
from model.cback import CBack
from model.jobs.backup import Backup
from views import get_job_view


@click.command('disable', short_help='disable a backup job')
@click.argument('id', cls=MutexArgument, not_required_if=['instance'], type=int)
@click.option('--instance', '-i', cls=MutexOption, not_required_if=['id'])
@pass_global_options
def backup_disable(global_options: GlobalOptions, id: Optional[int], instance: Optional[str]) -> None:

    config = global_options.get('config')

    cback = CBack(config)

    backup_mgr = cback.get_backup_manager()
    view = get_job_view(Backup)

    if id is not None:
        backup = backup_mgr.disable_by_id(id)
        view.print(backup)
    elif instance is not None:
        lst = backup_mgr.disable_by_instance(instance)
        view.print(lst)
