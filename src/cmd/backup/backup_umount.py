# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import os
import signal
import click
from time import sleep


@click.command('umount', short_help='Umount the restic repository')
@click.option('--pid-file', '-p', help='file where to store the pid of the daemon running the restic mount', show_default='tmp dir')
def backup_umount(pid_file: str) -> None:

    pid = read_pid_from_file(pid_file)
    os.kill(pid, signal.SIGTERM)

    # wait the process
    while is_running(pid):
        sleep(0.10)


def read_pid_from_file(file: str) -> int:
    with open(file, 'r') as f:
        return int(f.read())


def is_running(pid: int) -> bool:
    try:
        # the signal 0 is used to check if the process is still running
        # will not kil the process
        os.kill(pid, 0)
    except OSError:
        return False
    return True
