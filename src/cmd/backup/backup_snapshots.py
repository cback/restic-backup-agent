# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from views import get_snapshots_view


@click.command('snapshots', short_help='get all the snapshots')
@click.argument('id', type=int)
@click.option('--all', '-a', is_flag=True, help='whether show all the snapshots in the repository')
@click.option('--json', is_flag=True, help='format the output in json')
@click.option('--long', '-l', is_flag=True, help='print in long mode')
@pass_global_options
def backup_snapshots(global_options: GlobalOptions, id: int, all: bool, long: bool, json: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)

    backup_mgr = cback.get_backup_manager()

    snapshots = backup_mgr.snapshots(id)
    if snapshots:
        view = get_snapshots_view("json" if json else "table", long=long)
        view.print(snapshots)
    else:
        print(f"No snapshots in backup with id {id}")
