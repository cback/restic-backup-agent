# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from cmd.backup.backup_add import backup_add
from cmd.backup.backup_ls import backup_ls
from cmd.backup.backup_status import backup_status
from cmd.backup.backup_archive import backup_archive
from cmd.backup.backup_agent import backup_agent
from cmd.backup.backup_delete import backup_delete
from cmd.backup.backup_enable import backup_enable
from cmd.backup.backup_disable import backup_disable
from cmd.backup.backup_find import backup_find
from cmd.backup.backup_kill import backup_kill
from cmd.backup.backup_modify import backup_modify
from cmd.backup.backup_reset import backup_reset
from cmd.backup.backup_run import backup_run
from cmd.backup.backup_snapshots import backup_snapshots
from cmd.backup.backup_shell import backup_shell
from cmd.backup.backup_mount import backup_mount
from cmd.backup.backup_umount import backup_umount


@click.group()
def backup() -> None:
    pass


# list of sub-command of backup
backup.add_command(backup_add)
backup.add_command(backup_ls)
backup.add_command(backup_status)
backup.add_command(backup_archive)
backup.add_command(backup_agent)
backup.add_command(backup_delete)
backup.add_command(backup_enable)
backup.add_command(backup_disable)
backup.add_command(backup_find)
backup.add_command(backup_kill)
backup.add_command(backup_modify)
backup.add_command(backup_reset)
backup.add_command(backup_run)
backup.add_command(backup_snapshots)
backup.add_command(backup_shell)
backup.add_command(backup_mount)
backup.add_command(backup_umount)
