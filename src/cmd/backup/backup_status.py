# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from typing import Iterable, Optional

from cmd.utils import GlobalOptions, pass_global_options
from model.jobs.status import Status
from model.cback import CBack
from model.managers.jobs.job_manager import NoJobsError
from views import get_job_view, print_status_box

from model.jobs.backup import Backup


@click.command('status', short_help='show the status of backup jobs')
@click.argument('name', required=False)
@click.option('--group', '-g', help='get status of a specified group')
@click.option('--long', '-l', is_flag=True, help='print in long mode')
@click.option('--json', is_flag=True, help='format the output in json')
@pass_global_options
def backup_status(global_options: GlobalOptions, name: Optional[str], group: Optional[str], long: bool, json: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    backup_mgr = cback.get_backup_manager()

    if name:
        try:
            backup: Iterable['Backup'] = [backup_mgr.get_by_id(int(name))]
        except ValueError:  # backup does not exist
            try:
                backup = backup_mgr.get_by_name(name)
            except NoJobsError:
                backup = []
        finally:

            if backup:
                view = get_job_view(
                    Backup, "json" if json else "grid_table", long=long)
                view.print(backup)
            else:
                print(f'No backup jobs found with id/name {name}')
            return

    g = None
    if group:
        group_mgr = cback.get_group_manager()
        g = group_mgr.get_by_name(group)

    summary = backup_mgr.status(g)
    print('\n Backup status summary\n')
    print_status_box(summary)

    # print list of running backups
    try:
        backup_lst = backup_mgr.get_jobs(
            g, status=Status.RUNNING, order_by='started')
    except NoJobsError:
        print('\n No running backup jobs')
        return

    print('\n Running backup jobs\n')
    view = get_job_view(Backup, mode="simple_table", long=long, custom_fields=[
                        "id", "group", "name", "source", "status", "worker", "started"])
    view.print(backup_lst)
