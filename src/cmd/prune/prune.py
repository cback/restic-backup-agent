# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from cmd.prune.prune_add import prune_add
from cmd.prune.prune_enable import prune_enable
from cmd.prune.prune_kill import prune_kill
from cmd.prune.prune_reset import prune_reset
from cmd.prune.prune_status import prune_status
from cmd.prune.prune_disable import prune_disable
from cmd.prune.prune_ls import prune_ls
from cmd.prune.prune_run import prune_run
from cmd.prune.prune_agent import prune_agent


@click.group()
def prune() -> None:
    pass


# list of subcommands of prune
prune.add_command(prune_add)
prune.add_command(prune_status)
prune.add_command(prune_enable)
prune.add_command(prune_disable)
prune.add_command(prune_reset)
prune.add_command(prune_kill)
prune.add_command(prune_ls)
prune.add_command(prune_run)
prune.add_command(prune_agent)
