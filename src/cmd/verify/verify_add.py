# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.jobs.verify import Verify
from views import get_job_view


@click.command('add', short_help='add a verify job')
@click.argument('backup_id')
@click.option('--enable', '-e', is_flag=True, help='whether the verify is enabled')
@pass_global_options
def verify_add(global_options: GlobalOptions, backup_id: int, enable: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    verify_mgr = cback.get_verify_manager()

    verify = verify_mgr.add(backup_id=backup_id, enabled=enable)

    view = get_job_view(Verify)
    view.print(verify)
