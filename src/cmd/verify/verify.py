# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from cmd.verify.verify_add import verify_add
from cmd.verify.verify_enable import verify_enable
from cmd.verify.verify_kill import verify_kill
from cmd.verify.verify_reset import verify_reset
from cmd.verify.verify_status import verify_status
from cmd.verify.verify_disable import verify_disable
from cmd.verify.verify_ls import verify_ls
from cmd.verify.verify_run import verify_run
from cmd.verify.verify_agent import verify_agent


@click.group()
def verify() -> None:
    pass


# list of subcommands of verify
verify.add_command(verify_add)
verify.add_command(verify_status)
verify.add_command(verify_enable)
verify.add_command(verify_disable)
verify.add_command(verify_reset)
verify.add_command(verify_kill)
verify.add_command(verify_ls)
verify.add_command(verify_run)
verify.add_command(verify_agent)
