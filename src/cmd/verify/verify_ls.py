# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional
import click

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.jobs.status import Status
from model.jobs.verify import Verify
from model.managers.jobs.job_manager import NoJobsError
from views import get_job_view


@click.command('ls', short_help='list the verify jobs')
@click.argument('status', required=False, type=click.Choice(['failed', 'running', 'pending', 'completed', 'enabled', 'disabled']))
@click.argument('enabled', required=False, type=click.Choice(['enabled', 'disabled']))
@click.option('--group', '-g', 'group_name', help='filter by group')
@click.option('--json', is_flag=True, help='format the output in json')
@click.option('--long', '-l', is_flag=True, help='print in long mode')
@pass_global_options
def verify_ls(global_options: GlobalOptions, status: Optional[str], enabled: Optional[str], group_name: Optional[str], long: bool, json: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    verify_mgr = cback.get_verify_manager()
    view = get_job_view(Verify, "json" if json else "grid_table", long=long)

    # get group
    group = None
    if group_name:
        group_mgr = cback.get_group_manager()
        group = group_mgr.get_by_name(group_name)

    enab = None
    if status == 'enabled' or enabled == 'enabled':
        enab = True
    elif status == 'disabled' or enabled == 'disabled':
        enab = False

    try:
        st = Status.from_str(status) if status else None
    except ValueError:
        st = None

    try:
        verify_lst = verify_mgr.get_jobs(group, st, enab)
    except NoJobsError:
        print(
            f'No {enabled + " " if enabled else ""}{status + " " if status else ""}verify jobs')
        return
    view.print(verify_lst)
