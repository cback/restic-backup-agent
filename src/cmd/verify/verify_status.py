# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional, Union
import click

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.jobs.status import Status
from model.jobs.verify import Verify
from model.managers.jobs.job_manager import NoJobsError
from views import get_job_view, print_status_box


@click.command('status', short_help='show the status of verify jobs')
@click.argument('name', required=False)
@click.option('--group', '-g', help='get status of a specified group')
@click.option('--json', is_flag=True, help='format the output in json')
@click.option('--long', '-l', is_flag=True, help='print in long mode')
@pass_global_options
def verify_status(global_options: GlobalOptions, name: Optional[Union[str, int]], group: Optional[str], long: bool, json: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    verify_mgr = cback.get_verify_manager()

    if not name:

        g = None
        if group:
            group_mgr = cback.get_group_manager()
            g = group_mgr.get_by_name(group)

        summary = verify_mgr.status(g)
        print('\n Verify status summary\n')
        print_status_box(summary)

        # print list of running verify
        verify_lst = verify_mgr.get_jobs(
            g, status=Status.RUNNING, order_by='started')
        if verify_lst:
            print('\n Running verify jobs\n')
            view = get_job_view(Verify, "simple_table", custom_fields=[
                                "id", "group", "name", "source", "status", "worker", "started"])
            view.print(verify_lst)
        else:
            print('\nNo running verify jobs')

        return

    # try to get the verify by id
    try:
        res = [verify_mgr.get_by_id(int(name))]
    except:

        # the name provided could be the name of the backup, so we try to get all the backups
        # given the name, and see if some restores job that reference those backup exist

        res = []

        backup_mgr = cback.get_backup_manager()
        try:
            backup_lst = backup_mgr.get_by_name(str(name))
        except NoJobsError:
            pass
        for b in backup_lst:
            try:
                res += verify_mgr.get_by_backup_id(b.id)
            except:
                continue
    finally:
        if res:
            view = get_job_view(
                Verify, "json" if json else "grid_table", long=long)
            view.print(res)
        else:
            print(f'No verify jobs found with id/name {name}')
