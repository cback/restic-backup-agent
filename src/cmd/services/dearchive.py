# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict
import click
from dacite import from_dict
from dataclasses import dataclass

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.jobs.status import Status
from model.managers.jobs.job_manager import NoJobsError
from portal.user.errors import UserNotFoundError
from portal.user.manager import UserManager
from model.config.logs import get_logger


@dataclass(frozen=True)
class Config:
    user: Dict[str, Any]


@click.command('dearchive', short_help='resets archived backup jobs whose users have been re-enabled')
@pass_global_options
def dearchive(global_options: GlobalOptions) -> None:

    config = global_options.get('config')
    cback = CBack(config)

    log = get_logger()

    service_config = from_dict(
        data_class=Config, data=cback.get_service_config("dearchive"))

    user_mgr = UserManager(service_config.user)

    backup_mgr = cback.get_backup_manager()

    try:
        archived = backup_mgr.get_jobs(status=Status.ARCHIVED)
    except NoJobsError:
        return

    for backup in archived:
        if not backup.username:
            continue

        try:
            user = user_mgr.get_user(backup.username, skip_groups=True)
        except UserNotFoundError:
            log.debug('user not found', extra={
                      'user': backup.username, 'backup_id': backup.id})
            continue
        except KeyboardInterrupt:
            return
        except:
            log.exception('raised exception', extra={
                          'user': backup.username, 'backup_id': backup.id})
        else:
            if not user.active:
                log.info('user found, but no active', extra={
                    'user': backup.username, 'backup_id': backup.id})
                continue
            log.info('user found, re-enabling the backup', extra={
                'user': backup.username, 'backup_id': backup.id})
            backup_mgr.reset(backup.id)
