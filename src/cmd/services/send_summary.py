# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict
from attr import frozen
import click
from dacite import from_dict
from dataclasses import dataclass
from model.agents.agent import SharedAgentConfig

from cmd.utils import GlobalOptions, pass_global_options
from model.cback import CBack
from model.jobs.group import Group
from model.managers.jobs.status_strategies import TotalStatus, PendingStatus, RunningStatus, CompletedStatus, FailedStatus, CoverageStatus, NotEnabledStatus, ArchivedStatus


@dataclass(frozen=True)
class Config:
    template: str = 'cback.{{ group }}.summary.{{ job }}.{{ metric_name }}'


@click.command('send-summary', short_help='send jobs summary')
@pass_global_options
def send_summary(global_options: GlobalOptions) -> None:

    config = global_options.get('config')
    cback = CBack(config)
    shared_config = cback.get_shared_config()

    service_config = from_dict(
        data_class=Config, data=cback.get_service_config("send_summary"))

    monitoring = cback.get_metrics_sender()
    groups = cback.get_group_manager().list_groups()

    for group in groups:
        try:
            group_config = shared_config[group.name]
        except KeyError:
            # skip if configuration for group not found
            continue

        b = get_backup_metrics(cback, group, group_config)
        p = get_prune_metrics(cback, group, group_config)
        r = get_restore_metrics(cback, group, group_config)
        v = get_verify_metrics(cback, group, group_config)

        monitoring.publish_metrics(
            b, service_config.template, group=group.name, job="backup")
        monitoring.publish_metrics(
            p, service_config.template, group=group.name, job="prune")
        monitoring.publish_metrics(
            r, service_config.template, group=group.name, job="restore")
        monitoring.publish_metrics(
            v, service_config.template, group=group.name, job="verify")


def get_backup_metrics(cback: CBack, group: Group, group_config: SharedAgentConfig) -> Dict[str, Any]:
    backup_repo = cback.get_repository_factory().get_backup_repository()
    total = TotalStatus(backup_repo).compute(group)
    pending = PendingStatus(backup_repo).compute(group)
    running = RunningStatus(backup_repo).compute(group)
    completed = CompletedStatus(backup_repo).compute(group)
    failed = FailedStatus(backup_repo).compute(group)
    archived = ArchivedStatus(backup_repo).compute(group)
    disabled = NotEnabledStatus(backup_repo).compute(group)
    coverage = CoverageStatus(
        backup_repo, group_config.backup_expiration_time).compute(group)

    return {
        'total': total,
        'pending': pending,
        'running': running,
        'completed': completed,
        'archived': archived,
        'failed': failed,
        'disabled': disabled,
        'coverage': coverage
    }


def get_prune_metrics(cback: CBack, group: Group, group_config: SharedAgentConfig) -> Dict[str, Any]:
    prune_repo = cback.get_repository_factory().get_prune_repository()
    pending = PendingStatus(prune_repo).compute(group)
    running = RunningStatus(prune_repo).compute(group)
    completed = CompletedStatus(prune_repo).compute(group)
    failed = FailedStatus(prune_repo).compute(group)
    disabled = NotEnabledStatus(prune_repo).compute(group)
    coverage = CoverageStatus(
        prune_repo, group_config.prune_expiration_time).compute(group)

    return {
        'pending': pending,
        'running': running,
        'completed': completed,
        'failed': failed,
        'disabled': disabled,
        'coverage': coverage
    }


def get_restore_metrics(cback: CBack, group: Group, _: SharedAgentConfig) -> Dict[str, Any]:
    restore_repo = cback.get_repository_factory().get_restore_repository()
    pending = PendingStatus(restore_repo).compute(group)
    running = RunningStatus(restore_repo).compute(group)
    completed = CompletedStatus(restore_repo).compute(group)
    failed = FailedStatus(restore_repo).compute(group)
    disabled = NotEnabledStatus(restore_repo).compute(group)
    coverage = CoverageStatus(restore_repo, None).compute(group)

    return {
        'pending': pending,
        'running': running,
        'completed': completed,
        'failed': failed,
        'disabled': disabled,
        'coverage': coverage
    }


def get_verify_metrics(cback: CBack, group: Group, group_config: SharedAgentConfig) -> Dict[str, Any]:
    verify_repo = cback.get_repository_factory().get_verify_repository()
    pending = PendingStatus(verify_repo).compute(group)
    running = RunningStatus(verify_repo).compute(group)
    completed = CompletedStatus(verify_repo).compute(group)
    failed = FailedStatus(verify_repo).compute(group)
    disabled = NotEnabledStatus(verify_repo).compute(group)
    coverage = CoverageStatus(
        verify_repo, group_config.verify_expiration_time).compute(group)

    return {
        'pending': pending,
        'running': running,
        'completed': completed,
        'failed': failed,
        'disabled': disabled,
        'coverage': coverage
    }
