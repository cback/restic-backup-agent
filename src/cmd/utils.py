# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, List, Mapping, Tuple, Dict, cast
import click
from click.core import Context


class GlobalOptions(object):
    __slots__ = ['opts']

    def __init__(self) -> None:
        self.opts = cast(Dict[str, Any], {})

    def set(self, key: str, value: Any) -> None:
        self.opts[key] = value

    def get(self, key: str) -> Any:
        return self.opts[key]

    def __repr__(self) -> str:
        return self.opts.__repr__()


pass_global_options = click.make_pass_decorator(GlobalOptions)


class MutexArgument(click.Argument):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        self.not_required_if: List = kwargs.pop('not_required_if')
        self.required_if_missing: bool = kwargs.pop(
            'required_if_missing', False)

        assert self.not_required_if, "'not_required_if' parameter required"
        kwargs['required'] = False
        super().__init__(*args, **kwargs)

    def handle_parse_result(self, ctx: Context, opts: Mapping[str, Any], args: List[str]) -> Tuple[Any, List[str]]:
        # the argument is required if all the not_required_id arguments/options are not specified
        assert isinstance(self.name, str)
        if self.required_if_missing and (self.name not in opts or opts[self.name] is None) and not _all_provided(self.not_required_if, opts):
            raise click.UsageError(f'"{self.name}" required')

        if self.name in opts and opts[self.name] is not None:
            for mutex in self.not_required_if:
                if mutex in opts:
                    raise click.UsageError(
                        f'"{self.name}" is mutually exclusive with "{mutex}"')
        return super().handle_parse_result(ctx, opts, args)


def _all_provided(lst: List[str], opts: Mapping[str, Any]) -> bool:
    for e in lst:
        if e not in opts or opts[e] is None:
            return False
    return True


class MutexOption(click.Option):
    def __init__(self, *args: Any, **kwargs: Any):
        self.not_required_if: List[str] = kwargs.pop('not_required_if')
        self.required_if_missing: bool = kwargs.pop(
            'required_if_missing', False)

        assert self.not_required_if, "'not_required_if' parameter required"
        kwargs['help'] = kwargs.get(
            'help', '') + '. NOTE: Option mutually exclusive with ' + ', '.join(self.not_required_if)
        super().__init__(*args, **kwargs)

    def handle_parse_result(self, ctx: Context, opts: Mapping[str, Any], args: List[str]) -> Tuple[Any, List[str]]:
        # the option is required if all the not_required_id arguments/options are not specified
        if self.required_if_missing and self.name not in opts and not _all_provided(self.not_required_if, opts):
            raise click.UsageError(f'"{self.name}" required')

        for mutex in self.not_required_if:
            if mutex in opts:
                if self.name in opts and opts[mutex] is not None:
                    raise click.UsageError(
                        f'"{self.name}" is mutually exclusive with "{mutex}"')
        return super().handle_parse_result(ctx, opts, args)


def prompt(message: str) -> bool:
    while True:
        i = input(f"{message} [y/n]: ")
        if i in ['y', 'Y']:
            return True
        if i in ['n', 'N']:
            return False
