# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from cmd.restore.restore_add import restore_add
from cmd.restore.restore_ls import restore_ls
from cmd.restore.restore_status import restore_status
from cmd.restore.restore_enable import restore_enable
from cmd.restore.restore_disable import restore_disable
from cmd.restore.restore_delete import restore_delete
from cmd.restore.restore_reset import restore_reset
from cmd.restore.restore_agent import restore_agent
from cmd.restore.restore_run import restore_run


@click.group()
def restore() -> None:
    pass


restore.add_command(restore_add)
restore.add_command(restore_ls)
restore.add_command(restore_status)
restore.add_command(restore_enable)
restore.add_command(restore_disable)
restore.add_command(restore_delete)
restore.add_command(restore_reset)
restore.add_command(restore_agent)
restore.add_command(restore_run)
