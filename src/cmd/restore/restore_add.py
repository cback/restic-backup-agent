# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from typing import Optional

from cmd.utils import GlobalOptions, pass_global_options, MutexOption
from model.cback import CBack
from model.jobs.restore import Restore
from views import get_job_view


@click.command('add', short_help='add a new restore job')
@click.argument('backup_id')
@click.option('--pattern', '-p', required=True, help='a pattern specifying what restore')
@click.option('--snapshot', '-s', cls=MutexOption, not_required_if=['date'], help='from which snapshot restore the files')
@click.option('--date', '-t', cls=MutexOption, not_required_if=['snapshot'], help='from which date restore the file')
@click.option('--dest', '-d', help='where restore the files', show_default='<backup source>/__restore_<snapshot date>__')
@click.option('--enable', '-e', is_flag=True, help='whether the backup is enabled')
@pass_global_options
def restore_add(global_options: GlobalOptions, backup_id: int, pattern: str, snapshot: Optional[str], date: Optional[str], dest: Optional[str], enable: bool) -> None:

    config = global_options.get('config')

    cback = CBack(config)
    restore_mgr = cback.get_restore_manager()

    restore = restore_mgr.add(backup_id=backup_id, pattern=pattern,
                              snapshot=snapshot, date=date, destination=dest, enabled=enable)

    view = get_job_view(Restore)
    view.print(restore)
