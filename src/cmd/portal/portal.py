# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click
from cmd.portal.portal_run import portal_run
from cmd.portal.portal_mounter import portal_mounter
from cmd.portal.token import token as portal_token
from cmd.portal.scope import scope as portal_scope


@click.group()
def portal() -> None:
    pass


# list of subcommands of portal
portal.add_command(portal_run)
portal.add_command(portal_token)
portal.add_command(portal_mounter)
portal.add_command(portal_scope)
