# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import Any, Dict, List, Optional
import click
import secrets
from model.cback import CBack
from cmd.utils import GlobalOptions, pass_global_options
from portal.auth.providers.impersonator import TokenDB
from views import print_tokens


@click.command('new', short_help='generate a new token for access')
@click.option('--group', '-g', multiple=True, required=True)
@click.option('--comment', '-c')
@click.option('--expiration', '-e', type=click.DateTime(formats=['%Y-%m-%d']))
@pass_global_options
def token_new(global_options: GlobalOptions, group: List[str], comment: Optional[str], expiration: datetime) -> None:

    config = global_options.get('config')
    # FIXME: move this logic outside
    cback = CBack(config)
    config_token: Dict[str, Any] = cback.config.get('portal', {}).get(
        'auth', {}).get('providers', {}).get('impersonator', {})

    mgr = TokenDB(config_token)

    # generate a super strong secret
    secret = secrets.token_hex(32)
    token = mgr.add_token(secret, expiration, group, comment)
    token.hashed_token = secret
    print_tokens(token)
