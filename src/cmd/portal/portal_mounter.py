# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import click

import uvicorn


@click.command('mounter', short_help='run the mounter service')
@click.option('--debug', '-d', is_flag=True, help='run the mounter in debug mode')
@click.option('--port', '-p', default=8000, help='port where expose the mounter')
@click.option('--reload', '-r', is_flag=True, help='reload on code updates (do not use in production!)')
def portal_mounter(port: int, debug: bool = False, reload: bool = False) -> None:

    uvicorn.run("model.mounter.server.server:app", debug=debug,
                reload=reload, workers=1, host="0.0.0.0", port=port)
