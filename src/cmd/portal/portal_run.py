# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional

import click
import uvicorn


@click.command('run', short_help='run the portal')
@click.option('--debug', '-d', is_flag=True, help='run the portal in debug mode')
@click.option('--ssl_certfile', '-c', help='certificate file')
@click.option('--ssl_keyfile', '-k', help='key file')
@click.option('--reload', '-r', is_flag=True, help='reload on code updates (do not use in production!)')
def portal_run(debug: bool = False, reload: bool = False, ssl_certfile: Optional[str] = None, ssl_keyfile: Optional[str] = None) -> None:

    uvicorn.run("portal.portal:app", debug=debug,
                reload=reload, workers=2, host="0.0.0.0",
                ssl_keyfile=ssl_keyfile, ssl_certfile=ssl_certfile)
