# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from pydantic import parse_raw_as
from typing import Any, Dict
import click
from model.cback import CBack
from cmd.utils import GlobalOptions, pass_global_options
from portal.scope.providers.manual import ManualScopeProvider
from portal.scope.scope import Scope


@click.command('new', short_help='create a new user defined scope')
@click.option('--user', '-u', required=True)
@click.option('--scope', '-s', required=True)
@pass_global_options
def scope_new(global_options: GlobalOptions, user: str, scope: str) -> None:

    config = global_options.get('config')
    # FIXME: move this logic outside
    cback = CBack(config)
    config_scope: Dict[str, Any] = cback.config.get('portal', {}).get(
        'auth', {}).get('scope', {}).get('drivers', {}).get('manual', {})

    mgr = ManualScopeProvider(config_scope)

    s = parse_raw_as(Scope, scope)
    mgr.add_scope(s, user)
