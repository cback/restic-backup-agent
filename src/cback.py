#!/usr/bin/python3
# CERNBack Command Line Interface
# rvalverd@cern.ch, gdelmont@cern.ch
# -*- coding: utf8 -*-

# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

import click
from click.core import Context
from typing import Any
from cmd.portal.portal import portal
from cmd.utils import GlobalOptions
from cmd.backup.backup import backup
from cmd.restore.restore import restore
from cmd.prune.prune import prune
from cmd.switch.switch import switch
from cmd.group.group import group
from cmd.verify.verify import verify
from cmd.services.services import services
from platform import python_version
from model.restic.restic import Restic, ResticError

VERSION = 'git'
BUILD_DATE = '<unknown>'
GIT_COMMIT = '<unknown>'


def restic_version() -> str:
    try:
        return ".".join(map(str, Restic.version()))
    except ResticError:
        return 'bin not found'


msg_version = f"""%(prog)s [%(version)s]
  python version = {python_version()}
  restic version = {restic_version()}
  build date = {BUILD_DATE}
  commit = {GIT_COMMIT}"""


@click.group()
@click.option('--config', default='/etc/cback/config.yaml', help='config file', show_default=True)
@click.version_option(version=VERSION, message=msg_version)
@click.pass_context
def cback_cli(ctx: Context, **kwargs: Any) -> None:
    global_opts = GlobalOptions()
    for key, value in kwargs.items():
        global_opts.set(key, value)
    ctx.obj = global_opts


cback_cli.add_command(backup)
cback_cli.add_command(group)
cback_cli.add_command(restore)
cback_cli.add_command(prune)
cback_cli.add_command(switch)
cback_cli.add_command(verify)
cback_cli.add_command(services)
cback_cli.add_command(portal)


if __name__ == '__main__':
    cback_cli()
