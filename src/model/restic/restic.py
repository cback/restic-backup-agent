# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import collections
from enum import Enum, auto
import shutil
import signal
import threading
from types import TracebackType
from dataclasses import dataclass
from datetime import datetime
import tempfile
from typing import IO, Any, Callable, Deque, Dict, List, Optional, Tuple, Type, Union, cast
from shutil import which
from pwd import getpwnam
import subprocess
import shlex
import json
from json import JSONDecodeError
import os
from dateutil.parser import parse
from humanfriendly import parse_size

from model.config.logs import get_logger

StatusUpdateCallback = Callable[[dict[str, Any]], None]

class LockType(Enum):
    NONE = auto()
    SHARED = auto()
    EXCLUSIVE = auto()


@dataclass(frozen=True)
class Lock:
    time: datetime
    exclusive: bool
    hostname: str
    username: str
    pid: int
    uid: int
    gid: int


@dataclass(frozen=True)
class Match:
    atime: datetime
    ctime: datetime
    device_id: int
    gid: int
    mode: int
    mtime: datetime
    path: str
    permissions: str
    size: int
    type: str
    uid: int


@dataclass(frozen=True)
class Matches:
    snapshot_id: str
    matches: List[Match]


@dataclass(frozen=True)
class Snapshot:
    time: datetime
    parent: str
    tree: str
    paths: List[str]
    hostname: str
    username: str
    uid: int
    gid: int
    excludes: List[str]
    id: str
    short_id: str
    tags: List[str]


@dataclass(frozen=True)
class BackupStat:
    data_added: float
    data_blobs: float
    dirs_changed: int
    dirs_new: int
    dirs_unmodified: int
    files_changed: int
    files_new: int
    files_unmodified: int
    files_error_count: int
    files_error_list: List[Dict]
    snapshot_id: str
    total_bytes_processed: float
    total_duration: float  # TODO: review type
    total_files_processed: int


@dataclass(frozen=True)
class PruneStat:
    to_repack: int
    to_delete: int
    total_prune: int
    remaining: int

    def __add__(self, other: 'PruneStat') -> 'PruneStat':
        return PruneStat(self.to_repack + other.to_repack, self.to_delete + other.to_delete, self.total_prune + other.total_prune, self.remaining + other.remaining)


class ResticError(Exception):
    pass


class RepositoryLocked(Exception):
    pass


class Restic:

    def __init__(
        self,
        repository: str,
        password: str, *,
        env: Dict[str, str] = {},
        options: Dict[str, str] = {},
        cache_dir: Optional[str] = None
    ) -> None:
        self.path = repository
        self.env = env if env else {}

        # add the password to the env to use
        self.env['RESTIC_PASSWORD'] = password
        self.env['RESTIC_REPOSITORY'] = repository

        # TODO: add a configuration file for restic
        self.env['RESTIC_PROGRESS_FPS'] = '1'

        self.extendend_options = options
        self.cache_dir = cache_dir

        # check if the restic executable is installed in the machine
        self.restic_path = which('restic')
        if not self.restic_path:
            raise ResticError('restic not installed')

        self.log = get_logger()

        umount_bin = which('umount')
        if umount_bin is None:
            raise ResticError('cannot find umount binary')
        self.umount_bin = umount_bin

    def init(self) -> None:
        cmd = self._restic_cmd('init')
        Restic._check_execute(cmd, self.env)

    def exists(self) -> bool:
        try:
            self.snapshots(no_lock=True)
            return True
        except ResticError as e:
            if 'Is there a repository at the following location?' in str(e):
                return False
            raise e

    @staticmethod
    def _check_execute(cmd: List[str], env: Dict[str, str]) -> Tuple[List[str], List[str]]:
        p = ResticProcess(cmd, env)
        retcode = p.wait()
        Restic._process_error(retcode, p)
        return p.get_output()

    def _log_line(self, line: str) -> None:
        try:
            d = json.loads(line)
        except JSONDecodeError:
            return
        if isinstance(d, dict):
            pass
            self.log.debug('restic output', extra={
                **d, 'repository': self.path})
        else:
            # for sure this is not needed, but it is just a check
            # to not make crash cback.
            self.log.debug(line)

    def backup(self, source: Union[str, List[str]], *,
               exclude_list: List[str] = [], files_from: List[str] = [], no_scan: bool = False,
               host: Optional[str] = None, parent: Optional[str] = None, tags: List[str] = [],
               time: Optional[datetime] = None, hook: Optional[Callable] = None, username: Optional[str] = None,
               on_status_update: Optional[StatusUpdateCallback] = None) -> BackupStat:

        # this will keep the list of all the files error met during backup
        files_error = []

        def func_out(line: str) -> None:
            self._log_line(line)
            try:
                line_json: Dict = json.loads(line)
            except JSONDecodeError:
                return
            if on_status_update:
                on_status_update(line_json)

        def filter_files_error(line: str) -> None:
            try:
                line_json: Dict = json.loads(line)
            except JSONDecodeError:
                return
            # {"message_type":"error","error":{"Op":"open","Path":"<path-file>,"Err":13},"during":"archival","item":"<absolute-path-file>"}
            if line_json['message_type'] == 'error':
                err_code = line_json['error'].get('Err', None)
                err = os.strerror(err_code) if err_code is not None else ''
                files_error.append({
                    'op': line_json['error'].get('Op', 'Unknown'),
                    'err_code': err_code,
                    'err': err,
                    'path': line_json['item']
                })

        args = []
        if exclude_list:
            args += list(map(lambda el: f'--exclude {el}', exclude_list))
        if files_from:
            args += list(map(lambda el: f'--files-from {el}', files_from))
        if no_scan:
            args.append('--no-scan')
        if host:
            args.append(f'--host {host}')
        if parent:
            args.append(f'--parent {parent}')
        if tags:
            args.append('--tag ' + ','.join(tags))
        if time:
            args.append(f'--time \'{time.strftime("%Y-%m-%d %H:%M:%S")}\'')

        if isinstance(source, str):
            args.append(source)
        elif isinstance(source, list):
            args.append(' '.join(source))

        args.append('--json')

        cmd = self._restic_cmd('backup', *args)
        process = ResticProcess(cmd, self.env, username)
        if hook:
            hook(process)
        retcode = process.wait(
            keep_only=1, func_out=func_out, func_err=filter_files_error)
        out, _ = process.get_output()
        Restic._process_error(retcode, process, skip_list=[3])

        return self._parse_backup_stats(out, files_error)

    def _parse_backup_stats(self, out: List[str], files_error_list: List) -> BackupStat:
        out_json = [json.loads(line) for line in out]
        summary = out_json[-1]

        return BackupStat(
            data_added=summary['data_added'],
            data_blobs=summary['data_blobs'],
            dirs_changed=summary['dirs_changed'],
            dirs_new=summary['dirs_new'],
            dirs_unmodified=summary['dirs_unmodified'],
            files_changed=summary['files_changed'],
            files_new=summary['files_new'],
            files_unmodified=summary['files_unmodified'],
            files_error_count=len(files_error_list),
            files_error_list=files_error_list,
            snapshot_id=summary['snapshot_id'],
            total_bytes_processed=summary['total_bytes_processed'],
            total_duration=summary['total_duration'],
            total_files_processed=summary['total_files_processed']
        )

    def snapshots(self, *, path: List[str] = [], no_lock: bool = False) -> List['Snapshot']:
        args = []
        if path:
            args += list(map(lambda el: f'--path {el}', path))
        args.append('--json')
        if no_lock:
            args.append('--no-lock')

        cmd = self._restic_cmd('snapshots', *args)
        out, _ = Restic._check_execute(cmd, self.env)
        snaps = json.loads(out[-1])

        return list(map(self._dict_to_snapshot, snaps))

    def get_snapshot(self, id: str) -> 'Snapshot':
        snaps = self.snapshots()
        for s in snaps:
            if s.short_id == id or s.id == id:
                return s
        raise ResticError(f'snapshot with id {id} not found')

    def _get_time(self, e: Dict[str, Any], key: str = 'time') -> datetime:
        if key in e:
            return parse(e[key])
        return datetime.now()

    def _dict_to_snapshot(self, e: Dict[str, Any]) -> 'Snapshot':
        s = Snapshot(
            time=self._get_time(e),
            excludes=e.get('excludes', []),
            parent=e.get('parent', ''),
            tree=e.get('tree', ''),
            hostname=e.get('hostname', ''),
            username=e.get('username', ''),
            uid=e.get('uid', -1),
            gid=e.get('gid', -1),
            id=e.get('id', ''),
            short_id=e.get('short_id', ''),
            paths=e.get('paths', []),
            tags=e.get('tags', [])
        )
        return s

    def _spawn_shell(self, username: Optional[str] = None, mount: Optional[str] = None) -> None:
        env = {**os.environ, **self.env}
        env['PROMPT_COMMAND'] = 'PS1=`echo "cback ($(whoami)) > "`'
        if mount:
            env['RESTIC_MOUNT'] = mount
        print('Spawing a shell with a configured environent for the repository', self.path)
        # TODO (gdelmont): use the ResticProcess instead
        pre_exec = None
        if username:
            uid, gid = _resolve_username(username)
            pre_exec = _set_uid_gid(uid, gid)
        subprocess.run(os.environ.get('SHELL', '/bin/sh'),
                       env=env, preexec_fn=pre_exec)
        print('Exited from the restic shell')

    def shell(self, *, mount: bool = False, mount_dir: Optional[str], username: Optional[str] = None) -> None:
        if mount:
            with self.mount(mount_dir, username) as m:
                print('Mounted repository in', m)
                self._spawn_shell(username=username, mount=m)
        else:
            self._spawn_shell(username=username)

    def restore(self, snapshot: str, target: str, *, pattern: Union[str, List[str]] = [], path: Optional[str] = None, exclude: Union[str, List[str]] = [], host: Optional[str] = None, hook: Optional[Callable] = None, username: Optional[str] = None, verify: bool = False, on_status_update: Optional[StatusUpdateCallback] = None) -> None:
        def func_out(line: str) -> None:
            self._log_line(line)
            try:
                d = json.loads(line)
            except JSONDecodeError:
                return
            if on_status_update:
                on_status_update(d)
        
        args = []
        if pattern:
            if isinstance(pattern, str):
                pattern = [pattern]
            args += list(map(lambda p: f'--include {shlex.quote(p)}', pattern))
        if path:
            args.append(f'--path {path}')
        if exclude:
            if isinstance(exclude, str):
                exclude = [exclude]
            args += list(map(lambda p: f'--exclude {shlex.quote(p)}', exclude))
        if host:
            args.append(f'--host {host}')
        if verify:
            args.append('--verify')

        args.append(f'--target {target}')
        args.append('--json')

        cmd = self._restic_cmd('restore', snapshot, *args)
        process = ResticProcess(cmd, self.env, username)
        if hook:
            hook(process)
        retcode = process.wait(keep_only=3, func_out=func_out)
        Restic._process_error(retcode, process)

    @staticmethod
    def _process_error(retcode: int, process: 'ResticProcess', skip_list: List[int] = []) -> None:
        for skip in skip_list:
            if retcode == skip:
                return

        if retcode:
            # check the stderr to see if the job failed
            # because the repository is locked by an other restic process
            err = ''.join(process._stderr).strip()
            if 'repository is already locked' in err:
                # TODO: add informations on who holds the lock
                raise RepositoryLocked()
            raise ResticError(err)

    def forget(self, *, snapshot: Optional[str] = None, policies: Dict[str, int] = {}, host: Optional[str] = None, path: Optional[str] = None, group_by: List[str] = [], prune: bool = False, hook: Optional[Callable] = None) -> PruneStat:

        def _parse_forget_output(raw: List[str]) -> PruneStat:
            to_repack, to_delete, total_prune, remaining = 0, 0, 0, 0
            for line in raw:
                if line.startswith("total prune:"):
                    splitted = line.split()
                    humanized = splitted[5] + " " + splitted[6]
                    total_prune = parse_size(humanized)
                elif line.startswith("remaining:"):
                    splitted = line.split()
                    humanized = splitted[4] + " " + splitted[5]
                    remaining = parse_size(humanized)
                elif line.startswith("to repack:"):
                    splitted = line.split()
                    humanized = splitted[5] + " " + splitted[6]
                    to_repack = parse_size(humanized)
                elif line.startswith("to delete:"):
                    splitted = line.split()
                    humanized = splitted[5] + " " + splitted[6]
                    to_delete = parse_size(humanized)
            return PruneStat(
                to_repack=to_repack,
                to_delete=to_delete,
                remaining=remaining,
                total_prune=total_prune
            )

        args = []
        if snapshot:
            args.append(snapshot)
        if policies:
            args += list(map(lambda kv: f'--{kv[0]} {kv[1]}',
                         policies.items()))
        if host:
            args.append(f'--host {host}')
        if path:
            args.append(f'--path {path}')
        if group_by:
            args.append(f'--group-by ' + ','.join(group_by))
        if prune:
            args.append('--prune')

        # args.append('--json')

        cmd = self._restic_cmd('forget', *args)
        process = ResticProcess(cmd, self.env)
        if hook:
            hook(process)
        retcode = process.wait()
        Restic._process_error(retcode, process)

        # workaround as for now restic does not support json output for the forget cmd
        stdout, _ = process.get_output()
        return _parse_forget_output(stdout)

    def mount(self, folder: Optional[str] = None, username: Optional[str] = None, debug: bool = False) -> 'ResticMount':
        """Mount the repository in a folder. If the folder is not provided, a temp folder is used.

        Args:
            repository (string): Name of the repository

        Returns:
            str: the directory name in which the folder is mounted
        """
        return ResticMount(self, self.path, folder, self.umount_bin, username, self.env, debug, self.cache_dir)

    def _dict_to_match(self, m: Dict[str, Any]) -> 'Match':
        return Match(
            atime=self._get_time(m, 'atime'),
            ctime=self._get_time(m, 'ctime'),
            mtime=self._get_time(m, 'mtime'),
            device_id=m.get('device_id', -1),
            gid=m.get('gid', -1),
            mode=m.get('mode', ""),
            path=m['path'],
            permissions=m.get('permissions', '-'*10),
            size=m.get('size', 0),
            type=m.get('type', 'unknown'),
            uid=m.get('uid', -1)
        )

    def _dict_to_matches(self, m: Dict[str, Any]) -> 'Matches':
        return Matches(
            snapshot_id=m['snapshot'],
            matches=list(map(self._dict_to_match, m['matches']))
        )

    def find(self, pattern: Union[str, List[str]], *, host: Optional[str] = None, path: Optional[str] = None, snapshot: List[str] = []) -> List['Matches']:
        args = []

        if isinstance(pattern, str):
            pattern = [pattern]
        args.append(' '.join(map(lambda s: f"'{s}'", pattern)))

        if host:
            args.append(f'--host {host}')
        if path:
            args.append(f'--path {path}')
        if snapshot:
            args.append(" ".join(map(lambda s: f'--snapshot {s}', snapshot)))

        args.append('--json')

        cmd = self._restic_cmd('find', *args)
        out, _ = Restic._check_execute(cmd, self.env)
        matches_json = json.loads(out[-1])

        return list(map(self._dict_to_matches, matches_json))

    def stats(self) -> Any:  # TODO: parse the output
        cmd = self._restic_cmd('stats --json')
        out, _ = Restic._check_execute(cmd, self.env)
        return json.loads(out[-1])

    def unlock(self, all: bool = False) -> None:
        args = []
        if all:
            args.append('--remove-all')
        cmd = self._restic_cmd('unlock', *args)
        Restic._check_execute(cmd, self.env)

    def dump(self, snapshot_id: str, file: str) -> bytes:
        cmd = self._restic_cmd('dump', snapshot_id, file)
        p = ResticProcess(cmd, self.env, bytes_output=True)
        out, _ = p.proc.communicate()

        Restic._process_error(p.proc.returncode, p)
        return cast(bytes, out)

    def get(self, id: str, type: str) -> Dict[str, Any]:
        cmd = self._restic_cmd('cat', type, id)
        out, _ = Restic._check_execute(cmd, self.env)
        out_json = ''.join(out)
        return cast(Dict[str, Any], json.loads(out_json))

    def _dict_to_lock(self, d: Dict[str, Any]) -> Lock:
        return Lock(
            time=self._get_time(d),
            exclusive=d['exclusive'],
            hostname=d['hostname'],
            username=d['username'],
            pid=d['pid'],
            uid=d['uid'],
            gid=d['gid']
        )

    def get_locks(self, remove_stale: bool = True) -> List['Lock']:
        # this is not atomic, use with caution :)
        if remove_stale:
            self.unlock()
        cmd = self._restic_cmd('list', '--no-lock', 'locks', '--quiet')
        locks, _ = Restic._check_execute(cmd, self.env)
        l: List['Lock'] = []
        for lock_id in locks:
            try:
                lock_dict = self.get(lock_id, 'lock')
                l.append(self._dict_to_lock(lock_dict))
            except:
                # the lock could not exist anymore :)
                continue
        return l

    def is_locked(self) -> 'LockType':
        def _has_exclusive_lock(locks: List['Lock']) -> bool:
            for lock in locks:
                if lock.exclusive:
                    return True
            return False

        locks = self.get_locks(remove_stale=True)
        if not locks:
            return LockType.NONE
        if _has_exclusive_lock(locks):
            return LockType.EXCLUSIVE
        return LockType.SHARED

    def tag(self, tags: List[str], snapshots: List[str], *, no_lock: bool = False) -> None:
        if not tags:
            raise ValueError('tag list cannot be empty')
        if not snapshots:
            raise ValueError('snapshots list cannot be empty')

        args: List[str] = []
        args.append('--add ' + ','.join(tags))
        args.extend(snapshots)
        if no_lock:
            args.append('--no-lock')

        cmd = self._restic_cmd('tag', *args)
        Restic._check_execute(cmd, self.env)

    @staticmethod
    def version() -> Tuple[int, ...]:
        restic_bin = which('restic')
        if not restic_bin:
            raise ResticError(
                'cannot find the restic bin, make sure you installed it')
        out, _ = Restic._check_execute([restic_bin, 'version'], {})
        return tuple(map(int, out[0].split(" ")[1].split(".")))

    def _restic_cmd(self, cmd: str, *args: str) -> List[str]:
        ext_options = ''
        if self.extendend_options:
            ext_options = ' '.join(
                map(lambda kv: f'-o {kv[0]}={kv[1]}', self.extendend_options.items()))
        opts = ' '.join(args) if args else ''
        cache_dir = f'--cache-dir {self.cache_dir}' if self.cache_dir else '--no-cache'
        restic_cmd = f'{self.restic_path} {cache_dir} {ext_options} {cmd} {opts}'
        return shlex.split(restic_cmd)


def _resolve_username(username: str) -> Tuple[int, int]:
    passwd = getpwnam(username)
    return passwd.pw_uid, passwd.pw_gid


def _set_uid_gid(uid: int, gid: int) -> Callable[[], None]:
    def func() -> None:
        os.setgid(gid)
        os.setuid(uid)
    return func


class ResticProcess:

    __slots__ = ['proc', '_stdout', '_stderr']

    def __init__(self, cmd: List[str], env: Dict[str, str], username: Optional[str] = None, bytes_output: bool = False):
        # if username is specified, exec the process as the user
        preexec_fn = None
        if username:
            env['USER'] = username
            # resolve uid and gid of username
            uid, gid = _resolve_username(username)
            preexec_fn = _set_uid_gid(uid, gid)
        universal_newlines = not bytes_output
        self.proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1, universal_newlines=universal_newlines, env=env, preexec_fn=preexec_fn)

        self._stdout = cast(List[str], None)
        self._stderr = cast(List[str], None)

    def kill(self) -> None:
        self.proc.kill()

    def pid(self) -> int:
        return self.proc.pid

    def is_running(self) -> bool:
        return self.proc.poll() == None

    def get_output(self) -> Tuple[List[str], List[str]]:
        if self.is_running():
            raise ResticError('the process is still running')
        return self._stdout, self._stderr

    def wait(self, keep_only: int = 0, timeout: Optional[int] = None, func_out: Optional[Callable[[str], None]] = None, func_err: Optional[Callable[[str], None]] = None) -> int:
        def read_line(io: IO[str], func: Callable[[str], None], append: Callable[[Any], None]) -> None:
            for line in iter(io.readline, ""):
                if func:
                    func(line)
                append(line)

        def process_stream(stream: Optional[IO[str]], func: Optional[Callable[[str], None]]) -> Tuple[threading.Thread, Deque[str]]:
            q: Deque[str] = collections.deque(maxlen=keep_only)
            t = threading.Thread(
                target=read_line, args=(stream, func, q.append))
            t.daemon = True
            t.start()
            return t, q

        def wait_thread(t: threading.Thread, q: Deque[str]) -> List[str]:
            t.join()
            return list(q)

        try:
            if keep_only != 0:
                thread_out, q_out = process_stream(self.proc.stdout, func_out)
                thread_err, q_err = process_stream(self.proc.stderr, func_err)

                # wait the process to finish
                self.proc.wait(timeout=timeout)

                # wait the threads to finish
                self._stdout = wait_thread(thread_out, q_out)
                self._stderr = wait_thread(thread_err, q_err)
            else:
                out, err = self.proc.communicate(timeout=timeout)
                self._stdout = out.splitlines()
                self._stderr = err.splitlines()
        finally:
            self.proc.terminate()

        return self.proc.returncode


class ResticMount:
    def __init__(self, restic: Restic, repository: str, folder: Optional[str], umount_bin: str, username: Optional[str] = None, env: Dict[str, str] = {}, debug: bool = False, cache_dir: Optional[str] = None):
        self.repository = repository
        self.username = username
        if folder:
            self.folder = folder
            self.temp = False
        else:
            self.folder = tempfile.mkdtemp()
            self.temp = True
        if username:
            uid, gid = _resolve_username(username)
            os.chown(self.folder, uid, gid)
        self.restic = restic
        self.debug = debug
        self.env = env
        self.cache_dir = cache_dir
        self.umount_bin = umount_bin

    def mount(self) -> str:
        cmd = self.restic._restic_cmd('mount', self.folder)
        self.env = {**self.env}
        self.env['PATH'] = os.environ['PATH']
        self.rp = ResticProcess(cmd, self.env, self.username)
        while True:
            if self.rp.proc.poll():
                # maybe some problem in mounting the folder
                raise ResticError('Mounting failed')
            if self.rp.proc.stdout:
                line = self.rp.proc.stdout.readline().strip()
                if f'Now serving the repository at {self.folder}' in line:
                    return self.folder

    def umount(self) -> None:
        try:
            self.rp.proc.send_signal(signal.SIGINT)
            self.rp.proc.wait()
        finally:

            # even if we already sent a SIGINT signal that should have
            # unmounted the repo, we will call umount as a safeguard
            # see: https://forum.restic.net/t/correct-way-to-umount/185

            subprocess.run([self.umount_bin, '-f', self.folder])

            # delete cache dir if set
            if self.cache_dir:
                shutil.rmtree(self.cache_dir, ignore_errors=True)

            # delete folder only if it is temp
            if self.temp:
                try:
                    os.rmdir(self.folder)
                except FileNotFoundError:
                    pass

    def __enter__(self) -> str:
        return self.mount()

    def __exit__(self, type: Optional[Type[BaseException]] = None, value: Optional[BaseException] = None, tb: Optional[TracebackType] = None) -> None:
        self.umount()
