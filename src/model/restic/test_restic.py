# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import unittest

from utils.test.local_fs import *
from utils.test.restic import *
from datetime import datetime
import os
from os import path
import shutil
from pathlib import Path

from model.restic.restic import Restic, ResticError, Snapshot
from tempfile import mkdtemp


class TestRestic(unittest.TestCase):

    def test_init_new_repository(self) -> None:

        dir = mkdtemp(prefix=TMP_PREFIX)
        repo = Restic(dir, PASSWORD)
        repo.init()

        # check if the dir has been created
        self.assertTrue(os.path.isdir(dir))

        # check if is a restic repository looking at the content of the dir
        content = os.listdir(dir)
        self.assertSetEqual(
            set(content), {'config', 'data', 'index', 'keys', 'locks', 'snapshots'})

    def test_init_pre_existing_repository(self) -> None:

        repo, _, cleanup = new_empty_restic_repo()
        self.assertRaises(ResticError, repo.init)

        cleanup()

    def test_exists_on_existing_repository(self) -> None:

        repo, _, cleanup = new_empty_restic_repo()
        self.assertTrue(repo.exists())
        cleanup()

    def test_exists_on_not_existing_repository(self) -> None:

        repo = Restic('path_to_non_existing_repo', PASSWORD)
        self.assertFalse(repo.exists())

    def test_exists_on_existing_repository_password_wrong(self) -> None:

        repo, _, cleanup = new_empty_restic_repo()
        # creating the same repository but using a password typed by a cat walking on the keyboard
        same_repo = Restic(repo.path, PASSWORD + "asdhasjkdh")
        self.assertRaises(ResticError, same_repo.exists)

        cleanup()

    def test_backup_no_errors_default_user(self) -> None:

        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='excluded',
                    name='excluded_file'
                ),
                Directory(
                    content=[],
                    name='excluded_dir'
                )
            ]
        )

        to_backup, cleanup_dir = new_local_dir(d)
        repo, _, cleanup_repo = new_empty_restic_repo()

        exclude_list = list(map(lambda f: os.path.join(to_backup, f), [
                            'foo/excluded_file', 'foo/excluded_dir']))
        stats = repo.backup(to_backup, exclude_list=exclude_list)

        self.assertEqual(stats.dirs_new, 4)
        self.assertEqual(stats.files_new, 1)
        self.assertEqual(stats.files_error_count, 0)

        cleanup_dir()
        cleanup_repo()

    def test_backup_with_time(self) -> None:

        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
            ]
        )

        to_backup, cleanup_dir = new_local_dir(d)
        repo, _, cleanup_repo = new_empty_restic_repo()

        now = datetime.now()
        repo.backup(to_backup, time=now)

        latest_snaphot = repo.snapshots()[-1]
        latest_snaphot_time: datetime = latest_snaphot.time
        tz = latest_snaphot_time.tzinfo
        now = datetime(now.year, now.month, now.day, now.hour,
                       now.minute, now.second, 0, tz)

        self.assertEqual(now, latest_snaphot.time)

        cleanup_dir()
        cleanup_repo()

    def test_backup_files_error(self) -> None:

        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar', mode=0o000)],
                ),
                File(
                    content='excluded',
                    name='excluded_file',
                    mode=0o000
                ),
                Directory(
                    content=[],
                    name='excluded_dir'
                ),
                File(
                    content='foo',
                    name='foo'
                ),
            ]
        )

        to_backup, cleanup_dir = new_local_dir(d)
        repo, _, cleanup_repo = new_empty_restic_repo(owner='nobody')

        stats = repo.backup(to_backup, username='nobody')

        self.assertEqual(len(stats.files_error_list), 2)

        cleanup_dir()
        cleanup_repo()

    def test_backup_no_errors_different_user(self) -> None:

        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='excluded',
                    name='excluded_file'
                ),
                Directory(
                    content=[],
                    name='excluded_dir'
                )
            ]
        )

        to_backup, cleanup_dir = new_local_dir(d)
        repo, _, cleanup_repo = new_empty_restic_repo()

        exclude_list = list(map(lambda f: os.path.join(to_backup, f), [
                            'foo/excluded_file', 'foo/excluded_dir']))
        repo.backup(to_backup, exclude_list=exclude_list,
                    username=os.environ['USER'])

        cleanup_dir()
        cleanup_repo()

    def test_snapshots_empty_repository(self) -> None:

        repo, _, cleanup = new_empty_restic_repo()
        self.assertListEqual(repo.snapshots(), [])

        cleanup()

    def test_snapshots_not_empty_repository(self) -> None:
        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='foo',
                    name='foo'
                ),
                Directory(
                    content=[],
                    name='dir'
                )
            ]
        )

        repo, _, cleanup_repo = new_empty_restic_repo()
        to_backup, cleanup_dir = new_local_dir(d)
        repo.backup(to_backup)

        snapshots = repo.snapshots()

        self.assertIsInstance(snapshots, list)
        self.assertEqual(len(snapshots), 1)
        self.assertIsInstance(snapshots[0], Snapshot)

        cleanup_dir()
        cleanup_repo()

    def test_restore_dir(self) -> None:
        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='foo',
                    name='foo'
                ),
                Directory(
                    content=[],
                    name='dir'
                )
            ]
        )

        repo, _, cleanup_repo = new_empty_restic_repo()
        to_backup, cleanup_dir = new_local_dir(d)
        repo.backup(to_backup)

        # get the last snapshot
        snap_id = repo.snapshots()[-1].short_id
        target_restore = mkdtemp(prefix=TMP_PREFIX)

        repo.restore(snap_id, target_restore)

        restored_to_backup = target_restore + to_backup
        self.assertTrue(dir_equals(to_backup, restored_to_backup))

        shutil.rmtree(target_restore)
        cleanup_dir()
        cleanup_repo()

    def test_restore_dir_containing_spaces(self) -> None:
        d = Directory(
            name='bla bla bla',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='foo',
                    name='foo'
                ),
                Directory(
                    content=[],
                    name='dir'
                )
            ]
        )

        repo, _, cleanup_repo = new_empty_restic_repo()
        to_backup, cleanup_dir = new_local_dir(d)
        repo.backup(to_backup)

        # get the last snapshot
        snap_id = repo.snapshots()[-1].short_id
        target_restore = mkdtemp(prefix=TMP_PREFIX)

        repo.restore(snap_id, target_restore,
                     pattern=path.join(to_backup, "bla bla bla"))

        restored_to_backup = target_restore + to_backup
        self.assertTrue(dir_equals(to_backup, restored_to_backup))

        shutil.rmtree(target_restore)
        cleanup_dir()
        cleanup_repo()

    def test_mount_temp_dir(self) -> None:

        repo, _, cleanup_repo = new_empty_restic_repo()

        mount = None
        with repo.mount() as mount_folder:
            mount = mount_folder
            self.assertTrue(os.listdir(mount))

        dir = Path(mount)
        self.assertFalse(dir.exists())

        cleanup_repo()

    def test_mount_given_dir(self) -> None:

        repo, _, cleanup_repo = new_empty_restic_repo()

        dir = mkdtemp(prefix=TMP_PREFIX)
        with repo.mount() as d:
            self.assertTrue(os.listdir(d))

        dd = Path(dir)
        self.assertTrue(dd.exists())

        shutil.rmtree(dir)
        cleanup_repo()

    def test_find_file_in_repository(self) -> None:
        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='foo',
                    name='foo'
                ),
                Directory(
                    content=[],
                    name='dir'
                )
            ]
        )

        repo, _, cleanup_repo = new_empty_restic_repo()
        to_backup, cleanup_dir = new_local_dir(d)
        repo.backup(to_backup)

        match = repo.find('foo/foo')
        self.assertEqual(len(match), 1)
        self.assertEqual(len(match[0].matches), 1)

        cleanup_dir()
        cleanup_repo()

    def test_find_file_not_in_repository(self) -> None:
        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='foo',
                    name='foo'
                ),
                Directory(
                    content=[],
                    name='dir'
                )
            ]
        )

        repo, _, cleanup_repo = new_empty_restic_repo()
        to_backup, cleanup_dir = new_local_dir(d)
        repo.backup(to_backup)

        match = repo.find('not_in_repository')
        self.assertListEqual(match, [])

        cleanup_dir()
        cleanup_repo()

    def test_stats(self) -> None:
        repo, _, clean_repo = new_empty_restic_repo()
        stats = repo.stats()
        self.assertEqual(stats['total_file_count'], 0)

        clean_repo()

    def test_forget_given_snapshotID(self) -> None:
        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='foo',
                    name='foo'
                ),
                Directory(
                    content=[],
                    name='dir'
                )
            ]
        )
        repo, _, clean_repo = new_empty_restic_repo()
        to_backup, cleanup_dir = new_local_dir(d)
        repo.backup(to_backup)

        # get the latest snapshot id
        snap_id = repo.snapshots()[-1].short_id

        # remove it from the repo
        repo.forget(snapshot=snap_id)

        snap_after_forget = repo.snapshots()
        self.assertEqual(len(snap_after_forget), 0)

        cleanup_dir()
        clean_repo()

    def test_forget_given_snapshotID_prune(self) -> None:
        d = Directory(
            name='foo',
            content=[
                Directory(
                    name='bar',
                    content=[File(name='file', content='foobar')],
                ),
                File(
                    content='foo',
                    name='foo'
                ),
                Directory(
                    content=[],
                    name='dir'
                )
            ]
        )
        repo, _, clean_repo = new_empty_restic_repo()
        to_backup, cleanup_dir = new_local_dir(d)
        repo.backup(to_backup)

        # get the latest snapshot id
        snap_id = repo.snapshots()[-1].short_id

        # remove it from the repo
        repo.forget(snapshot=snap_id, prune=True)

        snap_after_forget = repo.snapshots()
        self.assertEqual(len(snap_after_forget), 0)

        cleanup_dir()
        clean_repo()
