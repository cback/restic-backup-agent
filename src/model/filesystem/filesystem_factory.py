# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Dict
from .eosxd import EOS, EOSConfig
from .cephfs import CephFS
from .local import Local

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .filesystem import FileSystem


class FileSystemFactory:
    @staticmethod
    def new(fs: str, config: Dict) -> 'FileSystem':
        if fs == "ceph":
            return CephFS()
        if fs == "eos":
            master_url = config.get('master_url', 'root://eos-example.org')
            eos_binary = config.get('eos_binary', '/usr/bin/eos')
            eos_config = EOSConfig(master_url, eos_binary)
            return EOS(eos_config)
        else:
            return Local()
