# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from os import getxattr, stat
from typing import Optional
from dateutil.tz import tzlocal

from model.filesystem.filesystem import FileSystem


class CephFS(FileSystem):

    def exists(self, path: str, user: Optional[str] = None) -> bool:
        try:
            stat(path)
        except FileNotFoundError:
            return False
        else:
            return True

    def timestamp(self, path: str, user: Optional[str] = None) -> datetime:
        date_str = getxattr(path, 'ceph.dir.rctime').decode('utf-8')
        return datetime.fromtimestamp(float(date_str), tz=tzlocal())
