# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import Optional
from model.filesystem.filesystem import FileSystem
from utils.utils import Utils
from dataclasses import dataclass
from dateutil.tz import tzlocal


@dataclass
class EOSConfig:
    master_url: str
    eos_binary: str


class EOS(FileSystem):

    def __init__(self, config: EOSConfig) -> None:
        super().__init__()
        self.config = config

    def exists(self, path: str, user: Optional[str] = None) -> bool:
        cmd = f"stat {path}"
        try:
            Utils.check_run(cmd, user=user)
            return True
        except KeyError:
            # user does not exist
            raise
        except Exception as e:
            return False

    def timestamp(self, path: str, user: Optional[str] = None) -> datetime:
        # TODO (gdelmont): consider to add a template string for convertion
        # an eos fuse mount path to an eos path
        eos_path = Utils.convert_path(path)
        cmd = f'{self.config.eos_binary} {self.config.master_url} file info {eos_path} -m'
        out = Utils.check_run(cmd, user=user)

        for line in out.splitlines():
            for word in line.split(' '):
                name, value = word.partition("=")[::2]
                if name == 'etag':
                    try:
                        return datetime.fromtimestamp(float(value.split(':')[1]), tz=tzlocal())
                    except ValueError:
                        return datetime.now(tz=tzlocal())
        return datetime.fromtimestamp(0.0, tz=tzlocal())
