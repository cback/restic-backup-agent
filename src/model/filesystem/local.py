# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import Optional
from model.filesystem.filesystem import FileSystem
from utils.utils import Utils
from dateutil.tz import tzlocal


class Local(FileSystem):

    def exists(self, path: str, user: Optional[str] = None) -> bool:
        cmd = f"stat {path}"
        try:
            Utils.check_run(cmd)
            return True
        except:
            return False

    def timestamp(self, path: str, user: Optional[str] = None) -> datetime:
        # in a local filesystem the parent timestamp is not update if any of the
        # children is update, so always return the 0 timestamp
        return datetime.fromtimestamp(0.0, tz=tzlocal())
