# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime, timedelta
from typing import List, Optional, cast
import unittest
from model.jobs.group import Group
from model.jobs.job import Repository
from model.jobs.status import Status
from model.managers.jobs.status_strategies.completed_status import CompletedStatus
from model.managers.jobs.status_strategies.archived_status import ArchivedStatus
from model.managers.jobs.status_strategies.failed_status import FailedStatus
from model.managers.jobs.status_strategies.not_enabled_status import NotEnabledStatus
from model.managers.jobs.status_strategies.pending_status import PendingStatus
from model.managers.jobs.status_strategies.running_status import RunningStatus
from model.managers.jobs.status_strategies.total_status import TotalStatus
from model.managers.jobs.status_strategies.coverage_status import CoverageStatus

from utils.test.repo import TestJob, InMemoryJobRepository


def new_test_job(id: int, group: Group, repo: Repository, username: Optional[str], enabled: bool, status: int, worker: Optional[str], started: Optional[datetime], finished: Optional[datetime], retries: int) -> TestJob:
    return TestJob(
        id=id,
        group=group,
        repository=repo,
        enabled=enabled,
        status=status,
        finished=finished,
        info=None,
        retries=retries,
        running_expiration=None,
        started=started,
        stats={},
        username=username,
        worker=worker,
    )


class TestCompletedStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.completed_status = CompletedStatus(self.repository)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.completed_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_no_completed_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.PENDING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.completed_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_completed_one_group(self) -> None:
        group = Group(id=1, name='test2')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.ARCHIVED, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.completed_status.compute(group)
        self.assertEqual(res, 1)

    def test_jobs_completed_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.PENDING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.completed_status.compute(None)
        self.assertEqual(res, 2)


class TestArchivedStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.archived_status = ArchivedStatus(self.repository)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.archived_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_no_archived_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.archived_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_archived_one_group(self) -> None:
        group = Group(id=1, name='test2')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.ARCHIVED, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.archived_status.compute(group)
        self.assertEqual(res, 1)

    def test_jobs_archived_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.ARCHIVED, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.ARCHIVED, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.archived_status.compute(None)
        self.assertEqual(res, 2)


class TestFailedStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.failed_status = FailedStatus(self.repository)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.failed_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_no_failed_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.failed_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_failed_one_group(self) -> None:
        group = Group(id=1, name='test2')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.FAILED, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.failed_status.compute(group)
        self.assertEqual(res, 1)

    def test_jobs_failed_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.FAILED, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.FAILED, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.failed_status.compute(None)
        self.assertEqual(res, 2)


class TestNotEnabledStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.notenabled_status = NotEnabledStatus(self.repository)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.notenabled_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_no_failed_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.notenabled_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_failed_one_group(self) -> None:
        group = Group(id=1, name='test2')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, False,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.FAILED, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.notenabled_status.compute(group)
        self.assertEqual(res, 1)

    def test_jobs_failed_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         Status.FAILED, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, False,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.FAILED, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.notenabled_status.compute(None)
        self.assertEqual(res, 2)


class TestPendingStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.pending_status = PendingStatus(self.repository)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.pending_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_no_pending_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        res = self.pending_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_pending_one_group(self) -> None:
        group = Group(id=1, name='test2')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.PENDING, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.pending_status.compute(group)
        self.assertEqual(res, 1)

    def test_jobs_pending_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.PENDING, None, None, None, 0),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.PENDING, None, None,  None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.pending_status.compute(None)
        self.assertEqual(res, 2)


class TestRunningStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.running_status = RunningStatus(self.repository)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.running_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_no_running_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.PENDING, None, None, None, 0),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.PENDING, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.running_status.compute(None)
        self.assertEqual(res, 0)

    def test_jobs_running_one_group(self) -> None:
        group = Group(id=1, name='test2')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.RUNNING, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.running_status.compute(group)
        self.assertEqual(res, 1)

    def test_jobs_running_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:2', datetime.now(), None, 0),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:2', datetime.now(), None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.running_status.compute(None)
        self.assertEqual(res, 2)


class TestTotalStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.total_status = TotalStatus(self.repository)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.total_status.compute(None)
        self.assertEqual(res, 0)

    def test_one_group(self) -> None:
        group = Group(id=1, name='test2')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.RUNNING, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.total_status.compute(group)
        self.assertEqual(res, 2)

    def test_all_groups(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:2', datetime.now(), None,  0),
            new_test_job(1, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', datetime.now(), None, 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:2', datetime.now(), None,  0),
        ]
        self._add_jobs(init_jobs)

        res = self.total_status.compute(None)
        self.assertEqual(res, 3)


class TestCoverageStatus(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    def setUp(self) -> None:
        self.repository = InMemoryJobRepository[TestJob]()
        self.expiration_time = 24*60*60
        self.coverage_status = CoverageStatus(
            self.repository, self.expiration_time)
        self.coverage_status_all = CoverageStatus(self.repository, None)

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    def test_no_jobs_in_repository(self) -> None:
        res = self.coverage_status.compute(None)
        self.assertEqual(res, 100.0)

    def test_one_group(self) -> None:
        group = Group(id=1, name='test2')
        now = datetime.now()
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now - timedelta(minutes=1), 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.RUNNING, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.coverage_status.compute(group)
        self.assertAlmostEqual(res, 50.0)

    def test_one_group_bis(self) -> None:
        group = Group(id=1, name='test2')
        now = datetime.now()
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
            new_test_job(1, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now, 10),
            new_test_job(2, group, cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=self.expiration_time+2), now - timedelta(hours=self.expiration_time+1), 10),
            new_test_job(3, group, cast(Repository, None), None, True,
                         Status.RUNNING, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.coverage_status.compute(group)
        self.assertAlmostEqual(res, 33.33)

    def test_all_groups(self) -> None:
        now = datetime.now()
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:2', now, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now - timedelta(minutes=30), 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now - timedelta(minutes=20), 10),
            new_test_job(3, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:2', now - timedelta(hours=self.expiration_time+3), now - timedelta(hours=self.expiration_time+2), 0),
        ]
        self._add_jobs(init_jobs)

        res = self.coverage_status.compute(None)
        self.assertEqual(res, 50.0)

    def test_coverage_for_all_without_expiration_time_one_groups(self) -> None:
        now = datetime.now()
        group = Group(id=0, name='test1')
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:2', now, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now - timedelta(minutes=30), 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now - timedelta(minutes=20), 10),
            new_test_job(3, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:2', now - timedelta(hours=self.expiration_time+3), now - timedelta(hours=self.expiration_time+2), 0),
        ]

        self._add_jobs(init_jobs)

        res = self.coverage_status_all.compute(group)
        self.assertEqual(res, 50.0)

    def test_coverage_for_all_without_expiration_time_all_groups(self) -> None:
        now = datetime.now()
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:2', now, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now - timedelta(minutes=30), 10),
            new_test_job(2, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:1', now - timedelta(hours=1), now - timedelta(minutes=20), 10),
            new_test_job(3, Group(id=1, name='test2'), cast(Repository, None), None, True,
                         Status.COMPLETED, 'worker:2', now - timedelta(hours=self.expiration_time+3), now - timedelta(hours=self.expiration_time+2), 0),
        ]

        self._add_jobs(init_jobs)

        res = self.coverage_status_all.compute(None)
        self.assertEqual(res, 75.0)
