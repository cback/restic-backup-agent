# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional, Union
from model.jobs.group import Group
from model.jobs.status import Status
from model.managers.jobs.status_strategies.status_strategy import StatusStrategy
from repository import Filter, JobRepository, Operator


class ArchivedStatus(StatusStrategy):

    def get_description(self) -> str:
        return 'Archived'

    def compute(self, group: Optional[Group]) -> Union[int, float]:
        filters = [Filter('status', Operator.EQUAL, Status.ARCHIVED)]
        if group:
            filters.append(Filter('group_id', Operator.EQUAL, group.id))
        return self.repository.count_filtered(filters)
