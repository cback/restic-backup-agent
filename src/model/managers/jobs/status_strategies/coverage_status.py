# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional, Union
from model.jobs.group import Group
from model.jobs.status import Status
from model.managers.jobs.status_strategies.status_strategy import StatusStrategy
from model.managers.jobs.status_strategies.total_status import TotalStatus
from model.managers.jobs.status_strategies.archived_status import ArchivedStatus
from datetime import datetime, timedelta

from repository import Filter, JobRepository, Operator


class CoverageStatus(StatusStrategy):

    def __init__(self, repository: JobRepository, job_expiration_time: Optional[int]):
        super().__init__(repository)
        self.job_expiration_time = job_expiration_time

    def get_description(self) -> str:
        return 'Coverage (%)'

    def compute(self, group: Optional[Group]) -> Union[int, float]:
        total = TotalStatus(self.repository)
        archived = ArchivedStatus(self.repository)

        total_runnable = int(total.compute(group) - archived.compute(group))

        filters = [Filter('status', Operator.EQUAL, Status.COMPLETED)]

        if group:
            filters.append(Filter('group_id', Operator.EQUAL, group.id))

        if self.job_expiration_time is not None:
            threshold_time = datetime.now() - timedelta(seconds=self.job_expiration_time)
            filters.append(
                Filter('finished', Operator.GREATER_EQUAL, threshold_time))
        covered_count = self.repository.count_filtered(filters=filters)
        if covered_count != 0:
            return round((covered_count / total_runnable) * 100, 2)
        if total_runnable == 0:
            return 100.0
        return 0.0
