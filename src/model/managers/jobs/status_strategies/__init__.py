# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from .status_strategy import StatusStrategy

from .archived_status import ArchivedStatus
from .completed_status import CompletedStatus
from .coverage_status import CoverageStatus
from .failed_status import FailedStatus
from .not_enabled_status import NotEnabledStatus
from .pending_status import PendingStatus
from .running_status import RunningStatus
from .total_status import TotalStatus
from .rescheduled_status import RescheduledStatus
