# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import List, Optional, cast
import unittest
from unittest.mock import patch

from model.jobs.group import Group
from model.jobs.job import Repository
from model.jobs.status import Status

from model.managers.group.group_manager import GroupManager
from utils.test.repo import MockInMemoryRepositoryFactory, InMemoryGroupRepository, MockTestJobManager, TestJob


def new_test_job(id: int, group: Group, repo: Repository, username: Optional[str], enabled: bool, status: int, worker: Optional[str], started: Optional[datetime], finished: Optional[datetime], retries: int) -> TestJob:
    return TestJob(
        id=id,
        group=group,
        repository=repo,
        enabled=enabled,
        status=status,
        finished=finished,
        info=None,
        retries=retries,
        running_expiration=None,
        started=started,
        stats={},
        username=username,
        worker=worker,
    )


class TestJobManager(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        self.repository_factory = MockInMemoryRepositoryFactory()  # type: ignore
        groups = ['test1', 'test2']
        self.group_manager = GroupManager(InMemoryGroupRepository(groups))
        self.job_manager = MockTestJobManager(
            self.repository_factory, self.group_manager, {})
        self.repository = self.repository_factory.get_repository_by_job(
            TestJob)

    def tearDown(self) -> None:
        return super().tearDown()

    def _add_jobs(self, jobs: List[TestJob]) -> None:
        for job in jobs:
            self.repository.add(job)

    ####################################  TEST ADD  ####################################

    def test_add_new_job_empty_repository(self) -> None:
        job = self.job_manager.add(group='test1', enabled=False)
        # test if the job has been added into the repository
        self.assertEqual(self.repository.count_filtered([]), 1)

    ####################################  TEST GET_JOBS  ####################################

    def test_get_jobs_empty_repository(self) -> None:
        res = self.job_manager.get_jobs(group=Group(0, 'test1'), enabled=True)
        self.assertListEqual(list(res), [])

    def test_get_jobs_not_empty_repository_empty_res(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0)
        ]
        self._add_jobs(init_jobs)

        res = self.job_manager.get_jobs(enabled=True)
        self.assertListEqual(list(res), [])

    def test_get_jobs_not_empty_repository_not_empty_res(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         0, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.job_manager.get_jobs(enabled=False, group=Group(0, 'test1'))
        self.assertEqual(len(list(res)), 1)
        self.assertEqual(list(res)[0], init_jobs[0])

    ####################################  TEST GET_BY_ID  ####################################

    def test_get_by_id_empty_repository(self) -> None:
        with self.assertRaises(ValueError):
            self.job_manager.get_by_id(9999)

    def test_get_by_id_no_id_in_repository(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         0, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        with self.assertRaises(ValueError):
            self.job_manager.get_by_id(9999)

    def test_get_by_id_id_in_repository(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         0, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        res = self.job_manager.get_by_id(1)
        self.assertEqual(res, init_jobs[1])

    ####################################  TEST ENABLE  ####################################

    def test_enable_not_existing_id(self) -> None:
        with self.assertRaises(ValueError):
            self.job_manager.enable_by_id(9999)

    def test_enable_existing_id_was_disabled(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         0, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        job = self.job_manager.enable_by_id(id=0)
        self.assertTrue(job.enabled)

    def test_enable_existing_id_was_enabled(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         0, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        job = self.job_manager.enable_by_id(id=1)
        # enable an already enabled job is a no-op
        self.assertTrue(job.enabled)

    ####################################  TEST DISABLE  ####################################

    def test_disable_not_existing_id(self) -> None:
        with self.assertRaises(ValueError):
            self.job_manager.enable_by_id(id=9999)

    def test_disable_existing_id_was_disabled(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None,  None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         0, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        job = self.job_manager.disable_by_id(id=0)
        # disable an already disabled job is a no-op
        self.assertFalse(job.enabled)

    def test_disable_existing_id_was_true(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         0, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        job = self.job_manager.disable_by_id(id=1)
        self.assertFalse(job.enabled)

    ####################################  TEST RESET  ####################################

    def test_reset_not_existing_id(self) -> None:
        with self.assertRaises(ValueError):
            self.job_manager.reset(9999)

    def test_reset_existing_id(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        job = self.job_manager.reset(1)

        self.assertEqual(job.status, Status.PENDING)
        self.assertIsNone(job.worker)
        self.assertIsNone(job.started)
        self.assertIsNone(job.finished)
        self.assertIsNone(job.info)
        self.assertEqual(job.retries, 0)

    ####################################  TEST DELETE  ####################################

    def test_delete_not_existing_id(self) -> None:
        with self.assertRaises(ValueError):
            self.job_manager.delete(9999, force=False)

    def test_delete_existing_id_job_running_force_false(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        with self.assertRaises(Exception):
            self.job_manager.delete(1, force=False)

    def test_delete_existing_id_job_running_force_true(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.RUNNING, 'worker:1', datetime.now(), None, 10),
        ]
        self._add_jobs(init_jobs)

        self.job_manager.delete(1, force=True)

        self.assertEqual(self.repository.count_filtered([]), 1)

    def test_delete_existing_id_job_pending(self) -> None:
        init_jobs = [
            new_test_job(0, Group(id=0, name='test1'), cast(Repository, None), None, False,
                         0, None, None, None, 0),
            new_test_job(1, Group(id=0, name='test1'), cast(Repository, None), None, True,
                         Status.PENDING, None, None, None, 0),
        ]
        self._add_jobs(init_jobs)

        self.job_manager.delete(1, force=False)

        self.assertEqual(self.repository.count_filtered([]), 1)
