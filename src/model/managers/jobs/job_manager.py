# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from typing import Any, Dict, Generic, Iterable, List, Optional, TYPE_CHECKING, Tuple, Type, TypeVar, Union

from model.agents.agent import SharedAgentConfig
from model.jobs.status import Status
from repository import Filter, Operator
from repository.interfaces.job_repository import EmptyListError
from utils.utils import Utils
from model.jobs.job import Job

if TYPE_CHECKING:
    from model.jobs.group import Group
    from model.managers.jobs.status_strategies.status_strategy import StatusStrategy
    from repository.factory.repository_factory import RepositoryFactory
    from model.managers.group.group_manager import GroupManager


J = TypeVar('J', bound=Job)


class NoJobsError(Exception):
    pass


class JobManager(ABC, Generic[J]):

    def __init__(self, repository_factory: 'RepositoryFactory', group_manager: 'GroupManager', shared_config: Dict[str, SharedAgentConfig]):
        super().__init__()
        self.shared_config = shared_config
        self.repository_factory = repository_factory
        self.group_mgr = group_manager

    def add(self, **params: Any) -> J:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        job = self._create_job(**params)
        return job_repository.add(job)

    @abstractmethod
    def _job_type(self) -> Type[J]: ...

    @abstractmethod
    def _create_job(self, **params: Any) -> J: ...

    def get_jobs(self, group: Optional['Group'] = None, status: Optional[int] = None, enabled: Optional[bool] = None, order_by: str = 'id') -> Iterable[J]:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())

        filters = []
        if group:
            filters.append(Filter('group_id', Operator.EQUAL, group.id))
        if status:
            filters.append(
                Filter('status', Operator.EQUAL, status))
        if enabled is not None:
            filters.append(Filter('enabled', Operator.EQUAL, enabled))

        try:
            return job_repository.get_filtered(order_by=order_by, filters=filters)
        except EmptyListError:
            raise NoJobsError()

    def get_by_id(self, id: int) -> J:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        return job_repository.get_by_id(id)

    @abstractmethod
    def _status_list(
        self, group: 'Group') -> List['StatusStrategy']: ...

    # return map[group]map[key-str]value
    def status(self, group: Optional['Group'] = None) -> Dict[str, Dict[str, Any]]:
        groups = [group] if group is not None else self.group_mgr.list_groups()
        group_status: Dict[Group, List[StatusStrategy]] = {}
        for group in groups:
            try:
                status_list = self._status_list(group)
            except:
                continue
            group_status[group] = status_list
        return {group.name: {s.get_description(): s.compute(group) for s in lst} for group, lst in group_status.items()}

    def enable_by_id(self, id: int) -> J:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        return job_repository.update_by_id(id, enabled=True)
    
    def set_tag(self, id: int, tag: str) -> J:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        return job_repository.update_by_id(id, tag=tag)

    def enable_by_instance(self, instance: str) -> Iterable[J]:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        inst = self.repository_factory.get_instance_repository().get_by_name(instance)
        return job_repository.update_by_instance(inst.id, enabled=True)

    def disable_by_id(self, id: int) -> J:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        return job_repository.update_by_id(id, enabled=False)

    def disable_by_instance(self, instance: str) -> Iterable[J]:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        inst = self.repository_factory.get_instance_repository().get_by_name(instance)
        return job_repository.update_by_instance(inst.id, enabled=False)

    def reset(self, id: int) -> J:
        # TODO: check if the job is locked (eg prune or verify are running for a backup)
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        return job_repository.update_by_id(id, status=Status.PENDING, retries=0, started=None, finished=None, info=None, worker=None)

    def delete(self, id: int, force: bool) -> None:
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        if not force:
            # check if the job is not in pending status
            job = job_repository.get_by_id(id)
            if job.status != Status.PENDING:
                raise Exception(f'job with id {id} is not in pending status')
        job_repository.delete_by_id(id)

    def _get_node_pid(self, job: J) -> Tuple[str, int]:
        if not job.worker:
            return "", 0
        node, pid = job.worker.split(':', maxsplit=1)
        return node, int(pid)

    def kill(self, id: int) -> None:
        # TODO (gdelmont): contact the agent to kill the running job
        job_repository = self.repository_factory.get_repository_by_job(
            self._job_type())
        job = job_repository.get_by_id(id)

        if job.status != Status.RUNNING:
            raise Exception(f'job {job.id} is not running')

        host, pid = self._get_node_pid(job)
        cmd = f'ssh -q -o StrictHostKeyChecking=no root@{host} \"kill -9 {pid}\"'
        print(Utils.call_subprocess(cmd))
