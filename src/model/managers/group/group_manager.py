# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import List, TYPE_CHECKING
from repository.mysql.group_repository import GroupRepository

if TYPE_CHECKING:
    from model.jobs.group import Group


class GroupManager:

    def __init__(self, group_repository: GroupRepository):
        self._group_repository = group_repository

    def add(self, name: str) -> 'Group':
        """add a new group

        Parameters:
        name (str): name of the new group

        Returns:
        Group: new group added
        """
        return self._group_repository.add(name)

    def get_by_name(self, name: str) -> 'Group':
        """get the group by its name

        Parameters:
        name (str): name of the group

        Returns:
        Group: group by name
        """
        return self._group_repository.get_by_name(name)

    def remove(self, name: str) -> None:
        """remove a group only if it does not contain jobs

        Parameters:
        name (str): name of the group to remove

        """
        raise NotImplementedError

    def list_groups(self) -> List['Group']:
        """get the list of all the groups

        Returns:
        List: all groups
        """
        return self._group_repository.get_all()
