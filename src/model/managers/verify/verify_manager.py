# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from model.jobs.group import Group
from model.managers.group.group_manager import GroupManager
from model.managers.jobs.job_manager import JobManager, NoJobsError
from model.jobs.backup import Backup
from model.jobs.verify import Verify
from model.managers.jobs.status_strategies import TotalStatus, CompletedStatus, PendingStatus, RunningStatus, FailedStatus, NotEnabledStatus, CoverageStatus, RescheduledStatus
from typing import TYPE_CHECKING, Any, Iterable, List, Optional, Type, Dict
from model.agents.agent import SharedAgentConfig
from repository.interfaces.job_repository import EmptyListError, Filter, Operator

from ..utils import utils

if TYPE_CHECKING:
    from repository.factory.repository_factory import RepositoryFactory
    from model.managers.backup.backup_manager import BackupManager
    from model.managers.jobs.status_strategies.status_strategy import StatusStrategy


class VerifyManager(JobManager[Verify]):
    """The VerifyManager class is responsible to manage the verify jobs (adding, removing, see the status,
    enable, disable, etc...) from the system.

    """

    def __init__(self, repository_factory: 'RepositoryFactory', group_manager: GroupManager, backup_manager: 'BackupManager', shared_config: Dict[str, SharedAgentConfig]):
        super().__init__(repository_factory, group_manager, shared_config)
        self.verify_repository = repository_factory.get_verify_repository()
        self.backup_mgr = backup_manager

    def _job_type(self) -> Type['Verify']:
        return Verify

    def _create_job(self, **params: Any) -> 'Verify':

        # check missing parameters
        missing = utils.missing_parameters({'backup_id', 'enabled'}, **params)
        if missing:
            raise Exception(f'missing parameters {missing}')

        backup_id: int = params['backup_id']

        backup: Backup = self.backup_mgr.get_by_id(backup_id)
        if not backup:
            raise Exception(f'the backup with id {backup_id} does not exists')

        enabled: bool = params['enabled']

        return Verify.new(
            backup=backup,
            enabled=enabled
        )

    def _status_list(self, group: Optional[Group]) -> List['StatusStrategy']:
        if group:
            expiration_time = self.shared_config[group.name].verify_expiration_time
        else:
            expiration_time = 24*60*60

        return [
            TotalStatus(self.verify_repository),
            CompletedStatus(self.verify_repository),
            PendingStatus(self.verify_repository),
            RescheduledStatus(self.verify_repository),
            RunningStatus(self.verify_repository),
            FailedStatus(self.verify_repository),
            NotEnabledStatus(self.verify_repository),
            CoverageStatus(self.verify_repository, expiration_time)
        ]

    def get_by_backup_id(self, backup_id: int) -> Iterable['Verify']:
        try:
            return self.verify_repository.get_filtered(
                order_by='id', filters=[Filter('backup_id', Operator.EQUAL, backup_id)])
        except EmptyListError:
            raise NoJobsError
