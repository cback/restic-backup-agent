# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from model.jobs.group import Group
from model.managers.group.group_manager import GroupManager
from model.managers.jobs.job_manager import JobManager, NoJobsError
from typing import TYPE_CHECKING, Any, Dict, Iterable, List, Optional, Type, Union
from model.managers.jobs.status_strategies import TotalStatus, CompletedStatus, PendingStatus, RunningStatus, FailedStatus, CoverageStatus, RescheduledStatus

from model.jobs.restore import Restore
from model.jobs.backup import Backup
from model.agents.agent import SharedAgentConfig
from model.restic.restic import Restic
from repository.interfaces.job_repository import EmptyListError, Filter, Operator
from ..utils import utils
from model.restic.restic import Restic, Snapshot
from model.agents.utils import get_credentials


if TYPE_CHECKING:
    from model.managers.jobs.status_strategies.status_strategy import StatusStrategy
    from repository.factory.repository_factory import RepositoryFactory
    from model.managers.backup.backup_manager import BackupManager


class RestoreManager(JobManager[Restore]):
    """The RestoreManager class is responsible to manage the restore jobs (adding, removing, see the status,
    enable, disable, etc...) from the system.

    """

    def __init__(self, repository_factory: 'RepositoryFactory', group_manager: GroupManager, backup_manager: 'BackupManager', shared_config: Dict[str, SharedAgentConfig]):
        super().__init__(repository_factory, group_manager, shared_config)
        self.restore_repository = repository_factory.get_restore_repository()
        self.backup_mgr = backup_manager

    def _job_type(self) -> Type['Restore']:
        return Restore

    def _get_restic(self, backup: Backup) -> Restic:
        repository = backup.repository
        config = self.shared_config[backup.group.name]
        password = get_credentials(config, repository)
        env = config.destination_env
        options = config.destination_options
        return Restic(repository.path, password, env=env, options=options)

    def _date_eq(self, d1: datetime, d2: datetime) -> bool:
        return d1.year == d2.year and d1.month == d2.month and d1.day == d2.day and \
            d1.hour == d2.hour and d1.minute == d2.minute and d1.second == d2.second

    def _get_snapshot_from_date(self, backup: Backup, date: datetime) -> Snapshot:
        restic = self._get_restic(backup)
        for snapshot in restic.snapshots(path=[backup.source]):
            if self._date_eq(snapshot.time, date):
                return snapshot
        raise ValueError(f'snapshot does not exist in {date}')

    def _create_job(self, **params: Any) -> 'Restore':

        # check missing parameters
        missing = utils.missing_parameters(
            {'backup_id', 'pattern', 'enabled'}, **params)
        if missing:
            raise Exception(f'missing parameters {missing}')

        backup_id: int = params['backup_id']

        backup: Backup = self.backup_mgr.get_by_id(backup_id)
        if not backup:
            raise Exception(f'the backup with id {backup_id} does not exists')

        enabled: bool = params['enabled']
        pattern: str = params['pattern']

        # optional parameters
        destination: Optional[str] = params.get('destination', None)
        username: Optional[str] = params.get('username', None)

        snapshot: Optional[str] = params.get('snapshot', None)
        if not snapshot:
            date: Union[datetime, str] = params['date']
            if isinstance(date, str):
                date = datetime.fromisoformat(date)
            snapshot = self._get_snapshot_from_date(backup, date).short_id
        
        trim_source: bool = params.get('trim_source', False)
        pretrim_scratch_path: Optional[str] = params.get('pretrim_scratch_path', None)

        return Restore.new(
            backup=backup,
            username=username,
            snapshot=snapshot,
            pattern=pattern,
            destination=destination,
            enabled=enabled,
            trim_source=trim_source,
            pretrim_scratch_path=pretrim_scratch_path
        )

    def _status_list(self, _: Group) -> List['StatusStrategy']:
        return [
            TotalStatus(self.restore_repository),
            PendingStatus(self.restore_repository),
            RescheduledStatus(self.restore_repository),
            RunningStatus(self.restore_repository),
            CompletedStatus(self.restore_repository),
            FailedStatus(self.restore_repository),
            CoverageStatus(self.restore_repository, None),
        ]

    def get_by_backup_id(self, backup_id: int) -> Iterable['Restore']:
        try:
            return self.restore_repository.get_filtered(
                order_by='id', filters=[Filter('backup_id', Operator.EQUAL, backup_id)])
        except EmptyListError:
            raise NoJobsError
