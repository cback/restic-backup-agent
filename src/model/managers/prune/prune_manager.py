# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from model.jobs.group import Group
from model.managers.group.group_manager import GroupManager
from model.managers.jobs.job_manager import JobManager, NoJobsError
from model.jobs.backup import Backup
from model.jobs.prune import Prune
from model.managers.jobs.status_strategies import TotalStatus, CompletedStatus, PendingStatus, RunningStatus, FailedStatus, NotEnabledStatus, CoverageStatus, RescheduledStatus
from typing import TYPE_CHECKING, Any, Iterable, List, Type, Dict
from model.agents.agent import SharedAgentConfig
from repository.interfaces.job_repository import EmptyListError, Filter, Operator

from ..utils import utils

if TYPE_CHECKING:
    from repository.factory.repository_factory import RepositoryFactory
    from model.managers.backup.backup_manager import BackupManager
    from model.managers.jobs.status_strategies.status_strategy import StatusStrategy


class PruneManager(JobManager[Prune]):
    """The PruneManager class is responsible to manage the prune jobs (adding, removing, see the status,
    enable, disable, etc...) from the system.

    """

    def __init__(self, repository_factory: 'RepositoryFactory', group_manager: GroupManager, backup_manager: 'BackupManager', shared_config: Dict[str, SharedAgentConfig]):
        super().__init__(repository_factory, group_manager, shared_config)
        self.prune_repository = repository_factory.get_prune_repository()
        self.backup_mgr = backup_manager

    def _job_type(self) -> Type['Prune']:
        return Prune

    def _create_job(self, **params: Any) -> 'Prune':

        # check missing parameters
        missing = utils.missing_parameters({'backup_id', 'enabled'}, **params)
        if missing:
            raise Exception(f'missing parameters {missing}')

        backup_id: int = params['backup_id']

        backup: Backup = self.backup_mgr.get_by_id(backup_id)
        if not backup:
            raise Exception(f'the backup with id {backup_id} does not exists')

        enabled: bool = params['enabled']

        return Prune.new(
            backup=backup,
            enabled=enabled
        )

    def _status_list(self, group: Group) -> List['StatusStrategy']:
        expiration_time = self.shared_config[group.name].prune_expiration_time

        return [
            TotalStatus(self.prune_repository),
            CompletedStatus(self.prune_repository),
            PendingStatus(self.prune_repository),
            RescheduledStatus(self.prune_repository),
            RunningStatus(self.prune_repository),
            FailedStatus(self.prune_repository),
            NotEnabledStatus(self.prune_repository),
            CoverageStatus(self.prune_repository, expiration_time)
        ]

    def get_by_backup_id(self, backup_id: int) -> Iterable['Prune']:
        try:
            return self.prune_repository.get_filtered(
                order_by='id', filters=[Filter('backup_id', Operator.EQUAL, backup_id)])
        except EmptyListError:
            raise NoJobsError
