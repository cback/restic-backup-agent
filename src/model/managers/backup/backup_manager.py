# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import time
from typing import Any, Dict, Iterable, List, TYPE_CHECKING, Optional, Type
from model.agents.agent import SharedAgentConfig
from model.jobs.group import Group
from model.jobs.status import Status

from model.managers.group.group_manager import GroupManager
from model.managers.jobs.job_manager import JobManager, NoJobsError
from model.managers.jobs.status_strategies import TotalStatus, CompletedStatus, PendingStatus, RunningStatus, FailedStatus, NotEnabledStatus, ArchivedStatus, CoverageStatus, RescheduledStatus
from repository.factory.repository_factory import RepositoryFactory
from model.jobs.backup import Backup
from repository import Filter, Operator
from repository.interfaces.job_repository import EmptyListError
from utils.boto import Boto
from model.config.logs import get_logger
from model.restic.restic import Restic, ResticError, Snapshot
from model.agents.utils import get_credentials

if TYPE_CHECKING:
    from model.managers.jobs.status_strategies.status_strategy import StatusStrategy


class BackupManager(JobManager[Backup]):
    """The BackupManager class is responsible to manage the backup job (adding, removing, see the status,
    enable, disable, etc...) from the system.

    """

    def __init__(self, repository_factory: RepositoryFactory, group_manager: GroupManager, shared_config: Dict[str, SharedAgentConfig]):
        super().__init__(repository_factory, group_manager, shared_config)
        self.backup_repository = self.repository_factory.get_backup_repository()
        self.shared_config = shared_config

        self.log = get_logger()

    def _job_type(self) -> Type['Backup']:
        return Backup

    def _status_list(self, group: Group) -> List['StatusStrategy']:
        expiration_time = self.shared_config[group.name].backup_expiration_time

        return [
            TotalStatus(self.backup_repository),
            CompletedStatus(self.backup_repository),
            PendingStatus(self.backup_repository),
            RescheduledStatus(self.backup_repository),
            RunningStatus(self.backup_repository),
            FailedStatus(self.backup_repository),
            NotEnabledStatus(self.backup_repository),
            ArchivedStatus(self.backup_repository),
            CoverageStatus(self.backup_repository, expiration_time)
        ]

    def _create_job(self, **params: Any) -> 'Backup':
        """create a new backup job.

        The name is used to construct the destination path in wich do the backup, using the group configuration.
        In particular are used the following parameters in the group configuration:
          - general.destination_type: type of the destination (for now only ceph). # TODO (gdelmont): specify other destination types (all the one supported by restic)
          - general.s3_endpoint: endpoint url (used when the destination_type is ceph)
          - general.bucket_prefix: template string used to create the destination bucket prefix # TODO (gdelmont): rename this property, too much related to ceph
          - ... (work in progress)

        Parameters:
        group (str): the group name in which add the new backup job
        name (str): name of the backup
        source (str): source path to backup
        enabled (bool): specifies if the backup job is enabled to be run
        force (str): if true force to run the job
        instance (str): instance in which the source of the backup belongs
        pinned_node (Optional[str]): node in which force execution of backup job
        repository (Optional[str]): location of restic repository (override the destination path build by the config)

        Returns:
        Backup: the newly created Backup job
        """

        # checking mandatory params
        missings = {'group', 'name', 'source',
                    'enabled', 'instance'}.difference(params)
        if missings:
            raise Exception('missing parameters ' + str(missings))

        # get instance
        instance_repository = self.repository_factory.get_instance_repository()
        instance: str = params['instance']

        # TODO(gdelmont): pass a variable create_instace: bool and if true
        # create the instance, otherwise raise an exception
        try:
            inst = instance_repository.get_by_name(instance)
        except ValueError:
            # if the instance does not exists, create it
            inst = instance_repository.add(instance)

        # get the group config
        group_name: str = params['group']
        group = self.group_mgr.get_by_name(group_name)
        group_config = self.shared_config[group.name]

        # TODO(gdelmont): change config dict in somethin less specific to ceph
        name: str = params['name']
        dest_config = group_config.destination_config or {}
        dest_config['bucket_prefix'] = group_config.bucket_prefix

        destination: Optional[str] = params.get('repository', None)
        if not destination:
            destination = self._get_destination(
                group_config.destination_type, name, dest_config)

        source: str = params['source']
        enabled: bool = params['enabled']

        # optional args
        pinned_node: Optional[str] = params.get('pinned_node', None)
        force: bool = params.get('force', False)
        username: Optional[str] = params.get('username', None)

        return Backup.new(group, name, username, source, destination, inst, Status.PENDING, pinned_node, force, enabled)

    def _get_destination(self, destination_type: str, name: str, config: Dict[str, str]) -> str:
        if destination_type == 'ceph':
            return f"{config['s3_endpoint']}/{config['bucket_prefix']}-{name}"
        else:
            raise ValueError('destination type not recognised')

    def get_by_name(self, name: str) -> Iterable['Backup']:
        try:
            return self.backup_repository.get_filtered(order_by='id', filters=[Filter('name', Operator.EQUAL, name)])
        except EmptyListError:
            raise NoJobsError()

    def archive(self, id: int) -> 'Backup':
        return self.backup_repository.update_by_id(id, status=Status.ARCHIVED)

    def delete(self, id: int, force: bool, *, delete_job: bool = False, delete_repository: bool = False) -> None:
        # TODO (gdelmont): check if the backup is locked
        backup = self.get_by_id(id)
        shared_config = self.shared_config[backup.group.name]
        if delete_repository:
            # TODO (gdelmont): delete destination depending on the destination_type
            self._delete_bucket(
                id, shared_config.destination_env, backup.repository.path)
        # TODO (gdelmont): if delete fails, set backup status to failed and don't remove entry in db
        if delete_job:
            super().delete(id, force)

    # s3:https://s3-fr-prevessin-1.cern.ch/<bucket-name>
    def _get_url(self, path: str) -> str:
        protocol, _, host, _ = path.split('/', maxsplit=3)
        try:
            i = protocol.index(':')
            protocol = protocol[i+1:]
        except ValueError:
            pass
        return f'{protocol}//{host}'

    def _get_bucket_name(self, path: str) -> str:
        _, _, _, bucket, *_ = path.split('/', maxsplit=4)
        return bucket

    def _delete_bucket(self, id: int, env: Dict[str, str], path: str) -> None:
        url = self._get_url(path)
        s3client = Boto(env, url)

        bucket_name = self._get_bucket_name(path)
        if s3client.bucket_exists(bucket_name):
            try:
                s3client.bucket_delete(bucket_name)
            except:
                self.log.exception(
                    f"[BACKUP id={id}] Error deleting {bucket_name} bucket")
            else:
                self.log.info(
                    f"[BACKUP id={id}] Bucket {bucket_name} deleted successfully")
        else:
            self.log.error(
                f"[BACKUP id={id}] Bucket {bucket_name} does not exist")

    def modify(self, id: int, *, repository: Optional[str] = None, source: Optional[str] = None, pinned_node: Optional[str] = None, desired_start_time: Optional[str] = None, group: Optional[str] = None) -> 'Backup':
        params: Dict[str, Any] = {}
        if repository:
            params['repository'] = repository
        if source:
            params['source'] = source
        if pinned_node == "":
            params['pinned_node'] = None
        elif pinned_node:
            params['pinned_node'] = pinned_node
        if desired_start_time:
            # check if the desired start time is a correct cronjob value
            try:
                from cron_converter import Cron
                Cron(desired_start_time)
            except ValueError:
                raise ValueError(
                    f"desired start time \"{desired_start_time}\" is not a cronjob value")
            params['desired_start_time'] = desired_start_time
        if group:
            g = self.group_mgr.get_by_name(group)
            params['group_id'] = g.id

        return self.backup_repository.update_by_id(id, **params)

    def snapshots(self, id: int) -> List[Snapshot]:
        restic = self.get_restic(id)
        try:
            return restic.snapshots()
        except ResticError as e:
            if 'Is there a repository at the following location?' in str(e):
                return []
            raise e

    def get_restic(self, id: int) -> Restic:
        backup: Backup = self.backup_repository.get_by_id(id)
        group_config = self.shared_config[backup.group.name]

        password = get_credentials(group_config, backup.repository)

        return Restic(backup.repository.path, password,
                      env=group_config.destination_env)
