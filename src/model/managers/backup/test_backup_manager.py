# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import unittest
from unittest.mock import patch
from .backup_manager import BackupManager
from model.managers.group.group_manager import GroupManager
from utils.test.repo import MockInMemoryRepositoryFactory, InMemoryGroupRepository
from model.agents.agent import SharedAgentConfig


class TestBackupManager(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        self.repository_factory = MockInMemoryRepositoryFactory()  # type: ignore
        self.groups = ['test1', 'test2']
        self.group_manager = GroupManager(InMemoryGroupRepository(self.groups))
        self.instance_repository = self.repository_factory.get_instance_repository()

    def tearDown(self) -> None:
        return super().tearDown()

    ####################################  TEST _create_job  ####################################

    def test_create_job_check_if_are_provided_manadatory_parameters(self) -> None:
        self.backup_manager = BackupManager(
            self.repository_factory, self.group_manager, {})

        # missing group
        with self.assertRaises(Exception):
            self.backup_manager.add(
                name='test', source='/path/to/folder', enabled=True, instance='something')

        # missing name
        with self.assertRaises(Exception):
            self.backup_manager.add(
                group='test1', source='/path/to/folder', enabled=True, instance='something')

        # missing source
        with self.assertRaises(Exception):
            self.backup_manager.add(
                group='test1', name='test', enabled=True, instance='something')

        # missing enabled
        with self.assertRaises(Exception):
            self.backup_manager.add(
                group='test1', name='test', source='/path/to/folder', instance='something')

        # missing instance
        with self.assertRaises(Exception):
            self.backup_manager.add(
                group='test1', name='test', source='/path/to/folder', enabled=True)

    def test_create_job_create_instance_if_not_exists(self) -> None:
        group_name = self.groups[0]
        shared_config = {
            group_name: SharedAgentConfig(
                destination_config={
                    's3_endpoint': 'url://example.org'
                },
                bucket_prefix='test-prefix',
                destination_type='ceph'
            )
        }

        self.backup_manager = BackupManager(
            self.repository_factory, self.group_manager, shared_config)

        missing_instance = 'instance'

        # ensure that the instance is not present in the repository
        with self.assertRaises(ValueError):
            self.instance_repository.get_by_name(missing_instance)

        self.backup_manager._create_job(group=group_name, name='backup_test',
                                        source='/path/to/res', enabled=False, instance=missing_instance)

        self.assertIsNotNone(
            self.instance_repository.get_by_name(missing_instance), 'the istance should be created when creating the new job')
