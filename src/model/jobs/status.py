# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
class Status:
    """The Status class defines some constant values which represent the status of a job,
    during its life cycle.

    """

    PENDING = 1
    RUNNING = 2
    COMPLETED = 3
    FAILED = 4
    ARCHIVED = 5
    RESCHEDULED = 6

    @staticmethod
    def to_str(status: int) -> str:
        if status == Status.PENDING:
            return 'pending'
        elif status == Status.RUNNING:
            return 'running'
        elif status == Status.COMPLETED:
            return 'completed'
        elif status == Status.FAILED:
            return 'failed'
        elif status == Status.ARCHIVED:
            return 'archived'
        elif status == Status.RESCHEDULED:
            return 'rescheduled'
        else:
            raise ValueError('status not recognised')

    @staticmethod
    def from_str(status: str) -> int:
        if status == 'pending':
            return Status.PENDING
        elif status == 'running':
            return Status.RUNNING
        elif status == 'completed':
            return Status.COMPLETED
        elif status == 'failed':
            return Status.FAILED
        elif status == 'archived':
            return Status.ARCHIVED
        elif status == 'rescheduled':
            return Status.RESCHEDULED
        else:
            raise ValueError('status not recognised')
