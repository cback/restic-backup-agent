# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional

from dataclasses import dataclass
from model.jobs.group import Group
from model.jobs.job import Job, Repository
from repository.interfaces.instance_repository import Instance


@dataclass
class Backup(Job):
    name: str
    source: str
    pinned_node: Optional[str]
    force: bool
    instance: Instance
    desired_start_time: Optional[str]
    tag: Optional[str]
    progress: float
    bytes_total: int
    data_added: int
    dirs_changed: int
    dirs_new: int
    dirs_unmodified: int
    files_changed: int
    files_new: int
    files_total: int
    files_unmodified: int
    file_errors: int

    @staticmethod
    def new(group: Group, name: str, username: Optional[str], source: str, destination: str, instance: Instance, status: int, pinned_node: Optional[str], force: bool, enabled: bool) -> 'Backup':
        return Backup(
            id=-1,
            group=group,
            username=username,
            name=name,
            repository=Repository(path=destination, password=''),
            status=status,
            worker=None,
            started=None,
            finished=None,
            source=source,
            enabled=enabled,
            pinned_node=pinned_node,
            instance=instance,
            desired_start_time=None,
            force=force,
            info=None,
            tag=None,
            retries=0,
            stats={},
            running_expiration=None,
            bytes_total=0,
            data_added=0,
            dirs_changed=0,
            dirs_new=0,
            dirs_unmodified=0,
            files_changed=0,
            files_new=0,
            files_total=0,
            files_unmodified=0,
            file_errors=0,
            progress=0.0
        )
