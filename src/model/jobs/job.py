# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import Any, Dict, Optional
from dataclasses import dataclass
from model.jobs.group import Group


@dataclass
class Repository:
    # represents where a restic repository is located
    path: str
    password: Optional[str]


@dataclass
class Job:
    id: int         # job's id
    group: Group    # group in which the job belongs to
    repository: Repository  # restic repository
    username: Optional[str]
    enabled: bool   # whether the job is enabled or not
    status: int     # the status of the job
    worker: Optional[str]   # worker of the job (agent:pid)
    # when the job started (if is not in PENDING status)
    started: Optional[datetime]
    # when the job finished (if is in COMPLETE or FAILED status)
    finished: Optional[datetime]
    # expiration time after that a running job is considered dead
    running_expiration: Optional[datetime]
    info: Optional[str]  # info of the runned job
    retries: int    # how many retries the job had to complete
    # stats dict filled after calling the run() method
    stats: Dict[str, Any]

    @property
    def type(self) -> str:
        return type(self).__name__.lower()
