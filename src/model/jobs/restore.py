# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Optional
from datetime import datetime

from dataclasses import dataclass
from model.jobs.job import Job
from model.jobs.backup import Backup
from model.jobs.status import Status


@dataclass
class Restore(Job):
    backup: Backup
    snapshot: str
    destination: Optional[str]
    pattern: str
    created: datetime
    progress: float
    trim_source: bool = False
    pretrim_scratch_path: Optional[str] = None

    @staticmethod
    def new(backup: Backup, username: Optional[str], snapshot: str, pattern: str, destination: Optional[str], enabled: bool, trim_source: bool = False, pretrim_scratch_path: Optional[str] = None) -> 'Restore':
        return Restore(
            id=-1,
            group=backup.group,
            username=username,
            repository=backup.repository,
            enabled=enabled,
            status=Status.PENDING,
            worker=None,
            started=None,
            finished=None,
            info=None,
            retries=0,
            stats={},
            running_expiration=None,
            created=datetime.now(),
            backup=backup,
            snapshot=snapshot,
            destination=destination,
            pattern=pattern,
            progress=0.0,
            pretrim_scratch_path=pretrim_scratch_path,
            trim_source=trim_source
        )
