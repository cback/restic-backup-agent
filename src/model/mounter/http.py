# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import base64
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
import os
import secrets
import socket
from shutil import copyfileobj
from socketserver import ThreadingMixIn
import threading
from typing import Any, Dict, List, Type
from .mounter import Mount, MountEndpoint, MountServer
from datetime import datetime
import urllib.parse

import json
import stat


class HTTPMounter(MountServer):

    def __init__(self, _: Dict[str, Any]) -> None:
        pass

    def serve(self, user: str, path: str) -> Mount:
        m = HTTPMountServer(user, path)
        m.serve()
        return m


class HTTPMountServer(Mount, ThreadingMixIn, HTTPServer):

    @staticmethod
    def _generate_password() -> str:
        return secrets.token_hex(32)

    def __init__(self, user: str, path: str) -> None:
        self.pw = HTTPMountServer._generate_password()
        self.user = user
        self.url = socket.gethostname()
        self._last_request = datetime.now()

        addr = ('', 0)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(addr)

        super().__init__(addr, mount_handler(self.user, self.pw, path), False)
        self.socket = sock

        self.port = sock.getsockname()[1]

    def serve(self) -> None:
        self.server_activate()
        t = threading.Thread(target=self.serve_forever)
        t.daemon = True
        t.start()

    @staticmethod
    def _encode_credentials(user: str, password: str) -> str:
        return base64.b64encode(bytes(f'{user}:{password}', 'utf-8')).decode('ascii')

    @property
    def endpoint(self) -> MountEndpoint:
        return MountEndpoint(
            url=f'{self.url}:{self.port}',
            credentials=HTTPMountServer._encode_credentials(
                self.user, self.pw),
            protocol="http"
        )

    @property
    def last_request(self) -> datetime:
        return self._last_request

    @last_request.setter
    def last_request(self, value: datetime) -> None:
        self._last_request = value

    def close(self) -> None:
        super().close()


def mount_handler(username: str, password: str, base_folder: str) -> Type:
    key = base64.b64encode(
        bytes(f'{username}:{password}', 'utf-8')).decode('ascii')

    class MountHandler(BaseHTTPRequestHandler):

        def _credentials_correct(self) -> bool:
            # basic authentication
            auth = self.headers.get('Authorization')
            return auth != None and str(auth) == f'Basic {key}'

        def _send_unauthorized(self) -> None:
            self.send_response(HTTPStatus.UNAUTHORIZED)
            self.end_headers()

        def _stat(self, path: str) -> Dict[str, Any]:
            s = os.stat(path)
            return {
                'name': os.path.basename(path),
                'atime': s.st_atime,
                'mtime': s.st_mtime,
                'ctime': s.st_ctime,
                'size': s.st_size,
                'ino': s.st_ino,
                'type': 'dir' if stat.S_ISDIR(s.st_mode) else 'file',
            }

        def handle_one_request(self) -> None:
            if isinstance(self.server, HTTPMountServer):
                self.server.last_request = datetime.now()
            super().handle_one_request()

        def _send_not_found(self) -> None:
            self.send_response(HTTPStatus.NOT_FOUND)
            self.end_headers()

        def _send_bad_request(self) -> None:
            self.send_response(HTTPStatus.BAD_REQUEST)
            self.end_headers()

        def do_POST(self) -> None:
            # get the content of a folder
            if not self._credentials_correct():
                self._send_unauthorized()
                return

            path = urllib.parse.unquote(self.path)

            p = os.path.join(base_folder, make_relative(path))
            res: List[Dict[str, Any]] = []

            try:
                for filename in os.scandir(p):
                    res.append(self._stat(filename.path))
            except FileNotFoundError:
                self._send_not_found()
                return
            except NotADirectoryError:
                self._send_bad_request()
                return

            self.send_response(HTTPStatus.OK)
            self.send_header('Content-type', 'application/json')
            self.end_headers()

            self.wfile.write(json.dumps(res).encode('utf-8'))

        def do_GET(self) -> None:

            if not self._credentials_correct():
                self._send_unauthorized()
                return

            path = urllib.parse.unquote(self.path)

            # stat file
            if self.headers.get('x-request') == 'stat':
                p = os.path.join(base_folder, make_relative(path))
                try:
                    stat = self._stat(p)
                except FileNotFoundError:
                    self._send_not_found()
                    return

                self.send_response(HTTPStatus.OK)
                self.send_header('Content-type', 'application/json')
                self.end_headers()

                self.wfile.write(json.dumps(stat).encode('utf-8'))
                return

            # get the file content
            p = os.path.join(base_folder, make_relative(path))
            print(p)
            try:
                with open(p, 'rb') as f:
                    self.send_response(HTTPStatus.OK)
                    self.send_header(
                        'Content-type', 'application/octet-stream')
                    self.end_headers()
                    copyfileobj(f, self.wfile)
            except FileNotFoundError:
                self._send_not_found()
                return
            except IsADirectoryError:
                self._send_bad_request()
                return

    return MountHandler


def make_relative(path: str) -> str:
    if len(path) == 0:
        return ""
    if path[0] == "/":
        return path[1:]
    return path
