# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from fastapi import FastAPI, HTTPException, status, Depends
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from pydantic import BaseModel
from model.mounter.mounter import MountService, Config
import settings
from model.cback import CBack
from typing import Optional
from model.mounter.errors import NotMountedError


class ServerConfig(Config):
    api_token: str


cback = CBack(settings.CBACK_CONFIG)
app = FastAPI()
config = ServerConfig.parse_obj(cback.config.get("mounter", {}))
backup_mgr = cback.get_backup_manager()

mount_service = MountService(config, backup_mgr)


class MountOut(BaseModel):
    endpoint: str
    credentials: str
    protocol: str


def _check_api_token(bearer_credentials: Optional[HTTPAuthorizationCredentials] = Depends(HTTPBearer(auto_error=False))) -> None:
    if not bearer_credentials or bearer_credentials.credentials != config.api_token:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={
                "WWW-Authenticate": "Bearer"
            },
        )


@ app.get("/mount", dependencies=[Depends(_check_api_token)])
def mount(id: int) -> MountOut:
    m = mount_service.mount(id)
    return MountOut(
        endpoint=m.url,
        protocol=m.protocol,
        credentials=m.credentials
    )


@ app.delete("/mount", dependencies=[Depends(_check_api_token)])
def umount(id: int) -> None:
    try:
        mount_service.umount(id)
    except NotMountedError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Not mounted"
        )
