# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod, abstractproperty
from dataclasses import dataclass
from datetime import datetime
from time import sleep
from typing import Any, Callable, Dict, List, Tuple

from pydantic import BaseModel
from model.managers.backup.backup_manager import BackupManager
from model.restic.restic import ResticMount
from .errors import NotMountedError
from threading import Lock, Thread


@dataclass
class MountEndpoint:
    url: str
    credentials: str
    protocol: str


class Mount(ABC):

    @abstractproperty
    def endpoint(self) -> MountEndpoint: ...

    @abstractproperty
    def last_request(self) -> datetime: ...

    @abstractmethod
    def close(self) -> None: ...


class MountServer(ABC):
    """The MountServer class is responsible of serving a path"""
    @abstractmethod
    def serve(self, user: str, path: str) -> Mount: ...


NewMountServerFunc = Callable[[Dict[str, Any]], MountServer]

_mounters: Dict[str, NewMountServerFunc] = {}


def register(name: str, new: NewMountServerFunc) -> None:
    _mounters[name] = new


def new(driver: str, config: Dict[str, Any]) -> MountServer:
    return _mounters[driver](config)


class Config(BaseModel):
    protocol: str
    protocols: Dict[str, Dict[str, Any]] = {}
    timeout: int = 300


class MountService:

    def __init__(self, config: Config, backup_mgr: BackupManager) -> None:
        self.config = config
        self.mounter = new(self.config.protocol,
                           self.config.protocols.get(self.config.protocol, {}))
        self.backup_mgr = backup_mgr
        self._mounts: Dict[int, Tuple[ResticMount, Mount]] = {}
        self._mutex = Lock()  # to access the _mounts dict

        self._run_watcher()

    def _run_watcher(self) -> None:
        # will spawn a thread that will watch all the mount
        # and will umount the ones that are not active anymore

        def is_expired(mount: Mount) -> bool:
            d = datetime.now() - mount.last_request
            return d.total_seconds() > self.config.timeout

        def kill_expired() -> None:
            while True:
                to_remove: List[int] = []
                with self._mutex:
                    for id, (restic_mount, mount) in self._mounts.items():
                        if is_expired(mount):
                            restic_mount.umount()
                            mount.close()
                            to_remove.append(id)

                    for id in to_remove:
                        del self._mounts[id]
                sleep(10)

        t = Thread(target=kill_expired)
        t.daemon = True
        t.start()

    def mount(self, id: int) -> MountEndpoint:
        with self._mutex:
            if id in self._mounts:
                return self._mounts[id][1].endpoint

            backup = self.backup_mgr.get_by_id(id)
            restic = self.backup_mgr.get_restic(id)

            restic_mount = restic.mount()
            path = restic_mount.mount()

            mount_server = self.mounter.serve(backup.name, path)

            self._mounts[id] = (restic_mount, mount_server)

        return mount_server.endpoint

    def umount(self, id: int) -> None:
        if id not in self._mounts:
            raise NotMountedError

        with self._mutex:
            restic_mount, mount = self._mounts[id]
            restic_mount.umount()
            mount.close()

            del self._mounts[id]
