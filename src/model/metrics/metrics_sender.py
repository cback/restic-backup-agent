# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from model.config.logs import get_logger
from typing import Any, Dict, Type
from dacite import from_dict

from dataclasses import dataclass, field
from model.agents.enqueued_job_runner.events import FinishedJobProcessing

from utils.pub_sub import Subscriber
from model.agents.agent import AgentConfig


@dataclass(frozen=True)
class Config:
    driver: str = "carbonplain"
    drivers: Dict[str, Dict[str, Any]] = field(default_factory=dict)


class MetricsSender(ABC):

    @abstractmethod
    def __init__(self, config: Dict[str, Any]) -> None:
        pass

    @abstractmethod
    def publish_metrics(self, metrics: Dict[str, Any], template: str, **kwargs: Any) -> None:
        pass


_metrics_register: Dict[str, Type[MetricsSender]] = {}


def register(name: str, driver: Type[MetricsSender]) -> None:
    _metrics_register[name] = driver


def new(config: Dict[str, Any]) -> MetricsSender:
    c = from_dict(data_class=Config, data=config)
    impl = _metrics_register[c.driver]
    config_driver = c.drivers[c.driver]
    return impl(config_driver)


class JobMetricsSender(Subscriber):

    def __init__(self, agent_config: AgentConfig, driver: MetricsSender) -> None:
        self.agent_config = agent_config
        self.driver = driver
        self.log = get_logger()

    def notify(self, event: object) -> None:
        from socket import gethostname
        if not isinstance(event, FinishedJobProcessing):
            return
        job = event.job
        shared_config = self.agent_config.shared[job.group.name]
        if not shared_config.send_metrics:
            # do not send metrics
            self.log.debug('metrics not sent: disabled in config',
                           extra={'job': job.id, 'type': job.type})
            return
        host = gethostname().split('.')[0]
        self.driver.publish_metrics(
            event.stats, self.agent_config.metrics_template, job=job, host=host)
