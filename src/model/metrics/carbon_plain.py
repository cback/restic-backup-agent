# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict
from dacite import from_dict

from dataclasses import dataclass
from time import time
from model.metrics.metrics_sender import MetricsSender
import socket
from jinja2 import Template
from model.config.logs import get_logger


@dataclass(frozen=True)
class Config:
    host: str = ''
    port: int = 0
    timeout: int = 30
    max_retry: int = 1


class CarbonPlain(MetricsSender):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = from_dict(data_class=Config, data=config)
        self.log = get_logger()

    def publish_metrics(self, metrics: Dict[str, Any], template: str, **kwargs: Any) -> None:
        t = Template(template)
        with socket.create_connection(
            address=(self.config.host, self.config.port),
            timeout=self.config.timeout
        ) as client:
            # TODO (gdelmont): add retry mechanism
            timestamp = time()
            for metric, value in metrics.items():
                key = t.render(metric_name=metric, **kwargs)

                try:
                    client.send(f'{key} {value} {timestamp}\n'.encode('utf-8'))
                except:
                    self.log.exception('error sending metrics', extra={
                        'metric': key, 'value': value, 'port': self.config.port})
                else:
                    self.log.debug('sending metrics', extra={
                        'metric': key, 'value': value, 'port': self.config.port})
