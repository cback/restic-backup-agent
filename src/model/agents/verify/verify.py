# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland
# all rights reserved
#
# cback - CERN Backup Orchestrator
#
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see <https://www.gnu.org/licenses/>.


import os
from typing import Any, Dict, Optional

from dacite import from_dict
from model.agents.agent import SharedAgentConfig
from model.agents.enqueued_job_runner.enqueued_job_runner import EnqueuedJob
from model.jobs.group import Group
from model.jobs.verify import Verify
from model.restic.restic import Restic
from model.config.logs import get_logger
from repository.factory.repository_factory import RepositoryFactory
from utils.utils import Utils
from dataclasses import dataclass, field
from model.agents.utils import get_credentials
from time import time


@dataclass
class VerifyConfig:
    enabled: bool = False
    timeout: int = 600
    destination_path: str = '/tmp/cback_verify'
    method: str = 'random'


@dataclass
class Config:
    # map[group_name]VerifyConfig
    _groups: Dict[str, VerifyConfig] = field(default_factory=dict)

    def get_for_group(self, name: str) -> VerifyConfig:
        try:
            return self._groups[name]
        except KeyError:
            raise ValueError(f'config not found for group {name}')


class EnqueuedVerify(EnqueuedJob[Verify]):

    # config is the config per group for the prune
    def __init__(self, repository_factory: RepositoryFactory, groups_config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> None:

        self._config = from_dict(data_class=Config, data={
            '_groups': groups_config})
        self._shared_config = shared_config
        self.verify_repository = repository_factory.get_verify_repository()
        self.log = get_logger()

    def is_enabled(self, group: Group) -> bool:
        try:
            return self._config.get_for_group(group.name).enabled
        except ValueError:
            return False

    def pick(self, group: Group) -> Optional[Verify]:
        # before selecting a job with the specified method
        # try to check if there is a rescheduled job
        verify = self.verify_repository.pick('rescheduled', group)
        if verify:
            return verify

        # try with a job in pending status
        # with the selected method
        method = self._config.get_for_group(group.name).method
        return self.verify_repository.pick(method, group)

    def run(self, verify: Verify) -> None:

        self.log.info('selected verify job', extra={
                      'job': 'verify', 'id': verify.id})

        shared_config = self._shared_config[verify.group.name]
        verify_config = self._config.get_for_group(verify.group.name)

        password = get_credentials(shared_config, verify.repository)

        env = shared_config.destination_env
        options = shared_config.destination_options

        try:
            cache_dir = os.path.join(
                shared_config.cache_dir, verify.backup.name)

            restic = Restic(verify.repository.path, password, env=env,
                            options=options, cache_dir=cache_dir)

            # check if the repository exists
            if not restic.exists():
                self.log.info('repository does not exist, skipping.', extra={
                              'repository': verify.repository.path, 'job': 'verify', 'id': verify.id})
                return
            else:
                self.log.debug('repository exists', extra={
                               'job': 'verify', 'id': verify.id, 'repository': verify.repository.path})

            # remove all stale locks and try to execute
            # if there are other locks the job will fail with
            # a RepositoryLocked exception
            restic.unlock()

            snapshots = restic.snapshots()

            if snapshots:
                # let's use the last snapshot for the check
                snapshot = snapshots[-1]

                self.log.info('running verify', extra={
                              'job': 'verify', 'id': verify.id})

                start_time = time()
                restic.restore(
                    snapshot.id, verify_config.destination_path, verify=True)
                end_time = time()

                elapsed_time = end_time - start_time

                self.log.info('verify completed', extra={
                              'job': 'verify', 'id': verify.id, 'elapsed_time': elapsed_time})
                verify.stats = {'success': 1,
                                'step': 1, 'elapsed_time': elapsed_time}
            else:
                self.log.info('verify skipped: no snapshots', extra={
                              'job': 'verify', 'id': verify.id})
                verify.stats = {'skipped': 1}
        except:
            self.log.exception('exception raised', extra={
                               'job': 'verify', 'id': verify.id})
            verify.stats = {'failed': 1, 'step': 1}
            raise
        finally:
            # remove the dir used for restoring the data
            Utils.delete_folder(verify_config.destination_path)

            # remove the cache dir
            Utils.delete_folder(cache_dir)
            self.log.debug('removed cache dir', extra={
                           'job': 'verify', 'id': verify.id, 'dir': cache_dir})
