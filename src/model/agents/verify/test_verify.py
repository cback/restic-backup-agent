# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, cast
import unittest
from unittest.mock import patch
from utils.test.local_fs import *
from utils.test.repo import MockInMemoryRepositoryFactory
from utils.test.restic import *
from model.jobs.group import Group
from model.agents.agent import SharedAgentConfig

from model.jobs.prune import Prune
from model.jobs.verify import Verify
from model.jobs.backup import Backup
from model.jobs.job import Repository
from model.jobs.status import Status
from .verify import EnqueuedVerify
from model.agents.backup.test_backup import new_backup


def new_verify(backup: Backup, group: Group, repository: Repository) -> Verify:
    return Verify(
        backup=backup,
        username=None,
        enabled=True,
        running_expiration=None,
        finished=None,
        group=group,
        id=0,
        info=None,
        repository=repository,
        retries=0,
        started=None,
        stats=cast(Dict[str, Any], None),
        status=Status.PENDING,
        worker=None
    )


USERNAME = os.environ.get('USER', 'root')


class TestVerify(unittest.TestCase):

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        self.repo_factory = MockInMemoryRepositoryFactory()  # type: ignore

    @staticmethod
    def get_verify_config(group_name: str, destination_folder: str) -> Dict[str, Any]:
        return {
            group_name: {
                'enabled': False,
                'destination_path': destination_folder
            }
        }

    @staticmethod
    def get_shared_config(group_name: str) -> Dict[str, SharedAgentConfig]:
        return {
            group_name: SharedAgentConfig()
        }

    def test_verify_successful(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        repo, pw, cleanup_repo = new_restic_repo_one_snapshot(d)
        restore_folder, cleanup_folder = new_tmp_dir()

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        backup_username = 'backup_username'
        backup = new_backup('', group, repository,
                            backup_username, Status.PENDING)
        verify = new_verify(backup, group, repository)

        config = TestVerify.get_verify_config(group.name, restore_folder)
        shared_config = TestVerify.get_shared_config(group.name)

        pr = EnqueuedVerify(self.repo_factory, config, shared_config)
        pr.run(verify)

        # check if restore folder was deleted
        self.assertFalse(dir_file_exist(restore_folder))

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup_username)
        self.assertFalse(dir_file_exist(cache_dir))

        cleanup_folder()
        cleanup_repo()
