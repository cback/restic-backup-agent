# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime, timedelta
from model.agents.switch.switch import JobSwitcher
from model.jobs.status import Status
from model.jobs.group import Group
from repository import Filter, Operator
from repository.interfaces.job_repository import EmptyListError
from cron_converter import Cron
from dateutil.tz import tzlocal


class BackupSwitcher(JobSwitcher):

    def switch(self, group: 'Group', threshold: int) -> int:

        backup_repo = self.repo_factory.get_backup_repository()

        # set to pending all backup jobs without desired time
        # that has completed a `threshold` seconds ago

        now = datetime.now()

        threshold_time = now - timedelta(seconds=threshold)

        filters = [
            Filter('status', Operator.EQUAL, Status.COMPLETED),
            Filter('finished', Operator.LESS, threshold_time),
            Filter('enabled', Operator.EQUAL, True),
            Filter('desired_start_time', Operator.EQUAL, None),
            Filter('group_id', Operator.EQUAL, group.id)
        ]

        n = backup_repo.update_by_filters(filters, status=Status.PENDING)

        try:
            backups_desired_time = backup_repo.get_filtered(order_by='id', filters=[
                Filter('status', Operator.EQUAL, Status.COMPLETED),
                Filter('enabled', Operator.EQUAL, True),
                Filter('desired_start_time', Operator.NOT_EQUAL, None),
                Filter('group_id', Operator.EQUAL, group.id)
            ])
        except EmptyListError:
            return n

        for backup in backups_desired_time:

            if not backup.started:
                backup.started = now

            assert(backup.desired_start_time is not None)
            next_start_time = Cron(backup.desired_start_time).schedule(
                backup.started).next()
            if now >= next_start_time:
                n += backup_repo.update_by_filters(
                    filters=[
                        # filters are here to guarantee that an other switch
                        # did not already switch to a pending status
                        Filter('id', Operator.EQUAL, backup.id),
                        Filter('status', Operator.EQUAL, Status.COMPLETED)
                    ], status=Status.PENDING)

        return n
