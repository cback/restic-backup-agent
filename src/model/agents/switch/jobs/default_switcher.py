# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime, timedelta
from typing import Type, cast
from model.agents.switch.switch import J, JobSwitcher
from model.jobs.job import Job
from model.jobs.status import Status
from model.jobs.group import Group
from model.jobs.prune import Prune
from repository import Filter, Operator
from repository.factory.repository_factory import RepositoryFactory
from repository.interfaces.job_repository import JobRepository


class DefaultSwitcher(JobSwitcher[J]):

    def __init__(self, repo_factory: 'RepositoryFactory', job_type: Type[J]) -> None:
        super().__init__(repo_factory)
        self.job_type = job_type

    def switch(self, group: 'Group', threshold: int) -> int:

        job_repo: JobRepository[J] = self.repo_factory.get_repository_by_job(
            self.job_type)

        threshold_time = datetime.now() - timedelta(seconds=threshold)

        filters = [
            Filter('status', Operator.EQUAL, Status.COMPLETED),
            Filter('finished', Operator.LESS, threshold_time),
            Filter('enabled', Operator.EQUAL, True),
            Filter('group_id', Operator.EQUAL, group.id)
        ]

        return job_repo.update_by_filters(filters, status=Status.PENDING)
