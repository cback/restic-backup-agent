# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, Generic, Iterable, List, Tuple, Type, TypeVar
from dacite.core import from_dict
from random import choice


from model.agents.agent import Runner
from model.jobs.group import Group
from model.jobs.job import Job
from repository.factory.repository_factory import RepositoryFactory
from model.managers.group.group_manager import GroupManager
from model.config.logs import get_logger


@dataclass
class SwitchConfig:
    enabled: bool = False
    expiration_times: Dict[str, int] = field(default_factory=dict)


@dataclass
class Config:
    # map[group_name]BackupConfig
    _groups: Dict[str, SwitchConfig] = field(default_factory=dict)

    def get_for_group(self, name: str) -> SwitchConfig:
        try:
            return self._groups[name]
        except:
            raise Exception(f'config not found for group {name}')


J = TypeVar('J', bound=Job)


class JobSwitcher(ABC, Generic[J]):

    def __init__(self, repo_factory: 'RepositoryFactory', *args: Any) -> None:
        self.repo_factory = repo_factory

    @abstractmethod
    def switch(self, group: 'Group', threshold: int) -> int:
        pass


_switch_register: Dict[str, Tuple[Type['JobSwitcher'], Tuple]] = {}


def register(name: str, switcher: Type['JobSwitcher'], *args: Any) -> None:
    # TODO: check if the switcher was already registered??
    _switch_register[name] = (switcher, args if args else tuple())


class SwitchRunner(Runner):

    def __init__(self, repo_factory: 'RepositoryFactory', group_manager: 'GroupManager', config: Dict[str, Any]) -> None:
        self.repo_factory = repo_factory
        self.config = from_dict(Config, {'_groups': config})
        self.group_manager = group_manager

        # create all the switchers specified in the configuration
        self._switchers: Dict[str, JobSwitcher] = {}
        for switch_config in self.config._groups.values():
            for agent_name in switch_config.expiration_times:
                if agent_name not in self._switchers:
                    switcher = _switch_register[agent_name]
                    self._switchers[agent_name] = switcher[0](
                        repo_factory, *switcher[1])

        self.log = get_logger()

    def run(self) -> None:
        group = self._get_random_group()

        self.log.info('selected group', extra={
                      'job': 'switch', 'group': group.name})

        for agent_name, switcher in self._switchers.items():

            # skip the switch for the agent if in the group does not declare a config
            if agent_name not in self.config.get_for_group(group.name).expiration_times:
                continue

            expiration_time = self.config.get_for_group(
                group.name).expiration_times[agent_name]

            self.log.info('running switch for job', extra={
                          'job': 'switch', 'target': agent_name, 'group': group.name, 'expiration_time': expiration_time})
            try:
                switched = switcher.switch(
                    group, expiration_time)
                self.log.info(f'set to pending {switched} {agent_name} jobs in group', extra={
                    'job': 'switch', 'target': agent_name, 'group': group.name})
            except:
                self.log.exception(f'got exception', extra={
                                   'job': 'switch', 'target': agent_name, 'group': group.name})

    def _get_random_group(self) -> 'Group':
        group_name = choice(list(self.config._groups.keys()))
        return self.group_manager.get_by_name(group_name)
