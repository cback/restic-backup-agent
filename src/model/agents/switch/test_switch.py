# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import random
from typing import List, cast, Optional, Dict, Any
import unittest
from unittest.mock import patch
from datetime import datetime
from model.jobs.group import Group
from model.jobs.job import Repository
from repository.interfaces.instance_repository import Instance
from utils.test.repo import MockInMemoryRepositoryFactory, InMemoryGroupRepository
from model.managers.group.group_manager import GroupManager
from .switch import SwitchRunner
from model.jobs.backup import Backup
from model.jobs.prune import Prune
from model.jobs.status import Status
from datetime import datetime, timedelta, time
from freezegun import freeze_time


def backup(id: int, status: int, started: Optional[datetime], finished: Optional[datetime], enabled: bool, desired_start_time: Optional[str], group: Group) -> Backup:
    return Backup(
        bytes_total=0,
        data_added=0,
        desired_start_time=desired_start_time,
        dirs_changed=0,
        dirs_new=0,
        dirs_unmodified=0,
        enabled=enabled,
        file_errors=0,
        files_changed=0,
        files_new=0,
        files_total=0,
        files_unmodified=0,
        finished=finished,
        force=False,
        group=group,
        id=id,
        tag=None,
        info=None,
        instance=cast(Instance, None),
        name="",
        pinned_node=None,
        repository=cast(Repository, None),
        running_expiration=None,
        retries=0,
        source="",
        started=started,
        status=status,
        stats={},
        username=None,
        worker=None,
        progress=0.0
    )


def prune(id: int, status: int, started: Optional[datetime], finished: Optional[datetime], enabled: bool, group: Group) -> Prune:
    return Prune(
        id=id,
        group=group,
        repository=cast(Repository, None),
        backup=cast(Backup, None),
        enabled=enabled,
        finished=finished,
        running_expiration=None,
        info=None,
        retries=0,
        started=started,
        stats={},
        status=status,
        username=None,
        worker=None
    )


seed_first_element = 1
seed_second_element = 0


class TestSwitcher(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        super().setUp()

        self.repository_factory = MockInMemoryRepositoryFactory()  # type: ignore
        groups = ['test1', 'test2']
        self.group_manager = GroupManager(InMemoryGroupRepository(groups))
        self.groups = self.group_manager.list_groups()

        self.backup_repository = self.repository_factory.get_repository_by_job(
            Backup)
        self.prune_repository = self.repository_factory.get_repository_by_job(
            Prune)

    def tearDown(self) -> None:
        return super().tearDown()

    def test_init_from_config_empty_groups(self) -> None:
        config: Dict[str, Any] = {
            self.groups[0].name: {},
            self.groups[1].name: {}
        }

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        self.assertDictEqual(runner._switchers, {},
                             'no agent declared, so this dict should be empty')

    def test_init_from_config_only_one_agent(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60  # 1 hour
                }
            },
            self.groups[1].name: {}
        }

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        self.assertEqual(len(runner._switchers), 1)
        self.assertTrue('backup' in runner._switchers)

    def test_init_from_config_different_agents_in_different_groups(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60  # 1 hour
                }
            },
            self.groups[1].name: {
                'enabled': True,
                'expiration_times': {
                    'prune': 60*60  # 1 hour
                }
            }
        }

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        self.assertEqual(len(runner._switchers), 2)
        self.assertTrue('backup' in runner._switchers)
        self.assertTrue('prune' in runner._switchers)

    def test_init_from_config_all_agents_in_all_groups(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60,  # 1 hour
                    'prune': 60*60  # 1 hour
                }
            },
            self.groups[1].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60,  # 1 hour
                    'prune': 60*60  # 1 hour
                }
            }
        }

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        self.assertEqual(len(runner._switchers), 2)
        self.assertTrue('backup' in runner._switchers)
        self.assertTrue('prune' in runner._switchers)

    def _add_all_backups(self, lst: List[Backup]) -> None:
        for backup in lst:
            self.backup_repository.add(backup)

    def _add_all_prunes(self, lst: List[Prune]) -> None:
        for prune in lst:
            self.prune_repository.add(prune)

    #################### BACKUP SWITCHER ####################

    @freeze_time("2021-12-12 10:00:00")
    def test_backup_switcher_different_groups(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60  # 1 hour
                }
            }
        }

        random.seed(seed_first_element)  # first group will be selected

        now = datetime.now()

        backups = [
            backup(1, Status.COMPLETED, now - timedelta(hours=1,
                   minutes=30), now - timedelta(minutes=30), True, None, self.groups[0]),
            backup(2, Status.COMPLETED, now - timedelta(hours=2), now -
                   timedelta(hours=1, minutes=30), True, None, self.groups[0]),
            backup(3, Status.COMPLETED, now - timedelta(hours=2), now -
                   timedelta(hours=1, minutes=30), True, None, self.groups[1]),
        ]
        self._add_all_backups(backups)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        # only the backup with id 2 should be in status PENDING
        self.assertEqual(self.backup_repository.get_by_id(
            2).status, Status.PENDING)
        self.assertEqual(self.backup_repository.get_by_id(
            1).status, Status.COMPLETED)
        self.assertEqual(self.backup_repository.get_by_id(
            3).status, Status.COMPLETED)

    @freeze_time("2021-12-12")
    def test_backup_switcher_one_group_different_status(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60  # 1 hour
                }
            }
        }

        now = datetime.now()
        random.seed(seed_first_element)  # first group will be selected

        backups = [
            backup(1, Status.PENDING, None, None, True, None, self.groups[0]),
            backup(2, Status.FAILED, now - timedelta(hours=2), now -
                   timedelta(hours=1, minutes=30), True, None, self.groups[0]),
            backup(3, Status.COMPLETED, now - timedelta(hours=2), now -
                   timedelta(hours=1, minutes=30), True, None, self.groups[0]),
            backup(4, Status.COMPLETED, now - timedelta(hours=2), now -
                   timedelta(hours=1, minutes=30), False, None, self.groups[0]),  # backup finished before threshold but not enabled
        ]
        self._add_all_backups(backups)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        # only the backup with id 3 should be in status PENDING
        self.assertEqual(self.backup_repository.get_by_id(
            3).status, Status.PENDING)

        self.assertEqual(self.backup_repository.get_by_id(
            1).status, Status.PENDING)
        self.assertEqual(self.backup_repository.get_by_id(
            2).status, Status.FAILED, 'failed job MUST remain failed')
        self.assertEqual(self.backup_repository.get_by_id(
            4).status, Status.COMPLETED, 'not enabled job MUST NOT be reset in pending')

    @freeze_time("2021-12-12")
    def test_backup_switcher_no_before_threshold(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60*24  # 24 hours = 1 day
                }
            }
        }

        now = datetime.now()
        random.seed(seed_first_element)  # first group will be selected

        backups = [
            backup(1, Status.COMPLETED, now - timedelta(hours=1,
                   minutes=30), now - timedelta(minutes=30), True, None, self.groups[0]),
            backup(2, Status.COMPLETED, now - timedelta(hours=2), now -
                   timedelta(hours=1, minutes=30), True, None, self.groups[0]),
            backup(3, Status.COMPLETED, now - timedelta(hours=2), now -
                   timedelta(hours=1, minutes=30), True, None, self.groups[1]),
        ]
        self._add_all_backups(backups)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        self.assertEqual(self.backup_repository.get_by_id(
            1).status, Status.COMPLETED)
        self.assertEqual(self.backup_repository.get_by_id(
            2).status, Status.COMPLETED)
        self.assertEqual(self.backup_repository.get_by_id(
            3).status, Status.COMPLETED)

    @freeze_time("2021-12-12 10:00:00")
    def test_backup_switcher_desired_start_time_no_time_to_switch(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60*24  # 24 hours = 1 day
                }
            }
        }

        random.seed(seed_first_element)  # first group will be selected

        backups = [
            backup(1, Status.COMPLETED, datetime(2021, 12, 11, 11, 1),  # 2021-12-11 11:01
                   datetime(2021, 12, 11, 11, 30), True, '0 11 * * *', self.groups[0]),
        ]
        self._add_all_backups(backups)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        self.assertEqual(self.backup_repository.get_by_id(
            1).status, Status.COMPLETED)

    @freeze_time("2021-12-12 10:00:00")
    def test_backup_switcher_desired_start_time__time_to_switch(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'backup': 60*60*24  # 24 hours = 1 day
                }
            }
        }

        random.seed(seed_first_element)  # first group will be selected

        backups = [
            backup(1, Status.COMPLETED, datetime(2021, 12, 11, 10, 1),
                   datetime(2021, 12, 11, 10, 31), True, '0 10 * * *', self.groups[0]),
        ]
        self._add_all_backups(backups)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        self.assertEqual(self.backup_repository.get_by_id(
            1).status, Status.PENDING)

    #################### PRUNE SWITCHER ####################

    @freeze_time("2021-12-12 10:00:00")
    def test_prune_switcher_different_groups(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'prune': 60*60  # 1 hour
                }
            }
        }

        random.seed(seed_first_element)  # first group will be selected

        now = datetime.now()

        prunes = [
            prune(1, Status.COMPLETED, now - timedelta(hours=1,
                                                       minutes=30), now - timedelta(minutes=30), True, self.groups[0]),
            prune(2, Status.COMPLETED, now - timedelta(hours=2), now -
                  timedelta(hours=1, minutes=30), True, self.groups[0]),
            prune(3, Status.COMPLETED, now - timedelta(hours=2), now -
                  timedelta(hours=1, minutes=30), True, self.groups[1]),
        ]
        self._add_all_prunes(prunes)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        # only the prune with id 2 should be in status PENDING
        self.assertEqual(self.prune_repository.get_by_id(
            2).status, Status.PENDING)
        self.assertEqual(self.prune_repository.get_by_id(
            1).status, Status.COMPLETED)
        self.assertEqual(self.prune_repository.get_by_id(
            3).status, Status.COMPLETED)

    @freeze_time("2021-12-12")
    def test_prune_switcher_one_group_different_status(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'prune': 60*60  # 1 hour
                }
            }
        }

        now = datetime.now()
        random.seed(seed_first_element)  # first group will be selected

        prunes = [
            prune(1, Status.PENDING, None, None, True, self.groups[0]),
            prune(2, Status.FAILED, now - timedelta(hours=2), now -
                  timedelta(hours=1, minutes=30), True, self.groups[0]),
            prune(3, Status.COMPLETED, now - timedelta(hours=2), now -
                  timedelta(hours=1, minutes=30), True, self.groups[0]),
            prune(4, Status.COMPLETED, now - timedelta(hours=2), now -
                  timedelta(hours=1, minutes=30), False, self.groups[0]),  # prune finished before threshold but not enabled
        ]
        self._add_all_prunes(prunes)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        # only the backup with id 3 should be in status PENDING
        self.assertEqual(self.prune_repository.get_by_id(
            3).status, Status.PENDING)

        self.assertEqual(self.prune_repository.get_by_id(
            1).status, Status.PENDING)
        self.assertEqual(self.prune_repository.get_by_id(
            2).status, Status.FAILED, 'failed job MUST remain failed')
        self.assertEqual(self.prune_repository.get_by_id(
            4).status, Status.COMPLETED, 'not enabled job MUST NOT be reset in pending')

    @freeze_time("2021-12-12")
    def test_prune_switcher_no_before_threshold(self) -> None:
        config = {
            self.groups[0].name: {
                'enabled': True,
                'expiration_times': {
                    'prune': 60*60*24  # 24 hours = 1 day
                }
            }
        }

        now = datetime.now()
        random.seed(seed_first_element)  # first group will be selected

        prunes = [
            prune(1, Status.COMPLETED, now - timedelta(hours=1,
                                                       minutes=30), now - timedelta(minutes=30), True, self.groups[0]),
            prune(2, Status.COMPLETED, now - timedelta(hours=2), now -
                  timedelta(hours=1, minutes=30), True, self.groups[0]),
            prune(3, Status.COMPLETED, now - timedelta(hours=2), now -
                  timedelta(hours=1, minutes=30), True, self.groups[1]),
        ]
        self._add_all_prunes(prunes)

        runner = SwitchRunner(self.repository_factory,
                              self.group_manager, config)
        runner.run()

        self.assertEqual(self.prune_repository.get_by_id(
            1).status, Status.COMPLETED)
        self.assertEqual(self.prune_repository.get_by_id(
            2).status, Status.COMPLETED)
        self.assertEqual(self.prune_repository.get_by_id(
            3).status, Status.COMPLETED)
