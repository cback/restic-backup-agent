# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from socket import gethostname
from dataclasses import dataclass, field
from model.agents.agent import SharedAgentConfig
from typing import Dict, Any, List, Optional
from model.agents.enqueued_job_runner.enqueued_job_runner import EnqueuedJob
from model.jobs.group import Group
from model.jobs.backup import Backup
from model.agents.utils import get_credentials, get_username, persist_progress
from datetime import datetime
from model.restic.restic import Restic
from repository import RepositoryFactory
from utils.utils import Utils
from dateutil.tz import tzlocal
from model.config.logs import get_logger
from model.filesystem.filesystem_factory import FileSystemFactory
import os
import jsonlines
from dacite import from_dict


@dataclass
class BackupConfig:
    enabled: bool = True
    force: bool = True
    files_error_log: str = "/var/log/cback/cback-backup-errors.log"
    exclude_list: List[str] = field(default_factory=list)
    metadata_save_percentage: int = 10
    metadata_save_enabled: bool = False
    method: str = 'random'
    node_pinning: bool = False
    no_scan: bool = False


@dataclass
class Config:
    # map[group_name]BackupConfig
    _groups: Dict[str, BackupConfig] = field(default_factory=dict)

    def get_for_group(self, name: str) -> BackupConfig:
        try:
            return self._groups[name]
        except KeyError:
            raise ValueError(f'config not found for group {name}')


class EnqueuedBackup(EnqueuedJob[Backup]):

    # config is the config per group for the backup
    def __init__(self, repository_factory: RepositoryFactory, groups_config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> None:
        super().__init__()
        self.config = from_dict(data_class=Config, data={
                                '_groups': groups_config})
        self.shared_config = shared_config
        self.backup_repository = repository_factory.get_backup_repository()
        self.node_name = gethostname()
        self.log = get_logger()

    def is_enabled(self, group: Group) -> bool:
        try:
            return self.config.get_for_group(group.name).enabled
        except ValueError:
            return False

    def pick(self, group: Group) -> Optional[Backup]:
        # try before with a rescheduled backup
        backup = self.backup_repository.pick('rescheduled', group)
        if backup:
            self.log.debug(f'picked backup job', extra={
                'backup_id': backup.id, 'method': 'rescheduled'})
            return backup

        group_config = self.config.get_for_group(group.name)

        # try with a pinned node job, otherwise try
        # to select the job using the selected method
        # for the randomly selected group

        if group_config.node_pinning:
            victim = self.backup_repository.pick(
                'pinned_node', group, pinned_node=self.node_name)
            if victim:
                self.log.debug(f'picked backup job', extra={
                    'backup_id': victim.id, 'method': 'pinned_node'})
                return victim

        method = group_config.method
        backup = self.backup_repository.pick(method, group)
        if backup:
            self.log.debug(f'picked backup job', extra={
                'backup_id': backup.id, 'method': method})
            return backup
        return None

    def run(self, backup: Backup) -> None:

        self.log.info(f'selected backup job', extra={'backup_id': backup.id})

        backup_config = self.config.get_for_group(backup.group.name)
        group_shared_config = self.shared_config[backup.group.name]

        password = get_credentials(
            group_shared_config, backup.repository)
        username = get_username(group_shared_config, backup)

        env = group_shared_config.destination_env
        options = group_shared_config.destination_options

        cache_dir = os.path.join(group_shared_config.cache_dir, backup.name)

        restic = Restic(backup.repository.path, password, env=env,
                        options=options, cache_dir=cache_dir)

        # We are pessimistic before starting
        stats = None
        backup.stats = {"failed": 1}
        backup.force = backup.force or backup_config.force

        try:

            src_type = group_shared_config.source_type
            src_config = group_shared_config.source_config

            # check if the source exists
            fs = FileSystemFactory.new(src_type, src_config)

            try:
                if not fs.exists(backup.source, username):
                    raise Exception(f'{backup.source} does not exist')
            except KeyError:
                raise Exception(f'user {username} does not exist')

            self.log.debug('source exists', extra={
                           'source': backup.source, 'job': 'backup', 'id': backup.id})

            # check if the repository exists
            if not restic.exists():
                self.log.info('repository does not exists', extra={
                              'repository': backup.repository.path, 'job': 'backup', 'id': backup.id})
                # the restic repository does not exists
                restic.init()
                self.log.info('repository initialized', extra={
                              'repository': backup.repository.path, 'job': 'backup', 'id': backup.id})
            else:
                self.log.debug('repository exists', extra={
                               'job': 'backup', 'id': backup.id, 'repository': backup.repository.path})

            # remove all stale locks and try to execute
            # if there are other locks the job will fail with
            # a RepositoryLocked exception
            restic.unlock()

            snapshots = restic.snapshots(path=[backup.source])
            latest_snapshot_time = snapshots[-1].time if snapshots else datetime.fromtimestamp(
                0.0, tz=tzlocal())
            parent_snapshot_id = snapshots[-1].id if snapshots else None

            self.log.debug('latest snapshot timestamp', extra={
                           'time': latest_snapshot_time, 'job': 'backup', 'id': backup.id})

            source_timestamp = fs.timestamp(backup.source, username)

            self.log.debug('source timestamp', extra={
                           'time': source_timestamp, 'job': 'backup', 'id': backup.id})

            if (source_timestamp < latest_snapshot_time) and source_timestamp != datetime.fromtimestamp(0.0, tz=tzlocal()) and not backup.force:
                backup.stats = {"skipped": 1}
                self.log.info('backup skipped', extra={
                              'job': 'backup', 'id': backup.id})

            else:  # do the backup
                if backup.force:
                    self.log.info('forcing backup to be run', extra={
                                  'job': 'backup', 'id': backup.id, 'parent': parent_snapshot_id})
                else:
                    self.log.info('backup will run', extra={
                                  'job': 'backup', 'id': backup.id, 'source_timestamp': source_timestamp, 'snapshot_timestamp': latest_snapshot_time, 'parent': parent_snapshot_id})

                exclude_list = backup_config.exclude_list

                self.log.debug('exclude file list', extra={
                               'job': 'backup', 'id': backup.id, 'list': exclude_list})

                self.log.info('running backup', extra={
                              'job': 'backup', 'id': backup.id})

                stats = restic.backup(
                    backup.source, exclude_list=exclude_list, username=username, parent=parent_snapshot_id, no_scan=backup_config.no_scan, on_status_update=persist_progress(backup, self.backup_repository), tags=[backup.tag] if backup.tag else [])
                self.log.info('backup completed', extra={
                              'job': 'backup', 'id': backup.id, 'stats': stats})

                backup.stats = {
                    "files_new": stats.files_new,
                    "files_changed": stats.files_changed,
                    "files_unmodified": stats.files_unmodified,
                    "dirs_new": stats.dirs_new,
                    "dirs_changed": stats.dirs_changed,
                    "dirs_unmodified": stats.dirs_unmodified,
                    "files_error": stats.files_error_count,
                    "success": 1,
                    "data_added": stats.data_added,
                    "duration": stats.total_duration
                }

                # save the stats in the db
                # bytes_total | data_added | file_errors | dirs_changed | dirs_new | dirs_unmodified | files_changed | files_new | files_total | files_unmodified
                self.backup_repository.update_by_id(
                    backup.id,
                    tag=None,
                    bytes_total=stats.total_bytes_processed,
                    data_added=stats.data_added,
                    file_errors=stats.files_error_count,
                    dirs_changed=stats.dirs_changed,
                    dirs_new=stats.dirs_new,
                    dirs_unmodified=stats.dirs_unmodified,
                    files_changed=stats.files_changed,
                    files_new=stats.files_new,
                    files_total=stats.total_files_processed,
                    files_unmodified=stats.files_unmodified,
                    progress=1.0,
                )

                # append to the BackupConfig.files_error_log file,
                # the list of error files during the backup
                if stats.files_error_list:
                    with jsonlines.open(backup_config.files_error_log, mode='a') as w:
                        w.write_all(stats.files_error_list)

                    # bacause there were some errors on some files
                    # we must ensure to re-run the backup next time
                    # as these error could be transient (like input/output errors)

                    self.backup_repository.update_by_id(backup.id, force=True)
                else:
                    # there were no errors, so if the backup was forced in this run
                    # we can remove the force flag
                    if backup.force:
                        self.backup_repository.update_by_id(
                            backup.id, force=False)

                if _has_error_on_source_folder(stats.files_error_list, backup.source):
                    raise Exception(
                        "no permissions to read source folder: "+backup.source)

        except:
            self.log.exception('exception raised', extra={
                               'job': 'backup', 'id': backup.id})
            raise
        finally:
            # remove the cache dir
            Utils.delete_folder(cache_dir)
            self.log.debug('removed cache dir', extra={
                           'dir': cache_dir, 'job': 'backup', 'id': backup.id, 'dir': cache_dir})


def _has_error_on_source_folder(files_error_list: List[Dict[str, Any]], source: str) -> bool:
    for err in files_error_list:
        if err['path'] == source:
            return True
    return False
