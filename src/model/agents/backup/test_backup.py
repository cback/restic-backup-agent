# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import time
from typing import Any, Dict, cast
import unittest
from unittest.mock import patch
from model.agents.agent import SharedAgentConfig
from utils.test.local_fs import *
from utils.test.restic import *
from utils.test.repo import *
from model.jobs.group import *


from model.jobs.backup import Backup
from model.jobs.job import Repository
from model.jobs.status import Status
from repository.interfaces.instance_repository import Instance
from .backup import EnqueuedBackup


def new_backup(source: str, group: Group, repository: Repository, username: Optional[str] = None, status: int = Status.PENDING, force: bool = False) -> Backup:
    return Backup(
        id=0,
        username=username,
        group=group,
        repository=repository,
        enabled=True,
        status=status,
        worker=None,
        started=None,
        finished=None,
        info=None,
        retries=0,
        stats=cast(Dict[str, Any], None),
        name="test",
        tag=None,
        running_expiration=None,
        source=source,
        pinned_node=None,
        desired_start_time=None,
        force=force,
        instance=Instance(id=0, name="test-instance"),
        bytes_total=0,
        data_added=0,
        dirs_changed=0,
        dirs_new=0,
        dirs_unmodified=0,
        files_changed=0,
        files_new=0,
        files_total=0,
        files_unmodified=0,
        file_errors=0,
        progress=0.0
    )


USERNAME = os.environ.get('USER', 'root')


class TestBackup(unittest.TestCase):

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        self.repo_factory = MockInMemoryRepositoryFactory()  # type: ignore

    @staticmethod
    def get_backup_config(group_name: str) -> Dict:
        return {group_name: {
            'enabled': True,
            'force': False,
            'exclude_list': [],
            'metadata_save_percentage': 10,
            'metadata_save_enabled': False
        }}

    @staticmethod
    def get_shared_config(group_name: str, cache_dir: str) -> Dict[str, SharedAgentConfig]:
        return {
            group_name: SharedAgentConfig(
                cache_dir=cache_dir
            )
        }

    def test_backup_non_existing_restic_repository(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="bar")],
        )

        source, clean_source = new_local_dir(d)
        repo, clean_repo = new_tmp_dir()
        cache_dir, clean_cache = new_tmp_dir()

        repository = Repository(path=repo, password=PASSWORD)
        group = Group(id=0, name='test')

        backup = new_backup(source, group, repository, username=USERNAME)
        self.repo_factory.get_backup_repository().add(backup)

        config = TestBackup.get_backup_config('test')
        shared_config = TestBackup.get_shared_config('test', cache_dir)

        br = EnqueuedBackup(self.repo_factory,  config, shared_config)
        br.run(backup)

        # ensure the repo has been created
        self.assertTrue(os.path.isdir(repo))
        self.assertTrue(os.listdir(repo))

        # ensure the backup run with success stat
        self.assertEqual(backup.stats["success"], 1)

        # ensure the cache for the user has been cleaned up
        self.assertFalse(dir_file_exist(os.path.join(cache_dir, backup.name)))

        clean_cache()
        clean_repo()
        clean_source()

    def test_backup_existing_restic_repository(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar")],
        )

        source, clean_source = new_local_dir(d)
        repo, pw, clean_repo = new_restic_repo_one_snapshot()
        cache_dir, clean_cache = new_tmp_dir()

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        backup = new_backup(source, group, repository, username=USERNAME)
        self.repo_factory.get_backup_repository().add(backup)

        config = TestBackup.get_backup_config('test')
        shared_config = TestBackup.get_shared_config('test', cache_dir)

        br = EnqueuedBackup(self.repo_factory,  config, shared_config)
        br.run(backup)

        # check if the repository has exactly 2 snapshots
        self.assertEqual(len(repo.snapshots()), 2)

        # ensure the backup run with success stat
        self.assertEqual(backup.stats["success"], 1)

        # ensure the cache for the user has been cleaned up
        self.assertFalse(dir_file_exist(os.path.join(cache_dir, backup.name)))

        clean_cache()
        clean_repo()
        clean_source()
