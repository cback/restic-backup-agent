# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, cast
import unittest
from unittest.mock import patch
from model.agents.agent import SharedAgentConfig
from model.restic.restic import ResticError
from utils.test.local_fs import *
from utils.test.repo import MockInMemoryRepositoryFactory
from utils.test.restic import *
from model.jobs.group import Group

from model.jobs.restore import Restore
from model.jobs.backup import Backup
from model.jobs.job import Repository
from model.jobs.status import Status
from .restore import EnqueuedRestore
from repository.interfaces.restore_repository import RestoreRepository
from model.agents.backup.test_backup import new_backup


def new_restore(restore_repo: RestoreRepository, backup: Backup, snapshot: str, destination: Optional[str], pattern: str, group: Group, repository: Repository, username: Optional[str] = None) -> Restore:
    r = Restore(
        id=0,
        group=group,
        username=username,
        repository=repository,
        backup=backup,
        running_expiration=None,
        destination=destination,
        enabled=True,
        finished=None,
        info=None,
        pattern=pattern,
        created=datetime.now(),
        retries=0,
        snapshot=snapshot,
        started=None,
        stats=cast(Dict[str, Any], None),
        status=Status.PENDING,
        worker=None,
        progress=0.0
    )
    return restore_repo.add(r)


USERNAME = os.environ.get('USER', 'root')


class TestRestore(unittest.TestCase):

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        self.repo_factory = MockInMemoryRepositoryFactory()  # type: ignore
        self.restore_repo = self.repo_factory.get_restore_repository()

    @staticmethod
    def get_restore_config(group_name: str) -> Dict:
        return {group_name: {
            'enabled': False,
        }}

    @staticmethod
    def get_shared_config(group_name: str) -> Dict[str, SharedAgentConfig]:
        return {
            group_name: SharedAgentConfig()
        }

    def test_restore_to_given_destination(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        expected = Directory(
            name="foo",
            content=[File(name="bar", content="foobar")],
        )

        exp, clean_exp = new_local_dir(expected)
        dest, clean_dest = new_tmp_dir()
        repo, pw, clean_repo = new_restic_repo_one_snapshot(d)

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        snapshot = repo.snapshots()[-1]
        source_path = snapshot.paths[0]
        pattern = os.path.join(snapshot.paths[0], "foo/bar")

        backup = new_backup(source_path, group, repository, force=True)
        restore = new_restore(self.restore_repo, backup, snapshot.id, dest,
                              pattern, group, repository, username=USERNAME)

        config = TestRestore.get_restore_config('test')
        shared_config = TestRestore.get_shared_config('test')

        rr = EnqueuedRestore(self.repo_factory, config, shared_config)
        rr.run(restore)

        print(exp, dest, os.path.join(dest, snapshot.paths[0][1:]))
        self.assertTrue(dir_equals(
            exp, os.path.join(dest, snapshot.paths[0][1:])))

        clean_repo()
        clean_dest()
        clean_exp()

    def test_restore_without_destination(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        expected = Directory(
            name="foo",
            content=[File(name="bar", content="foobar")],
        )

        exp, clean_exp = new_local_dir(expected)
        repo, pw, clean_repo = new_restic_repo_one_snapshot(d)

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        snapshot = repo.snapshots()[-1]
        source_path = snapshot.paths[0]
        pattern = os.path.join(source_path, "foo/bar")

        destination = None
        backup = new_backup(source_path, group, repository, force=True)
        restore = new_restore(self.restore_repo, backup, snapshot.id,
                              destination, pattern, group, repository, username=USERNAME)

        config = TestRestore.get_restore_config(group.name)
        shared_config = TestRestore.get_shared_config(group.name)

        rr = EnqueuedRestore(self.repo_factory, config, shared_config)
        rr.run(restore)

        # the destination should be a folder with name __restored_<xxxx>__ inside the source dir
        # let's find this directory, as it's name depend on when the restore was triggered
        restored_dir_name = None
        for dir in os.listdir(source_path):
            if dir.startswith("__restored_") and dir.endswith("__"):
                restored_dir_name = dir
                break
        self.assertIsNotNone(
            restored_dir_name, "__restored_<xxxx>__ not found in the source folder")

        destination = os.path.join(
            source_path, restored_dir_name if restored_dir_name else '', source_path[1:])
        self.assertTrue(dir_equals(destination, exp))

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup.name)
        self.assertFalse(dir_file_exist(cache_dir))

        clean_repo()
        clean_exp()

    def test_restore_folder_not_in_snapshot(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        dest, clean_dest = new_tmp_dir()
        repo, pw, clean_repo = new_restic_repo_one_snapshot(d)

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        snapshot = repo.snapshots()[-1]
        source_path = snapshot.paths[0]
        pattern = os.path.join(snapshot.paths[0], "not_in_snapshot")

        backup = new_backup(source_path, group, repository, force=True)
        restore = new_restore(self.restore_repo, backup, snapshot.id, dest,
                              pattern, group, repository, username=USERNAME)

        config = TestRestore.get_restore_config(group.name)
        shared_config = TestRestore.get_shared_config(group.name)

        rr = EnqueuedRestore(self.repo_factory,  config, shared_config)
        rr.run(restore)

        self.assertFalse(os.listdir(dest))

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup.name)
        self.assertFalse(dir_file_exist(cache_dir))

        clean_repo()
        clean_dest()

    def test_restore_into_a_folder_with_no_write_permissions(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        dest, clean_dest = new_tmp_dir()
        # change permissions so nobody has write permission
        os.chmod(dest, 0o000)

        repo, pw, clean_repo = new_restic_repo_one_snapshot(d)

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        snapshot = repo.snapshots()[-1]
        source_path = snapshot.paths[0]
        pattern = os.path.join(snapshot.paths[0], "foo/bar")

        backup = new_backup(source_path, group, repository, force=True)
        restore = new_restore(self.restore_repo, backup, snapshot.id, dest,
                              pattern, group, repository, username="nobody")

        config = TestRestore.get_restore_config('test')
        shared_config = TestRestore.get_shared_config('test')

        rr = EnqueuedRestore(self.repo_factory,  config, shared_config)

        with self.assertRaises(ResticError):
            rr.run(restore)

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup.name)
        self.assertFalse(dir_file_exist(cache_dir))

        clean_repo()

        os.chmod(dest, 0o777)
        clean_dest()
