# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland
# all rights reserved
#
# cback - CERN Backup Orchestrator
#
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see <https://www.gnu.org/licenses/>.


from datetime import datetime
import os
from typing import Any, Dict, Optional

from dacite.core import from_dict
from model.agents.agent import SharedAgentConfig
from model.agents.enqueued_job_runner.enqueued_job_runner import EnqueuedJob
from model.agents.utils import get_credentials, get_username, persist_progress
from model.jobs.group import Group
from model.restic.restic import Restic
from dataclasses import dataclass, field
from model.jobs.restore import Restore
from model.config.logs import get_logger
from utils.utils import Utils
from repository import RepositoryFactory
from time import time
import shutil


@dataclass
class RestoreConfig:
    enabled: bool = False
    method: str = 'random'


@dataclass
class Config:
    # map[group_name]RestoreConfig
    _groups: Dict[str, RestoreConfig] = field(default_factory=dict)

    def get_for_group(self, name: str) -> RestoreConfig:
        try:
            return self._groups[name]
        except KeyError:
            raise ValueError(f'config not found for group {name}')


class EnqueuedRestore(EnqueuedJob[Restore]):
    # config is the config per group for the restore
    def __init__(self, repository_factory: RepositoryFactory, groups_config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> None:
        self.config: Config = from_dict(
            data_class=Config, data={'_groups': groups_config})
        self.shared_config = shared_config
        self.restore_repository = repository_factory.get_restore_repository()
        self.log = get_logger()

    def is_enabled(self, group: Group) -> bool:
        try:
            return self.config.get_for_group(group.name).enabled
        except ValueError:
            return False

    def pick(self, group: Group) -> Optional[Restore]:
        # before selecting a job with the specified method
        # try to check if there is a rescheduled job
        restore = self.restore_repository.pick('rescheduled', group)
        if restore:
            return restore

        # no rescheduled jobs, try with a job in pending status
        # with the selected method
        method = self.config.get_for_group(group.name).method
        return self.restore_repository.pick(method, group)

    def run(self, restore: Restore) -> None:

        self.log.info('selected restore job', extra={
                      'job': 'restore', 'id': restore.id})

        config = self.config.get_for_group(restore.group.name)
        shared_config = self.shared_config[restore.group.name]

        password = get_credentials(shared_config, restore.repository)
        try:
            username = get_username(shared_config, restore)
        except Exception:
            # try with the username used for backup job
            username = get_username(shared_config, restore.backup)
        env = shared_config.destination_env
        options = shared_config.destination_options

        try:

            cache_dir = os.path.join(
                shared_config.cache_dir, restore.backup.name)

            restic = Restic(restore.repository.path, password, env=env,
                            options=options, cache_dir=cache_dir)

            # check if the repository exists
            if not restic.exists():
                raise Exception("repository does not exist")
            else:
                self.log.debug('repository exists', extra={
                               'job': 'restore', 'id': restore.id, 'repository': restore.repository.path})

            # remove all stale locks and try to execute
            # if there are other locks the job will fail with
            # a RepositoryLocked exception
            restic.unlock()

            # select a target to restore the file
            # if the target has been provided (self.destination != None)
            # restore the file in self.destination path into a folder with
            # name of the following pattern: __restored_<restore_time>__
            # if is not provided, try to store the files/folders into the source
            # where the snapshot was created

            now = datetime.now()

            if restore.trim_source:
                if not restore.pretrim_scratch_path:
                    raise Exception("temp space not provided in the configuration")
                target = os.path.join(restore.pretrim_scratch_path, f'{restore.snapshot}_{round(time())}')
                os.mkdir(target)

            else:
                if restore.destination:
                    target = restore.destination
                else:
                    target = os.path.join(restore.backup.source, format_restore_prefix(now))

                    self.restore_repository.update_by_id(
                        restore.id, destination=target)

            self.log.info(f'target folder {target}', extra={
                          'folder': target, 'job': 'restore', 'id': restore.id})

            snapshot = restore.snapshot

            # TODO (gdelmont): set timeout

            self.log.info('running restore', extra={
                          'job': 'restore', 'id': restore.id, 'snapshot': restore.snapshot, 'target': target, 'pattern': restore.pattern, 'username': username})

            start_time = time()
            restic.restore(snapshot, target,
                           pattern=restore.pattern, username=username, on_status_update=persist_progress(restore, self.restore_repository))

            if restore.trim_source:
                src_target = os.path.join(target, restore.backup.source.removeprefix('/'))
                dst_target = self.__after_restore_destination(restore, now) + f"/restore_{restore.snapshot}_{round(time())}"
                os.mkdir(dst_target)
                move_content(src_target, dst_target)
                shutil.rmtree(target, ignore_errors=True)
            end_time = time()

            elapsed_time = end_time - start_time
            self.log.info('restore completed', extra={
                          'job': 'restore', 'id': restore.id, 'elapsed_time': elapsed_time})

            self.restore_repository.update_by_id(
                restore.id,
                progress=1.0,
            )

            restore.stats = {"success": 1, "duration": elapsed_time}
        except:
            self.log.exception('exception raised', extra={
                               'job': 'restore', 'id': restore.id})
            restore.stats = {'failed': 1}
            raise
        finally:
            # remove the cache dir
            Utils.delete_folder(cache_dir)
            self.log.debug('removed cache dir', extra={
                           'dir': cache_dir, 'job': 'restore', 'id': restore.id, 'dir': cache_dir})

    def __after_restore_destination(self, restore: Restore, now: datetime) -> str:
        return restore.destination if restore.destination else os.path.join(restore.backup.source, format_restore_prefix(now))


def format_restore_prefix(now: datetime) -> str:
    return f"__restored_{now.strftime('%m_%d_%Y__%H_%M_%S')}__"

def move_content(src: str, dst: str) -> None:
    for file_name in os.scandir(src):
        source = os.path.join(src, file_name)
        destination = os.path.join(dst, file_name.path.removeprefix(src).removeprefix('/'))
        shutil.move(source, destination)
