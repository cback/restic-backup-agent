# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, cast
import unittest
from datetime import datetime
from unittest.mock import patch
from model.restic.restic import ResticError
from utils.test.local_fs import *
from utils.test.repo import MockInMemoryRepositoryFactory
from utils.test.restic import *
from model.jobs.group import Group
from model.agents.agent import SharedAgentConfig

from model.jobs.prune import Prune
from model.jobs.backup import Backup
from model.jobs.job import Repository
from model.jobs.status import Status
from .prune import EnqueuedPrune
from model.agents.backup.test_backup import new_backup


def new_prune(backup: Backup, group: Group, repository: Repository) -> Prune:
    return Prune(
        backup=backup,
        username=None,
        enabled=True,
        running_expiration=None,
        finished=None,
        group=group,
        id=0,
        info=None,
        repository=repository,
        retries=0,
        started=None,
        stats=cast(Dict[str, Any], None),
        status=Status.PENDING,
        worker=None
    )


def datestr(str_date: str) -> datetime:
    return datetime.strptime(str_date, '%d/%m/%y')


USERNAME = os.environ.get('USER', 'root')


class TestPrune(unittest.TestCase):

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        self.repo_factory = MockInMemoryRepositoryFactory()  # type: ignore

    @staticmethod
    def get_prune_config(group_name: str) -> Dict[str, Any]:
        return {
            group_name: {
                'enabled': False,
                'graceful_deletion': False, # do not use graceful deletion for basic unit testing
                'retention_policy': {'keep-daily': 7,
                                     'keep-weekly': 5, 'keep-monthly': 6}
            }
        }

    @staticmethod
    def get_shared_config(group_name: str) -> Dict[str, SharedAgentConfig]:
        return {
            group_name: SharedAgentConfig()
        }

    def test_prune_not_archived_backup(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        dates = list(map(datestr, ['07/12/20', '08/12/20', '09/12/20', '10/12/20',
                                   '12/12/20', '14/12/20', '15/12/20', '16/12/20', '17/12/20', '18/12/20']))
        repo, source, pw, cleanup_repo = new_restic_repo_dates(dates, d)

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        # test before the prune the number of snapshot is len(dates)
        self.assertEqual(len(repo.snapshots()), len(dates))

        backup_username = 'backup_username'
        backup = new_backup(source, group, repository,
                            backup_username, Status.PENDING)
        prune = new_prune(backup, group, repository)

        config = TestPrune.get_prune_config(group.name)
        shared_config = TestPrune.get_shared_config(group.name)

        pr = EnqueuedPrune(self.repo_factory, config, shared_config)
        pr.run(prune)

        self.assertEqual(len(repo.snapshots()), 7)

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup_username)
        self.assertFalse(dir_file_exist(cache_dir))

        cleanup_repo()

    def test_prune_different_hosts(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        dates = list(map(datestr, ['07/12/20', '08/12/20', '09/12/20', '10/12/20',
                                   '12/12/20', '14/12/20', '15/12/20', '16/12/20', '17/12/20', '18/12/20']))
        repo, source, pw, cleanup_repo = new_restic_repo_dates(
            dates, d, random_host=True)

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        # test before the prune the number of snapshot is len(dates)
        self.assertEqual(len(repo.snapshots()), len(dates))

        backup_username = 'backup_username'
        backup = new_backup(source, group, repository,
                            backup_username, Status.PENDING)
        prune = new_prune(backup, group, repository)

        config = TestPrune.get_prune_config(group.name)
        shared_config = TestPrune.get_shared_config(group.name)

        pr = EnqueuedPrune(self.repo_factory, config, shared_config)
        pr.run(prune)

        self.assertEqual(len(repo.snapshots()), 7)

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup_username)
        self.assertFalse(dir_file_exist(cache_dir))

        cleanup_repo()

    def test_prune_archived_backup(self) -> None:

        d = Directory(
            name="foo",
            content=[File(name="bar", content="foobar"),
                     File(name="other", content="other")],
        )

        dates = list(map(datestr, ['07/12/20', '08/12/20', '09/12/20', '10/12/20',
                                   '12/12/20', '14/12/20', '15/12/20', '16/12/20', '17/12/20', '18/12/20']))
        repo, source, pw, cleanup_repo = new_restic_repo_dates(dates, d)

        repository = Repository(path=repo.path, password=pw)
        group = Group(id=0, name='test')

        backup_username = 'backup_username'
        backup = new_backup(source, group,
                            repository, status=Status.ARCHIVED, username=backup_username)
        prune = new_prune(backup, group, repository)

        config = TestPrune.get_prune_config(group.name)
        shared_config = TestPrune.get_shared_config(group.name)

        pr = EnqueuedPrune(self.repo_factory, config, shared_config)
        pr.run(prune)

        self.assertEqual(len(repo.snapshots()), 1)
        self.assertEqual(repo.snapshots()[0].time.timestamp(), datestr(
            '18/12/20').timestamp())

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup_username)
        self.assertFalse(dir_file_exist(cache_dir))

        cleanup_repo()

    def test_check_id_cache_dir_is_deleted_on_exception(self) -> None:

        repo, pw, clean_repo = new_restic_repo_one_snapshot()

        # put wrong password to fail the prune job
        repository = Repository(path=repo.path, password=pw+'adkjsdaskj')
        group = Group(id=0, name='test')

        backup_username = 'backup_username'
        backup = new_backup(backup_username, group,
                            repository, status=Status.ARCHIVED)
        prune = new_prune(backup, group, repository)

        config = TestPrune.get_prune_config(group.name)
        shared_config = TestPrune.get_shared_config(group.name)

        pr = EnqueuedPrune(self.repo_factory, config, shared_config)

        with self.assertRaises(ResticError):
            pr.run(prune)

        # check if cache dir is deleted
        cache_dir = os.path.join(
            shared_config[group.name].cache_dir, backup_username)
        self.assertFalse(dir_file_exist(cache_dir))

        os.chmod(repo.path, 0o755)
        clean_repo()
