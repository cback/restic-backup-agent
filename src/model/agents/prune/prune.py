# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland
# all rights reserved
#
# cback - CERN Backup Orchestrator
#
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program. If not, see <https://www.gnu.org/licenses/>.


from datetime import datetime, timedelta
import os
from typing import Any, Dict, Optional, Tuple

from dacite import from_dict
from model.agents.agent import SharedAgentConfig
from model.agents.enqueued_job_runner.enqueued_job_runner import EnqueuedJob
from model.jobs.group import Group
from model.jobs.status import Status
from model.restic.restic import PruneStat, Restic, Snapshot
from model.config.logs import get_logger
from repository.factory.repository_factory import RepositoryFactory
from utils.utils import Utils
from dataclasses import dataclass, field
from model.jobs.prune import Prune
from model.agents.utils import get_credentials
from time import time


@dataclass
class PruneConfig:
    enabled: bool = False
    graceful_deletion: bool = True
    delete_graceful_period: int = 604800  # defaul to one week (in seconds)
    retention_policy: Dict[str, int] = field(default_factory=dict)
    method: str = 'random'


@dataclass
class Config:
    # map[group_name]PruneConfig
    _groups: Dict[str, PruneConfig] = field(default_factory=dict)

    def get_for_group(self, name: str) -> PruneConfig:
        try:
            return self._groups[name]
        except KeyError:
            raise ValueError(f'config not found for group {name}')


class EnqueuedPrune(EnqueuedJob[Prune]):

    # config is the config per group for the prune
    def __init__(self, repository_factory: RepositoryFactory, groups_config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> None:

        self._config = from_dict(data_class=Config, data={
            '_groups': groups_config})
        self._shared_config = shared_config
        self.prune_repository = repository_factory.get_prune_repository()
        self.log = get_logger()

    def is_enabled(self, group: Group) -> bool:
        try:
            return self._config.get_for_group(group.name).enabled
        except ValueError:
            return False

    def pick(self, group: Group) -> Optional[Prune]:
        # before selecting a job with the specified method
        # try to check if there is a rescheduled job
        prune = self.prune_repository.pick('rescheduled', group)
        if prune:
            return prune

        # no rescheduled jobs, try with a job in pending status
        # with the selected method
        method = self._config.get_for_group(group.name).method
        return self.prune_repository.pick(method, group)

    def _is_snapshot_to_delete(self, snapshot: Snapshot, delete_graceful_period: timedelta) -> bool:
        now = datetime.utcnow()
        for tag in filter(lambda t: t.startswith("delete"), snapshot.tags):
            request = datetime.fromtimestamp(int(tag.removeprefix("delete:")))
            if request + delete_graceful_period > now:
                return True
        return False

    def _prune_marked_to_delete_snapshots(self, prune: Prune, prune_config: PruneConfig, restic: Restic) -> PruneStat:
        stats = PruneStat(0, 0, 0, 0)
        for snapshot in restic.snapshots():
            if self._is_snapshot_to_delete(snapshot, timedelta(seconds=prune_config.delete_graceful_period)):
                self.log.debug('deleting snapshot from repository', extra={
                               'job': 'prune', 'id': prune.id, 'snapshot': snapshot})
                s = restic.forget(snapshot=snapshot.id, prune=True)
                stats += s
        return stats

    def run(self, prune: Prune) -> None:

        self.log.info('selected prune job', extra={
                      'job': 'prune', 'id': prune.id})

        shared_config = self._shared_config[prune.group.name]
        prune_config = self._config.get_for_group(prune.group.name)

        password = get_credentials(shared_config, prune.repository)

        env = shared_config.destination_env
        options = shared_config.destination_options

        try:
            cache_dir = os.path.join(
                shared_config.cache_dir, prune.backup.name)

            restic = Restic(prune.repository.path, password, env=env,
                            options=options, cache_dir=cache_dir)

            # check if the repository exists
            if not restic.exists():
                self.log.info('repository does not exist, skipping.', extra={
                              'repository': prune.repository.path, 'job': 'prune', 'id': prune.id})
                return
            else:
                self.log.debug('repository exists', extra={
                               'job': 'prune', 'id': prune.id, 'repository': prune.repository.path})


            # remove all stale locks and try to execute
            # if there are other locks the job will fail with
            # a RepositoryLocked exception
            restic.unlock()

            self.log.info('prune started', extra={
                          'job': 'prune', 'id': prune.id})

            start_time = time()
            if prune_config.graceful_deletion:
                self.log.info('deleting marked snapshots', extra={
                              'job': 'prune', 'id': prune.id})
                stats = self._prune_marked_to_delete_snapshots(
                    prune, prune_config, restic)
            else:

                policies = prune_config.retention_policy
                # If backup job is archived, we override the default policy to just keep the last one.
                if prune.backup.status == Status.ARCHIVED:
                    policies = {'keep-last': 1}

                self.log.info('policies selected', extra={
                    'policies': policies, 'job': 'prune', 'id': prune.id})

                stats = restic.forget(
                    policies=policies, prune=True, path=prune.backup.source, group_by=['path'])

            elapsed_time = time() - start_time
            self.log.info('prune completed', extra={
                          'job': 'prune', 'id': prune.id, 'elapsed_time': elapsed_time})
            prune.stats = {
                'success': 1, 'freed_size': stats.total_prune, 'elapsed_time': elapsed_time}
        except:
            self.log.exception('exception raised', extra={
                               'job': 'prune', 'id': prune.id})
            prune.stats = {'failed': 1}
            raise
        finally:
            # remove the cache dir
            Utils.delete_folder(cache_dir)
            self.log.debug('removed cache dir', extra={
                           'dir': cache_dir, 'job': 'prune', 'id': prune.id, 'dir': cache_dir})
