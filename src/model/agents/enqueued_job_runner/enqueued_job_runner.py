# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime, timedelta
from os import getpid
import random
from socket import gethostname
import threading
from time import sleep
from typing import Any, Callable, Dict, Generic, List, Optional, TypeVar
from model.agents.agent import Runner, SharedAgentConfig
from abc import ABC, abstractmethod
from model.jobs.group import Group
from model.jobs.job import Job
from model.jobs.status import Status
from model.restic.restic import RepositoryLocked
from repository.factory.repository_factory import RepositoryFactory
from repository.interfaces.job_repository import JobRepository
from utils.pub_sub import Publisher
from .events import ChangeJobStatus, FinishedJobProcessing


J = TypeVar('J', bound=Job)


class EnqueuedJob(ABC, Generic[J]):

    @abstractmethod
    def is_enabled(self, group: Group) -> bool:
        pass

    @abstractmethod
    def pick(self, group: Group) -> Optional[J]:
        pass

    @abstractmethod
    def run(self, job: J) -> None:
        # TODO: return the stats of the job
        pass


EnqueuedJobFactory = Callable[[RepositoryFactory, Dict[str, Any],
                               Dict[str, SharedAgentConfig]], EnqueuedJob]


class EnqueuedJobRunner(Runner, Publisher):

    def __init__(self, enqueued_job_factory: EnqueuedJobFactory, repo_factory: RepositoryFactory, groups_config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> None:
        super().__init__()
        self.repo_factory = repo_factory

        self.shared_config = shared_config

        self.enqueued_job = enqueued_job_factory(
            repo_factory, groups_config, shared_config)

    def _get_random_enabled_groups(self) -> List[Group]:

        group_repository = self.repo_factory.get_group_repository()
        enabled_groups = list(filter(lambda group: self.enqueued_job.is_enabled(
            group), group_repository.get_all()))

        random.shuffle(enabled_groups)
        return enabled_groups

    def run_job(self, job: Job) -> None:

        job_repository = self.repo_factory.get_repository_by_job(type(job))

        # launch the thread to update periodically the expiration time
        # of this running job
        stop_expiration_thread = self._start_thread_update_expiration_job(
            job, job_repository)

        shared_group_config = self.shared_config[job.group.name]

        max_retries = 1
        if shared_group_config.retry_if_error:
            max_retries = shared_group_config.max_retries

        try:
            # check if the job has reached the max number of retries
            if job.retries >= max_retries:
                raise Exception(f'reached max retries: {max_retries}')

            # set job in RUNNING state
            job = job_repository.update_by_id(
                job.id, status=Status.RUNNING, started=datetime.now(), finished=None, info=None, worker=self._get_worker())
            self._publish_event(ChangeJobStatus(job))

            # run the job
            self.enqueued_job.run(job)

        except RepositoryLocked:
            # set job in RESCHEDULED state
            job = job_repository.update_by_id(
                job.id, status=Status.RESCHEDULED, finished=datetime.now() + timedelta(minutes=15), info='Repository locked (rescheduled)')
            self._publish_event(ChangeJobStatus(job))
        except Exception as e:

            retries = job.retries + 1

            if shared_group_config.retry_if_error and retries < max_retries:
                # retry next time -> reschedule the job after 30 min
                job = job_repository.update_by_id(
                    job.id, retries=retries, status=Status.RESCHEDULED, info=str(e), started=datetime.now(), finished=datetime.now() + timedelta(minutes=30), worker=None)
                self._publish_event(ChangeJobStatus(job))
            else:
                # reached the max retries number
                err_message = '\n'.join(str(e).splitlines()[-1:])
                job = job_repository.update_by_id(
                    job.id, info=err_message, status=Status.FAILED, finished=datetime.now())
                self._publish_event(ChangeJobStatus(job))
        else:
            # the job run without raising exception
            # set the job as completed
            job_repository.update_by_id(
                job.id, info=f'Job completed', status=Status.COMPLETED, finished=datetime.now())
            self._publish_event(ChangeJobStatus(job))

        finally:
            stop_expiration_thread()
            self._publish_event(FinishedJobProcessing(
                job, job.stats))

    def _start_thread_update_expiration_job(self, job: Job, job_repository: JobRepository[Job]) -> Callable[[], None]:
        event = threading.Event()

        def stop() -> None:
            event.set()

        last_update = datetime.fromtimestamp(0)

        def passed_five_minutes() -> bool:
            nonlocal last_update
            now = datetime.now()
            return now - last_update > timedelta(minutes=5)

        def _run() -> None:
            nonlocal last_update

            while True:
                if event.is_set():
                    break
                if passed_five_minutes():
                    self._update_expiration_time(job, job_repository)
                    last_update = datetime.now()
                sleep(1)

        th = threading.Thread(target=_run)
        th.daemon = True
        th.start()

        return stop

    def _update_expiration_time(self, job: Job, job_repository: JobRepository[Job]) -> None:
        new_expiration_time = datetime.now() + timedelta(hours=1)
        job_repository.update_by_id(
            job.id, running_expiration=new_expiration_time)

    def _get_worker(self) -> str:
        # the worker is a string `hostanme:pid`
        return gethostname() + ":" + str(getpid())

    def run(self) -> None:
        # choose a group random
        # check if the backup is enabled for that group, if not come back to step 1 (trying all the groups)
        # pick a job enabled for that group
        # run it

        for group in self._get_random_enabled_groups():
            # try with all the enabled group
            # until I found one with at least one job to run
            job = self.enqueued_job.pick(group)

            if job:
                # we fond the job, so let's run it
                self.run_job(job)
                break
