# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Optional, Dict, cast
import unittest
from unittest.mock import patch
from model.agents.agent import SharedAgentConfig
from model.agents.enqueued_job_runner.enqueued_job_runner import EnqueuedJob, EnqueuedJobRunner
from model.agents.enqueued_job_runner.events import ChangeJobStatus
from model.jobs.job import Repository
from model.jobs.status import Status
from repository.factory.repository_factory import RepositoryFactory
from utils.pub_sub import Subscriber
from utils.test.repo import TestJob, MockInMemoryRepositoryFactory
from model.jobs.group import Group


class EnqueuedTestJob(EnqueuedJob[TestJob]):

    def is_enabled(self, _: Group) -> bool:
        return True

    def pick(self, group: Group) -> Optional[TestJob]:
        raise NotImplementedError

    def run(self, job: TestJob) -> None:
        pass


class FailsJob(EnqueuedJob[TestJob]):
    def is_enabled(self, _: Group) -> bool:
        return True

    def pick(self, group: Group) -> Optional[TestJob]:
        raise NotImplementedError

    def run(self, job: TestJob) -> None:
        raise Exception('error')


class TestEnqueuedJobRunner(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName)

    @patch.multiple(MockInMemoryRepositoryFactory, __abstractmethods__=set())
    def setUp(self) -> None:
        self.enqued_test_job = EnqueuedTestJob()
        self.enqued_test_job_fails = FailsJob()

        def factory(repo_factory: RepositoryFactory, groups_config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return self.enqued_test_job

        def factory_fails(repo_factory: RepositoryFactory, groups_config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return self.enqued_test_job_fails

        self.factory = factory
        self.factory_fails = factory_fails
        self.repository_factory = MockInMemoryRepositoryFactory()  # type: ignore
        self.repository = self.repository_factory.get_repository_by_job(
            TestJob)
        self.groups = [Group(id=0, name='group1'), Group(id=1, name='group2')]

    def tearDown(self) -> None:
        return super().tearDown()

    def _new_test_job(self, group: Group, repository: Repository, retries: int) -> TestJob:
        job = TestJob(
            id=-1,
            group=group,
            repository=repository,
            username=None,
            enabled=True,
            finished=None,
            info=None,
            retries=retries,
            running_expiration=None,
            started=None,
            stats={},
            status=Status.PENDING,
            worker=None,
        )
        return self.repository.add(job)

    def test_run_job_check_if_set_to_running_status(self) -> None:

        group = self.groups[0]
        shared_config = {
            group.name: SharedAgentConfig(
                max_retries=2,
                retry_if_error=True
            )
        }
        job = self._new_test_job(
            group, cast(Repository, None), shared_config[group.name].max_retries-1)

        job_runner = EnqueuedJobRunner(self.factory,
                                       self.repository_factory, {}, shared_config)

        not_notified = True

        class RunningChecker(Subscriber):
            def notify(this, event: object) -> None:
                if isinstance(event, ChangeJobStatus):
                    if event.job.status == Status.RUNNING:
                        # test passed
                        job_runner.unsubscribe(this)

                        nonlocal not_notified
                        not_notified = False
                    else:
                        self.fail(
                            f'status should be running, got={Status.to_str(event.job.status)}')

        job_runner.register_subscriber(RunningChecker())

        job_runner.run_job(job)
        self.failIf(
            not_notified, 'the job status was not running or no notification')

    def test_run_job_check_if_fail_set_to_pending_if_retry(self) -> None:

        group = self.groups[0]
        shared_config = {
            group.name: SharedAgentConfig(
                max_retries=2,
                retry_if_error=True
            )
        }
        job_init_retries = shared_config[group.name].max_retries-2
        job = self._new_test_job(
            group, cast(Repository, None), job_init_retries)

        # job will fails, but retry is < max_retries
        job_runner = EnqueuedJobRunner(self.factory_fails,
                                       self.repository_factory, {}, shared_config)

        job_runner.run_job(job)

        job_from_repo = self.repository.get_by_id(job.id)
        # check if the job is in RESCHEDULED status
        self.assertEqual(job_from_repo.status, Status.RESCHEDULED)

        # check if retries value in job is job_init_retries+1
        self.assertEqual(job_from_repo.retries, job_init_retries+1)

    def test_run_job_check_if_fail_set_to_failed_if_not_retry(self) -> None:

        group = self.groups[0]
        shared_config = {
            group.name: SharedAgentConfig(
                max_retries=2,
                retry_if_error=False
            )
        }
        job_init_retries = shared_config[group.name].max_retries-2
        job = self._new_test_job(
            group, cast(Repository, None), job_init_retries)

        # job will fail, but retry is < max_retries
        job_runner = EnqueuedJobRunner(self.factory_fails,
                                       self.repository_factory, {}, shared_config)

        job_runner.run_job(job)

        job_from_repo = self.repository.get_by_id(job.id)
        # check if the job is in FAILED status
        self.assertEqual(job_from_repo.status, Status.FAILED)

        # check if retries value in job is job_init_retries+1
        self.assertEqual(job_from_repo.retries, job_init_retries)

    def test_run_job_check_if_fail_set_to_failed_and_reached_max_retries(self) -> None:

        group = self.groups[0]
        shared_config = {
            group.name: SharedAgentConfig(
                max_retries=2,
                retry_if_error=True
            )
        }
        job_init_retries = shared_config[group.name].max_retries-1
        job = self._new_test_job(
            group, cast(Repository, None), job_init_retries)

        # job will fails, but retry is < max_retries
        job_runner = EnqueuedJobRunner(self.factory_fails,
                                       self.repository_factory, {}, shared_config)

        job_runner.run_job(job)

        job_from_repo = self.repository.get_by_id(job.id)
        # check if the job is in FAILED status
        self.assertEqual(job_from_repo.status, Status.FAILED)

        # check if retries value in job is job_init_retries+1
        self.assertEqual(job_from_repo.retries, job_init_retries)

    def test_run_job_check_if_no_errors_status_is_completed(self) -> None:

        group = self.groups[0]
        shared_config = {
            group.name: SharedAgentConfig(
                max_retries=2,
                retry_if_error=True
            )
        }
        job_init_retries = shared_config[group.name].max_retries-1
        job = self._new_test_job(
            group, cast(Repository, None), job_init_retries)

        # job will fails, but retry is < max_retries
        job_runner = EnqueuedJobRunner(self.factory,
                                       self.repository_factory, {}, shared_config)

        job_runner.run_job(job)

        job_from_repo = self.repository.get_by_id(job.id)
        # check if the job is in COMPLETED status
        self.assertEqual(job_from_repo.status, Status.COMPLETED)

        # check if retries value in job is job_init_retries
        self.assertEqual(job_from_repo.retries, job_init_retries)
