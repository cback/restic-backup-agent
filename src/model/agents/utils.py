# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from model.agents.agent import SharedAgentConfig
from model.jobs.job import Job, Repository
from repository.interfaces.job_repository import JobRepository
from typing import Any
from model.restic.restic import StatusUpdateCallback


def get_credentials(shared_config: SharedAgentConfig, repository: Repository) -> str:
    password = repository.password or shared_config.password
    if not password:
        raise Exception(f'no credentials found for {repository.path}')
    return password


def get_username(shared_config: SharedAgentConfig, job: Job) -> str:
    username = job.username or shared_config.username
    if not username:
        raise Exception(f'no username found for job {job.id}')
    return username

def persist_progress(job: Job, repository: JobRepository) -> StatusUpdateCallback:
    def f(status: dict[str, Any]) -> None:
        if status.get('message_type', None) != "status":
            return
        percent_done = status.get('percent_done', None)
        if not percent_done:
            return
        repository.update_by_id(job.id, progress=percent_done)
    return f
