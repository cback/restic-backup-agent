# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from model.config.logs import get_logger
from typing import Any, Callable, Dict, Optional
from repository.factory.repository_factory import RepositoryFactory
import time
import os
import random
from pathlib import Path
from dacite import from_dict
from dataclasses import dataclass, field
from utils.utils import Utils

@dataclass
class SharedAgentConfig:
    retry_if_error: bool = False
    max_retries: int = 3
    timeout: int = 600
    s3_connections: int = 32
    metadata_bucket: str = 'metadata'
    destination_type: str = 'local'
    destination_env: Dict[str, str] = field(default_factory=dict)
    destination_config: Dict[str, str] = field(default_factory=dict)
    destination_options: Dict[str, str] = field(default_factory=dict)
    bucket_prefix: str = ''
    source_type: str = 'local'
    source_config: Dict[str, str] = field(default_factory=dict)
    username: str = ''
    password: str = ''
    cache_dir: str = '/tmp/cback'
    send_metrics: bool = False

    # copied from switch config if set (otherwise = one day)
    backup_expiration_time: int = 24*60*60
    # copied from switch config if set (otherwise = one day)
    prune_expiration_time: int = 24*60*60
    # copied from switch config if set (otherwise = 30 days)
    verify_expiration_time: int = 30*24*60*60


@dataclass
class AgentConfig:
    lock_dir: str = '/etc/cback/locks'
    wait_time: int = 10
    metrics_template: str = 'cback.{{ job.group.name }}.{{ job.type }}.{{ host }}.{{ metric_name }}'
    shared: Dict[str, SharedAgentConfig] = field(default_factory=dict)
    groups: Dict[str, Any] = field(default_factory=dict)


class Runner(ABC):
    @abstractmethod
    def run(self) -> None:
        pass


RunnerFactory = Callable[[RepositoryFactory,
                          Dict[str, Any], Dict[str, SharedAgentConfig]], Runner]


class Agent:

    def __init__(self, repo_factory: RepositoryFactory, runner_factory: RunnerFactory, config: Dict[str, Dict[str, Any]], agent_name: str, agent_id: int):
        self._repo_factory = repo_factory
        self._config = from_dict(data_class=AgentConfig,
                                 data=config[agent_name])
        self.agent_name = agent_name
        self.agent_id = agent_id

        self._disabled_path = os.path.join(
            self._config.lock_dir, f'{agent_name}-{agent_id}.disabled')

        self._runner = runner_factory(
            repo_factory, self._config.groups, self._config.shared)

        self.log = get_logger()

    def loop(self) -> None:
        while True:
            if self.is_enabled():
                self.log.debug('agent is enabled', extra={
                               'agent': self.agent_name, 'id': self.agent_id})
                try:
                    self._runner.run()
                except KeyboardInterrupt:
                    raise
                except:
                    self.log.exception(
                        f'exception raised', extra={
                            'agent': self.agent_name, 'id': self.agent_id})
            else:
                reason = self.disable_reason()
                self.log.debug('agent is disabled', extra={
                               'agent': self.agent_name, 'id': self.agent_id, 'reason': reason})
            sleep_time = random.randint(1, self._config.wait_time)
            self.log.debug(f'agent will sleep for {sleep_time} secs', extra={
                'agent': self.agent_name, 'id': self.agent_id})
            time.sleep(sleep_time)

    def enable(self) -> None:
        try:
            if os.path.isfile(self._disabled_path):
                Path(self._disabled_path).unlink()
            self.log.info('agent/s enabled on worker node', extra={
                'agent': self.agent_name, 'id': self.agent_id, 'lock_dir': self._config.lock_dir})
        except OSError:
            self.log.exception('exception enabling the agent/s on worker node', extra={
                'agent': self.agent_name, 'id': self.agent_id, 'lock_dir': self._config.lock_dir})

    def disable_reason(self) -> Optional[str]:
        try:
            if os.path.isfile(self._disabled_path):
                with open(self._disabled_path, 'r') as f:
                    return f.read()
        except:
            pass
        return None

    def disable(self, reason: str = '') -> None:
        try:
            if not os.path.isfile(self._disabled_path):
                Utils.check_and_create_folder(self._config.lock_dir)
                with open(self._disabled_path, 'w') as f:
                    f.write(reason)
            self.log.info('agent/s disabled on worker node', extra={
                'agent': self.agent_name, 'id': self.agent_id, 'reason': reason, 'lock_dir': self._config.lock_dir})
        except OSError:
            self.log.exception('exception disabling the agent/s on worker node', extra={
                'agent': self.agent_name, 'id': self.agent_id, 'lock_dir': self._config.lock_dir})

    def is_enabled(self) -> bool:
        return not os.path.isfile(self._disabled_path)
