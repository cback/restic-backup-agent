# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, cast
from model.config.config import invert_agent_config, read_config
from model.config.logs import configure_logger

from model.managers.backup.backup_manager import BackupManager
from model.managers.group.group_manager import GroupManager
from model.managers.prune.prune_manager import PruneManager
from model.managers.verify.verify_manager import VerifyManager
from model.managers.restore.restore_manager import RestoreManager
from model.agents.agent import Agent, AgentConfig, Runner, SharedAgentConfig
from model.agents.backup.backup import EnqueuedBackup
from model.agents.prune.prune import EnqueuedPrune
from model.agents.verify.verify import EnqueuedVerify
from model.agents.restore.restore import EnqueuedRestore
from model.agents.enqueued_job_runner.enqueued_job_runner import EnqueuedJob, EnqueuedJobRunner
from model.agents.switch.switch import SwitchRunner

from dacite import from_dict
from dataclasses import dataclass, field
from model.metrics.metrics_sender import JobMetricsSender, MetricsSender, new
from repository.factory.repository_factory import RepositoryFactory, get_repository_factory


class CBack:

    def __init__(self, config_file: str):
        # load config
        from_file_config = read_config(config_file)
        self.config = invert_agent_config(from_file_config)

        # configure logging
        configure_logger(self.config)

        # managers
        self._group_mgr = cast(GroupManager, None)
        self._backup_mgr = cast(BackupManager, None)
        self._prune_mgr = cast(PruneManager, None)
        self._restore_mgr = cast(RestoreManager, None)
        self._verify_mgr = cast(VerifyManager, None)

        # repository
        self._repository_factory = cast(RepositoryFactory, None)

        # shared config
        self._shared_config = cast(Dict[str, SharedAgentConfig], None)

        # metrics sender
        self._metrics_sender = cast(MetricsSender, None)

        # job metrics sender
        self._job_metrics_sender = cast(JobMetricsSender, None)

    def get_shared_config(self) -> Dict[str, SharedAgentConfig]:
        if self._shared_config:
            return self._shared_config

        @dataclass
        class _SharedPerGroup:
            shared: Dict[str, SharedAgentConfig] = field(default_factory=dict)

        agents_config: Dict[str, Dict[str, Dict]
                            ] = self.config.get('agents', {})

        # the shared config is in one of the agents unders "agents" key
        shared = None
        for c in agents_config.values():

            if 'shared' in c:
                shared = c
                break

        if not shared:
            raise ValueError('shared config not defined')

        shared_per_group = from_dict(data_class=_SharedPerGroup, data=shared)
        self._shared_config = shared_per_group.shared
        return self._shared_config

    def get_repository_factory(self) -> RepositoryFactory:
        if not self._repository_factory:
            self._repository_factory = get_repository_factory(self.config)
        return self._repository_factory

    def get_group_manager(self) -> GroupManager:
        if not self._group_mgr:
            self._group_mgr = GroupManager(
                self.get_repository_factory().get_group_repository())
        return self._group_mgr

    def get_backup_manager(self) -> BackupManager:
        if not self._backup_mgr:
            self._backup_mgr = BackupManager(self.get_repository_factory(
            ), self.get_group_manager(), self.get_shared_config())
        return self._backup_mgr

    def get_prune_manager(self) -> PruneManager:
        if not self._prune_mgr:
            self._prune_mgr = PruneManager(
                self.get_repository_factory(), self.get_group_manager(), self.get_backup_manager(), self.get_shared_config())
        return self._prune_mgr

    def get_restore_manager(self) -> RestoreManager:
        if not self._restore_mgr:
            self._restore_mgr = RestoreManager(
                self.get_repository_factory(), self.get_group_manager(), self.get_backup_manager(), self.get_shared_config())
        return self._restore_mgr

    def get_verify_manager(self) -> VerifyManager:
        if not self._verify_mgr:
            self._verify_mgr = VerifyManager(
                self.get_repository_factory(), self.get_group_manager(), self.get_backup_manager(), self.get_shared_config())
        return self._verify_mgr

    def get_backup_agent(self) -> Agent:
        def enqueued_backup_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedBackup(repo_factory, config, shared_config)

        def enqueued_job_runner_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> Runner:
            job_runner = EnqueuedJobRunner(
                enqueued_backup_fn, repo_factory, config, shared_config)
            job_runner.register_subscriber(
                self.get_job_metrics_sender('backup'))
            return job_runner

        return Agent(self.get_repository_factory(),
                     enqueued_job_runner_fn, self._agent_config(), 'backup', 0)  # TODO: add random number for now

    def get_prune_agent(self) -> Agent:
        def enqueued_prune_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedPrune(repo_factory, config, shared_config)

        def enqueued_job_runner_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> Runner:
            job_runner = EnqueuedJobRunner(
                enqueued_prune_fn, repo_factory, config, shared_config)
            job_runner.register_subscriber(
                self.get_job_metrics_sender('prune'))
            return job_runner

        return Agent(self.get_repository_factory(),
                     enqueued_job_runner_fn, self._agent_config(), 'prune', 0)  # TODO: add random number for now

    def get_verify_agent(self) -> Agent:

        def enqueued_verify_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedVerify(repo_factory, config, shared_config)

        def enqueued_job_runner_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> Runner:
            job_runner = EnqueuedJobRunner(
                enqueued_verify_fn, repo_factory, config, shared_config)
            job_runner.register_subscriber(
                self.get_job_metrics_sender('verify'))
            return job_runner

        return Agent(self.get_repository_factory(),
                     enqueued_job_runner_fn, self._agent_config(), 'verify', 0)  # TODO: add random number for now

    def get_restore_agent(self) -> Agent:
        def enqueued_restore_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedRestore(repo_factory, config, shared_config)

        def enqueued_job_runner_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> Runner:
            job_runner = EnqueuedJobRunner(
                enqueued_restore_fn, repo_factory, config, shared_config)
            job_runner.register_subscriber(
                self.get_job_metrics_sender('restore'))
            return job_runner

        return Agent(self.get_repository_factory(),
                     enqueued_job_runner_fn, self._agent_config(), 'restore', 0)  # TODO: add random number for now

    def get_switch_agent(self) -> Agent:
        def switch_runner_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], _: Dict[str, SharedAgentConfig]) -> Runner:
            return SwitchRunner(repo_factory, self.get_group_manager(), config)
        return Agent(self.get_repository_factory(),
                     switch_runner_fn, self._agent_config(), 'switch', 0)  # TODO: add random number for now

    def get_backup_runner(self) -> EnqueuedJobRunner:
        backup_config = self._agent_config().get('backup', {}).get('groups', {})

        def enqueued_backup_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedBackup(repo_factory, config, shared_config)
        job_runner = EnqueuedJobRunner(enqueued_backup_fn, self.get_repository_factory(
        ), backup_config, self.get_shared_config())
        job_runner.register_subscriber(self.get_job_metrics_sender('backup'))
        return job_runner

    def get_prune_runner(self) -> EnqueuedJobRunner:
        prune_config = self._agent_config().get('prune', {}).get('groups', {})

        def enqueued_prune_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedPrune(repo_factory, config, shared_config)
        job_runner = EnqueuedJobRunner(enqueued_prune_fn, self.get_repository_factory(
        ), prune_config, self.get_shared_config())
        job_runner.register_subscriber(self.get_job_metrics_sender('prune'))
        return job_runner

    def get_verify_runner(self) -> EnqueuedJobRunner:
        verify_config = self._agent_config().get('verify', {}).get('groups', {})

        def enqueued_verify_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedVerify(repo_factory, config, shared_config)
        job_runner = EnqueuedJobRunner(enqueued_verify_fn, self.get_repository_factory(
        ), verify_config, self.get_shared_config())
        job_runner.register_subscriber(self.get_job_metrics_sender('verify'))
        return job_runner

    def get_restore_runner(self) -> EnqueuedJobRunner:
        restore_config = self._agent_config().get('restore', {}).get('groups', {})

        def enqueued_restore_fn(repo_factory: RepositoryFactory, config: Dict[str, Any], shared_config: Dict[str, SharedAgentConfig]) -> EnqueuedJob:
            return EnqueuedRestore(repo_factory, config, shared_config)
        job_runner = EnqueuedJobRunner(enqueued_restore_fn, self.get_repository_factory(
        ), restore_config, self.get_shared_config())
        job_runner.register_subscriber(self.get_job_metrics_sender('restore'))
        return job_runner

    def _agent_config(self) -> Dict[str, Any]:
        return self.config.get('agents', {})

    def get_service_config(self, name: str) -> Dict[str, Any]:
        services = self.config.get("services", {})
        return cast(Dict[str, Any], services.get(name, {}))

    def get_metrics_sender(self) -> MetricsSender:
        if not self._metrics_sender:
            config = self.config.get('metrics', {})
            self._metrics_sender = new(config)
        return self._metrics_sender

    def get_job_metrics_sender(self, job: str) -> JobMetricsSender:
        if not self._job_metrics_sender:
            driver = self.get_metrics_sender()
            agent_config = from_dict(
                data_class=AgentConfig, data=self._agent_config()[job])
            self._job_metrics_sender = JobMetricsSender(agent_config, driver)
        return self._job_metrics_sender
