# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import unittest
from typing import Dict, Any

from model.config.config import invert_agent_config


class TestConfig(unittest.TestCase):

    def test_invert_agent_config_empty_config(self) -> None:
        empty_config: Dict[str, Any] = {}

        res_config = invert_agent_config(empty_config)
        self.assertDictEqual(res_config, {}, 'result dict should be empty')

    def test_invert_agent_config_no_agents_key(self) -> None:
        no_agents_config = {
            'foo': {'bar': 'foobar'},
            'bar': {'foo': 'barfoo'}
        }

        res_config = invert_agent_config(no_agents_config)
        self.assertDictEqual(res_config, no_agents_config)

    def test_invert_agent_config_only_one_group_no_all(self) -> None:
        config_one_group = {
            'agents': {
                'cernbox': {
                    'backup': {
                        'enabled': True,
                        'force': False,
                        'exclude_list': ['.txt']
                    },
                    'prune': {
                        'enabled': False,
                        'retention_policy': {}
                    },
                    'shared': {
                        'max_retries': 3,
                        'timeout': 200
                    }
                }
            }
        }

        res_config = invert_agent_config(config_one_group)

        self.assertDictEqual(res_config, {
            'agents': {
                'backup': {
                    'shared': {
                        'cernbox': {
                            'max_retries': 3,
                            'timeout': 200
                        }
                    },
                    'groups': {
                        'cernbox': {
                            'enabled': True,
                            'force': False,
                            'exclude_list': ['.txt']
                        }
                    }
                },
                'prune': {
                    'shared': {
                        'cernbox': {
                            'max_retries': 3,
                            'timeout': 200
                        }
                    },
                    'groups': {
                        'cernbox': {
                            'enabled': False,
                            'retention_policy': {}
                        }
                    }
                }
            }
        })

    def test_invert_agent_config_only_one_group_with_all(self) -> None:
        config_one_group = {
            'agents': {
                'all': {
                    'lock_dir': '/etc/cback/agents.lock/',
                    'wait_time': 100,
                },
                'cernbox': {
                    'backup': {
                        'enabled': True,
                        'force': False,
                        'exclude_list': ['.txt']
                    },
                    'prune': {
                        'enabled': False,
                        'retention_policy': {}
                    },
                    'shared': {
                        'max_retries': 3,
                        'timeout': 200
                    }
                }
            }

        }

        res_config = invert_agent_config(config_one_group)

        self.assertDictEqual(res_config, {
            'agents': {
                'backup': {
                    'lock_dir': '/etc/cback/agents.lock/',
                    'wait_time': 100,
                    'shared': {
                        'cernbox': {
                            'max_retries': 3,
                            'timeout': 200
                        }
                    },
                    'groups': {
                        'cernbox': {
                            'enabled': True,
                            'force': False,
                            'exclude_list': ['.txt']
                        }
                    }
                },
                'prune': {
                    'lock_dir': '/etc/cback/agents.lock/',
                    'wait_time': 100,
                    'shared': {
                        'cernbox': {
                            'max_retries': 3,
                            'timeout': 200
                        }
                    },
                    'groups': {
                        'cernbox': {
                            'enabled': False,
                            'retention_policy': {}
                        }
                    }
                }
            }
        })

    def test_invert_agent_config_more_groups(self) -> None:
        config_one_group = {
            'agents': {
                'all': {
                    'lock_dir': '/etc/cback/agents.lock/',
                    'wait_time': 100,
                },
                'cernbox': {
                    'backup': {
                        'enabled': True,
                        'force': False,
                        'exclude_list': ['.txt']
                    },
                    'prune': {
                        'enabled': False,
                        'retention_policy': {}
                    },
                    'shared': {
                        'max_retries': 3,
                        'timeout': 200
                    }
                },
                'ceph': {
                    'backup': {
                        'enabled': False,
                        'force': False,
                    },
                    'prune': {
                        'enabled': True,
                        'retention_policy': {}
                    },
                    'shared': {
                        'max_retries': 20,
                        'timeout': 1
                    }
                }
            }

        }

        res_config = invert_agent_config(config_one_group)

        self.assertDictEqual(res_config, {
            'agents': {
                'backup': {
                    'lock_dir': '/etc/cback/agents.lock/',
                    'wait_time': 100,
                    'shared': {
                        'cernbox': {
                            'max_retries': 3,
                            'timeout': 200
                        },
                        'ceph': {
                            'max_retries': 20,
                            'timeout': 1
                        }
                    },
                    'groups': {
                        'cernbox': {
                            'enabled': True,
                            'force': False,
                            'exclude_list': ['.txt']
                        },
                        'ceph': {
                            'enabled': False,
                            'force': False,
                        }
                    }
                },
                'prune': {
                    'lock_dir': '/etc/cback/agents.lock/',
                    'wait_time': 100,
                    'shared': {
                        'cernbox': {
                            'max_retries': 3,
                            'timeout': 200
                        },
                        'ceph': {
                            'max_retries': 20,
                            'timeout': 1
                        }
                    },
                    'groups': {
                        'cernbox': {
                            'enabled': False,
                            'retention_policy': {}
                        },
                        'ceph': {
                            'enabled': True,
                            'retention_policy': {}
                        }
                    }
                }
            }
        })
