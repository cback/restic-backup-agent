# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import logging
from dataclasses import dataclass
from typing import Any, Dict, List, Mapping, Optional
from dacite import from_dict
from coloredlogs import ColoredFormatter, DEFAULT_FORMAT_STYLE
from pythonjsonlogger import jsonlogger
from termcolor import colored


@dataclass
class LogConfig:
    output: str = ""
    level: str = "info"


FIELD_STYLES = {
    'asctime': {'color': 'blue'},
    'hostname': {'color': 'magenta'},
    'pathname': {'color': 'white', 'bold': True},
    'lineno': {'color': 'white', 'bold': True},
    'name': {'color': 'blue'},
    'programname': {'color': 'cyan'},
    'username': {'color': 'yellow'}
}

LEVEL_STYLES = {
    'critical': {'bold': True, 'color': 'red'},
    'debug': {'color': 'white'},
    'error': {'color': 'red'},
    'info': {'color': 'white'},
    'success': {'bold': True, 'color': 'green'},
    'warning': {'color': 'yellow'}
}

JSON_KEYS = [
    'asctime',
    'created',
    'filename',
    'funcName',
    'levelname',
    'levelno',
    'lineno',
    'module',
    'msecs',
    'message',
    'name',
    'pathname',
    'process',
    'processName'
]

EXTRA_KEY_COLOR = 'green'


def _get_log_config(config: Dict[str, Any]) -> LogConfig:
    log_dict = config.get('log', {})
    return from_dict(data_class=LogConfig, data=log_dict)


def _get_level(level: str) -> int:
    if level == 'debug':
        return logging.DEBUG
    elif level == 'info':
        return logging.INFO
    elif level == 'warning':
        return logging.WARNING
    elif level == 'error':
        return logging.ERROR
    elif level == 'critical':
        return logging.CRITICAL
    else:
        raise ValueError(f'level {level} not recognised')


def make_record_with_extra(self: logging.Logger, name: str, level: int, fn: str, lno: int, msg: object, args: Any, exc_info: Any, func: Optional[str] = None, extra: Optional[Mapping[str, Any]] = None, sinfo: Optional[str] = None) -> logging.LogRecord:
    record = original_makeRecord(
        self, name, level, fn, lno, msg, args, exc_info, func, extra, sinfo)
    record._extra = extra  # type: ignore[attr-defined]
    return record


# wrap the original makeRecord func to save the extras:
# https://stackoverflow.com/questions/59176101/extract-the-extra-fields-in-logging-call-in-log-formatter
original_makeRecord = logging.Logger.makeRecord
logging.Logger.makeRecord = make_record_with_extra  # type: ignore


class ColoredFormatWithExtras(
    ColoredFormatter  # type: ignore[no-any-unimported]
):

    def __init__(self, fmt: Optional[str] = None, datefmt: Optional[str] = None, style: str = DEFAULT_FORMAT_STYLE, level_styles: Optional[Dict] = None, field_styles: Optional[Dict] = None, extra_key_color: str = 'white'):
        super().__init__(fmt, datefmt, style, level_styles, field_styles)
        self.extra_key_color = extra_key_color

    def _format_extras(self, extra: Dict[str, Any]) -> str:
        return ' '.join(
            (f'{colored(key+"=", self.extra_key_color)}{value}' for key, value in extra.items()))

    def format(self, record: logging.LogRecord) -> str:
        f = super().format(record)
        if '_extra' in record.__dict__ \
                and isinstance(record._extra, dict):  # type: ignore[attr-defined]
            f = \
                f"{f} {self._format_extras(record._extra)}"  # type: ignore[attr-defined]
        return str(f)


def configure_logger(config: Dict[str, Any]) -> None:
    c = _get_log_config(config)
    logger = get_logger()
    logger.handlers.clear()
    logger.setLevel(_get_level(c.level))

    colored_format = ColoredFormatWithExtras(
        f"%(asctime)s %(levelname)s %(pathname)s:%(lineno)d {colored('>', 'cyan')} %(message)s {colored('pid=', EXTRA_KEY_COLOR)}%(process)d",
        field_styles=FIELD_STYLES,
        extra_key_color=EXTRA_KEY_COLOR
    )

    # logging handlers
    console = logging.StreamHandler()
    _add_handler(logger, console, colored_format)

    if c.output != "":
        def log_format(
            x: List[str]) -> List[str]: return ['%({0:s})s'.format(i) for i in x]
        format_str = ' '.join(log_format(JSON_KEYS))

        json_format = jsonlogger.JsonFormatter(format_str)
        file = logging.FileHandler(filename=c.output, mode='a')
        _add_handler(logger, file, json_format)


def _add_handler(logger: logging.Logger, handler: logging.Handler, format: logging.Formatter) -> None:
    handler.setFormatter(format)
    logger.addHandler(handler)


def get_logger() -> logging.Logger:
    log = logging.getLogger("cback.logs")
    log.propagate = False
    return log
