# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import os
import json
import toml
import yaml
from yaml import Loader
import pathlib
import copy
from typing import Dict, Any, cast


def read_config(file: str) -> Dict[str, Any]:
    if not os.path.isfile(file):
        raise Exception('config file not found in ' + file)
    config_content = ''
    with open(file, 'r') as config:
        config_content = config.read()

    file_ext = pathlib.Path(file).suffix

    if file_ext == '.json':
        return cast(Dict[str, Any], json.loads(config_content))
    elif file_ext == '.toml':
        return {**toml.loads(config_content)}
    elif file_ext == '.yaml':
        yaml_parsed = yaml.load(config_content, Loader=Loader)
        if not isinstance(yaml_parsed, dict):
            raise TypeError('config is a dict')
        return yaml_parsed
    else:
        raise ValueError(f'file {file_ext} not supported for config')


def invert_agent_config(config: Dict[str, Any]) -> Dict[str, Any]:

    if 'agents' not in config:
        return config

    # map[group]map[agent]map
    agents_per_group: Dict[str, Dict[str, Any]] = config['agents']
    _check_is_dict(agents_per_group, 'agent is not a dict')

    # get the special key 'all' that will contain the configuration
    # for all the groups for all the agents

    all: Dict[str, Any] = agents_per_group.get('all', {})
    _check_is_dict(all, 'agent.all is not a dict')

    agents: Dict[str, Any] = {}
    shared_in_agent = {}
    for group, all_agents_in_group in agents_per_group.items():

        if group == 'all':
            continue

        for agent, config_group_agent in all_agents_in_group.items():

            if agent == 'shared':
                shared_in_agent[group] = copy.deepcopy(config_group_agent)
                continue

            agents[agent] = {**all, **agents.get(agent, {})}

            # put shared config in the agent config
            agents[agent]['shared'] = shared_in_agent

            # add group config in agents.<agent>.groups
            agents_agent_groups = agents[agent].setdefault('groups', {})
            agents_agent_groups[group] = copy.deepcopy(config_group_agent)

    res = copy.deepcopy(config)
    res['agents'] = agents
    return res


def _check_is_dict(obj: Any, msg: str) -> None:
    if not isinstance(obj, Dict):
        raise TypeError(msg)
