# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import Any, Dict, Iterable, List, Optional
from dataclasses import dataclass
from pydantic import BaseModel
from portal.auth.auth import Authenticator
from portal.user.user import User
from portal.auth.errors import InvalidCredentialsError
from repository.mysql.mysql_repository import MySQLConfig, MySQLRepository
import bcrypt


class Config(BaseModel):
    host: str
    user: str
    port: int
    password: str
    database: str


@dataclass
class Token:
    id: int
    hashed_token: str
    created: datetime
    expiration: Optional[datetime]
    comment: Optional[str]
    groups: List[str]


class TokenNotFoundError(Exception):
    pass


class TokenDB:
    """
    The TokenDB class manages the sql tables used to store all the info regarding the tokens
    There are two tables:
        - token(id, hashed_token, created, expiration, comment)
        - token_group(id, token_id, group)
    """

    @dataclass
    class IDToken:
        # just a convenient class to get only the id
        # and hashed token from the db
        id: int
        hashed_token: str

    def __init__(self, config: Dict[str, Any]) -> None:
        c = Config.parse_obj(config)
        mysql_config = MySQLConfig(
            host=c.host, user=c.user, port=c.port, password=c.password, database=c.database)
        self.repo = MySQLRepository(mysql_config)

    def get_token_id(self, token: str) -> int:
        """ checks if the token exists in the db """
        # as the token in the db is hashed, we need to check every generated token
        for e in self._get_all_tokens():
            if self._verify_token(e.hashed_token, token):
                return e.id
        raise TokenNotFoundError

    def _verify_token(self, hashed: str, token: str) -> bool:
        return bcrypt.checkpw(hashed_password=hashed.encode(), password=token.encode())

    def _get_all_tokens(self) -> Iterable['TokenDB.IDToken']:
        query = 'SELECT id, hashed_token FROM token'
        res = self.repo.query([query], [()]).res
        return map(lambda d: TokenDB.IDToken(id=d['id'], hashed_token=d['hashed_token']), res)

    def get_token(self, id: int) -> Token:
        query = """SELECT token.id as id, hashed_token, created, expiration, comment, `group`
                   FROM token, token_group
                   WHERE token.id = %s AND token.id = token_group.token_id"""
        res = self.repo.query([query], [(id,)]).res
        if not res:
            raise ValueError(f'token with id {id} not found')
        t = res[0]
        groups: List[str] = list(map(lambda t: str(t['group']), res))
        return Token(
            id=t['id'],
            hashed_token=t['hashed_token'],
            comment=t['comment'],
            created=t['created'],
            expiration=t['expiration'],
            groups=groups
        )

    def get_token_list(self) -> List[Token]:
        tokens_lst = self._get_all_tokens()
        res: List[Token] = []
        for token in tokens_lst:
            res.append(self.get_token(token.id))
        return res

    def add_token(self, secret: str, expiration: datetime, groups: List[str], comment: Optional[str] = None) -> Token:
        hashed_secret = bcrypt.hashpw(secret.encode(), bcrypt.gensalt())

        query_insert_token = """INSERT INTO token (hashed_token, created, expiration, comment)
                   VALUES (%s,CURRENT_TIMESTAMP,%s,%s)
        """
        id = self.repo.query([query_insert_token], [
                             (hashed_secret, expiration, comment)]).lastrowid

        query_insert_groups = "INSERT INTO token_group (token_id, `group`) VALUES "
        params: tuple = ()
        for group in groups:
            query_insert_groups += "(%s,%s),"
            params += (id, group)

        try:
            self.repo.query([query_insert_groups[:-1]], [params])
        except Exception as e:
            self.remove_token(id)
            raise e

        return self.get_token(id)

    def remove_token(self, id: int) -> None:
        query = "DELETE FROM token WHERE id = %s"
        self.repo.query([query], [(id,)])


class ImpersonatorAuthenticator(Authenticator):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)
        self.token_db = TokenDB(config)

    def authenticate(self, client_id: str, client_secret: str) -> User:
        # client_id is the username to impersonate
        # client_secret is the token previously generate to impersonate the user
        try:
            id = self.token_db.get_token_id(client_secret)
        except TokenNotFoundError:
            raise InvalidCredentialsError

        return self.user_manager.get_user(client_id)
