# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict
from pydantic import BaseModel
from portal.auth.auth import Authenticator
from portal.user.user import User
from portal.auth.errors import InvalidCredentialsError
from portal.user.errors import UserNotFoundError
from ldap3 import Server, Connection, Entry
from ldap3.utils.conv import escape_filter_chars
from ldap3.core.exceptions import LDAPInvalidCredentialsResult


class Attributes(BaseModel):
    cn: str = "cn"
    mail: str = "mail"
    display_name: str = "displayName"
    uid_number: str = "uidNumber"
    gid_number: str = "gidNumber"


class Config(BaseModel):
    hostname: str
    port: int = 636
    bind_username: str
    bind_password: str
    base_dn: str
    login_filter: str
    layout: Attributes = Attributes()


class LDAPAuthenticator(Authenticator):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)

    def _get_connection(self) -> Connection:
        server = Server(self.config.hostname, self.config.port)
        return Connection(server, self.config.bind_username, self.config.bind_password, raise_exceptions=True)

    def _client_login_filer(self, username: str) -> str:
        return self.config.login_filter.replace("{{login}}", escape_filter_chars(username))

    def authenticate(self, client_id: str, client_secret: str) -> User:
        with self._get_connection() as conn:

            if not conn.search(self.config.base_dn,
                               self._client_login_filer(client_id),
                               attributes=[
                                   self.config.layout.cn,
                                   self.config.layout.mail,
                                   self.config.layout.display_name,
                                   self.config.layout.uid_number,
                                   self.config.layout.gid_number
                               ]):
                raise UserNotFoundError(client_id)

            if len(conn.entries) != 1:
                raise UserNotFoundError(client_id)

            user: Entry = conn.entries[0]

            # check the user's secret
            try:
                s = conn.rebind(user.entry_dn, client_secret)
            except LDAPInvalidCredentialsResult as i:
                raise InvalidCredentialsError from i

            if not s:
                raise Exception(conn.result)

            u = User(
                username=user[self.config.layout.cn].value,
                display_name=user[self.config.layout.display_name].value,
                email=user[self.config.layout.mail].value,
                gid=user[self.config.layout.gid_number].value,
                uid=user[self.config.layout.uid_number].value,
                active=True
            )

            u.groups = self.user_manager.get_groups(u)
            return u
