# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime, timedelta
from http import client
from fastapi.security import HTTPBasic, HTTPBearer, HTTPBasicCredentials, HTTPAuthorizationCredentials
from abc import ABC, abstractmethod
from typing import Any, Callable, Dict, Iterable, List, Optional, Tuple
from fastapi import Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field, parse_obj_as
from jose import ExpiredSignatureError, JWTError, jwt
from model.config.logs import get_logger

from portal.scope.scope import Scope, ScopeProvider, new
from model.managers.group.group_manager import GroupManager
from .errors import InvalidCredentialsError
from portal.user.errors import UserNotFoundError
from model.managers.backup.backup_manager import BackupManager
from portal.user.user import User
from portal.user.manager import UserManager
from .cache import Cache, NotInCacheError, RedisConfig

log = get_logger()


class Credentials(BaseModel):
    method: str
    client_id: str
    client_secret: str


class Authenticator(ABC):

    @abstractmethod
    def authenticate(self, client_id: str, client_secret: str) -> User: ...

    def set_user_manager(self, user_manager: UserManager) -> None:
        self.user_manager = user_manager


class Config(BaseModel):
    class ScopeConfig(BaseModel):
        rules: Dict[str, List[str]] = {}  # map[group]list_of_scope_provider
        drivers: Dict[str, Any] = {}

    class JwtConfig(BaseModel):
        secret_key: str
        issuer: Optional[str]
        algorithm: str = 'HS256'
        expire: int = Field(default=30, ge=1)  # expressed in minutes

    providers: Dict[str, Any] = {}
    # TODO (gdelmont): allow an authentication method to have multiple mappings?
    # map[auth_method]provider_name (auth_method=["basic", "bearer", ...])
    mapping: Dict[str, List[str]] = {}
    scope: ScopeConfig = ScopeConfig()
    jwt: JwtConfig

    cache: RedisConfig


NewAuthFunc = Callable[[Dict[str, Any]], Authenticator]

_authenticators: Dict[str, NewAuthFunc] = {}


def register(name: str, new: NewAuthFunc) -> None:
    _authenticators[name] = new


# TODO (gdelmont): add support to api_key authentication


def get_credentials(
        basic_credentials: Optional[HTTPBasicCredentials] = Depends(
            HTTPBasic(auto_error=False)),
        bearer_credentials: Optional[HTTPAuthorizationCredentials] = Depends(
            HTTPBearer(auto_error=False))
) -> Credentials:
    log.debug("getting credentials from http request")
    if basic_credentials is not None:
        log.debug("found credentials in basic authentication",
                  extra={"client_id": basic_credentials.username})
        return Credentials(method="basic", client_id=basic_credentials.username, client_secret=basic_credentials.password)
    if bearer_credentials is not None:
        log.debug("found credentials in bearer authentication")
        return Credentials(method="bearer", client_id="", client_secret=bearer_credentials.credentials)
    log.info("no credentials found for http request")
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Invalid authentication credentials",
        headers={
            "WWW-Authenticate": "Basic"
        },
    )


class AuthenticatorManager:

    def __init__(self, config: Dict[str, Any], group_mgr: GroupManager, backup_manager: BackupManager, user_manager: UserManager) -> None:
        self.config = Config.parse_obj(config)
        self.group_mgr = group_mgr

        self._cache = Cache(self.config.cache)

        # create all the authentication driver mentioned in the mapping config
        self._authenticators: Dict[str, Authenticator] = {}
        for providers_list in self.config.mapping.values():
            for provider in providers_list:
                if provider in self._authenticators:
                    # already created
                    continue
                # check if the provider is in the registered ones
                try:
                    f = _authenticators[provider]
                except KeyError:
                    raise ValueError(f"provider {provider} not found")

                new_auth = f(self.config.providers.get(provider, {}))
                new_auth.set_user_manager(user_manager)
                self._authenticators[provider] = new_auth

        # create all the scope drivers (are used after authenticating the user to mint a token)
        # mentioned in the mapping config
        self._scope_drivers: Dict[str, ScopeProvider] = {}
        for drivers in self.config.scope.rules.values():
            for driver in drivers:
                if driver in self._scope_drivers:
                    # already created
                    continue
                self._scope_drivers[driver] = new(
                    driver,
                    self.config.scope.drivers.get(driver, {}),
                    backup_manager
                )

    def get_scope(self, user: User, groups: Iterable[str] = []) -> Scope:
        # gather all the scopes from the different driver, for every group
        # N.B. the groups not defined in the config are skipped!

        try:
            s = self._cache.get_scope_for_user(user)
        except NotInCacheError:
            log.info("scope: cache miss", extra={"user": user.username})
            pass
        else:
            log.info("scope: cache hit", extra={
                     "user": user.username, "scope": s})
            return s

        scope: Scope = []
        if not groups:
            groups = map(lambda g: g.name, self.group_mgr.list_groups())

        for group in groups:
            if group not in self.config.scope.rules:
                log.info("skipping group because is not in the config",
                         extra={"user": user.username, "group": group})
                continue
            drivers = self.config.scope.rules[group]
            log.debug("selected drivers for group", extra={
                      "user": user.username, "group": group, "drivers": drivers})
            for driver in drivers:
                scope_group = self._scope_drivers[driver].get_scope(
                    user, group)
                log.info("scope: got scope", extra={
                         "user": user.username, "group": group, "driver": driver, "scope": scope_group})
                scope += scope_group

        self._cache.set_scope_for_user(user, scope)
        log.debug("stored scope in cache", extra={
                  "user": user.username, "scope": scope})

        return scope

    def _mint_token(self, user: User) -> str:
        exp_time = datetime.utcnow() + timedelta(minutes=self.config.jwt.expire)
        token: str = jwt.encode({'user': jsonable_encoder(user), 'exp': exp_time},
                                self.config.jwt.secret_key, self.config.jwt.algorithm)
        return token

    def check_token(self, token: str) -> Optional[User]:
        # None if not valid
        # otherwise user + scope
        try:
            d: Dict[str, Any] = jwt.decode(
                token, self.config.jwt.secret_key, self.config.jwt.algorithm)
        except (JWTError, ExpiredSignatureError):
            return None
        if 'user' not in d:
            return None
        return parse_obj_as(User, d['user'])

    def authenticate(self, client_id: str, client_secret: str, method: str) -> Tuple[User, str]:
        # based on the auth method, we need to check which is the
        # provider to use in the mapping config
        log.info("authenticating user", extra={
                 "user": client_id, "method": method})
        if method not in self.config.mapping:
            log.error("authentication method not recognised",
                      extra={"user": client_id, "method": method})
            raise ValueError(f"authentication method {method} not recognised")
        drivers = self.config.mapping[method]
        log.debug("drivers selected for the chosen authentication methos", extra={
                  "user": client_id, "method": method, "drivers": drivers})
        # try with all the provider in the mapping list
        # until one of them authenticate the client_id with the secret
        last_exception: Optional[Exception] = None
        for driver_name in drivers:
            log.debug("authenticating user with driver", extra={
                      "user": client_id, "method": method, "driver": driver_name})
            try:
                user = self._authenticators[driver_name].authenticate(
                    client_id, client_secret)
            except Exception as e:
                last_exception = e
                log.debug("user cannot be authenticated with the provided credentials", extra={
                          "user": client_id, "method": method, "driver": driver_name, "error": str(e)})
                continue  # try with the next authentication method
            else:
                log.info("user authenticated", extra={
                         "user": client_id, "method": method, "driver": driver_name})
                break
        else:
            # if we reach this point means that no driver was able
            # to authenticate the client_id
            # will raise the last exeption
            if last_exception:
                raise last_exception
        token = self._mint_token(user)
        return user, token

    def __call__(self, c: Credentials = Depends(get_credentials)) -> Tuple[User, str]:
        try:
            return self.authenticate(c.client_id, c.client_secret, c.method)
        except (UserNotFoundError, InvalidCredentialsError):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="invalid user/password"
            )
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail=str(e)
            )
