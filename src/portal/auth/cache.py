# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import json
from pydantic import BaseModel
from pydantic import parse_raw_as
from fastapi.encoders import jsonable_encoder
import redis
from portal.user.user import User
from portal.scope.scope import Scope


class RedisConfig(BaseModel):
    host: str
    port: int
    password: str
    expiration_scope: int = 3600


class NotInCacheError(Exception):
    pass


class Cache:

    def __init__(self, config: RedisConfig) -> None:
        self.config = config
        self.redis = redis.Redis(
            host=self.config.host,
            port=self.config.port,
            password=self.config.password,
        )

    @staticmethod
    def _key_scope_user(u: User) -> str:
        return f"scope:{u.username}"

    def get_scope_for_user(self, u: User) -> Scope:
        res = self.redis.get(Cache._key_scope_user(u))
        if not res:
            raise NotInCacheError
        return parse_raw_as(Scope, res)

    def set_scope_for_user(self, u: User, s: Scope) -> None:
        scope = json.dumps(jsonable_encoder(s))
        key = Cache._key_scope_user(u)
        self.redis.set(key, scope, self.config.expiration_scope)
