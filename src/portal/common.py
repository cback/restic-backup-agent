# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict

from model.cback import CBack
import settings as settings
from model.managers.group.group_manager import GroupManager
from model.managers.backup.backup_manager import BackupManager


cback = CBack(settings.CBACK_CONFIG)


def get_portal_config() -> Dict[str, Any]:
    return cback.config.get("portal", {})


def get_group_mgr() -> GroupManager:
    return cback.get_group_manager()


def get_backup_mgr() -> BackupManager:
    return cback.get_backup_manager()
