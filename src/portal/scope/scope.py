# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from enum import Enum
from typing import Any, Callable, Dict, List, TYPE_CHECKING, Optional, Set, Union
from pydantic import BaseModel
from model.managers.backup.backup_manager import BackupManager

if TYPE_CHECKING:
    from portal.user.user import User


class Permission(str, Enum):
    RESTORE = "restore"
    CREATE = "create"
    RUN = "run"
    DELETE = "delete"


class Special(str, Enum):
    ALL = '*'


class ScopeTuple(BaseModel):
    """
    A ScopeTuple is composed by three elements:
        - group: can be a group name, the special value '*' with the semantic of 'ALL GROUPS'
                or None (if the opaque_id is set to a number)
        - id: is the id of a backup job, can be a number (string encoded), or the special
                    value '*', meaning all the jobs
        - permissions: a set of Permission, indicating the operation is possible to do to a target(s)
    The target is specified with the combination of group and id
    Possible values of group and id:
        - (None, n) = the target specified is the backup job n
        - ("group_name", "*") = the targets are all the backup jobs in the group named "group_name"
        - ("*", "*") = the targs are all the backups job in the system
    """
    group: Optional[str]
    id: Union[int, Special]
    permissions: List[Permission]


Scope = List[ScopeTuple]
EmptyScope: Scope = []


class ScopeProvider(ABC):

    """
    get_scope get the scope for the user, based on some rules
    """
    @abstractmethod
    def get_scope(self, user: 'User', group: str) -> Scope: ...

    def set_backup_manager(self, backup_mgr: BackupManager) -> None:
        self.backup_manager = backup_mgr


NewScopeFunc = Callable[[Dict[str, Any]], ScopeProvider]

_scope_providers: Dict[str, NewScopeFunc] = {}


def register(name: str, new: NewScopeFunc) -> None:
    _scope_providers[name] = new


def new(driver: str, config: Dict[str, Any], backup_manager: BackupManager) -> ScopeProvider:
    d = _scope_providers[driver](config)
    d.set_backup_manager(backup_manager)
    return d


class ParsedScope(BaseModel):
    permissions: List[Permission]


class AllGroups(ParsedScope):
    pass


class AllBackupsInGroup(ParsedScope):
    group: str


class BackupID(ParsedScope):
    id: int


def parse(scope: Scope) -> List[ParsedScope]:
    parsed_scope: List[ParsedScope] = []
    for s in scope:
        if isinstance(s.id, int):
            parsed_scope.append(BackupID(id=s.id, permissions=s.permissions))
            continue
        if s.id == Special.ALL:
            # all the jobs
            # there are two possibilities for the group
            #   - '*' for all the groups
            #   - "group_name" for a specific group
            if s.group == '*':  # special value for all the groups
                parsed_scope.append(AllGroups(permissions=s.permissions))
            else:
                if s.group is None:
                    raise ValueError("group cannot be None when id is *")
                parsed_scope.append(AllBackupsInGroup(
                    group=s.group, permissions=s.permissions))
    return parsed_scope
