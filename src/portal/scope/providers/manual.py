# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, Iterable, List, Mapping, Set

from pydantic import BaseModel
from portal.scope.scope import Permission, ScopeProvider, ScopeTuple, Special
from portal.user.user import User
from portal.scope.scope import Scope
from repository.mysql.mysql_repository import MySQLConfig, MySQLRepository


class Config(BaseModel):
    host: str
    user: str
    port: int
    password: str
    database: str


class ManualScopeProvider(ScopeProvider):
    """
    The ManualScopeProvider class manages the sql tables used to store all the info regarding a user defined scope
    The following are the used tables:
        - scopes(id, opaque_user, group, backup_id, permissions)
    """

    def __init__(self, config: Dict[str, Any]) -> None:
        c = Config.parse_obj(config)
        mysql_config = MySQLConfig(
            host=c.host, user=c.user, port=c.port, password=c.password, database=c.database)
        self.repo = MySQLRepository(mysql_config)
        self.config = c

    def get_scope(self, user: 'User', _: str) -> Scope:
        query = """SELECT id, opaque_user, `group`, backup_id, permissions
                    FROM  scopes
                    WHERE opaque_user = %s
                """
        params = (user.username,)

        r = self.repo.query([query], [params])
        scope: List[ScopeTuple] = []

        for entry in r.res:
            s = ScopeTuple(
                group=entry["group"],
                id=entry["backup_id"] if entry["backup_id"] !=
                -1 else Special.ALL,
                permissions=ManualScopeProvider._to_set_permissions(
                    entry["permissions"])
            )
            scope.append(s)

        return scope

    def add_scope(self, scope: Scope, username: str) -> None:
        query = "INSERT INTO scopes (opaque_user, `group`, backup_id, permissions) VALUES "
        params: tuple = tuple()
        for s in scope:
            perm = ManualScopeProvider._to_int_permissions(s.permissions)
            query += "(%s,%s,%s,%s),"
            backup_id = s.id if isinstance(s.id, int) else -1
            params += (username, s.group, backup_id, perm)
        self.repo.query([query[:-1]], [params])

    def remove_scope(self, username: str) -> None:
        query = "DELETE FROM scopes where opaque_user = %s"
        self.repo.query([query], [(username,)])

    def list_scopes(self) -> Mapping[str, Scope]:
        query = """SELECT id, opaque_user, `group`, backup_id, permissions
                    FROM  scopes
                """
        r = self.repo.query([query], [()])

        scopes: Dict[str, Scope] = {}

        for entry in r.res:
            user = entry["opaque_user"]

            if user not in scopes:
                scopes[user] = []
            s = ScopeTuple(
                group=entry["group"],
                id=entry["backup_id"] if entry["backup_id"] !=
                -1 else Special.ALL,
                permissions=ManualScopeProvider._to_set_permissions(
                    entry["permissions"])
            )
            scopes[user].append(s)

        return scopes

    @staticmethod
    def _to_int_permissions(perms: Iterable[Permission]) -> int:
        all_perms = list(Permission)
        perm = 0
        for p in perms:
            i = all_perms.index(p)
            perm |= (1 << i)
        return perm

    @staticmethod
    def _to_set_permissions(perm: int) -> List[Permission]:
        perms: List[Permission] = []
        for i, p in enumerate(list(Permission)):
            if perm & (1 << i):
                perms.append(p)
        return perms
