# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, TYPE_CHECKING
from pydantic import BaseModel, parse_file_as
from model.jobs.group import Group
from portal.scope.scope import EmptyScope, Scope, ScopeProvider

if TYPE_CHECKING:
    from portal.user.user import User


class Config(BaseModel):
    path: str


class MemoryScopeProvider(ScopeProvider):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)
        self._load()

    def _load(self) -> None:
        self.db = parse_file_as(Dict[str, Scope], self.config.path)

    def get_scope(self, user: 'User', group: str) -> Scope:
        # TODO (gdelmont): add group filtering
        if user.username not in self.db:
            return EmptyScope
        return self.db[user.username]
