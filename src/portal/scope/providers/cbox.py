# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import re
from typing import Dict, Any, TYPE_CHECKING, Iterable, List

from pydantic import BaseModel

from model.jobs.backup import Backup
from model.managers.jobs.job_manager import NoJobsError
from portal.scope.scope import Permission, Scope, ScopeProvider, ScopeTuple


if TYPE_CHECKING:
    from portal.user.user import User


class Config(BaseModel):
    # the egroups that has read access to project backups
    group_pattern: str = "cernbox-project-([a-z0-9\-]+)-writers"
    project_instance_prefix: str = "project-"


class CboxScopeProvider(ScopeProvider):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)
        self._group_re = re.compile(self.config.group_pattern)

    def _get_project_name(self, g: str) -> str:
        # g is of the type "cernbox-project-****-writers"
        l: List[str] = self._group_re.findall(g)
        return l[0]

    def _add_backups_to_scope(self, backups: Iterable[Backup], group: str, scope: Scope) -> None:
        for backup in backups:
            if backup.group.name == group:
                scope.append(ScopeTuple(
                    id=backup.id,
                    group=group,
                    permissions={Permission.RESTORE},
                ))

    def get_scope(self, user: 'User', group: str) -> Scope:
        scope: Scope = []
        try:
            backups_home_folder = self.backup_manager.get_by_name(
                user.username)
        except NoJobsError:
            pass
        else:
            self._add_backups_to_scope(backups_home_folder, group, scope)

        # get all the backups from the groups the user belong to
        # the user must be in the group specified by the group pattern
        # to have restore access on a backup

        for g in user.groups:
            if self._group_re.match(g):
                project_name = self._get_project_name(g)
                try:
                    backups_project_folder = self.backup_manager.get_by_name(
                        project_name)
                except NoJobsError:
                    continue
                self._add_backups_to_scope(
                    filter(lambda b: b.instance.name.startswith(self.config.project_instance_prefix), backups_project_folder), group, scope)

        return scope
