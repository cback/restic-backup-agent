# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import List, Optional, Tuple
from fastapi import APIRouter, Depends, HTTPException, status
from pydantic import BaseModel
from model.managers.jobs.job_manager import NoJobsError
from model.managers.restore.restore_manager import RestoreManager
from model.config.logs import get_logger

from portal.scope.scope import AllBackupsInGroup, AllGroups, BackupID, Permission, Scope
from portal.user.user import User
from .utils import RestoreOut, restore_to_out
from model.jobs.restore import Restore
from portal.auth.router import get_user_and_scope
from portal.backup.router import _check_scope_backup_get
from portal.scope.scope import parse
from portal.common import cback, get_group_mgr

router = APIRouter(prefix="/restores", tags=["restores"])

restore_mgr = cback.get_restore_manager()

log = get_logger()

@router.get("/")
def get_restores_list(user_scope: Tuple[User, Scope] = Depends(get_user_and_scope)) -> List[RestoreOut]:
    _, scope = user_scope
    parsed_scope = parse(scope)

    # check the scope to see which are the restore (relative to the backups in the scope)
    # the user has access to
    restores: List[RestoreOut] = []
    group_mgr = get_group_mgr()

    for s in parsed_scope:
        if Permission.RESTORE not in s.permissions:
            continue
        if isinstance(s, AllGroups):
            try:
                return list(map(restore_to_out, restore_mgr.get_jobs()))
            except NoJobsError:
                return []
        elif isinstance(s, AllBackupsInGroup):
            group = group_mgr.get_by_name(s.group)
            try:
                restores += list(map(restore_to_out,
                                     restore_mgr.get_jobs(group=group)))
            except NoJobsError:
                continue
        elif isinstance(s, BackupID):
            try:
                restores += list(map(restore_to_out,
                                     restore_mgr.get_by_backup_id(s.id)))
            except NoJobsError:
                continue

    return restores


def _get_restore_by_id(restore_mgr: RestoreManager, id: int) -> Restore:
    try:
        return restore_mgr.get_by_id(id)
    except ValueError:  # the restore job does not exist
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"restore job with id {id} does not exist"
        )


def _check_scope_restore_get(*, id: int, user_scope: Tuple[User, Scope] = Depends(get_user_and_scope)) -> None:
    """ Check from the scope that the user has access to the backup relative to the restore job """
    _, scope = user_scope
    parsed_scope = parse(scope)

    restore: Optional[Restore] = None

    for s in parsed_scope:
        if Permission.RESTORE not in s.permissions:
            continue
        if isinstance(s, AllGroups):
            return
        elif isinstance(s, AllBackupsInGroup):
            # here we have to check if the restore is in the group
            if not restore:
                restore = _get_restore_by_id(restore_mgr, id)
            if restore.group.name == s.group:
                return
        elif isinstance(s, BackupID):
            if not restore:
                restore = _get_restore_by_id(restore_mgr, id)
            if s.id == restore.backup.id:
                return
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="User has no permissions to get the restore"
    )


@router.get("/{id}", dependencies=[Depends(_check_scope_restore_get)])
def get_restore_id(id: int) -> RestoreOut:
    log.info("get restore by id", extra={"id": id})
    # TODO (gdelmont): we may use the restore got when the user has access to it
    restore = _get_restore_by_id(restore_mgr, id)
    log.debug("get restore by id", extra={"id": id, "restore": restore})
    return restore_to_out(restore)


class NewRestoreRequest(BaseModel):
    backup_id: int
    pattern: str
    snapshot: Optional[str] = None
    destination: Optional[str] = None
    date: Optional[datetime] = None
    enabled: bool = True
    trim_source: bool = False
    pretrim_scratch_path: Optional[str] = None


def _check_scope_new_restore(*, user_scope: Tuple[User, Scope] = Depends(get_user_and_scope), info: NewRestoreRequest) -> None:
    # check that the user has read access to the backup the restore is referring to
    _check_scope_backup_get(id=info.backup_id, user_scope=user_scope)


@router.post("/", dependencies=[Depends(_check_scope_new_restore)])
def new_restore(info: NewRestoreRequest) -> RestoreOut:
    if info.trim_source and info.pretrim_scratch_path is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='premove scratch space must be provided when trim source is enabled'
        )
    try:
        new = restore_mgr.add(**info.dict())
        return restore_to_out(new)
    except Exception as e:  # FIXME: this exception will change in future
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(e)
        )
