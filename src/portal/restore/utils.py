# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from typing import Optional
from pydantic import BaseModel
from model.jobs.restore import Restore
from model.jobs.status import Status


class RestoreOut(BaseModel):
    id: int
    backup_id: int
    snapshot: str
    destination: Optional[str]
    pattern: str
    status: str
    progress: float
    created: datetime


def restore_to_out(r: Restore) -> RestoreOut:
    return RestoreOut(
        id=r.id,
        backup_id=r.backup.id,
        snapshot=r.snapshot,
        destination=r.destination,
        pattern=r.pattern,
        status=Status.to_str(r.status),
        created=r.created,
        progress=r.progress
    )
