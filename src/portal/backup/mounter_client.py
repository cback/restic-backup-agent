# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Dict

from requests import Response
from model.mounter.server.server import MountOut
from model.mounter.errors import NotMountedError
import requests


class InvalidAPIToken(Exception):
    pass


class MounterClient:

    def __init__(self, url: str, api_token: str) -> None:
        self.url = url
        self.api_token = api_token

    @property
    def auth_header(self) -> Dict[str, str]:
        return {'Authorization': f'Bearer {self.api_token}'}

    def _check_status_code(self, r: Response) -> None:
        if r.status_code == 401:
            raise InvalidAPIToken
        elif r.status_code == 400:
            raise NotMountedError

    def mount(self, id: int) -> MountOut:
        r = requests.get(f"{self.url}/mount",
                         params={'id': id}, headers=self.auth_header)
        self._check_status_code(r)
        return MountOut.parse_raw(r.content)

    def umount(self, id: int) -> None:
        r = requests.delete(f"{self.url}/mount",
                            params={'id': id}, headers=self.auth_header)
        self._check_status_code(r)
