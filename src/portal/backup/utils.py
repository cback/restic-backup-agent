# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime
from enum import Enum
from model.jobs.status import Status
from model.restic.restic import Snapshot
from typing import Any, Dict, List, Optional
from model.jobs.backup import Backup
from pydantic import BaseModel


class GroupOut(BaseModel):
    id: int
    name: str


class BackupOut(BaseModel):
    id: int
    group: GroupOut
    status: str
    progress: float
    repository: str
    username: Optional[str]
    name: str
    source: str


class SnapshotOut(BaseModel):
    id: str
    time: datetime
    paths: list[str]
    tags: list[str]


def backup_to_out(b: Backup) -> BackupOut:
    return BackupOut(
        id=b.id,
        group=GroupOut(id=b.group.id, name=b.group.name),
        name=b.name,
        status=Status.to_str(b.status),
        repository=b.repository.path,
        username=b.username,
        source=b.source,
        progress=b.progress
    )


def snapshot_to_out(s: Snapshot) -> SnapshotOut:
    return SnapshotOut(
        id=s.short_id,
        time=s.time,
        paths=s.paths,
        tags=s.tags
    )


class FileType(str, Enum):
    FILE = "file"
    DIR = "dir"


class Xattr(BaseModel):
    name: str
    value: str


class FileStat(BaseModel):
    name: str
    type: FileType
    mtime: float
    atime: float
    ctime: float
    inode: int
    size: int


def stat_to_file_stat(d: Dict[str, Any]) -> FileStat:
    type = FileType.DIR if d['type'] == 'dir' else FileType.FILE
    return FileStat(
        name=d['name'],
        type=type,
        atime=d['atime'],
        mtime=d['mtime'],
        ctime=d['ctime'],
        inode=d['ino'],
        size=d['size']
    )
