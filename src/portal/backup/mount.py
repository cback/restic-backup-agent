# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import base64
from dataclasses import dataclass
from http import HTTPStatus
from typing import Any, AsyncGenerator, Dict, List
from requests import Response, get, post
import httpx


class WrongCredentialsError(Exception):
    pass


@dataclass
class HTTPMountClient:
    url: str
    user: str
    password: str

    @property
    def _auth_header(self) -> Dict[str, str]:
        tkn = base64.b64encode(
            bytes(f'{self.user}:{self.password}', 'utf-8')).decode('ascii')
        return {'Authorization': f'Basic {tkn}'}

    def _check_status_code(self, res: Response) -> None:
        if res.status_code == HTTPStatus.NOT_FOUND:
            raise FileNotFoundError
        elif res.status_code == HTTPStatus.BAD_REQUEST:
            raise NotADirectoryError
        elif res.status_code == HTTPStatus.UNAUTHORIZED:
            raise WrongCredentialsError

    def _get_url(self, endpoint: str) -> str:
        # if endpoint[-1] == "/":
        #     endpoint = endpoint[:-1]
        return f"http://{self.url}/{endpoint}"

    async def read(self, path: str) -> AsyncGenerator:
        async with httpx.AsyncClient() as client:
            async with client.stream("GET", self._get_url(path), headers=self._auth_header) as r:
                async for chunk in r.aiter_raw():
                    yield chunk

    def list(self, path: str) -> List[Dict[str, Any]]:
        r = post(self._get_url(path), headers=self._auth_header)
        self._check_status_code(r)
        l: List[Dict[str, Any]] = r.json()
        return l

    def stat(self, path: str) -> Dict[str, Any]:
        r = get(self._get_url(path), headers={
                **self._auth_header, 'x-request': 'stat'})
        self._check_status_code(r)
        d: Dict[str, Any] = r.json()
        return d
