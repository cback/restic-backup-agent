# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import base64
import os
import time
from typing import List, Optional, Tuple, Union
from fastapi import APIRouter, Depends, HTTPException, Query, status
from fastapi.responses import StreamingResponse
from pydantic import BaseModel
from cachetools import TTLCache, cached
from model.config.logs import get_logger
from model.jobs.backup import Backup
from model.managers.jobs.job_manager import NoJobsError
from model.mounter.server.server import MountOut
from portal.backup.mount import HTTPMountClient
from portal.user.user import User
from portal.auth.router import get_user_and_scope
from portal.common import cback, get_group_mgr, get_portal_config
from .utils import BackupOut, FileStat, backup_to_out, SnapshotOut, snapshot_to_out, stat_to_file_stat
from .mounter_client import MounterClient
from portal.scope.scope import Permission, Scope, parse, AllGroups, AllBackupsInGroup, BackupID


class MountConfig(BaseModel):
    nodes: List[str]
    api_token: str
    tmp_folder: str = "/tmp"
    cache_ttl: int = 300
    cache_size: int = 1000


router = APIRouter(prefix="/backups", tags=["backups"])

backup_mgr = cback.get_backup_manager()
verify_mgr = cback.get_verify_manager()
prune_mgr = cback.get_prune_manager()

mount_config = MountConfig.parse_obj(get_portal_config().get("mount", {}))
log = get_logger()


@router.get("/")
def get_backups_list(user_scope: Tuple[User, Scope] = Depends(get_user_and_scope)) -> List[BackupOut]:
    # TODO (gdelmont): add filters
    _, scope = user_scope
    parsed_scope = parse(scope)
    # check the scope to see which are the backups the user has access to
    backups: List[BackupOut] = []
    group_mgr = get_group_mgr()

    for s in parsed_scope:
        if Permission.RESTORE not in s.permissions:
            continue
        if isinstance(s, AllGroups):
            try:
                return list(map(backup_to_out, backup_mgr.get_jobs()))
            except NoJobsError:
                return []
        elif isinstance(s, AllBackupsInGroup):
            group = group_mgr.get_by_name(s.group)
            try:
                backups += list(map(backup_to_out,
                                    backup_mgr.get_jobs(group=group)))
            except NoJobsError:
                continue
        elif isinstance(s, BackupID):
            backups.append(backup_to_out(backup_mgr.get_by_id(s.id)))

    return backups


def _check_scope_backup_get(*, id: int, user_scope: Tuple[User, Scope] = Depends(get_user_and_scope)) -> None:
    """ Check from the scope that the user has access to the backup """
    log.debug("checking backup scope", extra={"requested id": id})
    _, scope = user_scope
    parsed_scope = parse(scope)
    log.debug("user scope", extra={"requested id": id, "scope": parsed_scope})

    backup: Optional[Backup] = None

    for s in parsed_scope:
        log.debug("processing scope", extra={
                  "requested id": id, "scope": s.dict()})
        if Permission.RESTORE not in s.permissions:
            continue
        if isinstance(s, AllGroups):
            log.debug("found scope for all groups => user authorized",
                      extra={"requested id": id, "scope": s.dict()})
            return
        elif isinstance(s, AllBackupsInGroup):
            # here we have to check if the backup is in the group
            log.debug("found scope for all backups in group", extra={
                      "requested id": id, "scope": s.dict(), "group": s.group})
            if not backup:
                backup = backup_mgr.get_by_id(id)
            if backup.group.name == s.group:
                log.debug("backup belongs to the same group of scope => user authorized", extra={
                          "requested id": id, "scope": s.dict(), "group": s.group})
                return
        elif isinstance(s, BackupID):
            log.debug("found scope for single backup",
                      extra={"requested id": id, "scope id": s.id, "scope": s.dict()})
            if s.id == id:
                log.debug("scope and requested id match => user authorized", extra={
                          "requested id": id, "scope id": s.id, "scope": s.dict()})
                return
    log.debug("backup not found in user scope => user forbidden",
              extra={"requested id": id, "scope": s.dict()})
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="User has no permissions to get the backup"
    )


def _check_scope_run_backup(*, id: int, user_scope: Tuple[User, Scope] = Depends(get_user_and_scope)) -> None:
    """ Check from the scope that the user has access to the backup """
    log.debug("checking backup scope", extra={"requested id": id})
    _, scope = user_scope
    parsed_scope = parse(scope)
    log.debug("user scope", extra={"requested id": id, "scope": parsed_scope})

    backup: Optional[Backup] = None

    for s in parsed_scope:
        log.debug("processing scope", extra={
                  "requested id": id, "scope": s.dict()})
        if Permission.RUN not in s.permissions:
            continue
        if isinstance(s, AllGroups):
            log.debug("found scope for all groups => user authorized",
                      extra={"requested id": id, "scope": s.dict()})
            return
        elif isinstance(s, AllBackupsInGroup):
            # here we have to check if the backup is in the group
            log.debug("found scope for all backups in group", extra={
                      "requested id": id, "scope": s.dict(), "group": s.group})
            if not backup:
                backup = backup_mgr.get_by_id(id)
            if backup.group.name == s.group:
                log.debug("backup belongs to the same group of scope => user authorized", extra={
                          "requested id": id, "scope": s.dict(), "group": s.group})
                return
        elif isinstance(s, BackupID):
            log.debug("found scope for single backup",
                      extra={"requested id": id, "scope id": s.id, "scope": s.dict()})
            if s.id == id:
                log.debug("scope and requested id match => user authorized", extra={
                          "requested id": id, "scope id": s.id, "scope": s.dict()})
                return
    log.debug("backup not found in user scope => user forbidden",
              extra={"requested id": id, "scope": s.dict()})
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="User has no permissions to get the backup"
    )


def _check_scope_delete_backup(*, id: int, user_scope: Tuple[User, Scope] = Depends(get_user_and_scope)) -> None:
    """ Check from the scope that the user has delete permissions on the backup """
    log.debug("checking backup scope", extra={"requested id": id})
    _, scope = user_scope
    parsed_scope = parse(scope)
    log.debug("user scope", extra={"requested id": id, "scope": parsed_scope})

    backup: Optional[Backup] = None

    for s in parsed_scope:
        log.debug("processing scope", extra={
                  "requested id": id, "scope": s.dict()})
        if Permission.DELETE not in s.permissions:
            continue
        if isinstance(s, AllGroups):
            log.debug("found scope for all groups => user authorized",
                      extra={"requested id": id, "scope": s.dict()})
            return
        elif isinstance(s, AllBackupsInGroup):
            # here we have to check if the backup is in the group
            log.debug("found scope for all backups in group", extra={
                      "requested id": id, "scope": s.dict(), "group": s.group})
            if not backup:
                backup = backup_mgr.get_by_id(id)
            if backup.group.name == s.group:
                log.debug("backup belongs to the same group of scope => user authorized", extra={
                          "requested id": id, "scope": s.dict(), "group": s.group})
                return
        elif isinstance(s, BackupID):
            log.debug("found scope for single backup",
                      extra={"requested id": id, "scope id": s.id, "scope": s.dict()})
            if s.id == id:
                log.debug("scope and requested id match => user authorized", extra={
                          "requested id": id, "scope id": s.id, "scope": s.dict()})
                return
    log.debug("backup not found in user scope => user forbidden",
              extra={"requested id": id, "scope": s.dict()})
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="User has no permissions to delete the backup"
    )


@router.get("/{id}", dependencies=[Depends(_check_scope_backup_get)])
def get_backup_id(id: int) -> BackupOut:
    log.info("get backup by id", extra={"id": id})
    backup = backup_mgr.get_by_id(id)
    log.debug("get backup by id", extra={"id": id, "backup": backup})
    return backup_to_out(backup)


@router.get("/{id}/snapshots", dependencies=[Depends(_check_scope_backup_get)])
def get_snapshots_from_backup_id(id: int) -> list[SnapshotOut]:
    log.info("get snapshots from backup id", extra={"id": id})
    snapshots = backup_mgr.snapshots(id)
    log.debug("get snapshots from backup id", extra={
              "id": id, "snapshots": snapshots})
    return list(map(snapshot_to_out, snapshots))


@router.delete("/{id}/snapshots/{snapshot_id}", dependencies=[Depends(_check_scope_delete_backup)])
def mark_snapshot_for_deletion(id: int, snapshot_id: str) -> None:
    log.info("mark snapshot for deletion", extra={
             "id": id, "snapshot_id": snapshot_id})
    restic = backup_mgr.get_restic(id)
    restic.tag([f"delete:{int(time.time())}"], [snapshot_id], no_lock=True)


class NewBackupRequest(BaseModel):
    name: str
    source: str
    instance: Optional[str]
    username: Optional[str]
    group: str
    enabled: bool = False
    prune_enabled: bool = False
    verify_enabled: bool = False


def _check_scope_new_backup(*, user_scope: Tuple[User, Scope] = Depends(get_user_and_scope), info: NewBackupRequest) -> None:
    _, scope = user_scope
    parsed_scope = parse(scope)

    for s in parsed_scope:
        if Permission.CREATE not in s.permissions:
            continue
        if isinstance(s, AllGroups):
            return
        if isinstance(s, AllBackupsInGroup) and s.group == info.group:
            return
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="User has no permissions to create a backup"
    )


@router.post("/", dependencies=[Depends(_check_scope_new_backup)])
def new_backup(info: NewBackupRequest) -> BackupOut:
    log.info("new backup", extra={"info": info.dict()})
    try:
        new = backup_mgr.add(**info.dict())
        log.info("new backup", extra={"backup": new})

        # creates also prune and verify job
        # TODO: this should go in the domain layer
        prune = prune_mgr.add(
            backup_id=new.id, enabled=info.enabled or info.prune_enabled)
        log.info("new prune", extra={"prune": prune})

        verify = verify_mgr.add(
            backup_id=new.id, enabled=info.enabled or info.verify_enabled)
        log.info("new verify", extra={"verify": verify})

        return backup_to_out(new)
    except Exception:  # FIXME: this exception will change in future
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Missing parameters"
        )

class RunBackupAsyncRequest(BaseModel):
    tag: Optional[str]


@router.post("/{id}/run", dependencies=[Depends(_check_scope_run_backup)])
def run_backup_async(id: int, req: RunBackupAsyncRequest) -> None:
    log.info("setting backup for running", extra={"id": id, "req": req})
    backup_mgr.enable_by_id(id)
    if req.tag is not None:
        backup_mgr.set_tag(id, req.tag)
    backup_mgr.reset(id)


@router.put("/{id}")
def update_backup_id(id: int) -> None:
    log.info("update backup by id", extra={"id": id})
    raise NotImplementedError


def make_absolute(path: str) -> str:
    if path == "":
        return "/"
    if path[0] != "/":
        return "/"+path
    return path


@router.get("/{id}/snapshots/{snapshot_id}/{path:path}", dependencies=[Depends(_check_scope_backup_get)])
def get_file_from_snapshot(id: int, snapshot_id: str, path: Optional[str] = None, is_timestamp: bool = Query(False, alias="timestamp")) -> StreamingResponse:
    log.info("get file from snapshot", extra={
             "id": id, "snapshot": snapshot_id, "path": path, "is_timestamp": is_timestamp})

    endpoint = mount(id)
    log.info("got endpoint", extra={
             "endpoint": endpoint.endpoint, "protocol": endpoint.protocol})
    path = make_absolute(path if path else "/")

    if endpoint.protocol != "http":
        raise Exception(
            f"protocol {endpoint.protocol} not supported for accessing the restic mount")

    user, password = _decode_mount_credentials(endpoint.credentials)

    client = HTTPMountClient(endpoint.endpoint, user, password)

    fname = os.path.basename(path)
    in_path = _get_internal_path(snapshot_id, path, is_timestamp)
    log.debug("internal path", extra={"path": in_path})

    try:
        stat = client.stat(in_path)
        log.debug("stat result", extra={"path": path, "stat": stat})
    except FileNotFoundError:
        log.info("file not found", extra={
                 "path": path, "internal_path": in_path})
        raise HTTPException(
            detail="Resource not found",
            status_code=status.HTTP_404_NOT_FOUND,
        )
    if stat["type"] == "dir":
        log.info("resource is a director", extra={
                 "path": path, "internal_path": in_path})
        raise HTTPException(
            detail="Resource is a directory",
            status_code=status.HTTP_400_BAD_REQUEST,
        )

    log.debug("sending content of file", extra={"path": path})
    return StreamingResponse(
        content=client.read(in_path),
        media_type="application/octet-stream",
        headers={"Content-Disposition": f'inline; filename="{fname}"'}
    )


def _decode_mount_credentials(token: str) -> Tuple[str, str]:
    s = base64.b64decode(token).decode('ascii')
    user, password = s.split(":", 1)
    return user, password


def _get_internal_path(snapshot_id: str, path: str, is_timestamp: bool) -> str:
    path = make_absolute(path)
    if is_timestamp:
        return f"snapshots/{snapshot_id}{path}"
    return f"ids/{snapshot_id}{path}"


@router.options("/{id}/snapshots/{snapshot_id}/{path:path}", dependencies=[Depends(_check_scope_backup_get)])
def stat_resource_from_snapshot(id: int, snapshot_id: str, path: str, content: bool = False, is_timestamp: bool = Query(False, alias="timestamp")) -> Union[List[FileStat], FileStat]:
    log.info("stat resource from snapshot", extra={
             "id": id, "snapshot": snapshot_id, "path": path, "is_timestamp": is_timestamp, "content": content})
    endpoint = mount(id)
    log.info("got endpoint", extra={
        "endpoint": endpoint.endpoint, "protocol": endpoint.protocol})
    path = make_absolute(path)

    if endpoint.protocol != "http":
        raise Exception(
            f"protocol {endpoint.protocol} not supported for accessing the restic mount")

    user, password = _decode_mount_credentials(endpoint.credentials)

    client = HTTPMountClient(endpoint.endpoint, user, password)
    in_path = _get_internal_path(snapshot_id, path, is_timestamp)
    log.debug("internal path", extra={"path": in_path})

    if content:
        try:
            lst = client.list(in_path)
            log.info("list result", extra={
                     "path": path, "internal_path": in_path, "list": lst})
        except FileNotFoundError:
            log.info("resource not found", extra={
                     "path": path, "internal_path": in_path})
            raise HTTPException(
                detail="Resource not found",
                status_code=status.HTTP_404_NOT_FOUND,
            )
        except NotADirectoryError:
            log.info("resource is not a directory", extra={
                     "path": path, "internal_path": in_path})
            raise HTTPException(
                detail="Resource is not a directory",
                status_code=status.HTTP_400_BAD_REQUEST,
            )
        return list(map(lambda d: stat_to_file_stat(d), lst))

    try:
        stat = client.stat(_get_internal_path(snapshot_id, path, is_timestamp))
        log.info("stat result", extra={
            "path": path, "internal_path": in_path, "stat": stat})
    except FileNotFoundError:
        log.info("resource not found", extra={
            "path": path, "internal_path": in_path})
        raise HTTPException(
            detail="Resource not found",
            status_code=status.HTTP_404_NOT_FOUND,
        )

    return stat_to_file_stat(stat)


def _select_mount_node(id: int, nodes: List[str]) -> str:
    i = hash(id) % len(nodes)
    return nodes[i]


@router.get("/{id}/mount", dependencies=[Depends(_check_scope_backup_get)])
@cached(cache=TTLCache(mount_config.cache_size, mount_config.cache_ttl))
def mount(id: int) -> MountOut:
    log.info("mount", extra={"id": id})
    node = _select_mount_node(id, mount_config.nodes)
    log.debug("selected mount node", extra={"id": id, "node": node})
    client = MounterClient(node, mount_config.api_token)
    out = client.mount(id)
    log.debug("mount output", extra={"id": id, "info": out.dict()})
    return out


@router.delete("/{id}/mount", dependencies=[Depends(_check_scope_backup_get)])
def umount(id: int) -> None:
    log.info("umount", extra={"id": id})
    node = _select_mount_node(id, mount_config.nodes)
    log.debug("selected mount node", extra={"id": id, "node": node})
    client = MounterClient(node, mount_config.api_token)
    client.umount(id)
    log.info("backup unmounted", extra={"id": id})
