# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Dict, List, Any, Callable, TYPE_CHECKING, Mapping
from pydantic import BaseModel, validator
from abc import ABC, abstractmethod
from portal.user.cache import Cache, NoCache, NotInCacheError, RedisCache, RedisConfig

if TYPE_CHECKING:
    from portal.user.user import User


class UserProvider(ABC):

    @abstractmethod
    def get_user(self, username: str, skip_groups: bool = False) -> 'User': ...

    @abstractmethod
    def get_groups(self, user: 'User') -> List[str]: ...


NewUserProviderFunc = Callable[[Dict[str, Any]], UserProvider]

_user_providers: Dict[str, NewUserProviderFunc] = {}


def register(name: str, new: NewUserProviderFunc) -> None:
    _user_providers[name] = new


class Config(BaseModel):
    driver: str = "memory"
    drivers: Dict[str, Any] = {}

    enable_cache: bool = False
    cache: Dict[str, Any] = {}


class UserManager:

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)

        # create the driver to use specified in the config
        try:
            n = _user_providers[self.config.driver]
        except KeyError:
            raise Exception("provider does not exists")

        self._provider = n(self.config.drivers.get(self.config.driver, {}))

        self._cache: Cache = NoCache()
        if self.config.enable_cache:
            redis_config = RedisConfig.parse_obj(self.config.cache)
            self._cache = RedisCache(redis_config)

    def get_user(self, username: str, skip_groups: bool = False) -> 'User':
        try:
            return self._cache.get_user(username)
        except NotInCacheError:
            pass

        user = self._provider.get_user(username, skip_groups)
        self._cache.set_user(user)
        return user

    def get_groups(self, user: 'User') -> List[str]:
        try:
            return self._cache.get_groups(user.username)
        except NotInCacheError:
            pass

        groups = self._provider.get_groups(user)
        self._cache.set_groups(user.username, groups)
        return groups
