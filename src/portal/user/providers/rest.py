# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from datetime import datetime, timedelta
import json
from typing import Any, Dict, List, Tuple
import requests
from requests.auth import HTTPBasicAuth
from dataclasses import dataclass

from pydantic import BaseModel
from portal.user.user import User
from portal.user.manager import UserProvider
from portal.user.errors import UserNotFoundError


class Config(BaseModel):
    oidc_token_endpoint: str  # Endpoint to generate token to access the API
    target_api: str  # The target application for which token needs to be generated
    api_base_url: str  # Base API Endpoint
    client_id: str  # Client ID needed to authenticate
    client_secret: str  # Client Secret


@dataclass
class NotFoundError(Exception):
    msg: str


@dataclass
class APIToken:
    token: str
    expiration_time: datetime

    @staticmethod
    def dummy() -> 'APIToken':
        return APIToken("", datetime.fromtimestamp(0))


class RestUserProvider(UserProvider):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)
        self._token = APIToken.dummy()

    def _refresh_token(self) -> None:
        if datetime.now() > self._token.expiration_time:
            # token expired
            token, expiration = self._get_api_token()
            self._token = APIToken(token, expiration)

    def _get_api_token(self) -> Tuple[str, datetime]:
        data = {
            "grant_type": "client_credentials",
            "audience":   self.config.target_api,
        }
        r = requests.post(self.config.oidc_token_endpoint, data=data, auth=HTTPBasicAuth(
            self.config.client_id, self.config.client_secret))

        r.raise_for_status()

        body = json.loads(r.content)
        if not isinstance(body, dict):
            raise Exception("response not in the correct format")

        access_token: str = body['access_token']
        expires_in: int = body['expires_in']
        expiration_time = datetime.now() + timedelta(seconds=expires_in)
        return access_token, expiration_time

    def _api_get_request(self, url: str) -> Dict[str, Any]:
        self._refresh_token()

        headers = {
            'Authorization': f"Bearer {self._token.token}",
        }
        r = requests.get(url, headers=headers)
        if r.status_code == 404:
            c = json.loads(r.text)
            raise NotFoundError(c['message'])
        r.raise_for_status()

        data = json.loads(r.content)
        if not isinstance(data, dict):
            raise Exception("response not in the correct format")
        return data

    def get_user(self, username: str, skip_groups: bool = False) -> User:
        url = f"{self.config.api_base_url}/api/v1.0/Identity/{username}?field=upn&field=displayName&field=uid&field=gid"
        try:
            res = self._api_get_request(url)
        except NotFoundError as e:
            raise UserNotFoundError(username) from e

        data = res['data']
        user = User(
            username=data['upn'],
            display_name=data['displayName'],
            uid=data['uid'],
            gid=data['gid'],
            active=data.get('activeUser', False),
            groups=[],
        )
        if not skip_groups:
            groups = self.get_groups(user)
            user.groups = groups

        return user

    def get_groups(self, user: User) -> List[str]:
        url = f"{self.config.api_base_url}/api/v1.0/Identity/{user.username}/groups?field=displayName&recursive=true"
        try:
            res = self._api_get_request(url)
        except NotFoundError as e:
            raise UserNotFoundError(user.username) from e

        return list(map(lambda e: str(e['displayName']), res['data']))
