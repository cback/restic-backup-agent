# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, List

from pydantic import BaseModel
from portal.user.user import User
from portal.user.manager import UserProvider
from portal.user.errors import UserNotFoundError
from ldap3 import Server, Connection, Entry
from ldap3.utils.conv import escape_filter_chars


class UserAttributes(BaseModel):
    cn: str = "cn"
    mail: str = "mail"
    display_name: str = "displayName"
    uid_number: str = "uidNumber"
    gid_number: str = "gidNumber"
    member_of: str = "memberOf"


class GroupAttributes(BaseModel):
    cn: str = "cn"


class Config(BaseModel):
    hostname: str
    port: int = 636
    bind_username: str
    bind_password: str
    user_filter: str
    group_filter: str
    base_dn: str
    user_layout: UserAttributes = UserAttributes()
    group_layout: GroupAttributes = GroupAttributes()


class LDAPUserProvider(UserProvider):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)

    def _get_connection(self) -> Connection:
        server = Server(self.config.hostname, self.config.port)
        return Connection(server, self.config.bind_username, self.config.bind_password, raise_exceptions=True)

    def _user_filter(self, username: str) -> str:
        return self.config.user_filter.replace("{{user}}", escape_filter_chars(username))

    def get_user(self, username: str, skip_groups: bool = False) -> User:
        with self._get_connection() as conn:
            if not conn.search(self.config.base_dn,
                               self._user_filter(username),
                               attributes=[
                                   self.config.user_layout.cn,
                                   self.config.user_layout.mail,
                                   self.config.user_layout.display_name,
                                   self.config.user_layout.uid_number,
                                   self.config.user_layout.gid_number
                               ]):
                raise UserNotFoundError(username)

            if len(conn.entries) != 1:
                raise UserNotFoundError(username)

            user: Entry = conn.entries[0]

            u = User(
                username=user[self.config.user_layout.cn].value,
                display_name=user[self.config.user_layout.display_name].value,
                email=user[self.config.user_layout.mail].value,
                gid=user[self.config.user_layout.gid_number].value,
                uid=user[self.config.user_layout.uid_number].value,
                active=True,  # TODO: check if in ldap this is always the case
                groups=[],
            )

            if not skip_groups:
                groups = self.get_groups(u)
                u.groups = groups
            return u

    def get_groups(self, user: User) -> List[str]:
        with self._get_connection() as conn:
            if not conn.search(self.config.base_dn,
                               self._user_filter(user.username),
                               attributes=[
                                   self.config.user_layout.member_of,
                               ]):
                raise UserNotFoundError(user.username)

            # we expect only one user
            if len(conn.entries) != 1:
                raise UserNotFoundError(user.username)

            entry: Entry = conn.entries[0]
            groups_dn: List[str] = entry[self.config.user_layout.member_of]

            groups: List[str] = []
            for dn in groups_dn:
                if not conn.search(dn, self.config.group_filter, attributes=[self.config.group_layout.cn]):
                    continue

                if len(conn.entries) != 1:
                    continue

                group: Entry = conn.entries[0]

                groups.append(
                    group[self.config.group_layout.cn].value
                )
            return groups
