# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, List

from pydantic import BaseModel, parse_file_as
from portal.user.user import User
from portal.user.manager import UserProvider
from portal.user.errors import UserNotFoundError


class Config(BaseModel):
    path: str


class UserInMemory(User):
    pass


class MemoryUserProvider(UserProvider):

    def __init__(self, config: Dict[str, Any]) -> None:
        self.config = Config.parse_obj(config)
        self._load()

    def _load(self) -> None:
        users = parse_file_as(List[UserInMemory], self.config.path)
        self.db = {user.username: user for user in users}

    def get_user(self, username: str, _: bool = False) -> User:
        try:
            return self.db[username]
        except KeyError:
            raise UserNotFoundError(username)

    def get_groups(self, user: User) -> List[str]:
        user = self.get_user(user.username)
        return user.groups
