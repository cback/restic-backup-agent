# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
import json
from typing import List
from pydantic import BaseModel, parse_raw_as
from portal.user.user import User
import redis


class RedisConfig(BaseModel):
    host: str
    port: int
    password: str
    expiration: int = 21600  # 6 hours


class NotInCacheError(Exception):
    pass


class Cache(ABC):
    @abstractmethod
    def get_user(self, username: str) -> User: ...

    @abstractmethod
    def set_user(self, user: User) -> None: ...

    @abstractmethod
    def get_groups(self, username: str) -> List[str]: ...

    @abstractmethod
    def set_groups(self, username: str, groups: List[str]) -> None: ...


class RedisCache(Cache):

    def __init__(self, config: RedisConfig) -> None:
        self.config = config
        self.redis = redis.Redis(
            host=self.config.host,
            port=self.config.port,
            password=self.config.password,
        )

    @staticmethod
    def _key_user(username: str) -> str:
        return f"users:{username}"

    @staticmethod
    def _key_groups(username: str) -> str:
        return f"groups:{username}"

    def get_user(self, username: str) -> User:
        u = self.redis.get(RedisCache._key_user(username))
        if not u:
            raise NotInCacheError
        return User.parse_raw(u)

    def set_user(self, user: User) -> None:
        key = RedisCache._key_user(user.username)
        self.redis.set(key, user.json(), self.config.expiration)

    def get_groups(self, username: str) -> List[str]:
        g = self.redis.get(RedisCache._key_groups(username))
        if not g:
            raise NotInCacheError
        return parse_raw_as(List[str], g)

    def set_groups(self, username: str, groups: List[str]) -> None:
        key = RedisCache._key_groups(username)
        g = json.dumps(groups)
        self.redis.set(key, g, self.config.expiration)


class NoCache(Cache):
    def get_user(self, username: str) -> User:
        raise NotInCacheError

    def set_user(self, user: User) -> None:
        return

    def get_groups(self, username: str) -> List[str]:
        raise NotInCacheError

    def set_groups(self, username: str, groups: List[str]) -> None:
        return
