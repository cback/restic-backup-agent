# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from fastapi import FastAPI

from .auth import router as auth
from .backup import router as backup
from .restore import router as restore
import settings as settings
import logging

logger = logging.getLogger("uvicorn.error")
logger.propagate = False

app = FastAPI(
    title=settings.APP_NAME,
)

app.include_router(auth.router)
app.include_router(backup.router)
app.include_router(restore.router)
