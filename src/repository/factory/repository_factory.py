# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from typing import Type, Dict, Any

from ..interfaces.backup_repository import BackupRepository
from ..interfaces.prune_repository import PruneRepository
from ..interfaces.restore_repository import RestoreRepository
from ..interfaces.instance_repository import InstanceRepository
from ..interfaces.group_repository import GroupRepository
from ..interfaces.job_repository import J, JobRepository
from ..interfaces.verify_repository import VerifyRepository


class RepositoryFactory(ABC):

    @abstractmethod
    def get_backup_repository(self) -> BackupRepository:
        pass

    @abstractmethod
    def get_prune_repository(self) -> PruneRepository:
        pass

    @abstractmethod
    def get_restore_repository(self) -> RestoreRepository:
        pass

    @abstractmethod
    def get_verify_repository(self) -> VerifyRepository:
        pass

    @abstractmethod
    def get_instance_repository(self) -> InstanceRepository:
        pass

    @abstractmethod
    def get_group_repository(self) -> GroupRepository:
        pass

    @abstractmethod
    def get_repository_by_job(self, job_type: Type[J]) -> JobRepository[J]:
        pass


def get_repository_factory(config: Dict[str, Any]) -> RepositoryFactory:
    from repository.mysql.mysql_factory import MySQLFactory
    return MySQLFactory(config.get('database', {}))
