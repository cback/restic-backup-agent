# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from typing import Any, Generic, Iterable, Optional, List, TypeVar
from dataclasses import dataclass
from enum import Enum
from model.jobs.group import Group
from model.jobs.job import Job


class Operator(Enum):
    LESS = 1
    LESS_EQUAL = 2
    EQUAL = 3
    GREATER = 4
    GREATER_EQUAL = 5
    NOT_EQUAL = 6


J = TypeVar('J', bound=Job)


@dataclass
class Filter:
    key: str
    operator: Operator
    value: Any


class EmptyListError(Exception):
    pass


class JobRepository(ABC, Generic[J]):

    @abstractmethod
    def add(self, job: J) -> J:
        """add the job into the repository"""
        pass

    @abstractmethod
    def get_filtered(self, order_by: str, filters: List[Filter]) -> Iterable[J]:
        pass

    @abstractmethod
    def get_by_id(self, id: int) -> J:
        pass

    @abstractmethod
    def count_filtered(self, filters: List[Filter]) -> int:
        pass

    @abstractmethod
    def update_by_id(self, id: int, **params: Any) -> J:
        pass

    @abstractmethod
    def update_by_instance(self, instance_id: int, **params: Any) -> Iterable[J]:
        # TODO (gdelmont): use update_by_filters instead
        # defining filters for columns in other table jobs
        pass

    @abstractmethod
    def update_by_filters(self, filters: List[Filter], **params: Any) -> int:
        pass

    @abstractmethod
    def delete_by_id(self, id: int) -> None:
        pass

    @abstractmethod
    def pick(self, method: str, group: Group, **kwargs: Any) -> Optional[J]:
        pass
