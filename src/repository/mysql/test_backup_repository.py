# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import unittest
from model.jobs.job import Repository
from repository import Instance
from repository.interfaces.job_repository import EmptyListError, Filter, Operator
from repository.mysql.backup_repository import MySQLBackupRepository
from repository.mysql.group_repository import MySQLGroupRepository
from model.jobs.backup import Backup
from model.jobs.group import Group
from model.jobs.status import Status
from utils.test.repo import TestRepository, get_config


class TestBackupRepository(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName=methodName)

        self.config = get_config()
        self.testRepo = TestRepository(self.config)

    def setUp(self) -> None:
        self.testRepo.drop_tables()
        self.testRepo.create_tables()

        self.group_repo = MySQLGroupRepository(self.config)
        self.backup_repo = MySQLBackupRepository(self.config)

    def tearDown(self) -> None:
        self.testRepo.drop_tables()

    ####################################  TEST ADD  ####################################

    def test_add_new_backup(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
        }
        self.testRepo.feed_database(data)

        backup = Backup.new(
            group=Group(id=data['group'][0]['id'],  # type:ignore
                        name=data['group'][0]['name']),  # type:ignore
            username='test',
            destination="/backup/destination",
            enabled=True,
            force=False,
            instance=Instance(id=data['instance'][0]  # type:ignore
                              ['id'], name=data['instance'][0]['name']),  # type:ignore
            name='testbackup',
            pinned_node='localhost',
            source='/root',
            status=Status.PENDING
        )
        b = self.backup_repo.add(backup)

        self.assertEqual(self.testRepo.count("backup"), 1)

    ####################################  TEST GET_FILTERED  ####################################

    def test_get_filtered_empty_table(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
        }
        self.testRepo.feed_database(data)

        with self.assertRaises(EmptyListError):
            self.backup_repo.get_filtered(
                filters=[Filter('id', Operator.EQUAL, 9999)])

    def test_get_filtered_not_empty_table_empty_result(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}]
        }
        self.testRepo.feed_database(data)

        with self.assertRaises(EmptyListError):
            self.backup_repo.get_filtered(
                filters=[Filter('name', Operator.EQUAL, 'NOT_EXISTING_BACKUP')])

    def test_get_filtered_single_filter(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'zzz', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'bbb', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'aaa', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.get_filtered(
            order_by='name', filters=[Filter('enabled', Operator.EQUAL, True)])
        r = list(res)
        self.assertEqual(len(r), 2)
        self.assertTrue(r[0].name < r[1].name,
                        'the list should be ordered by name')

    def test_get_filtered_multiple_filters(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'zzz', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'bbb', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'aaa', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.get_filtered(
            order_by='name', filters=[Filter('enabled', Operator.EQUAL, True), Filter('status', Operator.EQUAL, 2)])
        self.assertEqual(len(list(res)), 1)

    ####################################  TEST GET_BY_ID  ####################################

    def test_get_by_id_no_backup(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'zzz', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'bbb', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'aaa', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        with self.assertRaises(ValueError):
            self.backup_repo.get_by_id(9999)

    def test_get_by_id_backup_in(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'zzz', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'bbb', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'aaa', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        b = self.backup_repo.get_by_id(10)
        self.assertEqual(b.id, 10)

    ####################################  TEST GET_BY_NAME  ####################################

    def test_get_by_name_no_backup(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'backup3', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        with self.assertRaises(ValueError):
            self.backup_repo.get_by_name("NOT_IN_DATABASE")

    def test_get_by_name_backup_in(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'backup3', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        b = self.backup_repo.get_by_name('backup2')
        self.assertEqual(b.id, 11)

    ####################################  TEST COUNT_FILTERED  ####################################

    def test_count_filtered_no_filters_empty_table(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.count_filtered()
        self.assertEqual(res, 0)

    def test_count_filtered_no_filters_filled_table(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'backup3', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.count_filtered()
        self.assertEqual(res, 3)

    def test_count_filtered_one_filter_empty_result(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'backup3', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.count_filtered(
            filters=[Filter('name', Operator.EQUAL, "NOT_EXISTING_BACKUP")])
        self.assertEqual(res, 0)

    def test_count_filtered_one_filter(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'backup3', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.count_filtered(
            filters=[Filter('name', Operator.EQUAL, "backup2")])
        self.assertEqual(res, 1)

    def test_count_filtered_multiple_filters_empty_result(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'backup3', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.count_filtered(filters=[Filter(
            'enabled', Operator.EQUAL, False), Filter('status', Operator.EQUAL, 10)])
        self.assertEqual(res, 0)

    def test_count_filtered_multiple_filters(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
                {'id': 12, 'group_id': 1, 'instance_id': 1, 'name': 'backup3', 'source': '/root',
                    'enabled': True, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        res = self.backup_repo.count_filtered(filters=[Filter(
            'enabled', Operator.EQUAL, True), Filter('status', Operator.EQUAL, 2)])
        self.assertEqual(res, 1)

    ####################################  TEST UPDATE_BY_ID  ####################################

    def test_update_by_id_not_existing_id(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},

            ]
        }
        self.testRepo.feed_database(data)

        with self.assertRaises(Exception):
            self.backup_repo.update_by_id(99999, name="new_name")

    def test_update_by_id_existing_id_one_param(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},

            ]
        }
        self.testRepo.feed_database(data)

        # check the value before changing it
        self.assertEqual(self.testRepo.get_by_id(
            "backup", 10)['name'], 'backup1')

        self.backup_repo.update_by_id(10, name="new_name")

        self.assertEqual(self.testRepo.get_by_id(
            "backup", 10)['name'], 'new_name')

    def test_update_by_id_existing_id_zero_params(self) -> None:
        with self.assertRaises(Exception):
            self.backup_repo.update_by_id(10)

    def test_update_by_id_existing_id_multiple_params(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        # check the values before changing it
        self.assertEqual(self.testRepo.get_by_id(
            "backup", 10)['name'], 'backup1')
        self.assertEqual(self.testRepo.get_by_id(
            "backup", 10)['status'], 2)

        self.backup_repo.update_by_id(10, name="new_name", status=5)

        self.assertEqual(self.testRepo.get_by_id(
            "backup", 10)['name'], 'new_name')
        self.assertEqual(self.testRepo.get_by_id(
            "backup", 10)['status'], 5)

    ####################################  TEST DELETE_BY_ID  ####################################

    def test_delete_by_id_not_existing_id(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        # check number of entries before deletion
        self.assertEqual(self.testRepo.count("backup"), 2)

        # delete a not existing backup is a no-op
        self.backup_repo.delete_by_id(99999)

        # because the delete has been a no-op, the number of entries should stay the same
        self.assertEqual(self.testRepo.count("backup"), 2)

    def test_delete_by_id(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'backup1', 'source': '/root',
                    'enabled': True, 'status': 2, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'backup2', 'source': '/root',
                    'enabled': False, 'status': 1, 'repository': '/somewhere'},
            ]
        }
        self.testRepo.feed_database(data)

        # check number of entries before deletion
        self.assertEqual(self.testRepo.count("backup"), 2)

        # delete backup with id 10
        self.backup_repo.delete_by_id(10)

        self.assertEqual(self.testRepo.count("backup"), 1)
