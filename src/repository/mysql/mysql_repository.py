# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, List, Tuple, Type, TypeVar
from dataclasses import dataclass, field
import mysql.connector

from repository.mysql.common import MySQLConfig

T = TypeVar('T')


@dataclass
class Result:
    res: List[Dict[str, Any]] = field(default_factory=list)
    rowcount: int = 0
    lastrowid: int = -1


class MySQLRepository:
    ''' Repository provide access to the mysql DB '''

    def __init__(self, config: MySQLConfig):
        self._config = {
            'host': config.host,
            'user': config.user,
            'port': config.port,
            'password': config.password,
            'database': config.database
        }

    def query(self, queries: List[str], params: List[Tuple]) -> Result:
        """Query the database with all the queries passed by parameters,
        all encapsulated in the same transaction.
        Will return the last query info in the list, i.e. a dictionary with the following keys:
          - 'rowcount' => number of updated rows (if an update query)
          - 'lastrowid' => last used id (if insert)
          - 'res' => list of result (if select)

        Parameters:
        queries (List[str]): list of queries
        params (List[Tuple]): list of parameters to pass to each query

        Returns:
        Dict[str, any]: a dict content the result

        """

        if not queries:
            raise ValueError('no queries specified')

        if len(queries) != len(params):
            raise ValueError('len of queries and params lists does not match')

        res = Result()

        try:
            conn = mysql.connector.Connect(**self._config)
            conn.autocommit = False

            cursor = conn.cursor(dictionary=True)

            # exec all the queries
            for q, p in zip(queries, params):

                try:
                    cursor.execute(q, p)
                except mysql.connector.errors.InternalError as e:
                    if str(e) == "Unread result found":
                        # flush cursor
                        cursor.fetchall()
                        # re-exec the query
                        cursor.execute(q, p)
                    else:
                        raise e

            # get the last query infos
            res.rowcount = cursor.rowcount
            res.lastrowid = cursor.lastrowid

            try:
                res.res = cursor.fetchall()
            except mysql.connector.errors.InterfaceError as e:
                if str(e) == "No result set to fetch from.":
                    res.res = []

        except Exception as e:
            conn.rollback()
            raise
        else:
            conn.commit()
            return res
        finally:
            if conn and conn.is_connected:
                conn.close()

    def execute_procedure(self, procedure: str, result_type: Type[T], args: Tuple) -> T:
        args += (result_type(),)
        try:
            conn = mysql.connector.Connect(**self._config)
            cursor = conn.cursor()
            result = cursor.callproc(procedure, args)
            return result[-1]  # type: ignore[no-any-return]
        except:
            conn.rollback()
            raise
        finally:
            cursor.close()
            conn.close()
