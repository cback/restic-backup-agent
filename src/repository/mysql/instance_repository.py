# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, cast
from repository import InstanceRepository, Instance
from repository.mysql.mysql_repository import MySQLRepository

if TYPE_CHECKING:
    from repository.mysql.common import MySQLConfig


class MySQLInstanceRepository(InstanceRepository):

    def __init__(self, config: 'MySQLConfig'):
        self._repo = MySQLRepository(config)

    def get_by_name(self, name: str) -> 'Instance':
        """get the instance infos from DB by its name

        Parameters:
        name (str): instance name

        Returns:
        Dict[str, any]: instance data, containing the `id` in the DB,
                        and the `name` of the instance

        """
        query, params = [
            "SELECT `id`, `name` FROM `instance` WHERE `name` = %s"], [(name,)]
        res = self._repo.query(
            query, cast(List[Tuple], params)).res
        if res:
            return Instance(**res[0])
        raise ValueError(f'instance with name {name} does not exists')

    def add(self, name: str) -> 'Instance':
        """add a new instance in the DB

        Parameters:
        name (str): name of the new instance to add in the DB

        Returns:
        Dict[str, any]: instance created

        """

        queries = ["INSERT INTO `instance`(`name`) VALUES (%s)",
                   "SELECT `id`, `name` FROM `instance` WHERE `name` = %s"]
        params = [(name,), (name,)]
        res: Dict[str, Any] = self._repo.query(
            queries, cast(List[Tuple], params)).res[0]
        return Instance(**res)
