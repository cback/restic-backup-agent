# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import TYPE_CHECKING, Any, Dict, Iterable, List, Optional, Tuple, cast
from model.jobs.group import Group

from repository import Filter, Operator, RestoreRepository
from repository.interfaces.job_repository import EmptyListError
from repository.mysql.mysql_repository import MySQLRepository
from .backup_repository import _dict_to_backup
from model.jobs.restore import Restore
from model.jobs.status import Status
from repository.mysql.common import filters_to_sql


if TYPE_CHECKING:
    from repository.mysql.common import MySQLConfig


# #############################################################################################################
# The DB contains the following tables for restore jobs management:
#
#  - restore (id: int, backup_id: int, snapshot: str, date: datetime, destination: str, pattern: str,
#            enabled: bool, created: datetime, started: datetime, finished: datetime, worker: str, info: str,
#            retries: int, status: int)
#
# `restore` contains the list of all the restore jobs.
# ##############################################################################################################


class MySQLRestoreRepository(RestoreRepository):

    def __init__(self, config: 'MySQLConfig'):
        self._repo = MySQLRepository(config)

    def add(self, restore: 'Restore') -> 'Restore':
        """add a new Restore job in the repository

        Parameters:
        job (Restore): Restore to add

        Returns:
        Restore: the restore job added in the db with updated id and defaults values
        """

        query = "INSERT INTO restore(backup_id, group_id, username, snapshot, destination, pattern, enabled, status, trim_source, pretrim_scratch_path, created) " \
                "VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,CURRENT_TIMESTAMP)"
        restore_id = self._repo.query([query], params=[(restore.backup.id, restore.backup.group.id, restore.username, restore.snapshot,
                                      restore.destination, restore.pattern, restore.enabled, Status.PENDING, restore.trim_source, restore.pretrim_scratch_path)]).lastrowid

        # TODO (gdelmont): make it atomical
        return self.get_by_id(restore_id)

    def get_filtered(self, order_by: str = 'id', filters: List[Filter] = []) -> Iterable['Restore']:
        """return the list of all restore job that matches the filters

        Paramters:
        order_by (str): select how to order the jobs in the list
        filters: ...

        Returns:
        List[Restore]: list of filtered restores
        """

        params = cast(Tuple[Any, ...], ())

        query_str = """ SELECT r.id, r.backup_id, r.username, r.snapshot, r.destination, r.pattern, r.started, r.created,
                                r.finished, r.running_expiration, r.worker, r.info, r.retries, r.status, r.group_id, r.enabled, r.progress, r.trim_source, r.pretrim_scratch_path,
                                b.id as backup_id, g.id as backup_group_id, g.name as backup_group_name, b.name as backup_name, b.username as backup_username,
                                b.repository as backup_repository, b.password as backup_password, b.source as backup_source, b.enabled as backup_enabled, b.pinned_node as backup_pinned_node,
                                b.force as backup_force, i.id as backup_instance_id, i.name as backup_instance_name, b.started as backup_started, b.finished as backup_finished,
                                b.running_expiration as backup_running_expiration, b.worker as backup_worker, b.status as backup_status, b.retries as backup_retries,
                                b.desired_start_time as backup_desired_start_time, b.bytes_total as backup_bytes_total, b.data_added as backup_data_added,
                                b.dirs_changed as backup_dirs_changed, b.dirs_new as backup_dirs_new, b.dirs_unmodified as backup_dirs_unmodified,
                                b.files_changed as backup_files_changed, b.files_new as backup_files_new, b.files_total as backup_files_total,
                                b.files_unmodified as backup_files_unmodified, b.file_errors as backup_file_errors, b.progress as backup_progress, b.tag as backup_tag
                        FROM   restore as r, backup as b, `group` as g, instance as i
                        WHERE  r.backup_id = b.id AND g.id = b.group_id AND b.instance_id = i.id"""

        if filters:
            filters_query, params = filters_to_sql(
                filters, prefix_filters='r')
            query_str += f' AND {filters_query}'

        query_str += f" ORDER BY {order_by}"

        res = self._repo.query([query_str], params=[params])
        if res.res:
            return map(self._dict_to_restore, res.res)
        raise EmptyListError()

    def get_by_id(self, id: int) -> 'Restore':
        try:
            lst = self.get_filtered(filters=[Filter('id', Operator.EQUAL, id)])
        except EmptyListError:
            lst = []
        for r in lst:
            return r
        raise ValueError(f'restore job with id {id} does not exist')

    def count_filtered(self, filters: List[Filter] = []) -> int:
        """return the number of restore jobs that matches with the filters passed to the function

        Parameters:
        filters: ...

        Returns:
        int: number of matches
        """

        query_str = "SELECT count(*) as count FROM restore"
        params = cast(Tuple[Any, ...], ())
        if filters:
            filters_query, params = filters_to_sql(filters)
            query_str += f" WHERE {filters_query}"

        res = self._repo.query([query_str], params=[params]).res
        return cast(int, res[0]['count']) if res else 0

    def update_by_id(self, id: int, **params: Any) -> 'Restore':
        """atomically update and return the restore job with given id

        Parameters:
        id (int): ...
        kargs:

        Returns:
        Restore: the updated restore job
        """

        self.update_by_filters(
            filters=[Filter('id', Operator.EQUAL, id)], **params)
        return self.get_by_id(id)

    def update_by_filters(self, filters: List[Filter], **params: Any) -> int:

        if not params:
            raise Exception('specify parameters to update')
        query_str = f"UPDATE `restore` SET "

        entries = ', '.join(map(lambda x: f"`{x[0]}`=%s", params.items()))
        p = tuple([v for _, v in params.items()])

        query_str += entries

        if filters:
            filters_query, filters_params = filters_to_sql(filters)
            query_str += f' WHERE {filters_query}'
            p += filters_params

        return self._repo.query([query_str], params=[p]).rowcount

    def update_by_instance(self, instance_id: int, **params: Any) -> List['Restore']:
        raise NotImplementedError

    def delete_by_id(self, id: int) -> None:
        query_str = "DELETE FROM `restore` WHERE id=%s"
        self._repo.query([query_str], params=[(id,)])

    def pick(self, method: str, group: Group, **kwargs: Any) -> Optional['Restore']:
        id = None
        if method == 'random':
            id = self._repo.execute_procedure(
                'pickRandomRestoreJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'oldest':
            id = self._repo.execute_procedure(
                'pickOldestRestoreJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'rescheduled':
            id = self._repo.execute_procedure(
                'pickRescheduledRestoreJob', int, (group.id, Status.RESCHEDULED, Status.RUNNING))
        else:
            raise ValueError(f'method {method} not supported')
        if id is not None:
            return self.get_by_id(id)
        return None

    def _dict_to_restore(self, d: Dict[str, Any]) -> Restore:
        backup = _dict_to_backup(d, prefix='backup')
        return Restore(
            backup=backup,
            username=d['username'],
            destination=d['destination'],
            enabled=d['enabled'],
            finished=d['finished'],
            group=backup.group,
            running_expiration=d['running_expiration'],
            id=d['id'],
            info=d['info'],
            created=d['created'],
            pattern=d['pattern'],
            repository=backup.repository,
            retries=d['retries'],
            snapshot=d['snapshot'],
            started=d['started'],
            stats={},
            status=d['status'],
            worker=d['worker'],
            progress=d['progress'], 
            pretrim_scratch_path=d['pretrim_scratch_path'],
            trim_source=d['trim_source']
        )
