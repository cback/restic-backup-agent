# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, List, Tuple
from dataclasses import dataclass

from repository.interfaces.job_repository import Filter, Operator


@dataclass
class MySQLConfig:
    host: str = ""
    user: str = ""
    port: int = 1234
    password: str = ""
    database: str = ""


def filters_to_sql(filters: List[Filter], prefix_filters: str = "") -> Tuple[str, Tuple]:
    if prefix_filters:
        prefix_filters += '.'
    filters_query = ' AND '.join(map(
        lambda f: f"{prefix_filters}{f.key} {_operator_to_str(f.operator, f.value)} %s", filters))

    params = tuple((f.value for f in filters))

    return filters_query, params


def _operator_to_str(op: Operator, value: Any) -> str:
    if op == Operator.EQUAL:
        return "=" if value is not None else 'IS'
    elif op == Operator.GREATER:
        return ">"
    elif op == Operator.GREATER_EQUAL:
        return ">="
    elif op == Operator.LESS:
        return "<"
    elif op == Operator.LESS_EQUAL:
        return "<="
    elif op == Operator.NOT_EQUAL:
        return "<>" if value is not None else 'IS NOT'
    else:
        raise ValueError("operator not recognised")


def key_prefix(key: str, prefix: str) -> str:
    if prefix:
        return f"{prefix}_{key}"
    return key
