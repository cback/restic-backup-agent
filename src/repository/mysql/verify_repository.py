# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, Iterable, List, Optional, TYPE_CHECKING, Tuple, cast

from repository import Filter, Operator, VerifyRepository
from repository.interfaces.job_repository import EmptyListError
from repository.mysql.mysql_repository import MySQLRepository
from .backup_repository import _dict_to_backup
from model.jobs.verify import Verify
from model.jobs.status import Status
from model.jobs.group import Group

from repository.mysql.common import filters_to_sql

if TYPE_CHECKING:
    from repository.mysql.common import MySQLConfig

# ###############################################################################################
# The DB contains the following tables for verify jobs management:
#
#  - verify (id: int, backup_id: int, group_id: int, enabled: bool, started: datetime, finished: datetime,
#           running_expiration: datetime, worker: str, info: str, retries: int, status: int)
#
#
# `verify` contains the list of all the verify jobs.
# ###############################################################################################


class MySQLVerifyRepository(VerifyRepository):

    def __init__(self, config: 'MySQLConfig'):
        self._repo = MySQLRepository(config)

    def add(self, verify: 'Verify') -> 'Verify':
        """add a new verify job in the DB

        Parameters:
        job (Verify): Verify to add

        Returns:
        Verify: the verify job added in the db with updated id and defaults values
        """

        query_str = "INSERT INTO `verify`(`backup_id`, `group_id`, `enabled`, `status`) " \
                    "VALUES(%s,%s,%s,%s)"
        verify_id = self._repo.query([query_str], params=[(
            verify.backup.id, verify.backup.group.id, verify.enabled, verify.status)]).lastrowid

        return self.get_by_id(verify_id)

    def get_filtered(self, order_by: str = 'id', filters: List[Filter] = []) -> Iterable['Verify']:
        """return the list of all verify jobs that match the filter

        Paramters:
        order_by (str): select how to order the jobs in the list
        filters: ...

        Returns:
        List[Verify]: list of filtered verify
        """

        params = cast(Tuple[Any, ...], ())

        query_str = """ SELECT v.id, v.backup_id, v.enabled, v.started, v.finished,
                                v.running_expiration, v.worker, v.info, v.retries, v.status, v.group_id,
                                b.id as backup_id, g.id as backup_group_id, g.name as backup_group_name, b.name as backup_name, b.username as backup_username,
                                b.repository as backup_repository, b.password as backup_password, b.source as backup_source, b.enabled as backup_enabled, b.pinned_node as backup_pinned_node,
                                b.force as backup_force, i.id as backup_instance_id, i.name as backup_instance_name, b.started as backup_started, b.finished as backup_finished,
                                b.running_expiration as backup_running_expiration, b.worker as backup_worker, b.status as backup_status, b.retries as backup_retries,
                                b.desired_start_time as backup_desired_start_time, b.bytes_total as backup_bytes_total, b.data_added as backup_data_added,
                                b.dirs_changed as backup_dirs_changed, b.dirs_new as backup_dirs_new, b.dirs_unmodified as backup_dirs_unmodified,
                                b.files_changed as backup_files_changed, b.files_new as backup_files_new, b.files_total as backup_files_total,
                                b.files_unmodified as backup_files_unmodified, b.file_errors as backup_file_errors, b.progress as backup_progress, b.tag as backup_tag
                        FROM   verify as v, backup as b, `group` as g, instance as i
                        WHERE  b.id = v.backup_id AND g.id = b.group_id AND b.instance_id = i.id"""

        if filters:
            filters_query, params = filters_to_sql(filters, prefix_filters='v')
            query_str += f' AND {filters_query}'

        query_str += " ORDER BY v." + order_by

        res = self._repo.query([query_str], params=[params])
        if res.res:
            return map(lambda r: self._dict_to_verify(r), res.res)
        raise EmptyListError()

    def get_by_id(self, id: int) -> 'Verify':
        try:
            lst = self.get_filtered(filters=[Filter('id', Operator.EQUAL, id)])
        except EmptyListError:
            lst = []
        for v in lst:
            return v
        raise ValueError(f'id {id} does not exist')

    def count_filtered(self, filters: List[Filter] = []) -> int:
        """return the number of verify jobs that matches with the filters passed to the function

        Parameters:
        filters: ...

        Returns:
        int: number of matches
        """

        query_str = "SELECT count(*) as count FROM verify "
        params = cast(Tuple[Any, ...], ())
        if filters:
            filters_query, params = filters_to_sql(filters)
            query_str += f"WHERE {filters_query}"
        res: List[Dict[str, int]] = self._repo.query(
            [query_str], params=[params]).res

        return res[0]['count'] if res else 0

    def update_by_id(self, id: int, **params: Any) -> 'Verify':
        """update and return the verify job given the id

        Parameters:
        id (int): verify id to update
        params:

        Returns:
        Verify: the updated verify job
        """

        self.update_by_filters(
            filters=[Filter('id', Operator.EQUAL, id)], **params)
        return self.get_by_id(id)

    def update_by_instance(self, instance_id: int, **params: Any) -> List['Verify']:
        raise NotImplementedError

    def update_by_filters(self, filters: List[Filter], **params: Any) -> int:

        if not params:
            raise Exception('specify parameters to update')
        query_str = f"UPDATE `verify` SET "

        entries = ', '.join(map(lambda x: f"`{x[0]}`=%s", params.items()))
        p = tuple([v for _, v in params.items()])

        query_str += entries

        if filters:
            filters_query, filters_params = filters_to_sql(filters)
            query_str += f" WHERE {filters_query}"
            p += filters_params

        return self._repo.query([query_str], params=[p]).rowcount

    def delete_by_id(self, id: int) -> None:
        query_str = "DELETE FROM `verify` WHERE id=%s"
        self._repo.query([query_str], params=[(id,)])

    def pick(self, method: str, group: Group, **kwargs: Any) -> Optional['Verify']:
        id = None
        if method == 'random':
            id = self._repo.execute_procedure(
                'pickRandomVerifyJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'oldest':
            id = self._repo.execute_procedure(
                'pickOldestVerifyJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'rescheduled':
            id = self._repo.execute_procedure(
                'pickRescheduledVerifyJob', int, (group.id, Status.RESCHEDULED, Status.RUNNING))
        else:
            raise ValueError(f'method {method} not supported')
        if id is not None:
            return self.get_by_id(id)
        return None

    def _dict_to_verify(self, d: Dict[str, Any]) -> 'Verify':
        backup = _dict_to_backup(d, prefix='backup')

        return Verify(
            id=d['id'],
            backup=backup,
            username=None,
            enabled=d['enabled'],
            finished=d['finished'],
            group=backup.group,
            info=d['info'],
            repository=backup.repository,
            running_expiration=d['running_expiration'],
            retries=d['retries'],
            started=d['started'],
            stats={},
            status=d['status'],
            worker=d['worker']
        )
