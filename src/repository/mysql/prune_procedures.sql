DELIMITER //

DROP PROCEDURE IF EXISTS pickRandomPruneJob;
CREATE PROCEDURE pickRandomPruneJob(
    IN groupId INT,
    IN pendingStatus TINYINT,
    IN runningStatus TINYINT,
    OUT jobId INT)
BEGIN
    DECLARE jobNotFound BOOL DEFAULT FALSE;
    DECLARE exceptionNoWait BOOL DEFAULT FALSE;
    DECLARE selectedJobId INT DEFAULT NULL;

    DECLARE dateTimeNow DATETIME DEFAULT now();

    DECLARE jobCursor CURSOR FOR
        SELECT id
        FROM prune
        WHERE enabled=1 AND (status=pendingStatus OR (status=runningStatus AND running_expiration<dateTimeNow)) AND group_id=groupId
        ORDER BY RAND();

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET jobNotFound = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLSTATE 'HY000' set exceptionNoWait = TRUE;

    SET jobId = NULL;

    OPEN jobCursor;

    fetchJob: LOOP

        SET jobNotFound = FALSE;
        FETCH jobCursor INTO selectedJobId;

        IF jobNotFound THEN
            /* job not found */
            LEAVE fetchJob;
        END IF;

        SET exceptionNoWait = FALSE;

        /* a prune job was found, try to take a lock on it */
        SELECT id INTO selectedJobId
        FROM prune
        WHERE enabled=1 AND (status=pendingStatus OR (status=runningStatus AND running_expiration<dateTimeNow)) AND id=selectedJobId FOR UPDATE NOWAIT;

        IF jobNotFound THEN
            /* job either changed status or was disabled after being selected by the cursor */
            ITERATE fetchJob;
        END IF;

        IF exceptionNoWait THEN
            /* cannot take a lock on the row, will try with the next row */
            ITERATE fetchJob;
        END IF;

        /* we have the lock on the prune, we will update it with the new status */
        UPDATE prune
        SET status=runningStatus
        WHERE id=selectedJobId;

        SET jobId = selectedJobId;
        COMMIT;
        LEAVE fetchJob;

    END LOOP fetchJob;

    CLOSE jobCursor;

END; //


DROP PROCEDURE IF EXISTS pickOldestPruneJob;
CREATE PROCEDURE pickOldestPruneJob(
    IN groupId INT,
    IN pendingStatus TINYINT,
    IN runningStatus TINYINT,
    OUT jobId INT)
BEGIN
    DECLARE jobNotFound BOOL DEFAULT FALSE;
    DECLARE exceptionNoWait BOOL DEFAULT FALSE;
    DECLARE selectedJobId INT DEFAULT NULL;

    DECLARE dateTimeNow DATETIME DEFAULT now();

    DECLARE jobCursor CURSOR FOR
        SELECT id
        FROM prune
        WHERE enabled=1 AND (status=pendingStatus OR (status=runningStatus AND running_expiration<dateTimeNow)) AND group_id=groupId
        ORDER BY finished;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET jobNotFound = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLSTATE 'HY000' set exceptionNoWait = TRUE;

    SET jobId = NULL;

    OPEN jobCursor;

    fetchJob: LOOP

        SET jobNotFound = FALSE;
        FETCH jobCursor INTO selectedJobId;

        IF jobNotFound THEN
            /* job not found */
            LEAVE fetchJob;
        END IF;

        SET exceptionNoWait = FALSE;

        /* a prune job was found, try to take a lock on it */
        SELECT id INTO selectedJobId
        FROM prune
        WHERE enabled=1 AND (status=pendingStatus OR (status=runningStatus AND running_expiration<dateTimeNow)) AND id=selectedJobId FOR UPDATE NOWAIT;

        IF jobNotFound THEN
            /* job either changed status or was disabled after being selected by the cursor */
            ITERATE fetchJob;
        END IF;

        IF exceptionNoWait THEN
            /* cannot take a lock on the row, will try with the next row */
            ITERATE fetchJob;
        END IF;

        /* we have the lock on the prune, we will update it with the new status */
        UPDATE prune
        SET status=runningStatus
        WHERE id=selectedJobId;

        SET jobId = selectedJobId;
        COMMIT;
        LEAVE fetchJob;

    END LOOP fetchJob;

    CLOSE jobCursor;

END; //

DROP PROCEDURE IF EXISTS pickRescheduledPruneJob;
CREATE PROCEDURE pickRescheduledPruneJob(
    IN groupId INT,
    IN rescheduledStatus TINYINT,
    IN runningStatus TINYINT,
    OUT jobId INT)
BEGIN
    DECLARE jobNotFound BOOL DEFAULT FALSE;
    DECLARE exceptionNoWait BOOL DEFAULT FALSE;
    DECLARE selectedJobId INT DEFAULT NULL;

    DECLARE dateTimeNow DATETIME DEFAULT now();

    DECLARE jobCursor CURSOR FOR
        SELECT id
        FROM prune
        WHERE enabled=1 AND status=rescheduledStatus AND group_id=groupId AND finished<dateTimeNow
        ORDER BY finished;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET jobNotFound = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLSTATE 'HY000' set exceptionNoWait = TRUE;

    SET jobId = NULL;

    OPEN jobCursor;

    fetchJob: LOOP

        SET jobNotFound = FALSE;
        FETCH jobCursor INTO selectedJobId;

        IF jobNotFound THEN
            /* job not found */
            LEAVE fetchJob;
        END IF;

        SET exceptionNoWait = FALSE;

        /* a prune job was found, try to take a lock on it */
        SELECT id INTO selectedJobId
        FROM prune
        WHERE enabled=1 AND status=rescheduledStatus AND finished<dateTimeNow AND id=selectedJobId FOR UPDATE NOWAIT;

        IF jobNotFound THEN
            /* job either changed status or was disabled after being selected by the cursor */
            ITERATE fetchJob;
        END IF;

        IF exceptionNoWait THEN
            /* cannot take a lock on the row, will try with the next row */
            ITERATE fetchJob;
        END IF;

        /* we have the lock on the prune, we will update it with the new status */
        UPDATE prune
        SET status=runningStatus
        WHERE id=selectedJobId;

        SET jobId = selectedJobId;
        COMMIT;
        LEAVE fetchJob;

    END LOOP fetchJob;

    CLOSE jobCursor;

END; //
