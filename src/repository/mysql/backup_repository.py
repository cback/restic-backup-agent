# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, Iterable, List, Optional, TYPE_CHECKING, Tuple, cast
from datetime import datetime, timedelta, time
from model.jobs.job import Repository
from model.jobs.status import Status
from model.jobs.group import Group
from repository import BackupRepository, Filter, GroupRepository, Instance, Operator
from repository.interfaces.job_repository import EmptyListError
from repository.mysql.mysql_repository import MySQLRepository
from model.jobs.backup import Backup
from repository.mysql.restic_repository import ResticRepository
from repository.mysql.common import filters_to_sql, key_prefix

if TYPE_CHECKING:
    from repository.mysql.common import MySQLConfig


# ###############################################################################################
# The DB contains the following tables for backup jobs management:
#
#  - backup (id: int, group_id: int, name: str, repository: str, source: str, enabled: bool,
#            pinned_node: str, force: bool, instance_id: int, started: datetime, finished: datetime,
#            worker: str, retries: int, status_id: int, bytes_total: int, data_added: int,
#            file_errors: int, dirs_changed: int, dirs_new: int, dirs_unmodified: int,
#            files_changed: int, files_new: int, files_total: int, files_unmodified: int,
#            info: str)
#
#
# `backup` contains the list of all the backups.
# ###############################################################################################


class MySQLBackupRepository(BackupRepository):

    def __init__(self, config: 'MySQLConfig') -> None:
        self._repo = MySQLRepository(config)
        self._restic_repository = ResticRepository(config)

    def add(self, backup: 'Backup') -> 'Backup':
        """add a new backup job in the DB

        Parameters:
        job (Backup): Backup to add

        Returns:
        Backup: the backup job added in the db with updated id and defaults values
        """

        query_str = "INSERT INTO `backup`(`name`, `username`, `group_id`, `instance_id`, `source`, `enabled`, `status`, `repository`, `force`, `pinned_node`) " \
                    "VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        backup_id = self._repo.query([query_str], params=[(backup.name, backup.username, backup.group.id, backup.instance.id, backup.source,
                                     backup.enabled, Status.PENDING, backup.repository.path, backup.force, backup.pinned_node)]).lastrowid

        # TODO (gdelmont): make it atomical
        return self.get_by_id(backup_id)

    def get_filtered(self, order_by: str = 'name', filters: List[Filter] = []) -> Iterable['Backup']:
        """return the list of all backups job that matches the filter

        Paramters:
        order_by (str): select how to order the jobs in the list
        filters: ...

        Returns:
        List[Backup]: list of filtered backups
        """

        params = cast(Tuple, ())

        query_str = "SELECT backup.id as id, group_id, g.name as group_name, backup.name, username, repository, password, source, enabled, pinned_node, `force`, i.id as instance_id, i.name as instance_name, " \
                    "started, finished, running_expiration, worker, status, retries, desired_start_time, bytes_total, data_added, dirs_changed, dirs_new, dirs_unmodified, " \
                    "files_changed, files_new, files_total, files_unmodified, file_errors, info, tag, progress " \
                    "FROM backup, `group` as g, instance as i " \
                    "WHERE g.id = backup.group_id AND backup.instance_id = i.id"

        if filters:
            filters_query, params = filters_to_sql(
                filters, prefix_filters="backup")
            query_str += ' AND ' + filters_query

        query_str += " ORDER BY " + order_by

        res = self._repo.query([query_str], params=[params])
        if res.res:
            return map(lambda r: _dict_to_backup(r), res.res)
        raise EmptyListError()

    def get_by_id(self, id: int) -> 'Backup':
        try:
            lst = self.get_filtered(filters=[Filter('id', Operator.EQUAL, id)])
        except EmptyListError:
            lst = []
        for b in lst:
            return b
        raise ValueError(f'id {id} does not exist')

    def get_by_name(self, name: str) -> 'Backup':
        try:
            lst = self.get_filtered(
                filters=[Filter('name', Operator.EQUAL, name)])
        except EmptyListError:
            lst = []
        for b in lst:
            return b
        raise ValueError(f'name {name} does not exist')

    def count_filtered(self, filters: List[Filter] = []) -> int:
        """return the number of backup jobs that matches with the filters passed to the function

        Parameters:
        filters: ...

        Returns:
        int: number of matches
        """

        query_str = "SELECT count(*) as count FROM backup "
        params = cast(Tuple, ())
        if filters:
            filters_query, params = filters_to_sql(filters)
            query_str += f"WHERE {filters_query}"
        res = self._repo.query([query_str], params=[params]).res

        return cast(int, res[0]['count']) if res else 0

    def update_by_id(self, id: int, **params: Any) -> 'Backup':
        """update and return the backup job whose id is `id

        Parameters:
        id (int): ...
        kargs:

        Returns:
        Backup: the updated backup job
        """
        self.update_by_filters(
            filters=[Filter('id', Operator.EQUAL, id)], **params)
        return self.get_by_id(id)

    def update_by_instance(self, instance_id: int, **params: Any) -> Iterable['Backup']:
        # TODO (gdelmont): use update_by_filters instead
        # defining filters for columns in other table jobs
        raise NotImplementedError

    def update_by_filters(self, filters: List[Filter], **params: Any) -> int:
        # TODO: make it atomic

        if not params:
            raise Exception('specify parameters to update')
        query_str = f"UPDATE `backup` SET "

        entries = ', '.join(map(lambda x: f"`{x[0]}`=%s", params.items()))
        p = tuple([v for _, v in params.items()])

        query_str += entries

        if filters:
            filters_query, filters_params = filters_to_sql(
                filters, prefix_filters="backup")
            query_str += f" WHERE {filters_query}"
            p += filters_params

        return self._repo.query([query_str], params=[p]).rowcount

    # delete the given backup
    def delete_by_id(self, backup_id: int) -> None:
        query_str = "DELETE FROM `backup` WHERE id=%s"
        self._repo.query([query_str], params=[(backup_id,)])

    def pick(self, method: str, group: Group, **kwargs: Any) -> Optional['Backup']:
        """atomically select, basing on the method, a job, setting it to running status

        Parameters:
        method (str): method used to select the job. Can be 'random' or 'oldest'.
        node_pinning (bool): whether prefer the pinned node or not

        Returns:
        Backup: the choosed backup job
        """

        id = None
        if method == 'random':
            id = self._repo.execute_procedure(
                'pickRandomBackupJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'oldest':
            id = self._repo.execute_procedure(
                'pickOldestBackupJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'pinned_node':
            pinned_node: str = kwargs.get('pinned_node', '')
            id = self._repo.execute_procedure(
                'pickPinnedNodeBackupJob', int, (group.id, pinned_node, Status.PENDING, Status.RUNNING))
        elif method == 'rescheduled':
            id = self._repo.execute_procedure(
                'pickRescheduledBackupJob', int, (group.id, Status.RESCHEDULED, Status.RUNNING))
        else:
            raise ValueError(f'method {method} not supported')
        if id is not None:
            return self.get_by_id(id)
        return None

    # enable backups by instance
    # def enable_by_instance(self, instance_id):
    #     query_str = "UPDATE backup_backup SET enabled=%s WHERE instance_id=%s"
    #     return self._repo.update(query_str, params=(True, instance_id))

    # # disable the given backup
    # def disable(self, backup_id):
    #     return self.update_by_id(backup_id, enabled=False)

    # # disable backups by instance
    # def disable_by_instance(self, instance_id):
    #     query_str = "UPDATE backup_backup SET enabled=%s WHERE instance_id=%s"
    #     return self._repo.update(query_str, params=(False, instance_id))

    # # return backup information based on a given path
    # def get_by_path(self, path: str) -> List[Backup]:
    #     return self.get_list_filtered(path=path)


def _dict_to_backup(d: Dict[str, Any], *, prefix: str = '') -> 'Backup':
    group = Group(d[key_prefix('group_id', prefix)],
                  d[key_prefix('group_name', prefix)])
    instance = Instance(d[key_prefix('instance_id', prefix)],
                        d[key_prefix('instance_name', prefix)])
    repository = Repository(
        d[key_prefix('repository', prefix)], d[key_prefix('password', prefix)])

    return Backup(
        d[key_prefix('id', prefix)],
        group,
        repository,
        d[key_prefix('username', prefix)],
        d[key_prefix('enabled', prefix)],
        d[key_prefix('status', prefix)],
        d[key_prefix('worker', prefix)],
        d[key_prefix('started', prefix)],
        d[key_prefix('finished', prefix)],
        d[key_prefix('running_expiration', prefix)],
        d.get(key_prefix('info', prefix), None),
        d[key_prefix('retries', prefix)],
        cast(Dict[str, Any], {}), d[key_prefix('name', prefix)],
        d[key_prefix('source', prefix)],
        d[key_prefix('pinned_node', prefix)],
        d[key_prefix('force', prefix)],
        instance,
        d[key_prefix("desired_start_time", prefix)],
        d[key_prefix('tag', prefix)],
        d[key_prefix("progress", prefix)],
        d[key_prefix('bytes_total', prefix)],
        d[key_prefix('data_added', prefix)],
        d[key_prefix('dirs_changed', prefix)],
        d[key_prefix('dirs_new', prefix)],
        d[key_prefix('dirs_unmodified', prefix)],
        d[key_prefix('files_changed', prefix)],
        d[key_prefix('files_new', prefix)],
        d[key_prefix('files_total', prefix)],
        d[key_prefix('files_unmodified', prefix)],
        d[key_prefix('file_errors', prefix)]
    )
