# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, Type
from dacite import from_dict
from model.jobs.backup import Backup
from model.jobs.prune import Prune
from model.jobs.restore import Restore
from model.jobs.verify import Verify
from repository import JobRepository, BackupRepository, InstanceRepository, PruneRepository, RestoreRepository, J, VerifyRepository
from repository.factory.repository_factory import RepositoryFactory
from repository.mysql.backup_repository import MySQLBackupRepository
from repository.mysql.common import MySQLConfig
from repository.mysql.restore_repository import MySQLRestoreRepository
from repository.mysql.group_repository import GroupRepository, MySQLGroupRepository
from repository.mysql.instance_repository import MySQLInstanceRepository
from repository.mysql.prune_repository import MySQLPruneRepository
from repository.mysql.verify_repository import MySQLVerifyRepository


class MySQLFactory(RepositoryFactory):

    def __init__(self, config: Dict[str, Any]) -> None:
        super().__init__()
        self.config = from_dict(MySQLConfig, config)

    def get_backup_repository(self) -> BackupRepository:
        return MySQLBackupRepository(self.config)

    def get_prune_repository(self) -> PruneRepository:
        return MySQLPruneRepository(self.config)

    def get_restore_repository(self) -> RestoreRepository:
        return MySQLRestoreRepository(self.config)

    def get_verify_repository(self) -> VerifyRepository:
        return MySQLVerifyRepository(self.config)

    def get_instance_repository(self) -> InstanceRepository:
        return MySQLInstanceRepository(self.config)

    def get_group_repository(self) -> GroupRepository:
        return MySQLGroupRepository(self.config)

    def get_repository_by_job(self, job_type: Type[J]) -> JobRepository:
        if job_type == Backup:
            return self.get_backup_repository()
        if job_type == Prune:
            return self.get_prune_repository()
        if job_type == Restore:
            return self.get_restore_repository()
        if job_type == Verify:
            return self.get_verify_repository()

        raise ValueError(f'{job_type} repository does not exist')
