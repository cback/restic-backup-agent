# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import unittest
from model.jobs.restore import Restore
from repository.interfaces.job_repository import EmptyListError, Filter, Operator
from utils.test.repo import TestRepository, get_config
from repository.mysql.group_repository import MySQLGroupRepository
from repository.mysql.restore_repository import MySQLRestoreRepository
from repository.mysql.backup_repository import MySQLBackupRepository


class TestRestoreRepository(unittest.TestCase):

    def __init__(self, methodName: str) -> None:
        super().__init__(methodName=methodName)

        self.config = get_config()
        self.test_repo = TestRepository(self.config)
        self.test_repo.create_tables()

    def setUp(self) -> None:
        self.test_repo.drop_tables()
        self.test_repo.create_tables()

        self.group_repo = MySQLGroupRepository(self.config)
        self.backup_repo = MySQLBackupRepository(self.config)
        self.restore_repo = MySQLRestoreRepository(self.config)

    def tearDown(self) -> None:
        self.test_repo.drop_tables()

    ####################################  TEST ADD  ####################################

    def test_add_new_restore(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}]
        }

        self.test_repo.feed_database(data)

        backup = self.backup_repo.get_by_id(10)

        restore = Restore.new(
            backup=backup,
            username='test-restore',
            snapshot='s_id',
            pattern='*',
            destination='/mnt/restore',
            enabled=True
        )

        r = self.restore_repo.add(restore)

        self.assertEqual(self.test_repo.count("restore"), 1)
        self.assertIsNotNone(self.test_repo.get_by_id("restore", r.id))

        ####################################  TEST GET_FILTERED  ####################################

    def test_get_filtered_empty_table(self) -> None:
        with self.assertRaises(EmptyListError):
            self.restore_repo.get_filtered(
                filters=[Filter('id', Operator.EQUAL, 9999)])

    def test_get_filtered_not_empty_table_empty_result(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'group_id': 1, 'backup_id': 10, 'snapshot': 's_id', 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1}]
        }
        self.test_repo.feed_database(data)

        with self.assertRaises(EmptyListError):
            self.restore_repo.get_filtered(
                filters=[Filter('id', Operator.EQUAL, 9999)])

    def test_get_filtered_single_filter(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.get_filtered(
            order_by='id', filters=[Filter('enabled', Operator.EQUAL, True)])
        l = list(res)
        self.assertEqual(len(l), 2)

        self.assertTrue(l[0].id < l[1].id,
                        'the list should be ordered by id')

    def test_get_filtered_multiple_filters(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.get_filtered(filters=[Filter(
            'enabled', Operator.EQUAL, True), Filter('status', Operator.EQUAL, 1)])
        self.assertEqual(len(list(res)), 1)

    ####################################  TEST GET_BY_ID  ####################################

    def test_get_by_id_no_restore(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        with self.assertRaises(ValueError):
            self.restore_repo.get_by_id(9999)

    def test_get_by_id_restore_in(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        r = self.restore_repo.get_by_id(2)
        self.assertEqual(r.id, 2)

    ####################################  TEST COUNT_FILTERED  ####################################

    def test_count_filtered_no_filters_empty_table(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.count_filtered()
        self.assertEqual(res, 0)

    def test_count_filtered_no_filters_filled_table(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.count_filtered()
        self.assertEqual(res, 3)

    def test_count_filtered_one_filter_empty_result(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.count_filtered(
            filters=[Filter('id', Operator.EQUAL, 9999)])
        self.assertEqual(res, 0)

    def test_count_filtered_one_filter(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.count_filtered(
            filters=[Filter('id', Operator.EQUAL, 2)])
        self.assertEqual(res, 1)

    def test_count_filtered_multiple_filters_empty_result(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.count_filtered(filters=[Filter(
            'enabled', Operator.EQUAL, False), Filter('status', Operator.EQUAL, 10)])
        self.assertEqual(res, 0)

    def test_count_filtered_multiple_filters(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        res = self.restore_repo.count_filtered(filters=[Filter(
            'enabled', Operator.EQUAL, True), Filter('status', Operator.EQUAL, 2)])
        self.assertEqual(res, 1)

    ####################################  TEST UPDATE_BY_ID  ####################################

    def test_update_by_id_not_existing_id(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1}]
        }

        self.test_repo.feed_database(data)

        with self.assertRaises(Exception):
            self.restore_repo.update_by_id(9999, status=10)

    def test_update_by_id_existing_id_one_param(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1}]
        }

        self.test_repo.feed_database(data)

        # check the value before changing it
        self.assertEqual(self.test_repo.get_by_id(
            "restore", 1)['snapshot'], 's_id')

        self.restore_repo.update_by_id(1, snapshot='new_s_id')

        self.assertEqual(self.test_repo.get_by_id(
            "restore", 1)['snapshot'], 'new_s_id')

    def test_update_by_id_existing_id_zero_params(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1}]
        }

        with self.assertRaises(Exception):
            self.restore_repo.update_by_id(1)

    def test_update_by_id_existing_id_multiple_params(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1}]
        }

        self.test_repo.feed_database(data)

        # check the values before changing it
        self.assertEqual(self.test_repo.get_by_id(
            "restore", 1)['snapshot'], 's_id')
        self.assertEqual(self.test_repo.get_by_id("restore", 1)['status'], 1)

        self.restore_repo.update_by_id(1, snapshot='new_s_id', status=5)

        self.assertEqual(self.test_repo.get_by_id(
            "restore", 1)['snapshot'], 'new_s_id')
        self.assertEqual(self.test_repo.get_by_id("restore", 1)['status'], 5)

    ####################################  TEST DELETE_BY_ID  ####################################

    def test_delete_by_id_not_existing_id(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        # check number of entries before deletion
        self.assertEqual(self.test_repo.count("restore"), 3)

        # delete a not existing restire is a no-op
        self.restore_repo.delete_by_id(99999)

        # because the delete has been a no-op, the number of entries should stay the same
        self.assertEqual(self.test_repo.count("restore"), 3)

    def test_delete_by_id(self) -> None:
        data = {
            'group': [{'id': 1, 'name': 'testgroup'}],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [{'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup', 'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}],
            'restore': [{'id': 1, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'backup_id': 10, 'group_id': 1, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        # check number of entries before deletion
        self.assertEqual(self.test_repo.count("restore"), 3)

        self.restore_repo.delete_by_id(2)

        self.assertEqual(self.test_repo.count("restore"), 2)

    ##### OTHER TESTS #####

    def test_update_group_id_restore_when_updating_backup_group(self) -> None:
        data = {
            'group': [
                {'id': 1, 'name': 'testgroup1'},
                {'id': 2, 'name': 'testgroup2'}
            ],
            'instance': [{'id': 1, 'name': 'testinstance'}],
            'backup': [
                {'id': 10, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup',
                    'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'},
                {'id': 11, 'group_id': 1, 'instance_id': 1, 'name': 'testbackup',
                    'source': '/root', 'enabled': True, 'status': 1, 'repository': '/somewhere'}
            ],
            'restore': [{'id': 1, 'group_id': 1, 'backup_id': 10, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 1},
                        {'id': 2, 'group_id': 1, 'backup_id': 11, 'snapshot': 's_id', 'date': None,
                            'pattern': '*', 'destination': None, 'enabled': False, 'status': 2},
                        {'id': 3, 'group_id': 1, 'backup_id': 10, 'snapshot': 's_id', 'date': None, 'pattern': '*', 'destination': None, 'enabled': True, 'status': 2}]
        }

        self.test_repo.feed_database(data)

        self.backup_repo.update_by_id(10, group_id=2)

        # check if the prune jobs are updated according
        self.assertEqual(self.test_repo.get_by_id('restore', 1)['group_id'], 2)
        self.assertEqual(self.test_repo.get_by_id('restore', 3)['group_id'], 2)

        # check if the prune job with id 3 has the same group_id as before
        self.assertEqual(self.test_repo.get_by_id('restore', 2)['group_id'], 1)
