# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Any, Dict, Iterable, List, Optional, TYPE_CHECKING, Tuple, cast

from repository import Filter, Operator, PruneRepository
from repository.interfaces.job_repository import EmptyListError
from .backup_repository import _dict_to_backup
from repository.mysql.mysql_repository import MySQLRepository
from model.jobs.prune import Prune
from model.jobs.status import Status
from model.jobs.group import Group

from repository.mysql.common import filters_to_sql

if TYPE_CHECKING:
    from repository.mysql.common import MySQLConfig

# ###############################################################################################
# The DB contains the following tables for prune jobs management:
#
#  - prune (id: int, backup_id: int, enabled: bool, started: datetime, finished: datetime,
#           worker: str, info: str, retries: int, status: int)
#
#
# `prune` contains the list of all the prunes.
# ###############################################################################################


class MySQLPruneRepository(PruneRepository):

    def __init__(self, config: 'MySQLConfig'):
        self._repo = MySQLRepository(config)

    def add(self, prune: 'Prune') -> 'Prune':
        """add a new prune job in the DB

        Parameters:
        job (Prune): Prune to add

        Returns:
        Prune: the prune job added in the db with updated id and defaults values
        """

        query_str = "INSERT INTO `prune`(`backup_id`, `group_id`, `enabled`, `status`) " \
                    "VALUES(%s,%s,%s,%s)"
        prune_id = self._repo.query([query_str], params=[(
            prune.backup.id, prune.backup.group.id, prune.enabled, prune.status)]).lastrowid

        return self.get_by_id(prune_id)

    def get_filtered(self, order_by: str = 'id', filters: List[Filter] = []) -> Iterable['Prune']:
        """return the list of all prunes job that matches the filter

        Paramters:
        order_by (str): select how to order the jobs in the list
        filters: ...

        Returns:
        List[Prune]: list of filtered prunes
        """

        params = cast(Tuple[Any, ...], ())

        query_str = """ SELECT p.id, b.id as backup_id, p.enabled, p.started, p.finished,
                                p.running_expiration, p.worker, p.info, p.retries, p.status, p.group_id,
                                b.id as backup_id, g.id as backup_group_id, g.name as backup_group_name, b.name as backup_name, b.username as backup_username,
                                b.repository as backup_repository, b.password as backup_password, b.source as backup_source, b.enabled as backup_enabled, b.pinned_node as backup_pinned_node,
                                b.force as backup_force, i.id as backup_instance_id, i.name as backup_instance_name, b.started as backup_started, b.finished as backup_finished,
                                b.running_expiration as backup_running_expiration, b.worker as backup_worker, b.status as backup_status, b.retries as backup_retries,
                                b.desired_start_time as backup_desired_start_time, b.bytes_total as backup_bytes_total, b.data_added as backup_data_added,
                                b.dirs_changed as backup_dirs_changed, b.dirs_new as backup_dirs_new, b.dirs_unmodified as backup_dirs_unmodified,
                                b.files_changed as backup_files_changed, b.files_new as backup_files_new, b.files_total as backup_files_total,
                                b.files_unmodified as backup_files_unmodified, b.file_errors as backup_file_errors, b.progress as backup_progress, b.tag as backup_tag
                        FROM   backup as b, prune as p, `group` as g, instance as i
                        WHERE  b.id = p.backup_id AND g.id = b.group_id AND b.instance_id = i.id"""

        if filters:
            filters_query, params = filters_to_sql(filters, prefix_filters="p")
            query_str += f' AND {filters_query}'

        query_str += " ORDER BY " + order_by

        res = self._repo.query([query_str], params=[params])
        if res.res:
            return map(lambda r: self._dict_to_prune(r), res.res)
        raise EmptyListError()

    def get_by_id(self, id: int) -> 'Prune':
        try:
            lst = self.get_filtered(filters=[Filter('id', Operator.EQUAL, id)])
        except EmptyListError:
            lst = []
        for p in lst:
            return p
        raise ValueError(f'id {id} does not exist')

    def count_filtered(self, filters: List[Filter] = []) -> int:
        """return the number of prune jobs that matches with the filters passed to the function

        Parameters:
        filters: ...

        Returns:
        int: number of matches
        """

        query_str = "SELECT count(*) as count FROM prune "
        params = cast(Tuple[Any, ...], ())
        if filters:
            filters_query, params = filters_to_sql(filters)
            query_str += f"WHERE {filters_query}"
        res: List[Dict[str, int]] = self._repo.query(
            [query_str], params=[params]).res

        return res[0]['count'] if res else 0

    def update_by_id(self, id: int, **params: Any) -> 'Prune':
        """atomically update and return the prune job given the id

        Parameters:
        id (int): prune id to update
        params:

        Returns:
        Job: the updated prune job
        """

        self.update_by_filters(
            filters=[Filter('id', Operator.EQUAL, id)], **params)
        return self.get_by_id(id)

    def update_by_instance(self, instance_id: int, **params: Any) -> Iterable['Prune']:
        raise NotImplementedError

    def update_by_filters(self, filters: List[Filter], **params: Any) -> int:

        if not params:
            raise Exception('specify parameters to update')
        query_str = f"UPDATE `prune` SET "

        entries = ', '.join(map(lambda x: f"`{x[0]}`=%s", params.items()))
        p = tuple([v for _, v in params.items()])

        query_str += entries

        if filters:
            filters_query, filters_params = filters_to_sql(filters)
            query_str += f" WHERE {filters_query}"
            p += filters_params

        return self._repo.query([query_str], params=[p]).rowcount

    def delete_by_id(self, id: int) -> None:
        query_str = "DELETE FROM `prune` WHERE id=%s"
        self._repo.query([query_str], params=[(id,)])

    def pick(self, method: str, group: Group, **kwargs: Any) -> Optional['Prune']:
        id = None
        if method == 'random':
            id = self._repo.execute_procedure(
                'pickRandomPruneJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'oldest':
            id = self._repo.execute_procedure(
                'pickOldestPruneJob', int, (group.id, Status.PENDING, Status.RUNNING))
        elif method == 'rescheduled':
            id = self._repo.execute_procedure(
                'pickRescheduledPruneJob', int, (group.id, Status.RESCHEDULED, Status.RUNNING))
        else:
            raise ValueError(f'method {method} not supported')
        if id is not None:
            return self.get_by_id(id)
        return None

    def _dict_to_prune(self, d: Dict[str, Any]) -> 'Prune':
        backup = _dict_to_backup(d, prefix='backup')
        return Prune(
            id=d['id'],
            backup=backup,
            username=None,
            enabled=d['enabled'],
            finished=d['finished'],
            group=backup.group,
            info=d['info'],
            repository=backup.repository,
            running_expiration=d['running_expiration'],
            retries=d['retries'],
            started=d['started'],
            stats={},
            status=d['status'],
            worker=d['worker']
        )
