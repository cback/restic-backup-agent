CREATE TABLE IF NOT EXISTS `group` (
    `id` int not null auto_increment,
    `name` varchar(24) not null,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`name`)
);

CREATE TABLE IF NOT EXISTS `instance` (
    `id` int not null auto_increment,
    `name` varchar(120) not null,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`name`)
);

CREATE TABLE IF NOT EXISTS `backup` (
    `id` int not null auto_increment,
    `group_id` int not null,
    `name` varchar(120) not null,
    `username` varchar(120) default null,
    `password` varchar(120) default null,
    `repository` varchar(120) not null,
    `source` varchar(255) not null,
    `enabled` bool not null default '0',
    `pinned_node` varchar(120) default null,
    `force` bool not null default '0',
    `instance_id` int not null,
    `started` datetime default null,
    `finished` datetime default null,
    `running_expiration` datetime default null,
    `worker` varchar(120) default null,
    `retries` int not null default '0',
    `status` int not null,
    `tag` varchar(255) default null,
    `desired_start_time` varchar(120) default null,
    `progress` float not null default 0.0,
    `bytes_total` bigint(20) not null default '0',
    `data_added` bigint(20) not null default '0',
    `file_errors` bigint(20) not null default '0',
    `dirs_changed` bigint(20) not null default '0',
    `dirs_new` bigint(20) not null default '0',
    `dirs_unmodified` bigint(20) not null default '0',
    `files_changed` bigint(20) not null default '0',
    `files_new` bigint(20) not null default '0',
    `files_total` bigint(20) not null default '0',
    `files_unmodified` bigint(20) not null default '0',
    `info` text default null,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`instance_id`) REFERENCES `instance` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `prune` (
    `id` int not null auto_increment,
    `backup_id` int not null,
    `group_id` int not null,
    `enabled` bool not null,
    `started` datetime default null,
    `finished` datetime default null,
    `running_expiration` datetime default null,
    `worker` varchar(120) default null,
    `info` text default null,
    `retries` int not null default '0',
    `status` int not null,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`backup_id`) REFERENCES `backup` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `verify` (
    `id` int not null auto_increment,
    `backup_id` int not null,
    `group_id` int not null,
    `enabled` bool not null,
    `started` datetime default null,
    `finished` datetime default null,
    `running_expiration` datetime default null,
    `worker` varchar(120) default null,
    `info` text default null,
    `retries` int not null default '0',
    `status` int not null,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`backup_id`) REFERENCES `backup` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `restore` (
    `id` int not null auto_increment,
    `backup_id` int not null,
    `group_id` int not null,
    `username` varchar(120) default null,
    `snapshot` varchar(8) default null,
    `date` datetime default null,
    `destination` varchar(255) default null,
    `pattern` varchar(255) default null,    
    `enabled` bool not null,
    `created` datetime default CURRENT_TIMESTAMP,
    `started` datetime default null,
    `finished` datetime default null,
    `running_expiration` datetime default null,
    `worker` varchar(120) default null,
    `info` text default null,
    `retries` int not null default '0',
    `status` int not null,
    `progress` float not null default 0.0,
    `trim_source` bool not null default false,
    `pretrim_scratch_path` varchar(2048) default null,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`backup_id`) REFERENCES `backup` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `sync` (
    `id` int not null auto_increment,
    `backup_id` int not null,
    `group_id` int not null,
    `target` varchar(255) not null,
    `type` varchar(16) not null,
    `enabled` bool not null,
    `started` datetime not null,
    `finished` datetime default null,
    `running_expiration` datetime default null,
    `worker` varchar(120) default null,
    `info` text default null,
    `retries` int not null default '0',
    `status` int not null,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`backup_id`) REFERENCES `backup` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `token` (
    `id` int not null auto_increment,
    `hashed_token` varchar(60) not null,
    `created` datetime default CURRENT_TIMESTAMP,
    `expiration` datetime default null,
    `comment` varchar(255) default null,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `token_group` (
    `id` int not null auto_increment,
    `token_id` int not null,
    `group` varchar(24) not null,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`token_id`) REFERENCES `token` (`id`) ON DELETE CASCADE,
    FOREIGN KEY (`group`) REFERENCES `group` (`name`)
);

CREATE TABLE IF NOT EXISTS `scopes` (
    `id` int not null auto_increment,
    `opaque_user` varchar(255) not null,
    `group` varchar(255) default null,
    `backup_id` int default null,
    `permissions` int not null,
    PRIMARY KEY(`id`)
);

DROP TABLE IF EXISTS `release_version`;
CREATE TABLE `release_version` (
    `version` varchar(255) not null
);
INSERT INTO release_version (version) VALUES ('0.3.9'); 


#######
CREATE TRIGGER update_group_jobs AFTER UPDATE ON `backup` FOR EACH ROW
    BEGIN
        IF OLD.group_id <> NEW.group_id THEN
            UPDATE `prune` SET group_id = NEW.group_id WHERE backup_id = OLD.id;
            UPDATE `restore` SET group_id = NEW.group_id WHERE backup_id = OLD.id;
            UPDATE `verify` SET group_id = NEW.group_id WHERE backup_id = OLD.id;
        END IF;
    END;
