# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from pydantic import parse_obj_as
from repository import GroupRepository
from repository.mysql.mysql_repository import MySQLRepository
from typing import List, Dict, TYPE_CHECKING
from model.jobs.group import Group

if TYPE_CHECKING:
    from repository.mysql.common import MySQLConfig

# ####################################################################################
# The DB contains the followin tbale for group management:
#
#  - group (id: int, name: str)
#
# `group` contains the list of all the groups
# ####################################################################################


class MySQLGroupRepository(GroupRepository):

    def __init__(self, config: 'MySQLConfig'):
        self._repo = MySQLRepository(config)

    def add(self, name: str) -> 'Group':
        """create a new group into the DB given the name

        Parameters:
        name (str): name of the new group

        Returns:
        int: index of the new group

        """
        query_str = "INSERT INTO `group`(`name`) VALUES(%s)"
        id = self._repo.query([query_str], params=[(name,)]).rowcount
        return Group(id=id, name=name)

    def get_by_name(self, name: str) -> 'Group':
        """get the group by name

        Parameters:
        name (str): name of the group

        Returns:
        Dict: group info with the following keys: ['id', 'name']
        """

        query_str = "SELECT `id`, `name` FROM `group` WHERE name=%s"
        lst = self._repo.query([query_str], params=[(name,)]).res
        if not lst:
            raise ValueError(f'group with name {name} does not exist')
        return Group(**lst[0])

    def get_all(self) -> List['Group']:
        """get all the groups stored in the DB

        Returns:
        List[Dict]: list of all the group

        """
        query_str = "SELECT `id`, `name` FROM `group`"
        return parse_obj_as(List[Group], self._repo.query([query_str], params=[()]).res)
