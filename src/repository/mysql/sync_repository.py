# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
# from model.config.shared_config import get_shared_config
# from repository.mysql_repository import MySQLRepository
# from repository.model import Sync, SyncType, Status
# from enum import Enum
# from dacite import from_dict, Config


# class SyncRepository:

#     def __init__(self):
#         self.__repo = MySQLRepository(get_shared_config().database)
#         self.__table_name = "backup_sync"

#     def __cast_sync_from_dict(self, d):
#         return from_dict(Sync, d, Config(cast=[Enum], type_hooks={bool: bool}))

#     def get_by_id(self, sync_id):
#         query_str = "SELECT * " \
#                     "FROM backup_sync WHERE id=%s"
#         sync_row = self.__repo.query_one(query_str, params=(sync_id,))
#         return self.__cast_sync_from_dict(sync_row) if sync_row else None

#     def get_enabled_random(self):
#         query_str = "SELECT * " \
#                     f"FROM backup_sync WHERE enabled=true and status={Status.PENDING.value} ORDER BY RAND() LIMIT 1"
#         sync_row = self.__repo.query_one(query_str)
#         return self.__cast_sync_from_dict(sync_row) if sync_row else None

#     def get_list(self):
#         return self.get_list_filtered()

#     def get_list_by_status(self, status: Status):
#         return self.get_list_filtered(status=status)

#     def get_list_by_type(self, sync_type: SyncType):
#         return self.get_list_filtered(sync_type=sync_type)

#     def get_list_filtered(self, **kargs):
#         params = ()
#         query_str = "SELECT * " \
#                     "FROM backup_sync"

#         if kargs:
#             filters = ' AND '.join(
#                 list(map(lambda x: f"{x[0]}=%s", kargs.items())))
#             query_str += f" WHERE {filters}"
#             params = tuple([v.value if isinstance(v, Enum)
#                            else v for _, v in kargs.items()])

#         lst = self.__repo.query(query_str, params=params)
#         return list(map(self.__cast_sync_from_dict, lst)) if lst else None

#     def add(self, backup_id, target_url, sync_type: SyncType, status: Status, enabled):
#         query_str = f"INSERT INTO {self.__table_name}(backup_id, target_url, sync_type, status, enabled) " \
#                     "VALUES(%s, %s, %s, %s, %s)"
#         return self.__repo.insert(query_str, params=(
#             backup_id, target_url, sync_type.value, status.value, enabled))

#     def update_by_id(self, sync_id, **kargs):
#         if not kargs:
#             raise UpdateMissingArgsException
#         query_str = f"UPDATE {self.__table_name} SET "

#         entries = ', '.join(map(lambda x: f"{x[0]}=%s", kargs.items()))
#         params = tuple([v.value if isinstance(v, Enum)
#                        else v for _, v in kargs.items()])

#         query_str += f"{entries} WHERE id=%s"
#         params += (sync_id,)

#         return self.__repo.update(query_str, params=params)

#     def delete_by_id(self, sync_id):
#         sync_job = self.get_by_id(sync_id)
#         query_str = f"DELETE FROM {self.__table_name} WHERE id=%s"
#         self.__repo.update(query_str, params=(sync_id,))
#         return sync_job

#     def count(self):
#         query_str = f"SELECT count(*) as count FROM {self.__table_name}"
#         return self.__repo.query_one(query_str)['count']

#     def count_by(self, **kargs):
#         if not kargs:
#             raise Exception  # TODO: change
#         query_str = f"SELECT count(*) as count FROM {self.__table_name} "
#         filters = ' AND '.join(
#             list(map(lambda x: f"{x[0]}=%s", kargs.items())))
#         query_str += f"WHERE {filters}"
#         params = tuple([v.value if isinstance(v, Enum)
#                        else v for _, v in kargs.items()])

#         return self.__repo.query_one(query_str, params=params)['count']


# class UpdateMissingArgsException(Exception):
#     pass
