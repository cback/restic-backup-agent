# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from typing import Dict
import boto3


class Boto:
    def __init__(self, env: Dict[str, str], url: str):
        access_key = env.get('AWS_ACCESS_KEY_ID', '')
        secret_access_key = env.get('AWS_SECRET_ACCESS_KEY', '')

        self.client = boto3.resource(
            's3', endpoint_url=url, aws_access_key_id=access_key, aws_secret_access_key=secret_access_key,)

    def bucket_exists(self, bucket_name: str) -> bool:
        return not self.client.Bucket(bucket_name).creation_date is None

    def bucket_delete(self, bucket_name: str) -> None:
        bucket = self.client.Bucket(bucket_name)
        bucket.objects.all().delete()
        bucket.delete()
