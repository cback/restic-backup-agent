# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
from abc import ABC, abstractmethod
from typing import Any, List, Set


class Subscriber(ABC):
    @abstractmethod
    def notify(self, event: object) -> None:
        pass


class Publisher:
    __slots__ = ['_subscribers', '_publishing', '_unsubscribe_list']

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self._subscribers: Set[Subscriber] = set()
        self._publishing = False
        self._unsubscribe_list: List[Subscriber] = []

    def register_subscriber(self, s: Subscriber) -> None:
        self._subscribers.add(s)

    def unsubscribe(self, s: Subscriber) -> None:
        if self._publishing:
            self._unsubscribe_list.append(s)
        else:
            self._subscribers.remove(s)

    def _publish_event(self, e: object) -> None:
        self._publishing = True
        for s in self._subscribers:
            s.notify(e)
        self._publishing = False

        for s in self._unsubscribe_list:
            self._subscribers.remove(s)
        self._unsubscribe_list = []
