# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import random
import string
from .local_fs import *
from model.restic.restic import Restic
from datetime import datetime
from pwd import getpwnam
from itertools import chain


PASSWORD = 'koala'


def new_empty_restic_repo(owner: Optional[str] = None) -> Tuple[Restic, str, Callable]:
    dir, cleanup = new_tmp_dir()
    repo = Restic(dir, PASSWORD)
    repo.init()

    if owner:
        passwd = getpwnam(owner)
        uid, gid = passwd.pw_uid, passwd.pw_gid
        for root, dirs, files in os.walk(dir):
            for f in chain(dirs, files):
                os.chown(os.path.join(root, f), uid, gid)
        os.chown(dir, uid, gid)

    return repo, PASSWORD, cleanup


def new_restic_repo_one_snapshot(d: Optional[Directory] = None) -> Tuple[Restic, str, Callable]:
    if not d:
        d = Directory(
            name="foo",
            content=[File(name="bar", content="bar")],
        )
    repo, pw, cleanup = new_empty_restic_repo()
    source, cleanup_source = new_local_dir(d)
    repo.backup(source)

    # check if the repo has exactly 1 snapshot
    if len(repo.snapshots()) != 1:
        raise Exception('the test repo should have 1 snapshot')

    cleanup_source()
    return repo, pw, cleanup


def new_restic_repo_dates(times: List[datetime], d: Optional[Directory] = None, random_host: bool = False) -> Tuple[Restic, str, str, Callable]:
    if not d:
        d = Directory(
            name="foo",
            content=[File(name="bar", content="bar")],
        )
    repo, pw, cleanup = new_empty_restic_repo()
    source, cleanup_source = new_local_dir(d)

    for t in times:
        host = _get_random_str() if random_host else None
        repo.backup(source, time=t, host=host)

    # check if the repo has exactly len(times) snapshots
    if len(repo.snapshots()) != len(times):
        raise Exception(f'the test repo should have {len(times)} snapshots')

    cleanup_source()
    return repo, source, pw, cleanup


def _get_random_str(l: int = 10) -> str:
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(l))
