# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import mysql.connector
from typing import Any, Dict, Generic, List, Optional, Type, TypeVar, cast
from os import environ

from model.jobs.group import Group
from model.jobs.job import Job, Repository
from model.jobs.restore import Restore
from model.jobs.status import Status
from model.jobs.backup import Backup
from model.jobs.prune import Prune
from repository import InstanceRepository, Instance
from repository.mysql.common import MySQLConfig
from model.managers.jobs.job_manager import JobManager
from model.managers.jobs.status_strategies.status_strategy import StatusStrategy
from repository import JobRepository, Filter, RepositoryFactory, GroupRepository, BackupRepository, PruneRepository, RestoreRepository
from repository.interfaces.job_repository import Operator


class TestRepository:

    def __init__(self, config: MySQLConfig):
        self.db_config = {
            'host': config.host,
            'user': config.user,
            'port': config.port,
            'password': config.password,
            'database': config.database
        }

    def drop_tables(self) -> None:
        stmt = ['DROP DATABASE IF EXISTS', 'CREATE DATABASE']
        conn = mysql.connector.Connect(**self.db_config)

        for s in stmt:
            cursor = conn.cursor()
            ss = f'{s} {self.db_config["database"]}'
            cursor.execute(ss)
            conn.commit()

        conn.close()

    def _exec_stmt(self, stmt: str) -> None:
        conn = mysql.connector.Connect(**self.db_config)
        cursor = conn.cursor()
        cursor.execute(stmt)
        conn.commit()
        conn.close()

    def create_tables(self) -> None:
        separator = '#######'
        with open('src/repository/mysql/init.sql', 'r') as sql_script:
            s = sql_script.read().strip()

            tables, *triggers = s.split(separator)

            for t in tables.split(';'):
                stmt = t.strip()
                self._exec_stmt(stmt)

            for t in triggers:
                self._exec_stmt(t)

    def clean_table(self, tablename: str) -> None:
        conn = mysql.connector.Connect(**self.db_config)
        cursor = conn.cursor(dictionary=True)

        delete_query = f"DELETE from `{tablename}`"

        cursor.execute(delete_query)

        conn.commit()
        conn.close()

    def feed_database(self, data: Dict[str, List[Dict[str, Any]]]) -> None:
        for name, values in data.items():
            self._add_all(name, values)

    def _add_all(self, tablename: str, values: List[Dict[str, Any]]) -> None:
        conn = mysql.connector.Connect(**self.db_config)
        cursor = conn.cursor(dictionary=True)

        for v in values:
            query = f"INSERT into `{tablename}` (" + ','.join(v.keys()) + ") " \
                    "VALUES (" + ','.join(list(map(lambda _: r'%s', v.values()))) + ")"

            params = tuple(v.values())

            cursor.execute(query, params)

        conn.commit()
        conn.close()

    def get_by_id(self, tablename: str, id: int) -> Dict:

        conn = mysql.connector.Connect(**self.db_config)
        cursor = conn.cursor(dictionary=True)

        select_query = f"SELECT * from `{tablename}` WHERE id=%s"

        cursor.execute(select_query, (id,))
        res: List[Dict] = cursor.fetchall()

        conn.commit()
        conn.close()

        return res[0]

    def count(self, tablename: str) -> int:

        conn = mysql.connector.Connect(**self.db_config)
        cursor = conn.cursor(dictionary=True)

        count_query = f"SELECT count(*) as count FROM {tablename}"

        cursor.execute(count_query)
        res = cursor.fetchall()

        conn.commit()
        conn.close()

        return cast(int, res[0]['count'])


def get_config() -> MySQLConfig:
    return MySQLConfig(
        database=environ['MYSQL_DATABASE'],
        host=environ['MYSQL_HOST'],
        password=environ['MYSQL_ROOT_PASSWORD'],
        port=int(environ['MYSQL_PORT']),
        user="root"
    )


class TestJob(Job):
    pass


J = TypeVar('J', bound=Job)


class MockInMemoryRepositoryFactory(RepositoryFactory):

    def __init__(self) -> None:
        self._instances: Dict[Type[Job], JobRepository] = {}
        self._instance_repo = InMemoryInstanceRepository()

    def get_repository_by_job(self, job_type: Type[J]) -> JobRepository[J]:
        if job_type == Backup and Backup not in self._instances:
            self._instances[Backup] = InMemoryBackupRepository()
        elif job_type == Prune and Prune not in self._instances:
            self._instances[Prune] = InMemoryPruneRepository()
        elif job_type not in self._instances:
            self._instances[job_type] = InMemoryJobRepository()
        return self._instances[job_type]

    def get_instance_repository(self) -> InstanceRepository:
        return self._instance_repo

    def get_backup_repository(self) -> BackupRepository:
        return cast(BackupRepository, self.get_repository_by_job(Backup))

    def get_prune_repository(self) -> PruneRepository:
        return cast(PruneRepository, self.get_repository_by_job(Prune))

    def get_restore_repository(self) -> RestoreRepository:
        return cast(RestoreRepository, self.get_repository_by_job(Restore))


class InMemoryGroupRepository(GroupRepository):

    def __init__(self, groups: List[str] = []) -> None:
        self._repo: Dict[str, Group] = {}
        self._index = 0
        for group in groups:
            self._repo[group] = Group(id=self._index, name=group)
            self._index += 1

    def add(self, name: str) -> 'Group':
        self._repo[name] = Group(id=self._index, name=name)
        self._index += 1
        return self._repo[name]

    def get_by_name(self, name: str) -> 'Group':
        return self._repo[name]

    def get_all(self) -> List['Group']:
        return list(self._repo.values())


class InMemoryInstanceRepository(InstanceRepository):

    def __init__(self, instances: List[str] = []) -> None:
        self._repo: Dict[str, Instance] = {}
        self._index = 0
        for instance in instances:
            self._repo[instance] = Instance(id=self._index, name=instance)
            self._index += 1

    def get_by_name(self, name: str) -> Instance:
        try:
            return self._repo[name]
        except KeyError:
            raise ValueError(f'instance with name {name} not in repo')

    def add(self, name: str) -> Instance:
        self._repo[name] = Instance(id=self._index, name=name)
        self._index += 1
        return self._repo[name]


class MockTestJobManager(JobManager[TestJob]):

    def _create_job(self, **params: Any) -> TestJob:
        assert 'group' in params
        enabled = params.get('enabled', False)
        group = self.group_mgr.get_by_name(params['group'])
        username = 'TODO'
        return TestJob(id=-1, group=group, repository=cast(Repository, None), username=username, enabled=enabled, status=Status.PENDING, worker=None, started=None, finished=None, running_expiration=None, info=None, retries=0, stats={})

    def _job_type(self) -> Type[TestJob]:
        return TestJob

    def _status_list(self, group: Optional[Group]) -> List['StatusStrategy']:
        return []


class InMemoryJobRepository(JobRepository, Generic[J]):

    def __init__(self) -> None:
        self._repo: Dict[int, J] = cast(Dict[int, J], {})
        self._index = 0

    def add(self, job: J) -> J:
        if job.id is None or job.id < 0:
            job.id = self._index
            self._index += 1
        self._repo[job.id] = job
        return job

    def _in(self, job: J, filters: List[Filter]) -> bool:
        for filter in filters:
            if filter.key == 'group_id':
                # special case
                # FIXME when better filters design
                job_val = job.group.id
            else:
                job_val = job.__dict__[filter.key]
            if filter.operator == Operator.EQUAL:
                if not (job_val == filter.value):
                    return False
            elif filter.operator == Operator.GREATER:
                if not (job_val > filter.value):
                    return False
            elif filter.operator == Operator.GREATER_EQUAL:
                if not (job_val >= filter.value):
                    return False
            elif filter.operator == Operator.LESS:
                if not (job_val < filter.value):
                    return False
            elif filter.operator == Operator.LESS_EQUAL:
                if not (job_val <= filter.value):
                    return False
            elif filter.operator == Operator.NOT_EQUAL:
                if not (job_val != filter.value):
                    return False
            else:
                raise ValueError('Operator not supported')
        return True

    def get_filtered(self, order_by: str, filters: List[Filter]) -> List[J]:
        res: List[J] = []
        for job in self._repo.values():
            if self._in(job, filters):
                res.append(job)
        return sorted(res, key=lambda j: j.__dict__[order_by])  # type: ignore

    def get_by_id(self, id: int) -> J:
        if id not in self._repo:
            raise ValueError('job not in repository')
        return self._repo[id]

    def count_filtered(self, filters: List[Filter]) -> int:
        count = 0
        for job in self._repo.values():
            if self._in(job, filters):
                count += 1
        return count

    def update_by_id(self, id: int, **params: Any) -> J:
        job = self.get_by_id(id)
        for pname, pval in params.items():
            if pname not in job.__dict__:
                raise ValueError('parameter name not allowed')
            job.__dict__[pname] = pval
        return job

    def update_by_instance(self, instance_id: int, **params: Any) -> List[J]:
        raise NotImplementedError

    def update_by_filters(self, filters: List[Filter], **params: Any) -> int:
        count = 0
        for job in self._repo.values():
            if self._in(job, filters):
                self.update_by_id(job.id, **params)
                count += 1
        return count

    def delete_by_id(self, id: int) -> Any:
        if id not in self._repo:
            pass
        del self._repo[id]

    def pick(self, method: str, group: Group, **kwargs: Any) -> J:
        raise NotImplementedError


class InMemoryBackupRepository(BackupRepository, InMemoryJobRepository[Backup]):

    def get_by_name(self, name: str) -> Optional['Backup']:
        for backup in self._repo.values():
            if backup.name == name:
                return backup
        return None


class InMemoryPruneRepository(PruneRepository, InMemoryJobRepository[Prune]):
    pass
