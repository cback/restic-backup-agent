# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import os
import shutil
from typing import Callable, List, Optional, Tuple, Union
from dataclasses import dataclass
from tempfile import mkdtemp
from pathlib import Path

TMP_PREFIX = 'cback-test-'


@dataclass
class Directory:
    name: str
    content: List[Union['Directory', 'File']]


@dataclass
class File:
    name: str
    content: str
    mode: Optional[int] = None


def new_tmp_dir() -> Tuple[str, Callable]:
    dir = mkdtemp()

    def cleanup() -> None:
        try:
            shutil.rmtree(dir)
        except FileNotFoundError:
            pass
    return dir, cleanup


def dir_file_exist(f: str) -> bool:
    return Path(f).exists()


def new_local_dir(d: Directory) -> Tuple[str, Callable]:
    def _new_file(path: str, name: str, content: str, mode: Optional[int]) -> None:
        p = os.path.join(path, name)
        with open(p, "w") as file:
            file.write(content)
        if mode:
            os.chmod(p, mode)

    def _new_local_dir(path: str, dir: Directory) -> None:
        path_dir = os.path.join(path, dir.name)
        os.mkdir(path_dir)
        for f in dir.content:
            if isinstance(f, File):
                _new_file(path_dir, f.name, f.content, f.mode)
            else:
                _new_local_dir(path_dir, f)

    root_dir = mkdtemp(prefix=TMP_PREFIX)
    _new_local_dir(root_dir, d)

    def cleanup() -> None:
        shutil.rmtree(root_dir)
    return root_dir, cleanup


def dir_equals(dir1: str, dir2: str) -> bool:

    def _file_equals(path1: str, path2: str) -> bool:
        with open(path1, "r") as f1, open(path2, "r") as f2:
            return f1.read() == f2.read()

    list_dir1 = sorted(os.listdir(dir1))
    list_dir2 = sorted(os.listdir(dir2))

    if len(list_dir1) != len(list_dir2):
        return False

    for f1, f2 in zip(list_dir1, list_dir2):
        if f1 != f2:
            return False

        path1, path2 = os.path.join(dir1, f1), os.path.join(dir2, f2)
        if os.path.isfile(path1) and os.path.isfile(path2):
            if not _file_equals(path1, path2):
                return False
        elif os.path.isdir(path1) and os.path.isdir(path2):
            if not dir_equals(path1, path2):
                return False
        else:
            return False

    return True
