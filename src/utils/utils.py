# GPLv3 License
# Copyright (C) 2024 CERN/Switzerland 
# all rights reserved
# 
# cback - CERN Backup Orchestrator
# 
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either 
# version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY 
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this 
# program. If not, see <https://www.gnu.org/licenses/>.

 
import subprocess
import os
import shutil
import re
from typing import Callable, Dict, List, Optional, Tuple
from pwd import getpwnam
import shlex


EOS_FORMAT = r"^/eos/project-[a-z]/\w+|^/eos/home-[a-z]/\w+|^/eos/homecanary/[a-z]/\w+"


class Utils:

    @staticmethod
    def check_run(cmd: str, *, timeout: Optional[int] = None, retry: int = 1, env: Dict[str, str] = {}, user: Optional[str] = None) -> str:
        cmd_lst = shlex.split(cmd)
        env = env or {}
        preexec_fn = None
        if user:
            env['USER'] = user
            env['PATH'] = os.environ['PATH']
            uid, gid = Utils._resolve_username(user)
            preexec_fn = Utils._set_uid_gid(uid, gid)

        err = None
        for _ in range(retry):
            proc = subprocess.Popen(
                cmd_lst, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, env=env, preexec_fn=preexec_fn)
            out, err = proc.communicate(timeout=timeout)
            retcode = proc.poll()
            if not retcode:
                return out

        raise Exception(err)

    @staticmethod
    def _resolve_username(username: str) -> Tuple[int, int]:
        passwd = getpwnam(username)
        return passwd.pw_uid, passwd.pw_gid

    @staticmethod
    def _set_uid_gid(uid: int, gid: int) -> Callable:
        def func() -> None:
            os.setgid(gid)
            os.setuid(uid)
        return func

    # # Executes a command. Returns error and output
    # @staticmethod
    # def execute(command, atimeout=None, retry=1):
    #     # print("Running: " + command)
    #     ret = None
    #     for _ in range(retry):
    #         try:
    #             output = subprocess.check_output(command, shell=True, universal_newlines=True, stderr=subprocess.STDOUT,
    #                                              timeout=atimeout)
    #             return True, output
    #         except subprocess.CalledProcessError as e:
    #             if e.returncode == 3:
    #                 return True, e.output
    #             else:
    #                 ret = (False, e.output)
    #                 continue
    #         except subprocess.TimeoutExpired:
    #             ret = (False, f"timeout: {command}")
    #             continue
    #     return ret

    # Converts a humanized size description into aproximated bytes
    @staticmethod
    def size_dehumanize(humanized: str) -> int:
        value, unit = str(humanized).split()
        units = ("B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB")
        index = units.index(unit.strip())
        if index == 0:
            return int(value)
        else:
            base = 1024.0 ** index
            return int(base * float(value))

    # Delete a folder
    @staticmethod
    def delete_folder(path: str) -> None:
        if os.path.isdir(path):
            try:
                shutil.rmtree(path)
            except OSError as e:
                print(e.strerror)

    # Rename folder
    @staticmethod
    def rename_folder(src: str, dst: str) -> None:
        if os.path.isdir(src):
            try:
                shutil.move(src, dst)
            except OSError as e:
                print(e.strerror)
        else:
            print("utils: rename_folder: path not found!")

    # Check if a folder exists, if not, it creates it
    @staticmethod
    def check_and_create_folder(path: str) -> None:
        if not os.path.isdir(path):
            os.mkdir(path)

    # This functions are helpers for agent enable/disable functions

    # transform /eos/user/a/abc paths to /eos/home-a/abc format and /eos/project/a/abc paths to /eos/project-a/abc format
    @staticmethod
    def filename_eos_format(filename: str) -> str:
        if re.match(EOS_FORMAT, filename):
            return filename
        _, _, instance, tail = filename.split("/", maxsplit=3)
        if instance == "user":
            instance = "home"
        return f"/eos/{instance}-{tail}"

    # from an eos path in the form /eos/home-a/abc/def... it gets the eos root directory /eos/home-a/abc
    @staticmethod
    def get_eos_root_directory(filename: str) -> str:
        match: List[str] = re.findall(EOS_FORMAT, filename)
        if not match:
            raise Exception(f"Not root directory found in {filename}")
        return match[0]

    @staticmethod
    def fname_transform(path: str, reverse: bool = False) -> str:
        if reverse:
            ret = path.replace('home-', 'user/')
        else:
            ret = path.replace('user/', 'home-')
        return ret

    # transform /eos/project/a/abc paths to /eos/project-a/abc format
    @staticmethod
    def fname_transform_project(old_path: str) -> str:
        new = old_path.replace('project/', 'project-')
        return new

    # returns bucket name from a full restic s3 repository url.
    @staticmethod
    def extract_bucket_name(repo_url: str) -> str:
        repo_short = str(repo_url).partition("s3:")[2]
        if repo_short.startswith("http"):
            repo_short = repo_short.partition("://")[2]
        return repo_short.split("/")[1]

    # from a full restic s3 repository url, gets a plain name useful for file creation
    @staticmethod
    def get_plain_name(repo_url: str) -> str:
        repo_url = repo_url.partition("s3:")[2]  # remove s3: heading
        repo_url = repo_url.replace("://", "_")  # replace http special chars
        # replace double colon in port specification
        repo_url = repo_url.replace(':', '_')
        repo_url = repo_url.replace('.', '_')  # replace dots by underscore
        return repo_url.split('/')[0]  # remove bucket

    # run a shell command and get the resulting return code, stdout and stderr
    @staticmethod
    def call_subprocess(cmd: str) -> Tuple[str, str, int]:
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE, universal_newlines=True)
        out, err = proc.communicate()
        result = proc.returncode
        return out, err, result

    # @staticmethod
    # def kill_remote(hostname, process_name, pattern):
    #     cmd = f"ssh -q -o StrictHostKeyChecking=no root@{hostname} \"ps x | grep {process_name} | grep {pattern} | grep -v grep | grep -v ssh\""
    #     ok, out = Utils.execute(cmd)
    #     if ok:
    #         # running process
    #         pids_str = " ".join([line.split()[0] for line in out.splitlines()])
    #         kill_cmd = f"ssh -q -o StrictHostKeyChecking=no root@{hostname} \"kill {pids_str}\""
    #         kill_ok, kill_out = Utils.execute(kill_cmd)
    #         if kill_ok:
    #             print(f"Killed processes {pids_str} in {hostname}")
    #         else:
    #             print(
    #                 f"Error killing processes {pids_str}. Error:\n{kill_out}")
    #     else:
    #         print(
    #             f"No {process_name} process found in {hostname} with pattern {pattern}")

    @staticmethod
    def convert_path(path: str) -> str:
        if path.startswith("/eos/home-"):
            return path.replace("/eos/home-", "/eos/user/", 1)
        elif path.startswith("/eos/project-"):
            return path.replace("/eos/project-", "/eos/project/", 1)
        elif path.startswith("/eos/homecanary"):
            return path.replace("/eos/homecanary", "/eos/user")
        else:
            return path
